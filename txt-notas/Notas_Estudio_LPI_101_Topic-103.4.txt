103.4 Usar flujos, tuberias y redirecciones
============================================

== Info Objetivo ==

Peso: 4
Descripción: Los candidatos deberán ser capaces de redireccionar flujos y conectarlos
             para procesar datos de texto eficientemente. Las tareas incluyen redireccionar
             la entrada estándar, salida estándar, salida de error estándar, entubar la salida
             de un comando a la entrada de otro comando, usar la salida de un comando como
             argumento para otro comando y enviar la salida tanto a la salida estándar y un archivo.

Áreas de Conocimiento Clave:

    * Redireccionar la entrada estándar, salida estándar, y salida de error estándar.
    * Entubar la salida de un comando a la entrada de otro comando.
    * Usar la salida de un comando como argumento a otro comando.
    * Enviar la salida de un comando tanto a la salida estándar y a un archivo.

La siguiente es una lista de archivos, términos y utilerías usadas:

    * tee
    * xargs

== Introducción a la redirección de entrada y salida ==

- Para manipular la salida estandar (stdout), entrada estandar (stdin) y la salida
  de error estandar (stderr) en el shell se definen los "punteros" file descriptors:

    STDIN (0)
    STDOUT(1)
    STDERR(2)

Un file descriptor es un indicador abstracto para acceder a un archivo, el termino se utiliza
más en sistemas POSIX, en donde un file descriptor es un entero, especificamente un int de tipo C.

Hay 3 filedescriptors que un proceso puede esperar tener:

Integer value	Name
0		Standard Input (stdin)
1		Standard Output (stdout)
2		Standard Error (stderr)

En general, un file descriptor es un indice para una entrada en una estructura de datos en el 
kernel-residente, que contiene los detalles de todos los archivos abiertos. en sistemas POSIX,
esta estructura se llama "file descriptor table", y cada proceso tiene su propia file descriptor
table.

La aplicación pasa la llave (abstract Key) al kernel a través de una llamada de sistema (system call)
y el kernel accede al archivo on behalf of the application. La aplicación no puede leer directamente
el archivo.

En Unix, los file descriptros se pueden referir a archivos, directorios, dispositivos de
caracter y bloque (archivos especiales), sockets, FIFOs (named pipes) unamed pipes,

http://en.wikipedia.org/wiki/File_descriptor

Siempre hay 3 archivos predeterminados abiertos, stdin (el teclado), stdout( la pantalla) y stderr
(mensajes de error enviados a la pantalla). Estos y todos los archivos abiertos pueden ser redireccionados.
Redirección simplemente significa capturar la salida de un archivo, comando, programa, script o un bloque de
codigo de un script y enviarlo como entrada a otro archivo, comando, programa, o script.

Cada archivo abierto se le asigna un file descriptor (PER: Que pertenece a una file descriptor tabla que pertenece a un proceso)
Los file descriptors para stdin, stdout y stderr son 0, 1, y 2, respectivamente.

== Salida estandar ==

- Enviar stdout a la pantalla:

  $ ls -l *.jpg

- Enviar stdout a un archivo:

  $ ls -l *.jpg > /tmp/stdout.log
  $ ls -l *.jpg 1> /tmp/stdout.log

- Enviar stdout a al final (append) archivo:

  $ ls -l *.jpg >> /tmp/stdout.log

- Como evitar que > sobre escriba un archivo:

El control lo maneja el parámetro del shell noclobber, veamos su estado:

  $ set -o
  allexport       off
  braceexpand     on
  emacs           on
  errexit         off
  errtrace        off
  functrace       off
  hashall         on
  histexpand      on
  history         on
  ignoreeof       off
  interactive-comments    on
  keyword         off
  monitor         on
  noclobber       off
  noexec          off
  noglob          off
  nolog           off
  notify          off
  nounset         off
  onecmd          off
  physical        off
  pipefail        off
  posix           off
  privileged      off
  verbose         off                                                                                                                                                                 
  vi              off                                                                                                                                                                 
  xtrace          off

  $ set -o noclobber

  $ ls > salida
  bash: salida: cannot overwrite existing file

Se vuelve a poner:

  $ set +o noclobber

- Ejemplo de multiples comandos y salidas:

  # w;id

  # w; id > salida

  NOTA: Solo se guarda lo de el ultimo comando (id)

  # (w; id) > salida

- Redirección de salida con sudo

  $ sudo echo "jmedina.tuxjm.net" > /etc/hostname
  bash: /etc/hostname: Permission denied

  $ sudo sh -c 'echo "jmedina.tuxjm.net" > /etc/hostname'

  $ sudo ls -hal /root/ | sudo tee /root/test.out > /dev/null

  $ echo myhost.com > /etc/hostname
  -bash: /etc/hostname: Permission denied

  $ echo myhost.com | sudo tee /etc/hostname

  EMFJI If you want to create the file with one ig/gid, better to add -u option in sudo command.

  $ echo “bla bla bla” | sudo -u one tee /home/one/blablabla.file

== Enviar stderr a un archivo ==

  $ ls -l *.noexiste 2> /tmp/stdout.log

- Enviar stdout y stderr a un archivo:

  $ ls -l *.jpg *.noexiste > /tmp/stdout.log 2> /tmp/stderr.log
  $ ls -l *.jpg *.noexiste > /tmp/stdall.log 2>&1

- Enviar stdout y stderr a /dev/null:

  $ ls -l *.jpg *.noexiste > /dev/null 2>&1

== La entrada estandar ==

  Usamos un archivo de entrada:

  $ cat /tmp/fdisk-in.txt
  m
  p
  q

  # fdisk /dev/sdb < /tmp/fdisk-in.txt

  Enviar correo:

  $ mail root < /tmp/cuerpo-mensaje.txt

== Tuberias ==

el pipe

La salida de un comando, programa o script puede ser redireccionada hacía otro comando
a través de un pipe, es decir se redirecciona la salida de un comando hacía la entrada
de otro, en el shell bash se utiliza el carácter "|".

Los programas reciben la entrada como un flujo de texto, un flujo de lineas de caracteres,

muchos programas procesan línea por línea, carácter por caracter:

head, tail - Procesan la entrada por bloques de líneas.
more, less - Procesan la entrada por páginas.
uniq
sort

cut, sed, awk - Procesadores y editores de flujos de texto.

  # w | grep root

  # who | grep tty

  # cut -d: -f7 /etc/passwd | sort | uniq

NOTA: Parte de filosofia UNIX.
http://en.wikipedia.org/wiki/Unix_philosophy
http://www.faqs.org/docs/artu/ch01s06.html
http://books.google.com.mx/books?id=0Ktl1bB-vksC&printsec=frontcover&dq=unix+philosophy&source=bl&ots=8aEXKgtPkn&sig=UBGxuyy1jtWXPZRkNkKYb-XYgHM&hl=es-419&ei=dadoTd-eNsH-8Aaf2eXdCw&sa=X&oi=book_result&ct=result&resnum=8&ved=0CGAQ6AEwBw#v=onepage&q&f=false

 $ seq 1 50 | tail -n +15 | head -n 4

 $ foo < bar | loo

 $ fdisk /dev/sda < entrada.file | grep sda

== Duplicar la salida con el comando tee ==

  With the tee command you can read input from an input stream, and split the output stream
  in two directions, so it is both displayed on screen (stdout) and also re-direct it to a file.
  I needed to do this today when I wanted to monitor something that was running slow, and also
  keep an output log of the long-running process.

  $ ls -al | tee poop.out

  Mandando tanto stdout y stderr a pantalla y archivo usando tee:

  # strace -fp rsyslogd -c5 -dn 2>&1 |tee rsyslogd.debug

  En este caso se usa para depurar un programa y capturar toda la salida en un archivo.

== Duplicar la salida con el comando tee en modo append ==

  $ echo"hello world" | tee -a test.txt

== Manuales de referencía ==

== Archivos de referencía ==
