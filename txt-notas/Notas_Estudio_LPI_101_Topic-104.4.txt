104.4 Administrar cuotas de disco
=================================

== Info Objetivo ==

Peso: 1
Descripcion: Los candidatos deberán ser capaces de administrar cuotas para los usuarios.

Areas de Conocimiento Clave:

    * Establecer cuotas de disco para un sistema de archivos.
    * Editar, revisar, y generar reportes de cuotas de usuario. 

La siguiente es una lista de archivos, terminos y utilerias usadas:

    * quota
    * edquota
    * repquota
    * quotaon 

== Introducción a cuotas de disco ==

Por espacio:

- Por bloque: Un bloque corresponde a 1 kb (1024 octetos)
- Por inode (index node)

Por usuario y grupo:

- Se aplica a un usuario
- Se aplica a los miembros de un usuario.

NOTA: Si hay un fs con cuota para usuarios y grupos, las de usuario tiene prioridad.

Por tipo de limite:

  - soft - cuando este límite es alcanzado o superado un mensaje advierte al usuario cada
           vez que un nuevo bloque o archivo es escrito. 
  - hard - cuando este límite es alcanzado el usuario no puede escribir nuevos archivos o
           nuevos bloques.

Cuando se usa el límite SOFT, dos situaciones pueden ocurrir. La primera es que NO se tenga
establecido un tiempo de gracia, y entonces el usuario podrá seguir usando bloques o inodos
hasta llegar al límite HARD que será su límite absoluto de uso.

La segunda situación es que SI se tenga establecido el tiempo de gracia, que puede ser en días,
horas, minutos o segundos. En este caso, el usuario podrá seguir usando bloques o inodos hasta
que termine el tiempo de gracía o llegue al límite HARD, cualquiera que ocurra primero.

IMPORTANTE: El tiempo de gracia default es 7 días para bloques e inodos.

- Revisar el soporte de cuotas de disco en el kernel:

  $ grep QUOTA /boot/config-`uname -r`
  CONFIG_NETFILTER_XT_MATCH_QUOTA=m
  CONFIG_XFS_QUOTA=y
  CONFIG_QUOTA=y
  CONFIG_QUOTA_NETLINK_INTERFACE=y
  # CONFIG_PRINT_QUOTA_WARNING is not set
  CONFIG_QUOTA_TREE=m
  CONFIG_QUOTACTL=y

- Instalar los programas "userspace" para el control de cuotas:

  $ sudo apt-get install quota
  $ sudo yum install quota

- Montar el sistema de archivos con el soporte de cuotas:

  Montarlas manualmente:

  # mount -o defaults,usrquota,grpquota /var
  # mount -o defaults,usrquota,grpquota /home

  O remontarla con soporte cuota:

  # mount -o remount,usrquota,grpquota /var
  # mount -o remount,usrquota,grpquota /home

  Montarla de forma persitente con soporte cuota:

  # vim /etc/fstab
  ...
  ...
  LABEL=/home     /home   ext3    defaults,usrquota,grpquota      0 2
  LABEL=/var      /var    ext3    defaults,usrquota,grpquota      0 2

- Crear archivos de registro de quotas aquota.user y aquota.group:

  NOTA: Antes era quota.user y quota.group y se hacian a mano.

  # quotacheck -auvg
 
  Use la opción -m para la raiz si ya esta montada.

- Activar soporte de cuotas en el sistema de archivos:

  # quotaon -auvg

- Imprimir el estado de quotas:

  # quotaon -ap

- Revisar el estado de las cuotas:

  # repquota -a
  *** Report for user quotas on device /dev/xvda1
  Block grace time: 7days; Inode grace time: 7days
                          Block limits                File limits
  User            used    soft    hard  grace    used  soft  hard  grace
  ----------------------------------------------------------------------
  root      -- 1242296       0       0          36485     0     0       
  daemon    --       8       0       0              3     0     0       
  lp        --       8       0       0              2     0     0       
  nobody    --   36308       0       0              7     0     0       
  exim      --     148       0       0             37     0     0       
  avahi     --       8       0       0              3     0     0

  NOTA: -a solo muestra los sistemas de archivos que tienen uso de cuota.

  NOTA: Para ver el reporte de quotas para FS que no estan en uso use -v:

  # repquota -av
  *** Report for user quotas on device /dev/xvda1
  Block grace time: 7days; Inode grace time: 7days
                          Block limits                File limits
  User            used    soft    hard  grace    used  soft  hard  grace
  ----------------------------------------------------------------------
  root      -- 1242296       0       0          36485     0     0       
  daemon    --       8       0       0              3     0     0       
  lp        --       8       0       0              2     0     0       
  nobody    --   36308       0       0              7     0     0       
  exim      --     148       0       0             37     0     0       
  avahi     --       8       0       0              3     0     0       

  Statistics:
  Total blocks: 6
  Data blocks: 1
  Entries: 6
  Used average: 6.000000

- Asignando cuota para usuario:

  # edquota -u jperez
  Disk quotas for user jperez (uid 1002):
    Filesystem                   blocks       soft       hard     inodes     soft       hard
    /dev/sda1                      1242          0          0         36        0          0

  NOTA: Si quiere usar un editor diferente use la variable EDITOR=nano, por ejemplo.

- Asignando cuota para grupo:

  # edquota -g ventas
  Disk quotas for group ventas (gid 1013):grace
    Filesystem                   blocks       soft       hard     inodes     soft       hard
    /dev/sda1                      1342          0          0       4431        0          0

  NOTA: Si quiere usar un editor diferente use la variable EDITOR=nano, por ejemplo.

"blocks in use" es el numero total de bloques (en kilobytes) consumidos por el usuario en la partición.

"inodes in use" es el numero total de ficheros que tiene el usuario en la partición.

- Definir cuota con comando setquota

  # setquota -u jperez 90000 100000 0 0 /data/cobranza

- Ver las cuotas del mismo usuario:

  # quota
  Disk quotas for user root (uid 0): none

- Copiar las quotas del usuario foolano a todos los usuarios con UID mayor a 1000:

  # edquota -p pepito `awk -F: '$3 > 1000 {print $1}' /etc/passwd`

- Definiendo el Hard Limit:

  El 'hard limit' solo funciona cuando se activa un periodo de gracia. Especifica un
  limite absoluto en el uso del disco, no pudiendo un usuario con cuota ir mas allá de
  su limite hardware.

  # setquota -u jperez 90000 100000 0 0

- Definir el periodo de gracia para las cuotas:

  Ejecutado con el comando "edquota -t", el periodo de gracia es el tiempo limite antes de hacer
  cumplir el limite software a un usuario con cuota. Se pueden usar segundos, minutos, horas,
  días, semanas, y meses como unidades de tiempo.

  # edquota -t
  Grace period before enforcing soft limits for users:
  Time units may be: days, hours, minutes, or seconds
    Filesystem             Block grace period     Inode grace period
    /dev/sda1                    7days                  7days

- Cambiar el periodo de gracia por usuario:

  # edquota -f /mnt/datos1 -T username

  # setquota -T -u username 60000 0 /mnt/datos1

Las unidades de tiempo son:

x seconds
x minutes
x hours
x days

o nulo:

unset

Cambia el apartado de 0 días por cualquier espacio de tiempo que consideres razonable. Yo personalmente
 he escogido 7 días (o una semana).

- Notificando a los usuarios de sus quotas

  # warnquota

  /etc/warnquota.conf

== Manuales de referencía ==

== Archivos de referencía ==

== Sitios web de referencía ==

Quota mini-HOWTO:
http://www.tldp.org/HOWTO/Quota.html

Learn Linux, 101: Manage disk quotas:
http://www.ibm.com/developerworks/linux/library/l-lpic1-v3-104-4/index.html