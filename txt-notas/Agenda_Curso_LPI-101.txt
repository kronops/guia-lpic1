####################################################################################
                         Temario para curso LPI 101
####################################################################################

Los objetivos oficiales de LPI 101 son:

Tema 101: Arquitectura del Sistema

  101.1 Determinar y configurar parámetros del hardware
  101.2 Arrancar el sistema
  101.3 Cambiar los niveles de ejecución, apagar o reiniciar el sistema

Tema 102: Instalación de Linux y Administración de Paquetes

  102.1 Diseñar el esquema de discos duros
  102.2 Instalar un gestor de arranque
  102.3 Administrar bibliotecas compartidas
  102.4 Usar el administrador de paquetes Debian
  102.5 Usar el administrador de paquetes RPM y YUM

Tema 103: Comandos GNU y Unix

  103.1 Trabajar en la linea de comandos
  103.2 Procesar flujos de texto usando filtros
  103.3 Realizar administración de archivos basica
  103.4 Usar flujos, tuberias y redirecciones
  103.5 Crear, monitorizar y matar procesos
  103.6 Modificar las prioridades de ejecución de los procesos
  103.7 Buscar en archivos de texto usando expresiones regulares
  103.8 Realizar operaciones básicas de edición de archivos usando vi

Tema 104: Dispositivos, Sistemas de archivos Linux, Estándar de jerarquía del Sistemas de Archivos

  104.1 Crear particiones y sistemas de archivos
  104.2 Mantener la integridad de los sistemas de archivos
  104.3 Controlar el montaje y desmontaje de sistemas de archivos
  104.4 Administrar cuotas de disco
  104.5 Administrar propietarios y permisos de archivos
  104.6 Crear y cambiar enlaces duros y simbolicos
  104.7 Encontrar archivos de sistema y colocar archivos en su correcta ubicación

####################################################################################
                 DIA 1
####################################################################################

== Introducción a LPI ==
== Introducción a los sistemas GNU/Linux ==
== Instalación de sistemas GNU/Linux ==
== Arranque e inicio de sesión en sistemas GNU/Linux ==
== Modo de mantenimiento, reinicio y apagado de sistemas GNU/Linux ==
== 103.1 Trabajar en la linea de comandos ==
== 103.3 Realizar administración de archivos basica ==
== 103.8 Realizar operaciones básicas de edición de archivos usando vi ==

####################################################################################
                 DIA 2
####################################################################################

== 104.6 Crear y cambiar enlaces duros y simbolicos ==
== 104.5 Administrar propietarios y permisos de archivos ==
== 104.7 Encontrar archivos de sistema y colocar archivos en su correcta ubicación ==
== 103.7 Buscar en archivos de texto usando expresiones regulares ==
== 103.4 Usar flujos, tuberias y redirecciones ==
== 103.2 Procesar flujos de texto usando filtros ==

####################################################################################
                 DIA 3
####################################################################################

== 101.2 Arrancar el sistema ==
== 102.2 Instalar un gestor de arranque ==
== 101.3 Cambiar los niveles de ejecución, apagar o reiniciar el sistema ==
== 101.1 Determinar y configurar parámetros del hardware ==
== 102.3 Administrar bibliotecas compartidas ==
== 102.4 Usar el administrador de paquetes Debian ==
== 102.5 Usar el administrador de paquetes RPM y YUM ==

####################################################################################
                 DIA 4
####################################################################################

== 103.5 Crear, monitorizar y matar procesos ==
== 103.6 Modificar las prioridades de ejecución de los procesos ==
== 104.1 Crear particiones y sistemas de archivos ==
== 104.3 Controlar el montaje y desmontaje de sistemas de archivos ==
== 104.2 Mantener la integridad de los sistemas de archivos ==
== 104.4 Administrar cuotas de disco ==
