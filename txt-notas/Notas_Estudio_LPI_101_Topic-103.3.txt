103.3 Realizar administración de archivos basica
================================================

== Info Objetivo ==

Peso: 4
Descripción: Los candidatos deberán ser capaces de usar los comandos Linux básicos para
             administrar archivos y directorios.

Áreas de Conocimiento Clave:

    * Copiar, mover, borrar archivos y directorios individualmente.
    * Copiar múltiples archivos y directorios recursivamente.
    * Eliminar archivos y directorios recursivamente.
    * Usar comodines simples y avanzados en comandos
    * Usar find para localizar y actuar en base al tipo, tamaño y fecha de archivos.
    * Uso de tar, cpio y dd.

La siguiente es una lista de archivos, términos y utilerías usadas:

    * cp
    * find
    * mkdir
    * mv
    * ls
    * rm
    * rmdir
    * touch

    * tar
    * cpio
    * dd
    * file
    * gzip
    * gunzip
    * bzip2

    * file globbing

== Introducción a la estructura de archivos raíz ==

  Directorios de la raiz

== Usando el comando tree para ver la estructura de arbol ==

  # tree -d -L 1

== Tipos de archivos y directorios ==

  Como ver el tipo de archivos:
  
  $ ls -l

  Tipos de archivos y directorios:
  
  -: Archivo regular
  d: Directorio
  l: Enlace simbólico
  c: Character device
  b: Block device
  f: FIFO file

  Usando el comando file para identificar los tipos de archivos:

  Ejemplo con archivos de texto en /etc:

  $ file /etc/fstab 
  /etc/fstab: ASCII English text

  Ejemplo con archivos binarios y shared libraries:

  $ file /bin/cat 
  /bin/cat: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.15, stripped

  $ file /lib/libparted.so.0.0.1 
  /lib/libparted.so.0.0.1: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, stripped

  $ file /usr/share/sounds/Kopete_Sent.ogg

== Moviendose entre el arbol de directorios ==

- En donde estamos (Print Working Directory):
  
  $ pwd

- Cambiarse al home:

  $ cd
  $ cd ~
  $ cd $HOME

- Sobre directorios con espacios:

  $ cd Carpeta\ X\ Y
  $ cd "Carpeta X Y"

- Cambiarse un directorio atras:

  $ cd ..

- Cambiarse al ultimo directorio:

  $ cd ..

  NOTA: El alias cd.. de suse...

  $ cd /usr/share/doc/zip-xx
  $ less READme

  Ó

  $ less /usr/share/doc/zip-xx/README

  ó

  $ (cd /usr/share/doc/zip-xx; less README)


== Creando archivos y directorios ==

- Crear directorios:

  $ mkdir /tmp/bla

  $ mkdir a
  $ cd a
  $ mkdir b
  $ cd b
  $ mkdir c
  $ cd

  $ tree -d a

  O creando toda la cadena de directorios padres:
  
  $ mkdir -p a/b/c

  $ mkdir -m 777 /tmp/prueba

- Crear un archivo vacio:

  $ touch /tmp/archivo

- Copiando archivos:

  $ cp

  $ cp -v
  $ cp -R
  $ cp -i  # IMPORTANTE
  $ cp -f

  $ cp -a (-dpR)

  Ejemplo de copia de usuario y root.

- Usando el comando dd para copiar archivos:

  # dd if=/dev/sda1 of=/mnt/usb/sda1.img
  # dd if=/dev/zero of=/tmp/archivo10M bs=1M count=10

- Moviendo/Renombrando archivos:

  $ mv origen destino
  $ mv -v origen destino
  $ mv -i  # IMPORTANTE
  $ mv -f  # IMPORTANTE SOBRE ESCRIBE

== Moviendo y renombrando archivos y directorios ==

- Renombrar bloque de archivos:

  # ls -l
  total 0
  -rw-r--r-- 1 root root 0 Oct 31 22:33 aa-2009-01-01
  -rw-r--r-- 1 root root 0 Oct 31 22:33 bb-2009-02-01

  # for f in *2009*; do echo mv -i $f 2009${f#*-2009}-${f%-2009*}; done

  mv -i aa-2009-01-01 2009-01-01-aa
  mv -i bb-2009-02-01 2009-02-01-bb

  # for f in *2009*; do mv -i $f 2009${f#*-2009}-${f%-2009*}; done

  # ls -l
  total 0
  -rw-r--r-- 1 root root 0 Oct 31 22:33 2009-01-01-aa
  -rw-r--r-- 1 root root 0 Oct 31 22:33 2009-02-01-bb

  # rename 's/foo/bar/g' *

  # rename .oldextension .newextension *.oldextension

  # mmv 'banana_*_*.asc' 'banana_#2_#1.asc'

  # rename s/\(..\)-\(.*\)/\$2-\$1/ *2009*

== Eliminando archivos y directorios ==

- Eliminando archivos:

  $ rm archivo
  $ rm -v archivo
  $ rm -i archivo

- Eliminando directorios con rmdir:

  $ rmdir directoriovacio

  NOTA: Solo borra directorios vacios.

- Eliminando directorios con rm:

  $ rm -r directorio

  Interactivo/desciende

  $ rm -rf  

  IMPORTANTE: no pregunta: no rm -rf /

== Uso básico de tar ==

  tar simple
  gzip
  gunzip
  bzip2

== Creando respaldos con tar ==

Tar el (tape archiver) es la herramienta mas utilizada para respaldar sistemas linux,
Archiva ficheros en diferentes medios como una cinta magnetica, un floppy o un archivo
en disco. Los archivos tar por convension utilizan la tarminación (extensión) .tar

- Creando archivos tar

Respaldando el directorio /etc

# tar cf /tmp/respaldo-etc.tar /etc

NOTA: Agregando la opción -v podrá ver una salida detallada del proceso de respaldo, usar cvf.

Cuando creamos archivos con tar, las rutas absolutas se convierten en relativas por default,
esto significa la / en la ruta del archivo es removida, se puede ver en la salida:

tar: Removing leading / from member names

NOTA: En GNU/Linux no es necesario anteponer el guion "-" antes de las opciones.

NOTA: Agregando la opción -v podrá ver una salida detallada del proceso de respaldo, usar cvf.

- Opciones de compresión

Comprimiendo con gzip

# tar zcvf /tmp/respaldo-etc.tar.gz /etc

Comprimiendo con bzip2

# tar jcvf /tmp/respaldo-etc.tar.bz2 /etc

- Visualizando el contanido de un archivo tar

# tar tvf /tmp/respaldo-etc.tar

- Extrayendo archivos tar

Extrayendo el archivo en el directorio actual:

NOTA: Si cuando realizo el archivo utilizo como origen una ruta abusoluta,
no se recomienda que extraiga el archivo estando en la / ya que sobreescribiria
el directorio original.

NOTA: Agregando la opción -v podrá ver una salida detallada del proceso de respaldo, usar xvf.

# cd /tmp
# tar xvf respaldo-etc.tar

Extrayendo el archivo en otro directorio

# tar xvf /tmp/respaldo-etc.tar -C /media/usbdisk/

Extrayendo solo ciertos ficheros o directorios

# cd /tmp
# tar cvf respaldo-etc.tar X11/

Este comando solo extrae el contenido del directorio X11 dentro del archivo.

- Opciones para descomprimir

Extrayendo un archivo con compresión gzip

# cd /tmp
# tar zcf respaldo-etc.tar.gz

Extrayendo un archvo con compresión bzip2

# cd /tmp
# tar jcf respaldo-etc.tar.bz2

- Excluyendo archivos del respaldo

Si deseamos excluir ficheros de nuestro archivo, por ejemplo no queremos respaldoar
los archivos que terminan en "~" usamos:

# tar cv --exclude=*~ -f /tmp/respaldo-home-jorge.tar /home/jorge

# tar cv -X archivo-con-lista-de-exclusion -f /tmp/respaldo-home-jorge.tar /home/jorge
  NOTE: con los programas less y vim te permite navegar dentro de un archivo tar.

== Manuales de referencía ==

== Archivos de referencía ==
