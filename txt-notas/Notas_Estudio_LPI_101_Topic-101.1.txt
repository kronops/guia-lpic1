101.1 Determinar y configurar parámetros del hardware
=====================================================

== Info Objetivo ==

Peso: 2
Descripción: Los candidatos deberán ser capaces de determinar y configurar el hardware básico.

Áreas clave de Conocimiento:

    * Habilitar y deshabilitar periféricos integrados.
    * Configurar el sistema con o sin periféricos externos como teclado.
    * Diferenciar entre dispositivos de almacenamiento masivo.
    * Establecer correctamente el ID de hardware para diferentes dispositivos, especialmente dispositivos de arranque.
    * Conocer las diferencias entre dispositivos coldplug y hotplug.
    * Determinar recursos de hardware para los dispositivos.
    * Herramientas y utilerías para listar información diversa de hardware (e.g. lsusb, lspci, etc.).
    * Herramientas y utilerías para manipular dispositivos USB.
    * Entendimiento conceptual de sysfs, udev, hald y dbus.

La siguiente es una lista de archivos, términos y utilerías usadas:

    * /sys
    * /proc
    * /dev
    * modprobe
    * lsmod
    * lspci
    * lsusb

== Saber como habilitar los dispositivos perififericos desde el BIOS ==

El administrador del sistema debe de estar relacionado con los parámetros de configuración
del BIOS, la mayoría de los programas BIOS tienen opciones similares, algunas de las tareas
que debe saber realizar las siguientes:

  - Como entrar al BIOS en diferentes sistemas.

  - Identificar marca y modelo del BIOS.

  - Reconocer las opciones para habilitar dispositivos PCI y Almacenamiento.

  - Configurar orden de arranque estatico o sobre demanda.

  - Para discos SATA, asegurarse de que no esten en modo IDE, son mas lentos

  - Desactivar soporte USB en servidores? a menos que se respalde a discos USB.

  - Desabilitar tarjetas de sonido en servers?

  - Desactivar soporte ACPI APIC en kernel para problemas de conflictos

== Diferenciar entre los diferentes tipos de disco ==

  IDE
  SCSI
  SATA
  SAS
  SSD

- Identificar la información de dispositivos de almacenamiento

  en discos IDE se llaman...
  en discos SCSI/SATA/SAS se llaman...
  Nota del kernel 2.6.19


  DMA?

- Sobre el sistema de archivos /proc

- Sobre el sistema de archivos /dev

- Sobre el sistema de archivos /sys

== Encontrando información de hardware del sistema ==

  Para listar los dispositivos conectados al bus PCI use el comando lspci.

  Desktop:

  # lspci
  00:00.0 RAM memory: nVidia Corporation MCP55 Memory Controller (rev a2)
  00:01.0 ISA bridge: nVidia Corporation MCP55 LPC Bridge (rev a3)
  00:01.1 SMBus: nVidia Corporation MCP55 SMBus (rev a3)
  00:02.0 USB Controller: nVidia Corporation MCP55 USB Controller (rev a1)
  00:02.1 USB Controller: nVidia Corporation MCP55 USB Controller (rev a2)
  00:04.0 IDE interface: nVidia Corporation MCP55 IDE (rev a1)
  00:05.0 IDE interface: nVidia Corporation MCP55 SATA Controller (rev a3)
  00:05.1 IDE interface: nVidia Corporation MCP55 SATA Controller (rev a3)
  00:05.2 IDE interface: nVidia Corporation MCP55 SATA Controller (rev a3)
  00:06.0 PCI bridge: nVidia Corporation MCP55 PCI bridge (rev a2)
  00:08.0 Ethernet controller: nVidia Corporation MCP55 Ethernet (rev a3)
  00:0f.0 PCI bridge: nVidia Corporation MCP55 PCI Express bridge (rev a3)
  00:18.0 Host bridge: Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] HyperTransport Technology Configuration
  00:18.1 Host bridge: Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Address Map
  00:18.2 Host bridge: Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] DRAM Controller
  00:18.3 Host bridge: Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control
  01:07.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL-8139/8139C/8139C+ (rev 10)
  01:08.0 Ethernet controller: Accton Technology Corporation SMC2-1211TX (rev 10)
  01:0a.0 FireWire (IEEE 1394): Texas Instruments TSB43AB23 IEEE-1394a-2000 Controller (PHY/Link)
  02:00.0 VGA compatible controller: ATI Technologies Inc Unknown device 954f
  02:00.1 Audio device: ATI Technologies Inc Unknown device aa38

  Laptop:

  $ lspci
  00:00.0 Host bridge: Advanced Micro Devices [AMD] RS780 Host Bridge
  00:01.0 PCI bridge: Acer Incorporated [ALI] Device 9602
  00:05.0 PCI bridge: Advanced Micro Devices [AMD] RS780 PCI to PCI bridge (PCIE port 1)
  00:06.0 PCI bridge: Advanced Micro Devices [AMD] RS780 PCI to PCI bridge (PCIE port 2)
  00:07.0 PCI bridge: Advanced Micro Devices [AMD] RS780 PCI to PCI bridge (PCIE port 3)
  00:11.0 SATA controller: ATI Technologies Inc SB700/SB800 SATA Controller [AHCI mode]
  00:12.0 USB Controller: ATI Technologies Inc SB700/SB800 USB OHCI0 Controller
  00:12.1 USB Controller: ATI Technologies Inc SB700 USB OHCI1 Controller
  00:12.2 USB Controller: ATI Technologies Inc SB700/SB800 USB EHCI Controller
  00:13.0 USB Controller: ATI Technologies Inc SB700/SB800 USB OHCI0 Controller
  00:13.2 USB Controller: ATI Technologies Inc SB700/SB800 USB EHCI Controller
  00:14.0 SMBus: ATI Technologies Inc SBx00 SMBus Controller (rev 3a)
  00:14.2 Audio device: ATI Technologies Inc SBx00 Azalia (Intel HDA)
  00:14.3 ISA bridge: ATI Technologies Inc SB700/SB800 LPC host controller
  00:14.4 PCI bridge: ATI Technologies Inc SBx00 PCI to PCI Bridge
  00:18.0 Host bridge: Advanced Micro Devices [AMD] Family 11h Processor HyperTransport Configuration (rev 40)
  00:18.1 Host bridge: Advanced Micro Devices [AMD] Family 11h Processor Address Map
  00:18.2 Host bridge: Advanced Micro Devices [AMD] Family 11h Processor DRAM Controller
  00:18.3 Host bridge: Advanced Micro Devices [AMD] Family 11h Processor Miscellaneous Control
  00:18.4 Host bridge: Advanced Micro Devices [AMD] Family 11h Processor Link Control
  01:05.0 VGA compatible controller: ATI Technologies Inc RS780M/RS780MN [Radeon HD 3200 Graphics]
  01:05.1 Audio device: ATI Technologies Inc RS780 Azalia controller
  03:00.0 Ethernet controller: Broadcom Corporation NetXtreme BCM5764M Gigabit Ethernet PCIe (rev 10)
  06:00.0 Network controller: Atheros Communications Inc. AR928X Wireless Network Adapter (PCI-Express) (rev 01)
  0c:06.0 CardBus bridge: O2 Micro, Inc. Cardbus bridge (rev 01)
  0c:06.2 SD Host controller: O2 Micro, Inc. Integrated MMC/SD Controller (rev 02)
  0c:06.3 Mass storage controller: O2 Micro, Inc. Integrated MS/xD Controller (rev 03)

  Servidor:

  # lspci
  00:00.0 Host bridge: Intel Corporation 5000V Chipset Memory Controller Hub (rev b1)
  00:02.0 PCI bridge: Intel Corporation 5000 Series Chipset PCI Express x8 Port 2-3 (rev b1)
  00:03.0 PCI bridge: Intel Corporation 5000 Series Chipset PCI Express x4 Port 3 (rev b1)
  00:08.0 System peripheral: Intel Corporation 5000 Series Chipset DMA Engine (rev b1)
  00:10.0 Host bridge: Intel Corporation 5000 Series Chipset FSB Registers (rev b1)
  00:10.1 Host bridge: Intel Corporation 5000 Series Chipset FSB Registers (rev b1)
  00:10.2 Host bridge: Intel Corporation 5000 Series Chipset FSB Registers (rev b1)
  00:11.0 Host bridge: Intel Corporation 5000 Series Chipset Reserved Registers (rev b1)
  00:13.0 Host bridge: Intel Corporation 5000 Series Chipset Reserved Registers (rev b1)
  00:15.0 Host bridge: Intel Corporation 5000 Series Chipset FBD Registers (rev b1)
  00:16.0 Host bridge: Intel Corporation 5000 Series Chipset FBD Registers (rev b1)
  00:1c.0 PCI bridge: Intel Corporation 631xESB/632xESB/3100 Chipset PCI Express Root Port 1 (rev 09)
  00:1d.0 USB Controller: Intel Corporation 631xESB/632xESB/3100 Chipset UHCI USB Controller #1 (rev 09)
  00:1d.1 USB Controller: Intel Corporation 631xESB/632xESB/3100 Chipset UHCI USB Controller #2 (rev 09)
  00:1d.2 USB Controller: Intel Corporation 631xESB/632xESB/3100 Chipset UHCI USB Controller #3 (rev 09)
  00:1d.3 USB Controller: Intel Corporation 631xESB/632xESB/3100 Chipset UHCI USB Controller #4 (rev 09)
  00:1d.7 USB Controller: Intel Corporation 631xESB/632xESB/3100 Chipset EHCI USB2 Controller (rev 09)
  00:1e.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev d9)
  00:1f.0 ISA bridge: Intel Corporation 631xESB/632xESB/3100 Chipset LPC Interface Controller (rev 09)
  00:1f.1 IDE interface: Intel Corporation 631xESB/632xESB IDE Controller (rev 09)
  00:1f.2 RAID bus controller: Intel Corporation 631xESB/632xESB SATA RAID Controller (rev 09)
  00:1f.3 SMBus: Intel Corporation 631xESB/632xESB/3100 Chipset SMBus Controller (rev 09)
  01:00.0 PCI bridge: Intel Corporation 6311ESB/6321ESB PCI Express Upstream Port (rev 01)
  01:00.3 PCI bridge: Intel Corporation 6311ESB/6321ESB PCI Express to PCI-X Bridge (rev 01)
  02:00.0 PCI bridge: Intel Corporation 6311ESB/6321ESB PCI Express Downstream Port E1 (rev 01)
  02:02.0 PCI bridge: Intel Corporation 6311ESB/6321ESB PCI Express Downstream Port E3 (rev 01)
  04:00.0 Ethernet controller: Intel Corporation 80003ES2LAN Gigabit Ethernet Controller (Copper) (rev 01)
  04:00.1 Ethernet controller: Intel Corporation 80003ES2LAN Gigabit Ethernet Controller (Copper) (rev 01)
  08:0c.0 VGA compatible controller: ATI Technologies Inc ES1000 (rev 02)

  NOTA: Para ver más detalles sobre los dispositivos use el parámetro -v.

  Use el parámetro -k para identificar el driver del kernel que controla dicho dispositivo, por ejemplo:

  # lspci -k
  ....
  ....
  ....
  03:00.0 Ethernet controller: Broadcom Corporation NetXtreme BCM5764M Gigabit Ethernet PCIe (rev 10)
          Subsystem: Acer Incorporated [ALI] Device 013c
          Kernel driver in use: tg3
          Kernel modules: tg3
  06:00.0 Network controller: Atheros Communications Inc. AR928X Wireless Network Adapter (PCI-Express) (rev 01)
          Subsystem: Device 1a32:0303
          Kernel driver in use: ath9k
          Kernel modules: ath9k

  Use el comando update-pciids para actualizar la base de datos PCI IDs /usr/share/misc/pci.ids desde el sitio:
  http://pci‐ids.sourceforge.net.

  # update-pciids

  También puede usar el comando dmidecode o lshw para encontrar mayor información del sistema.

== Listando información sobre dispositivos SCSI ==

  # cat /proc/scsi/scsi 
  Attached devices:
  Host: scsi0 Channel: 00 Id: 00 Lun: 00
    Vendor: ATA      Model: WDC WD2500BEVS-2 Rev: 01.0
    Type:   Direct-Access                    ANSI  SCSI revision: 05
  Host: scsi1 Channel: 00 Id: 00 Lun: 00
    Vendor: Optiarc  Model: DVD RW AD-7560S  Rev: SX07
    Type:   CD-ROM                           ANSI  SCSI revision: 05

== Encontrando información de los dispositivos USB ==

  Con el comando lspci puede listas la controladora USB:

  # lspci | grep -i usb
  00:12.0 USB Controller: ATI Technologies Inc SB700/SB800 USB OHCI0 Controller
  00:12.1 USB Controller: ATI Technologies Inc SB700 USB OHCI1 Controller
  00:12.2 USB Controller: ATI Technologies Inc SB700/SB800 USB EHCI Controller
  00:13.0 USB Controller: ATI Technologies Inc SB700/SB800 USB OHCI0 Controller
  00:13.2 USB Controller: ATI Technologies Inc SB700/SB800 USB EHCI Controller

  Los dispositivos USB conectados se listan con lsusb:

  Ejemplo Desktop:

  # lsusb
  Bus 002 Device 001: ID 0000:0000  
  Bus 001 Device 002: ID 046d:c00e Logitech, Inc. M-BJ69 Optical Wheel Mouse
  Bus 001 Device 001: ID 0000:0000

  Ejemplo Laptop:

  $ lsusb
  Bus 005 Device 002: ID 147e:1000 Upek Biometric Touchchip/Touchstrip Fingerprint Sensor
  Bus 005 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
  Bus 004 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
  Bus 003 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
  Bus 002 Device 003: ID 04f2:b044 Chicony Electronics Co., Ltd Acer CrystalEye Webcam
  Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
  Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

  Ejemplo Servidor:

  # lsusb
  Bus 005 Device 002: ID 059b:0370 Iomega Corp. 
  Bus 005 Device 001: ID 0000:0000  
  Bus 004 Device 001: ID 0000:0000  
  Bus 003 Device 001: ID 0000:0000  
  Bus 002 Device 001: ID 0000:0000  
  Bus 001 Device 001: ID 0000:0000

  NOTA: Para ver más detalles sobre los dispositivos USB use el parámetro -v.

== Identificando los drivers del kernel cargados en el sistema ==

  Para identificar los modulos del kernel cargados y en uso use el comando lsmod, por ejemplo:

  # lsmod
  Module                  Size  Used by
  dummy                   7560  0 
  bridge                 62128  0 
  iptable_filter          7552  0 
  ip_tables              25304  1 iptable_filter
  x_tables               21640  1 ip_tables
  aes_x86_64             29992  0 
  dm_crypt               16272  0 
  ac                     10120  0 
  sbp2                   28680  0 
  lp                     18352  0 
  parport_pc             33176  1 
  parport                44684  2 lp,parport_pc
  i2c_nforce2            12288  0 
  pcspkr                  7680  0 
  serio_raw              11908  0 
  i2c_core               27392  1 i2c_nforce2
  tsdev                  12800  0 
  ext3                  137744  2 
  jbd                    65616  1 ext3
  ide_cd                 44944  0 
  cdrom                  40360  1 ide_cd
  sg                     40232  0 
  sd_mod                 25472  5 
  8139cp                 29952  0 
  amd74xx                19504  0 [permanent]
  usbhid                 53136  0 
  ohci1394               38088  0 
  ieee1394              361592  2 sbp2,ohci1394
  8139too                33152  0 
  mii                    10240  2 8139cp,8139too
  forcedeth              66704  0 
  sata_nv                17156  4 
  libata                106656  1 sata_nv
  scsi_mod              152336  4 sbp2,sg,sd_mod,libata
  ehci_hcd               36744  0 
  ohci_hcd               24964  0 
  usbcore               139560  4 usbhid,ehci_hcd,ohci_hcd
  dm_mirror              25712  0 
  dm_snapshot            20280  0 
  dm_mod                 63040  6 dm_crypt,dm_mirror,dm_snapshot
  thermal                20112  0 
  processor              43820  1 thermal
  fan                     9736  0 
  fuse                   48688  1

La ultima columna muestra una lista de modulos que hacen uso del modulo listado en la primer columna. 

  Drivers server USB:

  # lsmod | grep usb
  usb_storage            83136  0 
  libusual               23648  1 usb_storage
  usbhid                 35808  0 
  hid                    44864  1 usbhid
  scsi_mod              179000  4 usb_storage,sg,sd_mod,libata
  usbcore               171312  6 usb_storage,libusual,usbhid,ehci_hcd,uhci_hcd

  Desktop:

  # lsmod | grep usb                                                                                                                                                                  
  usbhid                 53136  0 
  usbcore               139560  4 usbhid,ehci_hcd,ohci_hcd

== Cargando modulos del kernel con modprobe ==

  Normalmente los controladores (drivers) de dispositivos de hardware se cargan como
  modulos del kernel, para cargar un modulo, por ejemplo el de la tarjeta de red:

  # modprobe 8139too

== Determinar recursos de hardware ==

  IRQs: Interrupciones
  
  Desktop:

  # cat /proc/interrupts 
           CPU0       CPU1       
    7:          0          0        Phys-irq  parport0
    9:          0          0        Phys-irq  acpi
   14:         94          0        Phys-irq  ide0
   16:         23          0        Phys-irq  ohci_hcd:usb1, libata
   17:          2          0        Phys-irq  ehci_hcd:usb2
   18:      12154          0        Phys-irq  libata
   19:          0          0        Phys-irq  libata
   21:          0          0        Phys-irq  eth1
   22:          3          0        Phys-irq  ohci1394
  253:    4738215          0        Phys-irq  eth0
  254:        803          0        Phys-irq  eth0
  255:        829          0        Phys-irq  eth0
  256:    3033552          0     Dynamic-irq  timer0
  257:       3459          0     Dynamic-irq  resched0
  258:         37          0     Dynamic-irq  callfunc0
  259:          0       4124     Dynamic-irq  resched1
  260:          0         67     Dynamic-irq  callfunc1
  261:          0     686447     Dynamic-irq  timer1
  262:          0          0     Dynamic-irq  mce
  263:          0          0     Dynamic-irq  console
  264:        132          0     Dynamic-irq  xenbus
  265:          0          0     Dynamic-irq  suspend
  NMI:          0          0 
  LOC:          0          0 
  ERR:          0
  MIS:          0

  Laptop:

  $ cat /proc/interrupts 
           CPU0       CPU1       
    0:         28         12   IO-APIC-edge      timer
    1:        492      13328   IO-APIC-edge      i8042
    7:          1          0   IO-APIC-edge    
    8:          0          1   IO-APIC-edge      rtc0
    9:       4673      12788   IO-APIC-fasteoi   acpi
   12:     613770      61940   IO-APIC-edge      i8042
   16:       1373       1738   IO-APIC-fasteoi   ohci_hcd:usb3, ohci_hcd:usb4, hda_intel
   17:          0          4   IO-APIC-fasteoi   ehci_hcd:usb1
   18:        781     230647   IO-APIC-fasteoi   ohci_hcd:usb5, ath9k, radeon
   19:          0         79   IO-APIC-fasteoi   ehci_hcd:usb2, hda_intel
   20:          0          7   IO-APIC-fasteoi   mmc0, yenta
   22:      64366       6842   IO-APIC-fasteoi   ahci
   43:          0          4   PCI-MSI-edge      eth0
  NMI:          0          0   Non-maskable interrupts
  LOC:     639771     627917   Local timer interrupts
  SPU:          0          0   Spurious interrupts
  PMI:          0          0   Performance monitoring interrupts
  PND:          0          0   Performance pending work
  RES:     618882     595742   Rescheduling interrupts
  CAL:         87         59   Function call interrupts
  TLB:      21136      13463   TLB shootdowns
  TRM:          0          0   Thermal event interrupts
  THR:          0          0   Threshold APIC interrupts
  MCE:          0          0   Machine check exceptions
  MCP:         12         12   Machine check polls
  ERR:          1
  MIS:          0

  Servidor con acpi y apic off:

  # cat /proc/interrupts 
             CPU0                                                                                                                                                                     
    0:      11987    XT-PIC-XT        timer                                                                                                                                           
    1:          2    XT-PIC-XT        i8042                                                                                                                                           
    2:          0    XT-PIC-XT        cascade                                                                                                                                         
    5:          0    XT-PIC-XT        libata                                                                                                                                          
    6:  491795616    XT-PIC-XT        eth1                                                                                                                                            
    8:          1    XT-PIC-XT        rtc                                                                                                                                             
   10:   13431817    XT-PIC-XT        aacraid                                                                                                                                         
   11:  515004039    XT-PIC-XT        eth0                                                                                                                                            
   12:          4    XT-PIC-XT        i8042                                                                                                                                           
   14:        105    XT-PIC-XT        libata                                                                                                                                          
   15:          0    XT-PIC-XT        libata                                                                                                                                          
  NMI:          0   Non-maskable interrupts                                                                                                                                           
  LOC:  362269544   Local timer interrupts                                                                                                                                            
  RES:          0   Rescheduling interrupts                                                                                                                                           
  CAL:          0   function call interrupts                                                                                                                                          
  TLB:          0   TLB shootdowns
  TRM:          0   Thermal event interrupts
  THR:          0   Threshold APIC interrupts
  SPU:          0   Spurious interrupts

DMA:

# cat /proc/dma 
 4: cascade

# cat /proc/ioports 
0000-0cf7 : PCI Bus 0000:00
  0000-001f : dma1
  0020-0021 : pic1
  0040-0043 : timer0
  0050-0053 : timer1
  0060-0060 : keyboard
  0064-0064 : keyboard
  0070-0071 : rtc0
  0080-008f : dma page reg
  00a0-00a1 : pic2
  00c0-00df : dma2
  00f0-00ff : fpu
  0220-022f : pnp 00:06
  03c0-03df : vga+
  040b-040b : pnp 00:06
  04d0-04d1 : pnp 00:06
  04d6-04d6 : pnp 00:06
  0530-0537 : pnp 00:06
  087f-087f : pnp 00:06
  0c00-0c01 : pnp 00:06
  0c14-0c14 : pnp 00:06
  0c50-0c52 : pnp 00:06
  0c6c-0c6c : pnp 00:06
  0c6f-0c6f : pnp 00:06
  0cd0-0cd1 : pnp 00:06
  0cd2-0cd3 : pnp 00:06
  0cd4-0cd5 : pnp 00:06
  0cd6-0cd7 : pnp 00:06
  0cd8-0cdf : pnp 00:06
0cf8-0cff : PCI conf1
0d00-ffff : PCI Bus 0000:00
  0f40-0f47 : pnp 00:06
  1000-1fff : PCI Bus 0000:09
  2000-2fff : PCI Bus 0000:0c
    2000-20ff : PCI CardBus 0000:0d
    2400-24ff : PCI CardBus 0000:0d
  8000-805f : pnp 00:06
    8000-8003 : ACPI PM1a_EVT_BLK
    8004-8005 : ACPI PM1a_CNT_BLK
    8008-800b : ACPI PM_TMR
    8010-8015 : ACPI CPU throttle
    8020-8027 : ACPI GPE0_BLK
    8040-8047 : piix4_smbus
  8100-81ff : pnp 00:06
  8200-82ff : pnp 00:06
    8200-8200 : ACPI PM2_CNT_BLK
  8400-840f : 0000:00:11.0
    8400-840f : ahci
  8410-8413 : 0000:00:11.0
    8410-8413 : ahci
  8414-8417 : 0000:00:11.0
    8414-8417 : ahci
  8418-841f : 0000:00:11.0
    8418-841f : ahci
  8420-8427 : 0000:00:11.0
    8420-8427 : ahci
  9000-9fff : PCI Bus 0000:01
    9000-90ff : 0000:01:05.0

== Que son los dispositivos coldplug y hotplug ==

Los dispositivos son coldplugged cuando se tiene que apagar la computadora para conectarlos, y son
hotplug cuando se pueden conectar y desconectar sin apagar el sistema.

Hotplug: Mouse PS/2, Memoria USB
Colplug: tarjetas PCI, ISA, discos IDE, CPU y memoria.

En sistemas normales el CPU y la memoria son coldplugables, pero en algunos mainframes son hotplug.

"As mentioned in the article on hot swap, the terms hot plug and cold plug, can be taken to mean two different things, depending on the context. In a more generic context, hot plug is the ability to add or remove hardware without powering down the system, while cold plug is the inability to do so. In the context of comparing certain hot-pluggable devices, however, hot plug can be taken to mean the ability of the system to autonomously detect the addition or removal of hardware as it occurs, while cold plug can be taken to mean the ability to add or remove devices without powering down or rebooting the system, but the inability of the system to detect these changes, in which case the system operator would have to tell the system software that the change has occurred."

https://secure.wikimedia.org/wikipedia/en/wiki/Coldplug


== Introducción a udev, sysfs ==

  Traducionalmente, en los sistemas Linux, los archivos especiales de dispositivo se creaban en el directorio /dev
  de forma estática, existian nodos para cualquier posible dispositivo, exista o no en el sistema. Después se
  introdujo devfs, con el cual solo se creaban node devices para dispositivos que si existen en el sistema.

  En el kernel 2.6 se introduce udev, un demonio a nivel de usuario que permite la creación de los archivos especiales
  de dispositivo de forma dinámica, los archivos de dispositivo son creados cuando se conecta o desconecta el dispositivo,
  solo crea los node devices para dispositivos que en realidad estan presentes, permite definir nombres persistentes para diferentes dispositivos.

  Por ejemplo para diferenciar impresoras en /dev/lptX, discos duros, interfaces de red.

  Ejemplo con USB DVD y USB CD-RW, dependiendo de como los conectas puede ser /dev/sr0 o /dev/sr1, y tenias que
  arreglartelas para saber cual es cual. Con udev puedes crear reglas para crear nombres consistentes, por ejemplo,
  para el DVD /dev/usbdvd y para el CD-RW /dev/usbcdrw, independientemente del orden en que se conectan.

  udev usa la información que pone a disposición sysfs en el sistema de archivos virtual /sys. y en base a algunas
  reglas crea los archivos de dispositivo.

  $ udevadm info -q path -n /dev/sr0
  /devices/pci0000:00/0000:00:11.0/host1/target1:0:0/1:0:0:0/block/sr0

  Now let’s use the product information gleaned above to identify the devices and add udev naming rules. Create a file called /etc/udev/rules.d/40-cdvd.rules and add the rules shown in Listing One to it.

  LISTING ONE: A new file for udev configuration

  BUS=”usb”, SYSFS{idProduct}=”0701″, SYSFS{idVendor}=”05e3″, 
  KERNEL=”sr[0-9]*”, NAME=”%k”, SYMLINK=”usbdvd” 

  BUS=”usb”, SYSFS{idProduct}=”0302″, SYSFS{idVendor}=”0dbf”, 
  KERNEL=”sr[0-9]*”, NAME=”%k”, SYMLINK=”usbcdrw” 


  Para probar la regla:

  # udevadm test /sys/block/sr0

  Las reglas de udev se definen en el directorio /etc/udev/rules.d y /lib/udev/rules.d, los archivos de reglas tienen finalización:
  .rules, los archivos de las reglas son leidos alfabeticamente.

  key, [key,...] NAME [, SYMLINK]


  Ejemplo para interfaces de red:

  # PCI device 0x14e4:0x1684 (tg3)
  SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="00:1d:72:e7:ce:f4", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="eth*", NAME="eth0"

  # PCI device 0x168c:0x002a (ath9k)
  SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="00:17:c4:5f:30:e4", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="wlan*", NAME="wlan0"

- Usando udevminfo/udevadm para consultar información sobre un dispositivo:

  Ejemplo para consultar infor sobre disco /dev/sda:

  # udevadm info -q path -n /dev/sda
  /devices/pci0000:00/0000:00:11.0/host0/target0:0:0/0:0:0:0/block/sda

  Termina en block/sda:

  # udevadm info -a -p /sys/block/sda

  http://www.signal11.us/oss/udev/

- El sistema de archivos /sys

  sysfs es un sistema de archivos virtual que exporta información de los dispositivos en una jerarquia de
  archivos en /sys, la cual puede ser usada para obtener información de los dispositivos y también editar
  o cambiar algún parámetro.

 The class interface abstracts the idea that each device falls under a broader class (or category) of devices. A USB mouse, a PS/2 keyboard, and a joystick, all fall under the input class and own entries under /sys/class/input/.

  NOTA: recordar que se usa para detectar nuevos discos scsi?

    http://es.wikipedia.org/wiki/Sysfs
    http://www.mjmwired.net/kernel/Documentation/filesystems/sysfs.txt

  # find /sys -name eth0

  La información de la interfaz de red eth0 se representa en:

  /sys/devices/pci0000:00/0000:00:05.0/0000:03:00.0/net/eth0

  Vea el PCI ID con lspci:
  # lspci | grep Ethernet
  ...
  ...
  03:00.0 Ethernet controller: Broadcom Corporation NetXtreme BCM5764M Gigabit Ethernet PCIe (rev 10)

  Tambien usa enlaces simbolicos:

  /sys/class/net/eth0

  Veamos la información exportada para los dispositivos de la clase net:

  # ls -l /sys/class/net
  total 0
  lrwxrwxrwx 1 root root 0 2010-12-23 18:32 eth0 -> ../../devices/pci0000:00/0000:00:05.0/0000:03:00.0/net/eth0
  lrwxrwxrwx 1 root root 0 2010-12-23 18:31 lo -> ../../devices/virtual/net/lo
  lrwxrwxrwx 1 root root 0 2010-12-23 18:32 wlan0 -> ../../devices/pci0000:00/0000:00:06.0/0000:06:00.0/net/wlan0

  # cat /sys/class/net/eth0/address 
  00:1d:72:e7:ce:f4
  # cat /sys/class/net/eth0/duplex 
  full
  # cat /sys/class/net/eth0/operstate 
  down
  # cat /sys/class/net/eth0/statistics/tx_bytes 
  10785471

  La dirección dde un dispositivo Bluetooth:

  # cat /sys/class/bluetooth/hci0/address 
  00:23:4E:F2:8F:1F

  hald es:
  dbus es:

== Desactivar soporte ACPI ==

  Si el soporte ACPI esta compilado como estatico en el kernel:

  # grep -i acpi /boot/config-2.6.35-25-generic
  CONFIG_ACPI=y

  Agregar el parámetro acpi=off al kernel en grub:

  # vim /boot/grub/menu.lst

  title           Ubuntu 8.04.4 LTS, kernel 2.6.24-28-server
  root            (hd0,0)
  kernel          /vmlinuz-2.6.24-28-server root=/dev/sda3 ro acpi=off
  initrd          /initrd.img-2.6.24-28-server

- Desactivar soporte APIC

  title           Ubuntu 8.04.4 LTS, kernel 2.6.24-28-server
  root            (hd0,0)
  kernel          /vmlinuz-2.6.24-28-server root=/dev/sda3 ro noapic
  initrd          /initrd.img-2.6.24-28-servers

== Desabilitar soporte USB ==

Si el soporte se cargo como módulos metalos a la lista negra de modprobe:

  $ sudo sh -c 'echo blacklist ehci_hcd > /etc/modprobe.d/blacklist-ehci'
  $ sudo sh -c 'echo blacklist uhci_hcd > /etc/modprobe.d/blacklist-uhci'
  $ sudo sh -c 'echo blacklist ohci_hcd > /etc/modprobe.d/blacklist-ohci'

Actualizar la imagen initrd:

  $ sudo update-initramfs -u

or

  $ sudo update-initramfs -u -k `uname -r`

== Manuales de referencía ==

lspci (8)            - list all PCI devices
update-pciids (8)    - download new version of the PCI ID list
lsusb (1)            - list USB devices
lsusb (8)            - list USB devices
lsmod (8)            - program to show the status of modules in the Linux Kernel
modprobe (8)         - program to add and remove modules from the Linux Kernel
modprobe.conf (5)    - Configuration directory/file for modprobe
modprobe.d (5)       - Configuration directory/file for modprobe
udevadm (8)          - udev management tool
update-initramfs (8) - generate an initramfs image

== Archivos de referencía ==

/proc/scsi/scsi
/proc/interrupts
/proc/dma
/proc/ioports
/sys/class/net
/sys/block
/etc/modprobe.d/blacklist*
/boot/grub/menu.lst