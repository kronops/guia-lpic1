<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Gnu/Linux" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1579145571787"><hook NAME="MapStyle" zoom="1.5">
    <properties edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff,#808080ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="40" RULE="ON_BRANCH_CREATION"/>
<node TEXT="101.1 Determine y configure las opciones de hardware" FOLDED="true" POSITION="right" ID="ID_470455371" CREATED="1469395568591" MODIFIED="1545528932604">
<edge STYLE="bezier" COLOR="#ff0000"/>
<node TEXT="Hardware abstraction layer (HAL)" FOLDED="true" ID="ID_1748750929" CREATED="1469397051190" MODIFIED="1469403453380">
<node TEXT="Detecta autom&#xe1;ticamente la mayoria de los dispositivos de hardware" ID="ID_1448596734" CREATED="1469403468019" MODIFIED="1469403716331"/>
<node TEXT="Iniciar el administrador de archivos cuando una usb o c&#xe1;mara es insertada" ID="ID_1347366361" CREATED="1469403723173" MODIFIED="1469403822814"/>
<node TEXT="Iniciar una aplicaci&#xf3;n en caso que una c&#xe1;mara web este conectada al ordenador." ID="ID_1364845583" CREATED="1469403823848" MODIFIED="1469403931915"/>
<node TEXT="Hal es una interfaz de software entre el hardware y el sistema operativo." ID="ID_1278483002" CREATED="1469403936766" MODIFIED="1469403985305"/>
<node TEXT="Demonio Hal o hald" FOLDED="true" ID="ID_141990695" CREATED="1469404154443" MODIFIED="1469404882032">
<node TEXT="Tiene una base de datos del hardware conectado al sistema, es&#xa;inicializado durante el arranque del sistema operativo.&#xa;Sincroniza la informaci&#xf3;n tomada de los archivos de informaci&#xf3;n de dispositivos.&#xa;Ejemplo: /etc/fstab y manejar ciclo de vida de dispositivos de hardware." ID="ID_1112369509" CREATED="1469404883386" MODIFIED="1469413441013"/>
</node>
<node TEXT="/sys alias sysfs" FOLDED="true" ID="ID_1167305949" CREATED="1469413588203" MODIFIED="1469413660308">
<node TEXT="Sistema de archivos virtual que provee de informaci&#xf3;n acerca de los distintos procesos del sistema. Toda la informaci&#xf3;n se guarda en los distintos directorios dentro de  /sys" ID="ID_1302426837" CREATED="1469413661684" MODIFIED="1469414427610"/>
</node>
<node TEXT="udev" FOLDED="true" ID="ID_1116781905" CREATED="1469414429751" MODIFIED="1469414450040">
<node TEXT="Demonio responsable de monitorear el sistema ante la presencia de un nuevo dispositivo." ID="ID_809258571" CREATED="1469414451550" MODIFIED="1469414556507"/>
<node TEXT="Si una usb se conecta, udev crea un nuevo archivo de dispositivo en el directorio /dev" ID="ID_1449202992" CREATED="1469414557531" MODIFIED="1469414703616"/>
</node>
<node TEXT="GUI Interfaz de  usuario grafica" FOLDED="true" ID="ID_932928867" CREATED="1469414706576" MODIFIED="1469414743191">
<node TEXT="Se consulta para ver las acciones de se&#xf1;ales que emiten los dispositivos de&#xa;hardware Recibidas a trav&#xe9;s del Dbus, este habilita la comunicaci&#xf3;n entre los&#xa;procesos del sistema (IPC) con las apliicaciones de escritorio." ID="ID_1265127882" CREATED="1469414745841" MODIFIED="1469415015007"/>
</node>
<node TEXT="Puntos de estudio." FOLDED="true" ID="ID_1951424235" CREATED="1469416082544" MODIFIED="1469416093642">
<node TEXT="Explique con sus propias palabras que significa HAL" FOLDED="true" ID="ID_1660141421" CREATED="1469588879905" MODIFIED="1469588951492">
<node TEXT="Capa de abstracci&#xf3;n de hardware" ID="ID_1400780519" CREATED="1469761685110" MODIFIED="1469761715006"/>
</node>
<node TEXT="Conecte una usb al sistema, luego revise que udev haya creado un nuevo archivo de dispositivo&#xa;en el directorio /dev" FOLDED="true" ID="ID_737750225" CREATED="1469588952560" MODIFIED="1469678952205">
<node TEXT="Es similar cuando conectamos la placa arduino y le damos privilegios con&#xa;chmod" ID="ID_1552204164" CREATED="1470795484954" MODIFIED="1470795569600"/>
</node>
<node TEXT="Explique la funci&#xf3;n de dbus" FOLDED="true" ID="ID_1660097386" CREATED="1469678954696" MODIFIED="1469679301296">
<node TEXT="Dbus (Desktop bus) es una aplicaci&#xf3;n de software para comunicarse entre&#xa;procesos." ID="ID_1114872915" CREATED="1470796613836" MODIFIED="1470800875409"/>
</node>
<node TEXT="Estudie el directorio /sys" FOLDED="true" ID="ID_602234073" CREATED="1469679302507" MODIFIED="1469679350549">
<node TEXT="Permite obtener informaci&#xf3;n sobre el sistema y sus componentes&#xa;en su mayoria (instalado y conectado hardware) de una forma estructurada." ID="ID_1135557032" CREATED="1471318683097" MODIFIED="1471318796190"/>
</node>
</node>
</node>
<node TEXT="Segunda parte" FOLDED="true" ID="ID_1824971589" CREATED="1472425321065" MODIFIED="1472439275615">
<node TEXT="I/O Ports (Direcciones de entrada y salida)" FOLDED="true" ID="ID_1345328919" CREATED="1472439287610" MODIFIED="1472522665938">
<node TEXT="Son ubicaci&#xf3;nes &#xfa;nicas dentro de la memoria del sistema, reservada&#xa;para las comunicaci&#xf3;nes entre dispositivos de hardware y el cpu.&#xa;No deber&#xed;a ser comprartidas en circunstancias normales ya que estan asociadas con dispositivos especificos." ID="ID_321401900" CREATED="1472523493079" MODIFIED="1472523848486"/>
<node TEXT="Los puertos de entrada y salida estan en formato hexadecimal, es por eso que empiezan&#xa;con 0x" FOLDED="true" ID="ID_210551541" CREATED="1472523887221" MODIFIED="1472523946695">
<node TEXT="Ejemplo: 0x3f8 es la notaci&#xf3;n para un puerto serial." ID="ID_249148816" CREATED="1472523946752" MODIFIED="1472524027131"/>
</node>
</node>
<node TEXT="Interrups" FOLDED="true" ID="ID_250568882" CREATED="1472522680521" MODIFIED="1472522701711">
<node TEXT="Iterruption reques (Interrupciones). Se&#xf1;al enviada al cpu solicitando&#xa;la interrupci&#xf3;n de la actividad de procesamiento, para manejar la actividad&#xa;de eventos externos como la actividad de un teclado" FOLDED="true" ID="ID_1406472005" CREATED="1472524728477" MODIFIED="1472525069848">
<node TEXT="Ejemplo: introducci&#xf3;n de informaci&#xf3;n mediante el teclado" ID="ID_1010349839" CREATED="1472525069901" MODIFIED="1472525115470"/>
</node>
<node TEXT="Las 16 IRQ disponibles estan numeradas del 0 al 15, las interrupciones&#xa;compartidas fueron  debido a que las interrupciones de peticion  son escasas, evitan conflicto&#xa;con las distintas interrupciones." FOLDED="true" ID="ID_129820895" CREATED="1472525120416" MODIFIED="1472525486846">
<node TEXT="Interrupciones compartidas un mismo IRQ puede ser utilizado&#xa;por dos controladores dstintos, algunos son asignados por el&#xa;mismo hardware" FOLDED="true" ID="ID_689673545" CREATED="1472525486914" MODIFIED="1472525615504">
<node TEXT="Ejmplo: lista de iRQ mas utlizadas por el&#xa;sistema&#xa;IRQ 0 controlador de la hora&#xa;IRQ 1 Interrupci&#xf3;n del teclado&#xa;IRQ 3 COM2&#xa;IRQ 4 COM1&#xa;IRQ 5 Tarjeta de sonido, LPT2&#xa;IRQ 7 LPT1&#xa;IRQ 14 Controlador de disco f&#xed;sico (1&#xb0; chip)&#xa;IRQ 15 Controlador de disco f&#xed;sico (2&#xb0; chip)" ID="ID_241688500" CREATED="1472525799451" MODIFIED="1472526170289"/>
</node>
</node>
</node>
<node TEXT="DMA" FOLDED="true" ID="ID_106021549" CREATED="1472522702132" MODIFIED="1472522715495">
<node TEXT="Acceso directo a memoria" FOLDED="true" ID="ID_1394122216" CREATED="1473912330174" MODIFIED="1473912348845">
<node TEXT="Es una alternativa al metodo de comunicaci&#xf3;n de los puertos&#xa;de entrada y salida, permite transferir informaci&#xf3;n directamente&#xa;sin la parci&#xf3;n de la unidad de procesamiento central. es mejor por que se usa menos&#xa;la participaci&#xf3;n del CPU y de los puertos de entrada y salida" ID="ID_316082620" CREATED="1473912352830" MODIFIED="1474074575588"/>
<node TEXT="Udted encontrar&#xe1; informaci&#xf3;n de los canales acceso a memoria&#xa;en el directorio:" FOLDED="true" ID="ID_1898910097" CREATED="1474074576475" MODIFIED="1474074713138">
<node TEXT="#cat /proc/dma" ID="ID_787275856" CREATED="1474074717160" MODIFIED="1474074780444"/>
</node>
</node>
</node>
<node TEXT="/proc" FOLDED="true" ID="ID_460674736" CREATED="1472522716359" MODIFIED="1472522721817">
<node TEXT="Con el siguiente comando se pueden ver lospuertos de entrada y&#xa;salida actualmente en uso" FOLDED="true" ID="ID_961226852" CREATED="1472524030415" MODIFIED="1472524456344">
<node TEXT="cat /proc/ioports" ID="ID_1698898434" CREATED="1472524462040" MODIFIED="1472524493183"/>
</node>
<node TEXT="Es un sistema de archivos virtual, provee una interfaz directa con el n&#xfa;cleo&#xa;desl software o kernel, tambi&#xe9;n informaci&#xf3;n en tiempo real" FOLDED="true" ID="ID_861580258" CREATED="1474075339159" MODIFIED="1474075460031">
<node TEXT="Se pueden combiar opciones del nucleo mediante los archivos&#xa;ubicados en el directorio /proc" ID="ID_1986856456" CREATED="1474075461319" MODIFIED="1474076729150"/>
<node TEXT="Algunos  archivos relevantes dentro del directorio /proc" FOLDED="true" ID="ID_422754935" CREATED="1474076737712" MODIFIED="1474076758485">
<node TEXT="$cat /proc/iomem" FOLDED="true" ID="ID_692037070" CREATED="1474076761570" MODIFIED="1474077120920">
<node TEXT="Puede ver toda la memoria de entrada y salida" ID="ID_1011326338" CREATED="1474077122998" MODIFIED="1474077260031"/>
</node>
<node TEXT="$cat /proc/cpuinfo" FOLDED="true" ID="ID_730174443" CREATED="1474077261887" MODIFIED="1474077284898">
<node TEXT="Informaci&#xf3;n detallada acerca de todos los cpu&apos;s detallados" ID="ID_291047368" CREATED="1474077289618" MODIFIED="1474078749387"/>
<node TEXT="Nota: cada cpu en el hardware, nucleos, etc son vistos como cpu&#x15b;&#xa;individuales." ID="ID_1218922670" CREATED="1474078751144" MODIFIED="1474078799475"/>
</node>
<node TEXT="$cat /proc/devices" FOLDED="true" ID="ID_98740218" CREATED="1474078983134" MODIFIED="1474079007124">
<node TEXT="Encontrar&#xe1; todos los dispositivos de caracteres y orientados&#xa;por bloque, que son soportados por el nucleo de software" ID="ID_413041530" CREATED="1474079009111" MODIFIED="1474079254966"/>
</node>
<node TEXT="$cat /proc/partition" FOLDED="true" ID="ID_154611948" CREATED="1474079257357" MODIFIED="1474079496596">
<node TEXT="Lista de todas las particiones reconocidas con su&#xa;respectivo tama&#xf1;o de bloque" ID="ID_1307646379" CREATED="1474079497084" MODIFIED="1474079539877"/>
</node>
<node TEXT="$cat /proc/pci" FOLDED="true" ID="ID_22437113" CREATED="1474079542677" MODIFIED="1474079556947">
<node TEXT="Todos los dispositivos pci conectados al sistema" ID="ID_1462607920" CREATED="1474079557602" MODIFIED="1474079601158"/>
<node TEXT="En muchas distribuciones de gnu linux se puede encontrar de esta forma.&#xa;$cd /sys/bus/pci; ls -la" ID="ID_1087258886" CREATED="1474079960645" MODIFIED="1474080110504"/>
</node>
</node>
<node TEXT="Puntos de estudio" ID="ID_1378696363" CREATED="1474081326327" MODIFIED="1474081333498"/>
</node>
</node>
<node TEXT="Puntos de estudio" FOLDED="true" ID="ID_360053969" CREATED="1474341858979" MODIFIED="1474341866960">
<node TEXT="Investigar tablas de alojaci&#xf3;n de dispositivos de Hardware" ID="ID_980580172" CREATED="1474342144329" MODIFIED="1474342391877"/>
<node TEXT="Memorice la lista de peticiones de IRQ" FOLDED="true" ID="ID_1017114059" CREATED="1474342392673" MODIFIED="1474342437918">
<node TEXT="Nombre&#xa; Int (hex)&#xa;XT: Descripci&#xf3;n AT: Descripci&#xf3;n&#xa;NMI ---         Paridad* Paridad*&#xa;0 08         Temporizador* Temporizador*&#xa;1 09         Teclado* Teclado*&#xa;IRQ2 0A          Reservado Interrupciones 8 a 15 (PIC#2)&#xa;IRQ3 0B Puertos serie COM2/COM4 Puerto serie COM2/COM4&#xa;IRQ4 0C Puertos serie COM1/COM3 Puertos serie COM1/COM3&#xa;IRQ5 0D           Disco duro Impresora secundaria LPT2&#xa;IRQ6 0E            Disquete Disquete&#xa;IRQ7 0F   Impresora primaria LPT1 Impresora primaria LPT1&#xa;8 70            No aplicable Reloj de tiempo real*&#xa;9 71            No aplicable Redirigido a IRQ2*&#xa;IRQ10 72            No aplicable no asignado&#xa;IRQ11 73            No aplicable no asignado&#xa;IRQ12 74            No aplicable Rat&#xf3;n PS2&#xa;13 75            No aplicable Coprocesador 80287*&#xa;IRQ14 76            No aplicable Contr. disco IDE primario&#xa;IRQ15 77            No aplicable Contr. disco IDE secundario" ID="ID_1610387571" CREATED="1475430707105" MODIFIED="1475430895014"/>
<node TEXT="https://es.wikipedia.org/wiki/Interrupci%C3%B3n" ID="ID_1090392307" CREATED="1475432036185" MODIFIED="1475432042550"/>
</node>
<node TEXT="Investigue otros conceptos sobre Acceso Directo a Memoria" FOLDED="true" ID="ID_1907393586" CREATED="1474342445265" MODIFIED="1475432366078">
<node TEXT="permite a cierto tipo de componentes de una computadora acceder a la memoria del sistema para leer o escribir" ID="ID_454227162" CREATED="1475432930477" MODIFIED="1475433025952"/>
</node>
<node TEXT="Estudie los directorios que se encuentran ubicados en /proc" FOLDED="true" ID="ID_1209738451" CREATED="1474343549482" MODIFIED="1474343570041">
<node TEXT="/proc/1&#xa;&#xa;    Un directorio con informaci&#xf3;n acerca del proceso n&#xfa;mero 1. Cada proceso tiene un directorio debajo de /proc cuyo nombre es el n&#xfa;mero de identificaci&#xf3;n del proceso (PID).&#xa;/proc/cpuinfo&#xa;&#xa;    Informaci&#xf3;n acerca del procesador: su tipo, marca, modelo, rendimiento, etc.&#xa;/proc/devices&#xa;&#xa;    Lista de controladores de dispositivos configurados dentro del n&#xfa;cleo que est&#xe1; en ejecuci&#xf3;n.&#xa;/proc/dma&#xa;&#xa;    Muestra los canales DMA que est&#xe1;n siendo utilizados.&#xa;/proc/filesystems&#xa;&#xa;    Lista los sistemas de archivos que est&#xe1;n soportados por el kernel.&#xa;/proc/interrupts&#xa;&#xa;    Muestra la interrupciones que est&#xe1;n siendo utilizadas, y cuantas de cada tipo ha habido.&#xa;/proc/ioports&#xa;&#xa;    Informaci&#xf3;n de los puertos de E/S que se est&#xe9;n utilizando en cada momento.&#xa;/proc/kcore&#xa;&#xa;    Es una imagen de la memoria f&#xed;sica del sistema. Este archivo tiene exactamente el mismo tama&#xf1;o que la memoria f&#xed;sica, pero no existe en memoria como el resto de los archivos bajo /proc, sino que se genera en el momento en que un programa accede a este. (Recuerde: a menos que copie este archivo en otro lugar, nada bajo /proc usa espacio en disco).&#xa;/proc/kmsg&#xa;&#xa;    Salida de los mensajes emitidos por el kernel. Estos tambi&#xe9;n son redirigidos hacia syslog.&#xa;/proc/ksyms&#xa;&#xa;    Tabla de s&#xed;mbolos para el kernel.&#xa;/proc/loadavg&#xa;&#xa;    El nivel medio de carga del sistema; tres indicadores significativos sobre la carga de trabajo del sistema en cada momento.&#xa;/proc/meminfo&#xa;&#xa;    Informaci&#xf3;n acerca de la utilizaci&#xf3;n de la memoria f&#xed;sica y del archivo de intercambio.&#xa;/proc/modules&#xa;&#xa;    Indica los m&#xf3;dulos del n&#xfa;cleo que han sido cargados hasta el momento.&#xa;/proc/net&#xa;&#xa;    Informaci&#xf3;n acerca del estado de los protocolos de red.&#xa;/proc/self&#xa;&#xa;    Un enlace simb&#xf3;lico al directorio de proceso del programa que est&#xe9; observando a /proc. Cuando dos procesos observan a /proc, obtienen diferentes enlaces. Esto es principalmente una conveniencia para que sea f&#xe1;cil para los programas acceder a su directorio de procesos.&#xa;/proc/stat&#xa;&#xa;    Varias estad&#xed;sticas acerca del sistema, tales como el n&#xfa;mero de fallos de p&#xe1;gina que han tenido lugar desde el arranque del sistema.&#xa;/proc/uptime&#xa;&#xa;    Indica el tiempo en segundos que el sistema lleva funcionando.&#xa;/proc/version&#xa;&#xa;    Indica la versi&#xf3;n del n&#xfa;cleo" ID="ID_696478030" CREATED="1475435462682" MODIFIED="1475435523735"/>
</node>
</node>
</node>
<node TEXT="Tercera parte" FOLDED="true" ID="ID_555016964" CREATED="1478122429846" MODIFIED="1478122585069">
<node TEXT="BIOS" FOLDED="true" ID="ID_1186911456" CREATED="1478122589071" MODIFIED="1478122599422">
<node TEXT="Sistema b&#xe1;sico de entrada  y salida" ID="ID_686429482" CREATED="1478122604013" MODIFIED="1478122678250"/>
<node TEXT="Inicializa el hardware existente en una computadora&#xa;y despues el sistema operativo." ID="ID_1766989691" CREATED="1478123447156" MODIFIED="1478124650105"/>
<node TEXT="La inicializaci&#xf3;n del hardware es realizado mediante el POST (Power On Self Test) o&#xa;autodiagnostico al encender" ID="ID_312087004" CREATED="1478124657400" MODIFIED="1478125088969"/>
<node TEXT="Forma de acceder" FOLDED="true" ID="ID_452117695" CREATED="1478125093015" MODIFIED="1478125243603">
<node TEXT="DeL, Esc, F1, F2 o F10&#xa;Pero puede cambiar, seg&#xfa;n el modelo y marca de la&#xa;computadora" ID="ID_1158481496" CREATED="1478125246632" MODIFIED="1478125333104"/>
</node>
<node TEXT="Dertermina el orden de los dispositivos que seran utilizados para inicializar el sistema" ID="ID_1721781695" CREATED="1478125437065" MODIFIED="1478125483863"/>
<node TEXT="Con respecto al control de  errores en el BIOS es recomendable hacer ajustes en &quot;Detener en todos los errores&quot;excepto en los que provienen del teclado, esta configuraci&#xf3;n asegura que se inicie el sistema incluso si el BIOS no detecta el teclado conectado" ID="ID_964384418" CREATED="1478125660178" MODIFIED="1478125857804"/>
<node TEXT="Puntos de estudio" FOLDED="true" ID="ID_737661569" CREATED="1478125862206" MODIFIED="1478126220867">
<node TEXT="Investigue con que teclas puede acceder al BIOS de su&#xa;computadora" ID="ID_665594914" CREATED="1478126222563" MODIFIED="1478126250656"/>
<node TEXT="Pruebe Cambiando el orden de booteo&#xa;puede arrancar el sistema desde un CD o USB" ID="ID_266341644" CREATED="1478126251960" MODIFIED="1478126308698"/>
</node>
</node>
</node>
<node TEXT="Cuarta Parte" FOLDED="true" ID="ID_1742531571" CREATED="1479832927961" MODIFIED="1479832943367">
<node TEXT="Coldplug" FOLDED="true" ID="ID_1697291253" CREATED="1479833023317" MODIFIED="1479833850364">
<node TEXT="Son los dispositivos de almacenamiento que se pueden&#xa;conectar a su ordenador mientras esta encendido o trabajando" FOLDED="true" ID="ID_92275271" CREATED="1479833966151" MODIFIED="1479834100083">
<node TEXT="Ejemplo: cualquier dispositivo que se conecte por vis USB&#xa;Memoria Flash&#xa;Camaras&#xa;Celulares, etc&#xa;Tambi&#xe9;n memorias sd" ID="ID_687937941" CREATED="1479834101533" MODIFIED="1479834917754"/>
</node>
</node>
<node TEXT="Hotplug" FOLDED="true" ID="ID_819741493" CREATED="1479833851235" MODIFIED="1479833865431">
<node TEXT="Son los dispositivos de almacenamiento los cuales pueden&#xa;conectarse a su ordenador mientras se encuentre apagado." ID="ID_1841489882" CREATED="1479834946526" MODIFIED="1479835423801"/>
</node>
<node TEXT="Pata" FOLDED="true" ID="ID_915542679" CREATED="1479833866539" MODIFIED="1479833872150">
<node TEXT="Parallel Advance Technology Attachment" ID="ID_723629223" CREATED="1480349020021" MODIFIED="1480349260385"/>
<node TEXT="Disco Ide ordinario y soporta como maximo 4 discos&#xa;duros" ID="ID_1906096346" CREATED="1480349263629" MODIFIED="1480349310781"/>
<node TEXT="Es detectado como /dev/hda&#xa;/dev/deb etc" ID="ID_61634754" CREATED="1480349405335" MODIFIED="1480349484053"/>
</node>
<node TEXT="Sata" FOLDED="true" ID="ID_878849703" CREATED="1479833872672" MODIFIED="1479833876916">
<node TEXT="Serial Advanced Tecnology Attschmrnt" ID="ID_1200685162" CREATED="1480349595702" MODIFIED="1480349635660"/>
<node TEXT="Soporta 4 discos como maximo y es Hotplug, es&#xa;decir se puede cnectar en caliente, mayor transferencia de datos&#xa;que las unidades Pata." ID="ID_1047790123" CREATED="1480349639251" MODIFIED="1480350107783"/>
<node TEXT="Es detectado como /dev/sda&#xa;/dev/sdb etc" ID="ID_1269500173" CREATED="1480349987348" MODIFIED="1480350217057"/>
</node>
<node TEXT="SCSI" FOLDED="true" ID="ID_1987586059" CREATED="1479833907128" MODIFIED="1479833947928">
<node TEXT="Small computer System Interfaces" ID="ID_1429998614" CREATED="1480350380059" MODIFIED="1480350394474"/>
<node TEXT="Necesita un cotrolador de host, soporta 16 Discos&#xa;duros en el sistema como Maximo" ID="ID_1305296011" CREATED="1480350425340" MODIFIED="1480350741652"/>
<node TEXT="Es detectado como /dev/sda&#xa;/dev/sdb etc" ID="ID_151787332" CREATED="1480350742032" MODIFIED="1480350761398"/>
</node>
<node TEXT="Usb" FOLDED="true" ID="ID_948145746" CREATED="1479833948886" MODIFIED="1479833957711">
<node TEXT="Universal Serie Bus" ID="ID_1922251306" CREATED="1480350771225" MODIFIED="1480350891077"/>
<node TEXT="Son capacidad mobil y mucho mas felxibles  externos" ID="ID_309866307" CREATED="1480350893342" MODIFIED="1480350967582"/>
<node TEXT="Son detectados como /dev/sda&#xa;/dev/sdb etc" ID="ID_886348120" CREATED="1480350968284" MODIFIED="1480350990310"/>
</node>
<node TEXT="Puntos de estudo" FOLDED="true" ID="ID_1415699776" CREATED="1480351158088" MODIFIED="1480351165526">
<node TEXT="Explique con sus propias palabras&#xa;la diferencia entre dispositivos coldplug y hotplug" FOLDED="true" ID="ID_1305789509" CREATED="1480351167006" MODIFIED="1480351204996">
<node TEXT="Los dispositivos hotplug no necesitan que el ordenador se&#xa;encuentre apagado para conectarse, y los coldplug si se tiene que apagar&#xa;para poder conectarse." ID="ID_210322842" CREATED="1480914190614" MODIFIED="1480914275004"/>
</node>
<node TEXT="Verifique con el comqando fdisk -l que medios de&#xa;almacenamiento masivo se encuentran en&#xa;su sistema" FOLDED="true" ID="ID_592769969" CREATED="1480351206116" MODIFIED="1480351290259">
<node TEXT="Por el momento solo el disco duro local:&#xa;Dispositivo Inicio    Comienzo      Fin      Bloques  Id  Sistema&#xa;/dev/sda1   *        2048      206847      102400    7  HPFS/NTFS/exFAT&#xa;/dev/sda2          206848   102402047    51097600    7  HPFS/NTFS/exFAT&#xa;/dev/sda3       102404094   234440703    66018305    5  Extendida&#xa;/dev/sda5       102404096   106307583     1951744   82  Linux swap / Solaris&#xa;/dev/sda6       106309632   125839359     9764864   83  Linux&#xa;/dev/sda7       125841408   234440703    54299648   83  Linux" ID="ID_1882899465" CREATED="1481597281919" MODIFIED="1481597304960"/>
</node>
<node TEXT="Memoricel los distintos metodos de cl&#xe1;sificaci&#xf3;n de los tipos de&#xa;almacenamiento masivo" ID="ID_1169703772" CREATED="1480351336399" MODIFIED="1480351439464"/>
<node TEXT="&#xbf;Que dispositivos son capas de conectarse en caliente y cuales en&#xa;frio?" FOLDED="true" ID="ID_235909739" CREATED="1480351440760" MODIFIED="1480351547588">
<node TEXT="Coldplug Dispositivos Pata, IDE&#xa;Hotplug Dispositivos Sata, USB" ID="ID_820728729" CREATED="1481597776568" MODIFIED="1481597916135"/>
</node>
</node>
</node>
<node TEXT="Quinta Parte" FOLDED="true" ID="ID_641455606" CREATED="1482203122336" MODIFIED="1482203392949">
<node TEXT="USB" FOLDED="true" ID="ID_829429782" CREATED="1482203400548" MODIFIED="1482206907895">
<node TEXT="Universal Serial Bus.&#xa;Es un sistema de bus en serie para conectar dispositivos&#xa;externos a un ordenador" ID="ID_1987726514" CREATED="1482206920329" MODIFIED="1482207202714"/>
</node>
<node TEXT="OHCI" FOLDED="true" ID="ID_1981111042" CREATED="1482207182069" MODIFIED="1482207604124">
<node TEXT="Open Host Controller Interfaces." ID="ID_679311885" CREATED="1482207604157" MODIFIED="1482208210710"/>
</node>
<node TEXT="UHCI" FOLDED="true" ID="ID_1494554702" CREATED="1482208212841" MODIFIED="1482208502433">
<node TEXT="Universal Host Controller Interfaces" ID="ID_271963076" CREATED="1482208506532" MODIFIED="1482208526211"/>
</node>
<node TEXT="EHCI" FOLDED="true" ID="ID_254371266" CREATED="1482208789772" MODIFIED="1482208795609">
<node TEXT="Enhanced Host Controller Interfaces" FOLDED="true" ID="ID_1132636582" CREATED="1482208797482" MODIFIED="1482208817178">
<node TEXT="Necesita para su operacion un controlador&#xa;OHCI o UHCI" ID="ID_681047806" CREATED="1482209061219" MODIFIED="1482209123762"/>
</node>
</node>
<node TEXT="lspci" FOLDED="true" ID="ID_1517696962" CREATED="1482209128941" MODIFIED="1482209147633">
<node TEXT="Muestra los controladores del sistema disponibles" ID="ID_731281925" CREATED="1482209180403" MODIFIED="1482209236343"/>
</node>
<node TEXT="lsmod" FOLDED="true" ID="ID_1997066190" CREATED="1482209239371" MODIFIED="1482209251349">
<node TEXT="Lista todos los m&#xf3;dulos del kernel cargados" ID="ID_267523692" CREATED="1482209256240" MODIFIED="1482209307048"/>
<node TEXT="Nuclo monolitico o estatico, es aquel que posee todos los modulos&#xa;ya compilados dentro del mismo." ID="ID_893872006" CREATED="1524370162797" MODIFIED="1524370287414"/>
<node TEXT="N&#xfa;cleo modular, generalmente son n&#xfa;cleos modernos y los modulos no&#xa;estan compilados dentro del mismo. Los drivers de hardware son unicamente car&#xa;gados en caso de necesitarse." ID="ID_1410186242" CREATED="1524370330221" MODIFIED="1524370922628"/>
<node TEXT="Se pueden encontrar en: cd /lib/modules/KERNELVERSION/kernel/drivers" ID="ID_388478919" CREATED="1524370928361" MODIFIED="1524372815171"/>
</node>
<node TEXT="modprobe" FOLDED="true" ID="ID_824928246" CREATED="1482209316539" MODIFIED="1482209329082">
<node TEXT="Los m&#xf3;dulos son cargados con dicho comando" FOLDED="true" ID="ID_213176109" CREATED="1482209329645" MODIFIED="1482209346201">
<node TEXT="Se puede encontrar en la ruta:&#xa;/etc/modprobe.conf" ID="ID_45912352" CREATED="1524373559935" MODIFIED="1524373631028"/>
</node>
</node>
<node TEXT="depmode" FOLDED="true" ID="ID_1288921397" CREATED="1524372821773" MODIFIED="1524372839984">
<node TEXT="Investiga las dependencias de todos los modulos y escribe los resultados en&#xa;/lib/modules/KERNELVERSION/kernel/modules.dep" ID="ID_87807134" CREATED="1524372850216" MODIFIED="1524372979507"/>
</node>
<node TEXT="insmod" FOLDED="true" ID="ID_1317266443" CREATED="1524444517690" MODIFIED="1524444612473">
<node TEXT="Es utiliado para cargar un modulo, para un buen funcionamiento hay que agregarle a insmod la ruta completa del modulo que se desea cargar" FOLDED="true" ID="ID_1357528060" CREATED="1524444612491" MODIFIED="1524444806032">
<node TEXT="insmod /lib/modules/KERNELVERSION/kernel/drivers/bluetoot/bfusb.ko" ID="ID_1886766953" CREATED="1524444809418" MODIFIED="1524444984964"/>
</node>
<node TEXT="Tambi&#xe9;n se &#x1e55;ueden pasar par&#xe1;metros adicionales al modulo" ID="ID_1699079779" CREATED="1524444985966" MODIFIED="1524445251672"/>
</node>
<node TEXT="/etc/modprobe.conf" FOLDED="true" ID="ID_1563765461" CREATED="1524445253570" MODIFIED="1524445324301">
<node TEXT="Define los parametros para los modulos" ID="ID_998768691" CREATED="1524445327847" MODIFIED="1524445492418"/>
<node TEXT="Se pueden usar alias para usar un termino mas com&#xfa;n ejemplo:" FOLDED="true" ID="ID_700579289" CREATED="1524445493341" MODIFIED="1524445525371">
<node TEXT="Alias &quot;NOMBRE DEL ALIAS&quot; &quot;nOMBRE DEL MODULO&quot;&#xa;alis snd-card-0 snd-intel8x0" ID="ID_1774087005" CREATED="1524445525376" MODIFIED="1524445737483"/>
</node>
</node>
</node>
</node>
<node TEXT="101.2Iniciando el sistema" FOLDED="true" POSITION="right" ID="ID_983810203" CREATED="1526272639120" MODIFIED="1574530700979">
<edge COLOR="#808080"/>
<font SIZE="10"/>
<node TEXT="Se pueden contar nueve pasos principalmente para en la inicializaci&#xf3;n del&#xa;sistema." FOLDED="true" ID="ID_1530048571" CREATED="1526272652827" MODIFIED="1526273334936">
<node TEXT="Cuando usted enciende la computadora lo primero que inica es el BIOS&#xa;por sus siglas en ingl&#xe9;s (Basic input and output system)." ID="ID_1425044557" CREATED="1526273334947" MODIFIED="1526273756837"/>
<node TEXT="En seguida se inicia Post (Porwer on sef) en otras palabras se encarga de verificar el auto&#xa;diagnostico al  encender" ID="ID_425784860" CREATED="1526273759181" MODIFIED="1526273963425"/>
<node TEXT="Luego entonces el siguiente en ejecutarse es el MBR (Master Boot Record)" FOLDED="true" ID="ID_1388847714" CREATED="1526874277596" MODIFIED="1526874774774">
<node TEXT="Consta de los primeros 512 bytes en el disco primario o secuendario del sistema" ID="ID_1934022373" CREATED="1526874776364" MODIFIED="1526874846896"/>
<node TEXT="Existe un segundo gestor de arranque  Boot loader por sus siglas en ingl&#xe9;s, denominado&#xa;Lilo o Grub" FOLDED="true" ID="ID_410399554" CREATED="1526874848071" MODIFIED="1526875145054">
<node TEXT="Es capaz de acceder a diferentes particiones y as&#xed; seleccionar&#xa;el sistema operativo deseado." ID="ID_1829592869" CREATED="1526875148950" MODIFIED="1526875217611"/>
</node>
<node TEXT="EL MBR lee el archivo que se encuentra en /boot/grub/menu.lst en los sistemas modernos" FOLDED="true" ID="ID_1796093273" CREATED="1526875292549" MODIFIED="1526875455049">
<node TEXT="Especifica entre otras coasas donde se encuentra el kernel necesario&#xa;para el inicio del sistema." ID="ID_103734324" CREATED="1526875455052" MODIFIED="1526875504586"/>
<node TEXT="Tambi&#xe9;n se pueden pasar a parametros especiales, ejemplo: deshabilitar el soporte para acpi, acpi=off" ID="ID_558361951" CREATED="1527134055605" MODIFIED="1527134180795"/>
<node TEXT="initrd proporciona todos los modulos del n&#xfa;cleo necesarios que el kernel puede cargar de ser necesario," ID="ID_1105388658" CREATED="1534907456969" MODIFIED="1534909790322"/>
<node TEXT="En este caso el disco RAM tambi&#xe9;n llamado initrd entra en funcionamiento. Proporciona todos los modulos necesarios que el kernel puede cargar, esto puede incluir por ejemplo modulos de un sistema de archivos o un controlador raid, si el n&#xfa;cleo est&#xe1; inicializado se inicia el progra init" ID="ID_1157833091" CREATED="1527134216263" MODIFIED="1527134473386"/>
<node TEXT="Init inici ahora los diversos servicios del sistema gnu linux tambi&#xe9;n llamados demonios, es el padre de todos los procesos, el id del proceso es 1 por defecto:" FOLDED="true" ID="ID_132975962" CREATED="1527134474136" MODIFIED="1527134638469">
<node TEXT="ps aux | grep init" ID="ID_1930859348" CREATED="1527134649006" MODIFIED="1527134711503"/>
<node TEXT="De acuerdo con la configuraci&#xf3;n del archivo /etc/inittab init inicia el sistema deacuerdo con el nivel de ejecuci&#xf3;n deseado y se ejecutan todos los scrips necesarios." ID="ID_718699822" CREATED="1527134712264" MODIFIED="1527134877196"/>
</node>
</node>
</node>
</node>
<node TEXT="Archivos logs de arranque." FOLDED="true" ID="ID_1322220659" CREATED="1534910228290" MODIFIED="1534910242710">
<node TEXT="Todos los sistemas Gnu linux registran todos los mensajes del sistema en&#xa;una especie de bitacora, el demonio responsable de esto se llama syslogd" FOLDED="true" ID="ID_850904975" CREATED="1534910248333" MODIFIED="1534910614774">
<node TEXT="Esde demonio wacribe en el archivo /var/log/messages" ID="ID_1504742137" CREATED="1534910680385" MODIFIED="1534910731083"/>
<node TEXT="Se encuentran mensajes del kernel caapturado mediante&#xa;el proceso de arranque. Pero estos mensajes no son completos." ID="ID_1935916004" CREATED="1534910731825" MODIFIED="1534910988736"/>
<node TEXT="Para iniciar syslogd el kernel debe cargarse y luego el sistema de archivos debe ser montado." ID="ID_1345393794" CREATED="1534910990176" MODIFIED="1534913781706"/>
<node TEXT="Con el comando less /var/log/messages se pueden ver problemas relacionados con el kernel," FOLDED="true" ID="ID_1159029099" CREATED="1534913782337" MODIFIED="1534913987602">
<node TEXT="syslogd no puede registrar todos los mensajes del proceso de inicio por qie&#xa;no es guardado antes de la generaci&#xf3;n de los mensajes, por lo tanto el n&#xfa;cleo&#xa;guarda todos los mensajes importantes en la memoria del sistema durante el proceso&#xa;de arranque" ID="ID_798021095" CREATED="1535338547508" MODIFIED="1535431792325"/>
</node>
<node TEXT="El n&#xfa;cleo guarda los mensajes mas importantes en la memoria del sistema durante el proceso de arranque. Para ver esos mensajes se usa el programa dmesg" FOLDED="true" ID="ID_109446624" CREATED="1534913988605" MODIFIED="1534914310549">
<node TEXT="Este a su vez contiene informaci&#xf3;n concerniente a los componentes fundamentales del sistema" ID="ID_1049878246" CREATED="1534914310558" MODIFIED="1534914353752"/>
</node>
</node>
</node>
</node>
<node TEXT="101.3 Cambiar el nivel de ejecuci&#xf3;n y apagar o reiniciar el  sistema." FOLDED="true" POSITION="right" ID="ID_1172057148" CREATED="1545528766656" MODIFIED="1545528979084">
<edge COLOR="#808080"/>
<node TEXT="Niveles de ejecuci&#xf3;n y como pasar de un nivel de ejecuci&#xf3;n a otro." FOLDED="true" ID="ID_270131372" CREATED="1545528979660" MODIFIED="1545529362828">
<node TEXT="El archivo en el que se establece el nivel de ejecuci&#xf3;n actual es&#xa;/etc/inittab" FOLDED="true" ID="ID_1566814582" CREATED="1545529364244" MODIFIED="1545529476769">
<node TEXT="Contiene una entrada que le dice al proceso init que nivel de ejecuci&#xf3;n es el predeterminado ejemplo: id:5:initdefault:" ID="ID_1526461105" CREATED="1545535156704" MODIFIED="1545535255574"/>
<node TEXT="Es configurado entre los valores 1 y 5 a excepci&#xf3;n del 4 que esta reservado para el sistema." ID="ID_17503720" CREATED="1545535256202" MODIFIED="1545535286478"/>
</node>
<node TEXT="Nivel de ejecuci&#xf3;n 0 En este nivel el sistema se apaga sin necesidad de reiniciar; la acci&#xf3;n involucra al programa halt.&#xa;Nivel de ejecuci&#xf3;n 6. El sistema se apaga y se reinicia. Esta acci&#xf3;n involucra al programa reboot.&#xa;Nivel de ejecuci&#xf3;n s (1). El sistema opera en modo de usuario &#xfa;nico (single-user-mode) esta reservada para el administrador del sistema.&#xa;Nivel de ejecuci&#xf3;n 2. Multiusuario sin red.&#xa;Nivel de ejecuci&#xf3;n 3. Multiusuario con red.&#xa;Nivel de ejecuci&#xf3;n 5. Multiusuario con red e inicio de sesi&#xf3;n gr&#xe1;fica.&#xa;Nivel de ejecuci&#xf3;n 4. No es ejecutado." FOLDED="true" ID="ID_592488090" CREATED="1545529565790" MODIFIED="1545534362828">
<node TEXT="La diferencia entre el nivel de ejecuci&#xf3;n 1 y nivel de ejecucion s es que el nivel de ejecuci&#xf3;n 1 necesita del archivo /etc/inittabpara cambiar a un nivel de ejecuci&#xf3;n diferente, el nivel de ejecuci&#xf3;n s no monta ning&#xfa;n sistema de archivos" ID="ID_1804258891" CREATED="1545534611687" MODIFIED="1545535150090"/>
</node>
<node TEXT="Init" FOLDED="true" ID="ID_979062959" CREATED="1545535518592" MODIFIED="1545535523326">
<node TEXT="Para cambiar de un sistema de configuraci&#xf3;n a otro utiliza el programa init, seguido por el  nivel de ejecuci&#xf3;n deseado. ejemp&#x13a;o:# init 6&#xa;Con eso el sistema esta apunto de reinicarse." ID="ID_1694369917" CREATED="1545535524882" MODIFIED="1545535643729"/>
<node TEXT="Tambi&#xe9;n se puede usar el programa telinit de la misma forma que init de hehco telinit es un enlace simb&#xf3;lico del programa init" ID="ID_1914873839" CREATED="1545535651857" MODIFIED="1545536137388"/>
<node TEXT="" ID="ID_261265486" CREATED="1545536343177" MODIFIED="1545536343177"/>
</node>
</node>
<node TEXT="Para apaga o reiniciar  un sistema Gnu linux se pueden usar los comandos:&#xa;halt, poweroff, reboot y shutdown" ID="ID_1836043161" CREATED="1545611001328" MODIFIED="1545611187729"/>
<node TEXT="Se puede apagar o reiniciar el sistema operativo con el promgrama init 0, o init 6, sin embargo no es recomendable hacerlo de esa forma." FOLDED="true" ID="ID_909803789" CREATED="1545611191316" MODIFIED="1545611325145">
<node TEXT="Para apagar un sistema operativo adecuadamente se deben terminar ciertas tareas como por ejemplo: terminaci&#xf3;n de los servicios en curso y los programas de usuario A dem&#xe1;s se deber&#xed;a informar al usuario que el sistema ser&#xe1; apagado." ID="ID_1826136719" CREATED="1545611325152" MODIFIED="1545611507081"/>
<node TEXT="Los programas halt y poweroff matan directamente los programas activos y luego apagan el sistema, tampoco informa a los usuarios que el programa se cerra&#x155;a lo cual puede ser muy incomodo para los mismos." ID="ID_1032409197" CREATED="1545611509303" MODIFIED="1545611924425"/>
<node TEXT="El programa shutdown es el mejor programa para apagar el sistema ya que termina los procesos correctamente y env&#xed;a un mensaje a los usuarios autenticados que se apagar&#xe1; el sistema." ID="ID_1366333921" CREATED="1545611925332" MODIFIED="1545612080360"/>
<node TEXT="Shutdown" FOLDED="true" ID="ID_1948801993" CREATED="1545613196017" MODIFIED="1545613213082">
<node TEXT="La sintaxis es&#xa;#shutdown [opciones] [tiempo]" FOLDED="true" ID="ID_1202215529" CREATED="1545613218966" MODIFIED="1545613777850">
<node TEXT="-r proviene de la opci&#xf3;n reboot o reiniciar, se utiliza para apagar el sistema e iniciarlo nuevamente" ID="ID_1432258036" CREATED="1545613901527" MODIFIED="1545614127698"/>
<node TEXT="-h proviene de la palabra halt en ingl&#xe9;s detener o alto sirve para apagar el sistema." ID="ID_1453964475" CREATED="1545619624780" MODIFIED="1545619658348"/>
<node TEXT="-c proviene de la palabra cancel en ingl&#xe9;s que significa cancelar y el sistema cancela cualquier otra opci&#xf3;n por lo tanto no se apagar&#xe1;." ID="ID_849702244" CREATED="1545619659620" MODIFIED="1545619723678"/>
<node TEXT="-k informa a los usuarios el apagado pr&#xf3;ximo del sistema, pero en realidad no se apagar&#xe1;, sirve al administrador si desea solo detener un servicio en espec&#xed;fico." ID="ID_1107114793" CREATED="1545619724443" MODIFIED="1545619810166"/>
<node TEXT="La opcion&#xf3;n del tiempo puede ser espeficicado como hh:mm, +m,; now donde hh es el n&#xfa;mero de horas y mm el n&#xfa;mero de minutos que tardar&#xe1; en apagar el sistema." ID="ID_1547932535" CREATED="1545620339151" MODIFIED="1545620540885"/>
<node TEXT="La mejor forma &#xe1;ra apagar el sistema es shutdown  -h now; exit o si se desea reiniciar shutdown -r now; exit" ID="ID_961862112" CREATED="1545620548931" MODIFIED="1545620661141"/>
</node>
</node>
<node TEXT="Los scrpts dentro del directorio /etc/init.d" FOLDED="true" ID="ID_142365640" CREATED="1545620816027" MODIFIED="1545620939004">
<node TEXT="Permiten al servidor controlar los distintos procesos del servidor" FOLDED="true" ID="ID_1376939208" CREATED="1545620940909" MODIFIED="1545620959649">
<node TEXT="Start  -inicia el servico asociado&#xa;stop  -detiene el servicio asociado.&#xa;reload  -recarga el archivo de configuraci&#xf3;n sin parar o reiniciar el servicio&#xa;restart  - detiene el servicio y lo reinica nuevamente.&#xa;force-reload  -recarga el archivo de configuraci&#xf3;n o reinicia un servicio: La acci&#xf3;n es especifica en cada servicio.&#xa;status  -devuelve el estado actual del servicio." ID="ID_1253216551" CREATED="1545621411999" MODIFIED="1545623443290"/>
</node>
<node TEXT="En general esos procesos estan acargo  de los demonios dentro del directorio /usr/sbin" ID="ID_616000628" CREATED="1545625537504" MODIFIED="1545625803510"/>
</node>
</node>
</node>
<node TEXT="102.1 Dise&#xf1;o y estructura  de un disco duro" FOLDED="true" POSITION="right" ID="ID_1660604485" CREATED="1546405770300" MODIFIED="1546406104176">
<edge COLOR="#808080"/>
<node TEXT="FHS (Jerarquia standar del sistema de archivos)." FOLDED="true" ID="ID_1061150649" CREATED="1546406111545" MODIFIED="1546406254500">
<node TEXT="Particiones.- Practica de dividir el espacio de almacenamiento    en &#xe1;rea de datos independiente." ID="ID_755841861" CREATED="1546406299346" MODIFIED="1546406477541"/>
<node TEXT="Con ayuda de software especificos un disco duro se puede dividir en varias partes llamadas particiones" FOLDED="true" ID="ID_1196023965" CREATED="1546406478484" MODIFIED="1546406568161">
<node TEXT="Son independientes entre si y pueden ser tratadas Por los diferentes sistemnas operativos como unidades f&#xed;sicas." ID="ID_1385050985" CREATED="1546406568169" MODIFIED="1546406770177"/>
<node TEXT="Ventajas de las particiones:&#xa;-El contenido del sistema de archivos, puede ser istribuido entre varios discos f&#xed;sicos.&#xa;-Los Backups o copias de respaldo son m&#xe1;s f&#xe1;ciles de programar.&#xa;- Insolaci&#xf3;n de los sistemas de archivos por normas de serguridad.&#xa;-A diferencia de otros sistemas Gnu linux no funciona con letras de unidad pero si con &#xe1;rbol de directoriios." ID="ID_1827794493" CREATED="1546406776104" MODIFIED="1546485534168"/>
<node TEXT="Tipos de particiones" FOLDED="true" ID="ID_639867586" CREATED="1546485535099" MODIFIED="1546485546992">
<node TEXT="Primaria: Son las divisiones m&#xe1;s elementales. Solo puede haber 4 de estas en un disco duro MBR, aunque si necesitamos hacer m&#xe1;s divisiones podemos recurrir a las particiones extendidas." ID="ID_1121493682" CREATED="1546485548240" MODIFIED="1546486295494"/>
<node TEXT="Extendida: Conocidas como &#x201c;particiones secundarias&#x201d;, cuenta como una partici&#xf3;n primaria, pero, en lugar de utilizar su espacio para almacenar directamente los datos se utiliza para almacenar dentro otras particiones adicionales." ID="ID_1365349527" CREATED="1546486299546" MODIFIED="1546486402400"/>
<node TEXT="L&#xf3;gica: Divisiones que se sit&#xfa;an dentro de una partici&#xf3;n extendida. Podemos crear un m&#xe1;ximo de 32 dentro de cada una de estas." ID="ID_1277114265" CREATED="1546486403014" MODIFIED="1546486443868"/>
</node>
<node TEXT="Ejemplo de un disco particionado." FOLDED="true" ID="ID_1009176278" CREATED="1546490867648" MODIFIED="1546490888901">
<node TEXT="/boot  512 Mb&#xa;/         30 gb&#xa;swap  4gb&#xa;/home indefinido" ID="ID_110170086" CREATED="1546490889973" MODIFIED="1546491076286"/>
</node>
</node>
<node TEXT="Como su nombre lo indica, mantiene una jerarqu&#xed;a en forma de &#xe1;rbol, siendo / la punta o la principal" FOLDED="true" ID="ID_232417188" CREATED="1546494208468" MODIFIED="1546494533009">
<node TEXT="/  .- Ra&#xed;z o root, con tiene todo el sistema de jerarqu&#xed;a o mejor dicho todos los dem&#xe1;s ficheros o directorios." FOLDED="true" ID="ID_152748206" CREATED="1546494539255" MODIFIED="1546494672343">
<node TEXT="/bin .- Contiene los programas  y comandos b&#xe1;sicos del sistema." ID="ID_535545050" CREATED="1546494672350" MODIFIED="1546495205546"/>
<node TEXT="/dev .- Contiene los portales o dispositivos esenciales. ejemplo una placa arduino a trav&#xe9;s de un puerto usb cuando es conectada." ID="ID_1700366162" CREATED="1546495206045" MODIFIED="1546496462732"/>
<node TEXT="/etc .- Contiene archivos de configuraci&#xf3;n del sistema." ID="ID_1147504097" CREATED="1546495312656" MODIFIED="1546495339139"/>
<node TEXT="/lib .- contiene las bibliotecas compartidas de los programas alojados." ID="ID_925049164" CREATED="1546495339726" MODIFIED="1546495591479"/>
<node TEXT="/sbin .- Almacena comandos y programas exclusivos del superusuario." ID="ID_1558188916" CREATED="1546495378480" MODIFIED="1546495717727"/>
<node TEXT="/root .- Es el directorio home del usuario root." ID="ID_1665844614" CREATED="1546495719045" MODIFIED="1546495773009"/>
<node TEXT="/usr .- Contiene la mayoria de la utilidades y aplicaciones multiusuario." ID="ID_1168547771" CREATED="1546495774171" MODIFIED="1546496245792"/>
<node TEXT="/var .- En el se encuentran archivos variables tales como logs." ID="ID_740789474" CREATED="1546496246352" MODIFIED="1546496279276"/>
<node TEXT="/home .- Contiene los directorios de trabajo de los usuarios ejemplo, Documentos M&#xfa;sica v&#xed;deos etc" ID="ID_1044295450" CREATED="1546496279507" MODIFIED="1546496372956"/>
<node TEXT="/media .- Contiene los puntos de montaje de los medios extraibles ejemplo usb, unidad de cd externa" ID="ID_1739164943" CREATED="1546496373359" MODIFIED="1546496439626"/>
<node TEXT="/mnt .- Sistema de archivos montados temporales." ID="ID_249160536" CREATED="1546496464623" MODIFIED="1546496506990"/>
<node TEXT="/opt .- Contiene paquetes de programas opcionales de aplicaciones est&#xe1;ticas." ID="ID_1160735940" CREATED="1546496507586" MODIFIED="1546496621033"/>
<node TEXT="/srv .- Lugar especifico de datos que son servidos por el sistema." ID="ID_1454206053" CREATED="1546496559649" MODIFIED="1546496613859"/>
<node TEXT="/tmp .- Archivos temporales." ID="ID_298010287" CREATED="1546496621739" MODIFIED="1546496656727"/>
<node TEXT="/proc .- Sistema de archivos virtuales que documentan al n&#xfa;cleo." ID="ID_1608383833" CREATED="1546496657103" MODIFIED="1546496688501"/>
<node TEXT="/boot .- Archivos cargadores de arranque." ID="ID_798435572" CREATED="1546496688944" MODIFIED="1546497878395"/>
</node>
</node>
</node>
</node>
<node TEXT="101.3 Cambiar el nivel de ejecuci&#xf3;n o apagar y reiniciar el sistema parte 1" FOLDED="true" POSITION="right" ID="ID_1692612477" CREATED="1551577910481" MODIFIED="1551740104904">
<edge COLOR="#808080"/>
<node TEXT="Niveles de ejecuci&#xf3;n y como pasar de un nivel de ejecuci&#xf3;n a otro" FOLDED="true" ID="ID_1353368919" CREATED="1551577981264" MODIFIED="1551578116090">
<node TEXT="&#xbf;Que es?&#xa;Es el modo de operaci&#xf3;n de los sistemas operativos." ID="ID_769248972" CREATED="1551578118782" MODIFIED="1551579724138"/>
<node TEXT="Los servivios estan disponibles en ciertos niveles y en otros no." ID="ID_1605039121" CREATED="1551579728175" MODIFIED="1551650032920"/>
<node TEXT="El archivo en el que se establece el nivel actual del sistema es:&#xa;/etc/inittab" ID="ID_595297632" CREATED="1551650033472" MODIFIED="1551650267743"/>
<node TEXT="Existen 6 niveles de ejecici&#xf3;n tambi&#xe9;n se les conoce como&#xa;Runlevel" FOLDED="true" ID="ID_134814539" CREATED="1551650268914" MODIFIED="1551650621062">
<node TEXT="Runlevel 0.- El sistema se apaga sin necesidad de reiniciar,&#xa;Involucra al programa halt." ID="ID_948183189" CREATED="1551650285008" MODIFIED="1551650863014"/>
<node TEXT="Runlevel 6.- El sistema apaga y reinicia, esta acci&#xf3;n involucra al&#xa;programa reboot." ID="ID_286069302" CREATED="1551650674460" MODIFIED="1551650933333"/>
<node TEXT="Runlevel s (1). El sistema opera en modo de usuario &#xfa;nico (single-user-mode)&#xa;es una modalidad que esta reservada para el administrador del sistema." ID="ID_1526108568" CREATED="1551650934071" MODIFIED="1551651089893"/>
<node TEXT="Runlevel 2.- Multiusuario sin red" ID="ID_917997021" CREATED="1551651092110" MODIFIED="1551651135873"/>
<node TEXT="Runlevel 3.- Multiusuario con red." ID="ID_260238058" CREATED="1551651136303" MODIFIED="1551651158211"/>
<node TEXT="Runlevel 5.- Mutiusuario con red e inicio de sesi&#xf3;n gr&#xe1;fica" ID="ID_1322761527" CREATED="1551651174152" MODIFIED="1551652697685"/>
<node TEXT="Runlevel 4.- No es utilizado." ID="ID_993178642" CREATED="1551652698803" MODIFIED="1551652717350"/>
</node>
<node TEXT="La diferencia entre el nivel de ejecuci&#xf3;n 1 y el nivel de ejecuci&#xf3;n s" FOLDED="true" ID="ID_900626053" CREATED="1551652865836" MODIFIED="1551652891569">
<node TEXT="Runlevel 1 necesita el uso del archivo /etc/inittab&#xa;para cambiar a un nivel de ejecuci&#xf3;n diferente." ID="ID_1613100487" CREATED="1551652898283" MODIFIED="1551652977484"/>
<node TEXT="El nivel de ejecuci&#xf3;n s no necesita de este archivo, si el archivo /etc/inittab&#xa;es da&#xf1;ado o borrado, a&#xfa;n se puede utilizar el comando init s para ingresar al&#xa;sistema en modo de uuario &#xfa;nico." ID="ID_867901800" CREATED="1551652978104" MODIFIED="1551653129082"/>
<node TEXT="El runlevel 1 es conocido como modo de administracion y&#xa;el runlevel s es conocido como modo de reparacion ya que el runlevel s no&#xa;muestra ning&#xfa;n sistema de archivos." ID="ID_631069805" CREATED="1551653155271" MODIFIED="1551653356314"/>
</node>
<node TEXT="El archivo /etc/initab" FOLDED="true" ID="ID_398344591" CREATED="1551653401338" MODIFIED="1551653417326">
<node TEXT="Contiene una entrada que le dice al proceso init&#xa;que runlevel es el predeterminado." ID="ID_846119362" CREATED="1551653421346" MODIFIED="1551654054519"/>
<node TEXT="Para configurar el runlevel predeterminado en el&#xa;archivo /etc/inittab se utiliza el parametro initdefault&#xa;ejemplo:" FOLDED="true" ID="ID_1116159008" CREATED="1551654093851" MODIFIED="1551654155080">
<node TEXT="id:5:initdefault:" ID="ID_1321383048" CREATED="1551654158263" MODIFIED="1551654207356"/>
</node>
<node TEXT="El nivel de ejecuci&#xf3;n predetermminado es configurado generalmente&#xa;entre los valores uno y 5 a excepci&#xf3;n del 4 que est&#xe1; reservado para el sistema." ID="ID_1872754595" CREATED="1551654316127" MODIFIED="1551654439234"/>
</node>
<node TEXT="Init" FOLDED="true" ID="ID_416845314" CREATED="1551654630768" MODIFIED="1551654635808">
<node TEXT="Para cambiar maualmente a un nivel de ejecuci&#xf3;n el adminiatrador&#xa;utiliza el prpgrama init seguido runlever deseado.&#xa;Ejemplo:" FOLDED="true" ID="ID_789439076" CREATED="1551654639295" MODIFIED="1551654761311">
<node TEXT="#init 6" ID="ID_62564876" CREATED="1551654763116" MODIFIED="1551654772498"/>
</node>
</node>
<node TEXT="telinit funciona exactamente de la misma forma que el comando init, b&#xe1;sicamente&#xa;es un enlace simb&#xf3;lico al al comando init." ID="ID_433840152" CREATED="1551654831923" MODIFIED="1551654921581"/>
</node>
</node>
<node TEXT="101.3 Cambiar el nivel de ejecuci&#xf3;n o apagar y reiniciar el sistema parte 2" FOLDED="true" POSITION="right" ID="ID_971771156" CREATED="1551739990245" MODIFIED="1551740117235">
<edge COLOR="#808080"/>
<node TEXT="Para apagar el sistema se pueden usar los comandos # Init 0&#xa;En el caso de reiniciar #init 6 pero no es lo mas adecuado." ID="ID_1855958694" CREATED="1551740141930" MODIFIED="1551740301841"/>
<node TEXT="Para apagar un sistema, varias tareas deben llevarse a cabo previamente.&#xa;Ejemplo:" FOLDED="true" ID="ID_506947051" CREATED="1551755064503" MODIFIED="1551760472734">
<node TEXT="Terminaci&#xf3;n de los  servicios en curso.&#xa;Programas de usuario." ID="ID_616296078" CREATED="1551760478439" MODIFIED="1551760595300"/>
<node TEXT="Informar al usuario de la sesi&#xf3;n en curso que el sistema&#xa;ser&#xe1; apagado." ID="ID_549401654" CREATED="1551760681279" MODIFIED="1551760704950"/>
</node>
<node TEXT="Los programas:" FOLDED="true" ID="ID_1183270424" CREATED="1551760798794" MODIFIED="1551760853829">
<node TEXT="Halt" FOLDED="true" ID="ID_995398423" CREATED="1551760856421" MODIFIED="1551760862509">
<node TEXT="Mata los procesos activos y luego apagan el sistema." ID="ID_344399781" CREATED="1551844277783" MODIFIED="1551844297593"/>
<node TEXT="Apaga el sistema inmediatamente." ID="ID_175940181" CREATED="1551845445141" MODIFIED="1551845458228"/>
<node TEXT="No informa a los usuarios que el sistema se apagar&#xe1;." ID="ID_512859581" CREATED="1551845671472" MODIFIED="1551845689571"/>
</node>
<node TEXT="Poweroff" FOLDED="true" ID="ID_1480147392" CREATED="1551760863079" MODIFIED="1551773891746">
<node TEXT="Mata los procesos activos y luego apaga el sistema." ID="ID_1765567292" CREATED="1551844304213" MODIFIED="1551844319489"/>
<node TEXT="Se puede programar el apagado." ID="ID_1700155069" CREATED="1551845467623" MODIFIED="1551845556310"/>
<node TEXT="Apaga el sistema inmediatamente." ID="ID_654549636" CREATED="1551845650061" MODIFIED="1551845662717"/>
<node TEXT="No informa a los usuarios que el sistema se apagar&#xe1;." ID="ID_1908999277" CREATED="1551845560646" MODIFIED="1551845581518"/>
</node>
<node TEXT="Shutdown" FOLDED="true" ID="ID_47568606" CREATED="1551848020526" MODIFIED="1551848034310">
<node TEXT="El mejor programa para apagar o reinicar un sistema." ID="ID_436436163" CREATED="1551848034312" MODIFIED="1551848395041"/>
<node TEXT="Termina  todos los procesos correctamente." ID="ID_322273341" CREATED="1551848396102" MODIFIED="1551848453390"/>
<node TEXT="Ofrece la opci&#xf3;n de enviar un mensaje a todos los usuarios autenticados&#xa;Y les informa que el sistema se apagar&#xe1;." ID="ID_1918393571" CREATED="1551848455288" MODIFIED="1551850326742"/>
<node TEXT="Sintaxis del comando shutdown" FOLDED="true" ID="ID_1032797740" CREATED="1551850328375" MODIFIED="1551850542469">
<node TEXT="shutdown opciones tiempo" FOLDED="true" ID="ID_641948043" CREATED="1551850545273" MODIFIED="1551850706191">
<node TEXT="# shutdown -h now; exit" FOLDED="true" ID="ID_92101841" CREATED="1551850707465" MODIFIED="1551850756630">
<node TEXT="Para apagar el sistema." ID="ID_1650704299" CREATED="1551850757197" MODIFIED="1551850768371"/>
</node>
<node TEXT="# shutdown -r now; exit" FOLDED="true" ID="ID_121140908" CREATED="1551850770374" MODIFIED="1551850789215">
<node TEXT="Para reiniciar el sistema." ID="ID_1401400504" CREATED="1551850792588" MODIFIED="1551850804117"/>
</node>
</node>
<node TEXT="Opciones del comando shutdown" FOLDED="true" ID="ID_1275906473" CREATED="1551850883372" MODIFIED="1551850895939">
<node TEXT="-r .- Con esta opci&#xf3;n se apaga el sistema y se vuelve a iniciar nuevamente&#xa;corresponde al nivel de ejecuci&#xf3;n 6." FOLDED="true" ID="ID_247261789" CREATED="1551850897139" MODIFIED="1551851288705">
<node TEXT="Proviene de reboot en ingl&#xe9;s, reiniciar" ID="ID_1290912437" CREATED="1551851688010" MODIFIED="1551851718155"/>
</node>
<node TEXT="-h .- Con esta opci&#xf3;n se apaga el sistema sin reiniciar&#xa;corresponde al nivel de ejecuci&#xf3;n 0" FOLDED="true" ID="ID_1234745429" CREATED="1551851291817" MODIFIED="1551851642225">
<node TEXT="Proviene de halt en ingl&#xe9;s, detener" ID="ID_1732192777" CREATED="1551851722991" MODIFIED="1551851757115"/>
</node>
<node TEXT="-c Con esta opci&#xf3;n el apagado del sistema ser&#xe1; interrumpido." FOLDED="true" ID="ID_1943487787" CREATED="1551851643255" MODIFIED="1551851811510">
<node TEXT="Proviene de Cancel en ingl&#xe9;s, cancelar" ID="ID_1616194192" CREATED="1551851811516" MODIFIED="1551851923882"/>
</node>
<node TEXT="-k .-Informa a todos los usuarios sobre el apagado del sistema, pero en realidad no se apagr&#xe1;! Esta opci&#xf3;n es &#xfa;til si el administrador quiere reiniciar o detener un servicio del sistema." ID="ID_654978978" CREATED="1551851932554" MODIFIED="1551852061962"/>
<node TEXT="La opcion del tiempo se puede especificar de varias formas" FOLDED="true" ID="ID_1539877968" CREATED="1551852817428" MODIFIED="1551853075712">
<node TEXT="hh:mm; +m; now" FOLDED="true" ID="ID_1988517838" CREATED="1551852860220" MODIFIED="1551852923705">
<node TEXT="Donde hh son las horas y mm son los minutos" ID="ID_1025730764" CREATED="1551852868986" MODIFIED="1551852902721"/>
<node TEXT="+m donde m es el n&#xfa;mero de minutos que se especificar&#xe1;n&#xa;antes de apagar el sistema." ID="ID_1918279744" CREATED="1551852925692" MODIFIED="1551852972242"/>
<node TEXT="now en combinacion con el comando shutdown se interpreta como&#xa;m + 0" ID="ID_394641412" CREATED="1551852974047" MODIFIED="1551853184480"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Los scripts dentro del directorio /etc/init.d" FOLDED="true" ID="ID_609791411" CREATED="1551937644214" MODIFIED="1551937698452">
<node TEXT="Permiten al administrador controlar los distintos procesos&#xa;del servidor" ID="ID_1312456844" CREATED="1551937727680" MODIFIED="1551937745905"/>
<node TEXT="Opciones de control" FOLDED="true" ID="ID_920750908" CREATED="1551937787713" MODIFIED="1551937799655">
<node TEXT="start .- Inicia el servicio asociado." ID="ID_680979860" CREATED="1551937800459" MODIFIED="1551937831203"/>
<node TEXT="stop .- Detiene el servicio asociado." ID="ID_88878108" CREATED="1551937831964" MODIFIED="1551937859864"/>
<node TEXT="reload .- Recarga el archivo de configuraci&#xf3;  asociado sin parar o reinicar." ID="ID_1815489964" CREATED="1551937860160" MODIFIED="1551938066411"/>
<node TEXT="restart .- Detiene el servicio y lo reinicia nevamente." ID="ID_933921247" CREATED="1551938015776" MODIFIED="1551938061593"/>
<node TEXT="force-reload .- Recarga el archivo de configuraci&#xf3;n o reinicia un servicio. la&#xa;acci&#xf3;n es especifica en cada servicio." ID="ID_1531596191" CREATED="1551938068128" MODIFIED="1551938192591"/>
<node TEXT="status .- Devuelve el estado actual del servicio." ID="ID_657772598" CREATED="1551938194296" MODIFIED="1551938464822"/>
</node>
</node>
<node TEXT="Un servicio puede ser reiniciado op detenido de la siguiente&#xa;forma, ejemplo:" FOLDED="true" ID="ID_151661591" CREATED="1551938469489" MODIFIED="1551938814625">
<node TEXT="# /etc/init.d/ntpd status" ID="ID_1966816915" CREATED="1551938497414" MODIFIED="1551938753612"/>
<node TEXT="# /etc/init.d/ntpd restart" ID="ID_672497576" CREATED="1551938760204" MODIFIED="1551938793606"/>
</node>
</node>
<node TEXT="102.1 Dise&#xf1;o y estructura de un disco duro parte 1" FOLDED="true" POSITION="right" ID="ID_1127134929" CREATED="1551754893917" MODIFIED="1554140721470">
<edge COLOR="#808080"/>
<node TEXT="FIlesystem Hierarchy Standar (FHS)" FOLDED="true" ID="ID_531332310" CREATED="1554140721513" MODIFIED="1554141748709">
<node TEXT="Particiones" FOLDED="true" ID="ID_555223314" CREATED="1554141751211" MODIFIED="1554141757358">
<node TEXT="Es el arte de dividir el espacio de almacenamiento del disco duro en &#xe1;rea&#xa;de datos independientes conocidas como particiones." FOLDED="true" ID="ID_1914974281" CREATED="1554141758499" MODIFIED="1554141866611">
<node TEXT="Con ayuda de software especificos, un disco duro se puede dividir en&#xa;varias &#xe1;rtes que luego se llaman particiones." ID="ID_506313443" CREATED="1554141886024" MODIFIED="1554141945933"/>
</node>
<node TEXT="Las particiones son diferentes entre si y pueden ser identificadas como unidades fisicas." ID="ID_1722840538" CREATED="1554141867737" MODIFIED="1554142330328"/>
<node TEXT="Ventajas" FOLDED="true" ID="ID_1940583050" CREATED="1554142330680" MODIFIED="1554142334246">
<node TEXT="El contenido del sistema de archivos puede ser distribuido entre varios discos f&#xed;sicos" ID="ID_1094274581" CREATED="1554142335052" MODIFIED="1554142406544"/>
<node TEXT="Los Backups (copias de respaldo son m&#xe1;s f&#xe1;ciles de programar)" ID="ID_1810270173" CREATED="1554142407273" MODIFIED="1554142458976"/>
<node TEXT="A diferencia de otros sistemas Gnu linux funciona no con letras de unidad, pero si con un &#xe1;rbol de directorios. Todas las particiones en uso se montan a este &#xe1;rbol de directorios." ID="ID_1274604962" CREATED="1554142459569" MODIFIED="1554143080263"/>
<node TEXT="FHS, el estandar de jerarqu&#xed;a del sistema de archivos define los directorios y sus contenidos en los sistemas gnu linux" FOLDED="true" ID="ID_221744940" CREATED="1554143081104" MODIFIED="1554143206307">
<node TEXT="Mediante la estandarizaci&#xf3;n del sistema de archivos, los usuarios pueden buscar&#xa;Archivos o directorios en cualquier sistema gnu linux con mayor f&#xe1;cilidad" ID="ID_1361230356" CREATED="1554143209320" MODIFIED="1554143300074"/>
</node>
</node>
<node TEXT="Root / &#xf3; ra&#xed;z" FOLDED="true" ID="ID_50973075" CREATED="1554143305427" MODIFIED="1554233277934">
<node TEXT="Contiene todo el sistema de archivos Gnu linux" FOLDED="true" ID="ID_1127297888" CREATED="1554143328644" MODIFIED="1554233277931">
<node TEXT="/bin" FOLDED="true" ID="ID_1017503672" CREATED="1554143383727" MODIFIED="1554143390204">
<node TEXT="Contiene los programas b&#xe1;sicos del sistema que son necesarios para iniciarlo." FOLDED="true" ID="ID_231137125" CREATED="1554143394830" MODIFIED="1554143614984">
<node TEXT="Ejecutables, comandos etc, ejemplo: mount, cp." ID="ID_751104587" CREATED="1554145705237" MODIFIED="1554145815567"/>
</node>
</node>
<node TEXT="/dev" FOLDED="true" ID="ID_1510375791" CREATED="1554145289474" MODIFIED="1554145302082">
<node TEXT="COntiene los archivos de los dispositivos, es decir el hardware conectado se representa en modo archivo ubicados en el directorio /dev" FOLDED="true" ID="ID_598508246" CREATED="1554145305429" MODIFIED="1554146068075">
<node TEXT="Ejemplo: cuando conectamos un dispositivo externo usb, ya sea impresora&#xa;celular etc." ID="ID_1847447378" CREATED="1554146071706" MODIFIED="1554146144626"/>
</node>
</node>
<node TEXT="/boot" FOLDED="true" ID="ID_1230127542" CREATED="1554146151491" MODIFIED="1554147656279">
<node TEXT="Ficheros de configuraci&#xf3;n del arranque, n&#xfa;cleos y otros ficheros necesarios para el arranque (boot) del equipo." ID="ID_1300728430" CREATED="1554147658088" MODIFIED="1554147681245"/>
</node>
<node TEXT="/etc" FOLDED="true" ID="ID_1203373997" CREATED="1554147687725" MODIFIED="1554177359575">
<node TEXT="Contiene informaci&#xf3;n de donde estan motadas las particiones y a dem&#xe1;s  ficheros de configuraci&#xf3;n del sistema, scripts de arranque etc." ID="ID_194590824" CREATED="1554177359966" MODIFIED="1554177451996"/>
</node>
<node TEXT="/lib" FOLDED="true" ID="ID_407075933" CREATED="1554177768510" MODIFIED="1554177831534">
<node TEXT="Contiene las librer&#xed;as del sistema.- Incluye las bibliotecas esenciales  que son necesarios para que se puedan ejecutar correctamente todos los binarios que se encuentran en los directorios /bin y /sbin, as&#xed; como los m&#xf3;dulos del propio kernel." ID="ID_1439106436" CREATED="1554177832245" MODIFIED="1554180999940"/>
</node>
<node TEXT="/sbin" FOLDED="true" ID="ID_1907552200" CREATED="1554180411746" MODIFIED="1554180920736">
<node TEXT="El directorio /bin es un directorio est&#xe1;tico y es donde se almacenan todos los binarios necesarios para garantizar las funciones b&#xe1;sicas a nivel de usuario. Solo almacena los ejecutables de usuario, ya que los binarios necesarios para tareas administrativas gestionadas por el usuario root o superusuario del sistema se encuentran en el directorio /sbin." ID="ID_18573191" CREATED="1554180921499" MODIFIED="1554180926381"/>
</node>
<node TEXT="/root" FOLDED="true" ID="ID_1975213806" CREATED="1554181023229" MODIFIED="1554181087154">
<node TEXT="No confundir con / el contenedor de todos los sistemas de archivos. /root es el directorio home del super usuario." ID="ID_175640832" CREATED="1554181088424" MODIFIED="1554181370195"/>
</node>
<node TEXT="/usr" FOLDED="true" ID="ID_1239285944" CREATED="1554181411647" MODIFIED="1554181883483">
<node TEXT="Recursos del sistema linux por sus siglas en ingl&#xe9;s, todos los datos est&#xe1;ticos que no participan en el proceso de arranque se almacenan en este directorio. Contiene archivos de programas y aplicaciones instaladas." ID="ID_1046882441" CREATED="1554181883487" MODIFIED="1554228662872"/>
</node>
<node TEXT="/var" FOLDED="true" ID="ID_1545609986" CREATED="1554228689523" MODIFIED="1554228710487">
<node TEXT="Los archivos variables cuyo funcionamiento se espera que cambie durante el funcionamiento del sistema tales como los archivos logs, archivos teporales de correo electr&#xf3;nico por mencionar algunos se almacenan en dicho directorio." ID="ID_647674701" CREATED="1554228711109" MODIFIED="1554228870560"/>
</node>
<node TEXT="/home" FOLDED="true" ID="ID_1130669033" CREATED="1554228872089" MODIFIED="1554228875303">
<node TEXT="En este directorio se guardan los archivos personales de los usuarios, as&#xed; como configuraciones  personales." ID="ID_1709400067" CREATED="1554228877162" MODIFIED="1554229053365"/>
</node>
<node TEXT="/media" FOLDED="true" ID="ID_798263183" CREATED="1554229054703" MODIFIED="1554229340858">
<node TEXT="Se encuentran los puntos de montaje de medios removibles tales como cdroom y anteriormente como floppy" ID="ID_665477235" CREATED="1554229340862" MODIFIED="1554229404370"/>
</node>
<node TEXT="/mnt" FOLDED="true" ID="ID_935674916" CREATED="1554229421952" MODIFIED="1554229586573">
<node TEXT="" FOLDED="true" ID="ID_659679893" CREATED="1554229586578" MODIFIED="1554229586578">
<node TEXT="Es utilizado para montar sistemas de archivos temporalmente. En otras palabras es un sistema de archivos montado temporalmente en el disco duro." ID="ID_331640341" CREATED="1554229589620" MODIFIED="1554229742340"/>
</node>
</node>
<node TEXT="/opt" FOLDED="true" ID="ID_1393320581" CREATED="1554229744841" MODIFIED="1554229791077">
<node TEXT="En este directorio se instala todo el software adicional (software de terceros)." ID="ID_1042880138" CREATED="1554229791079" MODIFIED="1554230863290"/>
</node>
<node TEXT="/srv" FOLDED="true" ID="ID_985632116" CREATED="1554230868645" MODIFIED="1554231079486">
<node TEXT="Puede contener archivos que se sirven a otros sistemas como por ejemplo, archivos relacionados con servidores web FTP etc." ID="ID_465863763" CREATED="1554231079491" MODIFIED="1554231266689"/>
</node>
<node TEXT="/tmp" FOLDED="true" ID="ID_1205578608" CREATED="1554231268668" MODIFIED="1554231289183">
<node TEXT="Ofrece a los usuarios guardar sus archivos temporales." ID="ID_1014170788" CREATED="1554231289189" MODIFIED="1554231355804"/>
</node>
<node TEXT="/proc" FOLDED="true" ID="ID_1570069375" CREATED="1554231357730" MODIFIED="1554231362855">
<node TEXT="Es un sistema de archivos virtual del kernel, directorio din&#xe1;mico especial que mantiene informaci&#xf3;n sobre el estado del sistema, incluyendo los procesos actualmente en ejecuci&#xf3;n." ID="ID_45332080" CREATED="1554231444015" MODIFIED="1554231534592"/>
</node>
<node TEXT="/boot" FOLDED="true" ID="ID_947337123" CREATED="1554231562050" MODIFIED="1554231568422">
<node TEXT="Contiene todos los archivos necesarios para iniciar el sistema." ID="ID_108036106" CREATED="1554231568428" MODIFIED="1554232025841"/>
</node>
<node TEXT="/sys" FOLDED="true" ID="ID_747793118" CREATED="1554232030000" MODIFIED="1554232037452">
<node TEXT="Archivos virtuales con informaci&#xf3;n de eventos del sistema." ID="ID_895283148" CREATED="1554232037459" MODIFIED="1554232112150"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="102.1 Dise&#xf1;o y estructura de un disco duro parte 2" FOLDED="true" POSITION="right" ID="ID_1673651274" CREATED="1555008395084" MODIFIED="1555008461453">
<edge COLOR="#808080"/>
<node TEXT="El tipo de particionado de nuestro disco duro va a depender del uso que se le dar&#xe1; a nuesro sistema operativo, es decir, si solo es para uso domestico, o un servidor de datos etc, tambi&#xe9;n se tiene que tomar encuenta los recursos del hardware." ID="ID_394798509" CREATED="1555008463374" MODIFIED="1555008794725"/>
<node TEXT="Raiz o /" FOLDED="true" ID="ID_442257191" CREATED="1555008797941" MODIFIED="1555008810270">
<node TEXT="Nivel superior del directorio, aqu&#xed; se encuentra en sistema de archivos linux en forma de un &#xe1;rbol de directorios. Por lo general con 20 GB de espacio asignados es lo ideal, claro depender&#xe1; del espacio que tenga el disco duro." ID="ID_1554008293" CREATED="1555008815647" MODIFIED="1555009488412"/>
</node>
<node TEXT="/boot" FOLDED="true" ID="ID_1023259399" CREATED="1555009223459" MODIFIED="1555009230315">
<node TEXT="Por lo general simpre se coloca esta partici&#xf3;n por separado, el espacio asignado ideal ser&#xe1; de 512 MB a 1GB." ID="ID_1929970455" CREATED="1555009239727" MODIFIED="1555009563533"/>
</node>
<node TEXT="/swap" FOLDED="true" ID="ID_1244795775" CREATED="1555009581322" MODIFIED="1555009788005">
<node TEXT="Area de intercambio sirve para Ayudar a la memoria RAM en caso de que esta este muy saturada, en lugar de ocupar espacio en la memoria RAM esta ocupa espacio directo del disco duro, se le asigna por lo regular el doble de la memoria RAM." ID="ID_1269437132" CREATED="1555009789792" MODIFIED="1555010270613"/>
</node>
<node TEXT="File server" FOLDED="true" ID="ID_831304663" CREATED="1555010271357" MODIFIED="1555010771361">
<node TEXT="Quiere decir servidor de archivos y es donde se almacenaran los ficheros  personales o de trabajo deacuerdo al fin del S.O. En realidad es un NFS Network File System o servidor de archivos en la red por sus siglas en ingl&#xe9;s. En el cual se pueden instalar servicios como samba, el cual sirve para intercambiar archivos con otros sistemas operativos. FTP Protocolo de transferencias de archivos, sirve para transferir archivos  en la red." ID="ID_686878218" CREATED="1555010772362" MODIFIED="1555011119769"/>
<node TEXT="/home" FOLDED="true" ID="ID_1449911056" CREATED="1555011135914" MODIFIED="1555011140820">
<node TEXT="Es por lo general la particion donde el usuario almacena los ficheros y directorios personales en caso de ser un sistema de escritorio." ID="ID_1418713695" CREATED="1555011144613" MODIFIED="1555011189916"/>
</node>
</node>
<node TEXT="Servidor web" FOLDED="true" ID="ID_1955118718" CREATED="1555011616808" MODIFIED="1555011632346">
<node TEXT="Contiene datos de registros tambien llamados logs, estos se pueden ir acumulando cada d&#xed;a y al mismo tiempo saturan el espacio designado a dicha partici&#xf3;n, por tal motivo se recomienda usar otro disco para dicha partici&#xf3;n, o simplemente una partici&#xf3;n separada al resto de los archivos." ID="ID_1156993414" CREATED="1555011636328" MODIFIED="1555011869343"/>
<node TEXT="/var" FOLDED="true" ID="ID_565731605" CREATED="1555011870086" MODIFIED="1555011874088">
<node TEXT="Esta particion es seleccionada para almacenar los logs y datos variables, es seleccionada para servidor web." ID="ID_1656012029" CREATED="1555011875029" MODIFIED="1555012072913"/>
</node>
</node>
<node TEXT="Logical block Addressing (LBA)" FOLDED="true" ID="ID_1711172808" CREATED="1555012226841" MODIFIED="1555012419596">
<node TEXT="Direccionamiento de bloques l&#xf3;gico por sus siglas en ingl&#xe9;s.- Un disco o discos est&#xe1; dividido en bloques. Cada bloque con una &#xfa;nica direcci&#xf3;n. Sin embargo, hay diferentes m&#xe9;todos de direccionamiento. La Direcci&#xf3;n L&#xf3;gica no tiene que coincidir necesariamente con la Direcci&#xf3;n F&#xed;sica. A los bloques tambi&#xe9;n a veces se les llama &apos;cuadros&apos; (frame) o &apos;sectores&apos; (sectors)." ID="ID_1242050436" CREATED="1555012265561" MODIFIED="1555012801976"/>
<node TEXT="Con la llegada e los discos duros mayores a 8 Gb la divisi&#xf3;n cilindroo cabeza sector se volvi&#xf3; obsoleto sin embargo esta organizaci&#xf3;n es utilizada en muchas herramientas dee particionamiento." ID="ID_1867156647" CREATED="1555012803333" MODIFIED="1555013020304"/>
<node TEXT="Partici&#xf3;n primaria" FOLDED="true" ID="ID_1537631292" CREATED="1555013029759" MODIFIED="1555013045542">
<node TEXT=" Son las divisiones crudas o primarias del disco, solo puede haber 4 de &#xe9;stas o 3 primarias y una extendida. Depende de una tabla de particiones. Un disco f&#xed;sico completamente formateado consiste, en realidad, de una partici&#xf3;n primaria que ocupa todo el espacio del disco y posee un sistema de archivos. Ejemplo /, /boot, /swap." ID="ID_1788345401" CREATED="1555013047569" MODIFIED="1555013618384"/>
</node>
<node TEXT="Partici&#xf3;n extendida" FOLDED="true" ID="ID_1915171173" CREATED="1555013651397" MODIFIED="1555013673613">
<node TEXT="Tambi&#xe9;n conocida como partici&#xf3;n secundaria es otro tipo de partici&#xf3;n que act&#xfa;a como una partici&#xf3;n primaria; sirve para contener m&#xfa;ltiples unidades l&#xf3;gicas en su interior. Fue ideada para romper la limitaci&#xf3;n de 4 particiones primarias en un solo disco f&#xed;sico. Solo puede existir una partici&#xf3;n de este tipo por disco, y solo sirve para contener particiones l&#xf3;gicas. Por lo tanto, es el &#xfa;nico tipo de partici&#xf3;n que no soporta un sistema de archivos directamente." ID="ID_899286182" CREATED="1555013675607" MODIFIED="1555013766626"/>
</node>
<node TEXT="Partici&#xf3;n l&#xf3;gica" FOLDED="true" ID="ID_223670903" CREATED="1555013768057" MODIFIED="1555013783311">
<node TEXT="Ocupa una porci&#xf3;n de la partici&#xf3;n extendida o la totalidad de la misma, la cual se ha formateado con un tipo espec&#xed;fico de sistema de archivos (FAT32, NTFS, ext2,...) y se le ha asignado una unidad, as&#xed; el sistema operativo reconoce las particiones l&#xf3;gicas o su sistema de archivos. Puede haber un m&#xe1;ximo de 32 particiones l&#xf3;gicas en una partici&#xf3;n extendida. Linux impone un m&#xe1;ximo de 15, incluyendo las 4 primarias, en discos SCSI y en discos IDE 8963." ID="ID_841126723" CREATED="1555013784230" MODIFIED="1555013835506"/>
</node>
</node>
</node>
<node TEXT="102.2 instalaci&#xf3;n y gestor de arranque boot loader parte 1" FOLDED="true" POSITION="right" ID="ID_1728315239" CREATED="1561065315772" MODIFIED="1561065401761">
<edge COLOR="#808080"/>
<node TEXT="&#xbf;Que es un gestor de arranque?" FOLDED="true" ID="ID_1076963780" CREATED="1561065818494" MODIFIED="1561065836041">
<node TEXT="Sistema sencillo que esta dedicado unicamente al inicio&#xa;de un sistema operativo." ID="ID_1906408654" CREATED="1561065841117" MODIFIED="1561065869984"/>
<node TEXT="Existen dos gestores de arranque com&#xfa;n mente asosiados con las&#xa;distribuciones gnu/linux" FOLDED="true" ID="ID_695818334" CREATED="1561065403290" MODIFIED="1561065669335">
<node TEXT="Grub" FOLDED="true" ID="ID_674206980" CREATED="1561065677264" MODIFIED="1561065684835">
<node TEXT="Grand Unified Boot Loader" ID="ID_1104916431" CREATED="1561065685778" MODIFIED="1561065735918"/>
<node TEXT="En caso de que no este instalado use&#xa;#grub-install" ID="ID_1772859510" CREATED="1561066180645" MODIFIED="1561066221423"/>
<node TEXT="Tambi&#xe9;n puede checar el manual con :&#xa;#man grub-install" ID="ID_134491281" CREATED="1561066223588" MODIFIED="1561066263319"/>
<node TEXT="Con el comando fdisk -l uds puede ver los dispositivos de almacenamiento de su Disco Duro: # fdisk -l" FOLDED="true" ID="ID_295086093" CREATED="1561066523523" MODIFIED="1561066596675">
<node TEXT="Disco /dev/sda: 596.2 GiB, 640135028736 bytes, 1250263728 sectores&#xa;Disk model: Hitachi HTS54756&#xa;Unidades: sectores de 1 * 512 = 512 bytes" ID="ID_1299004" CREATED="1561066599193" MODIFIED="1561066822836"/>
<node TEXT="/dev/sda es considerado el dispositivo desde donde nuestro sistema iniciar&#xe1;" ID="ID_576509665" CREATED="1561066823649" MODIFIED="1561066848021"/>
</node>
<node TEXT="Configuraci&#xf3;n" FOLDED="true" ID="ID_619376559" CREATED="1561067099059" MODIFIED="1561067118420">
<node TEXT="Ingrese al archivo: /boot/grub/menu.lst" FOLDED="true" ID="ID_405043904" CREATED="1561067121568" MODIFIED="1561067150668">
<node TEXT="Es dividido en varias secciones que se pueden cofigurar para los diferentes&#xa;kernels o n&#xfa;cleos." ID="ID_871119267" CREATED="1561067163452" MODIFIED="1561067227075"/>
</node>
<node TEXT="Ejemplo: Colocar una imagen en el grub" FOLDED="true" ID="ID_894947987" CREATED="1564103924471" MODIFIED="1564103950880">
<node TEXT="1.- Valla al directorio /etc/default: #cd  /etc/default" ID="ID_1643956045" CREATED="1564103951903" MODIFIED="1564104200987"/>
<node TEXT="2.- haga una copia de seguridad del archivo grub en caso de que al momento de editar se borre algo accidentalmente:# cp grub grub.copia." ID="ID_1174452622" CREATED="1564104204470" MODIFIED="1564104384379"/>
<node TEXT="3.- Modifique el archivo con el editor de textos favorito en mi caso lo hare con vim: #vim grub" ID="ID_698995801" CREATED="1564104392466" MODIFIED="1564104469874"/>
<node FOLDED="true" ID="ID_1694948719" CREATED="1564104472132" MODIFIED="1564104553057"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      4.- Al final del archivo agregue las siguientes lineas de texto:
    </p>
    <a http-equiv="content-type" content="text/html; charset=utf-8" class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/results?search_query=%23Fondo">#Fondo</a>&#xa0;de pantalla para el grub GRUB_BACKGROUND="/directorio_de_la_imagen/fondo.png"

    <p>
      &#xa0;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Nota: las imagenes deben estar en formato png o tga unicamente." ID="ID_1169769725" CREATED="1564104555080" MODIFIED="1564104587697"/>
</node>
<node TEXT="5.- Actualice el grub con el siguiente comando: # update-grub" ID="ID_204082898" CREATED="1564104591539" MODIFIED="1564104641816"/>
<node TEXT="6.- Reinicie su computadora." ID="ID_1741132888" CREATED="1564104647729" MODIFIED="1564104662827"/>
</node>
</node>
</node>
<node TEXT="Lilo" FOLDED="true" ID="ID_912349775" CREATED="1561065902482" MODIFIED="1561065911653">
<node TEXT="Linux loader" ID="ID_151304227" CREATED="1561065911661" MODIFIED="1561065916645"/>
<node TEXT="Fichero de configuraci&#xf3;n /etc/lilo.conf" ID="ID_1653971491" CREATED="1561065920887" MODIFIED="1564201723354"/>
<node TEXT="Interactuar con lilo" FOLDED="true" ID="ID_482014891" CREATED="1564203277092" MODIFIED="1564203305175">
<node TEXT="Presione la tecla tab durante los primeros 5 segundos de haber inicado el sistema" FOLDED="true" ID="ID_193524472" CREATED="1564203305232" MODIFIED="1564203407664">
<node TEXT="La imagen del kernel mas el nivel de ejecuci&#xf3;n deseado" ID="ID_1616847507" CREATED="1564203414262" MODIFIED="1564203497706"/>
<node TEXT="" ID="ID_1024872565" CREATED="1564203449852" MODIFIED="1564203449852"/>
</node>
</node>
</node>
</node>
<node TEXT="Recide en los primeros 512 bytes de un Disco duro y es necesario para iniciar el sistema" FOLDED="true" ID="ID_144278540" CREATED="1561065998383" MODIFIED="1561066032402">
<node TEXT="Este sector se llama Master boot record (MBR)" ID="ID_1205105477" CREATED="1561066050681" MODIFIED="1561066068463"/>
</node>
<node TEXT="Sup&#xe9;r block" FOLDED="true" ID="ID_945274125" CREATED="1564282000321" MODIFIED="1564282009818">
<node TEXT="Todo sistema de archivos contiene un superblock" FOLDED="true" ID="ID_292313368" CREATED="1564282013415" MODIFIED="1564282249250">
<node TEXT="Contiene informaci&#xf3;n acerca del sistema de archivos como:&#xa;-Que tipo de sistema de archivo es.&#xa;-Tama&#xf1;o.&#xa;-Informaci&#xf3;n sobre otras estructuras de metadatos." ID="ID_143618521" CREATED="1564282251224" MODIFIED="1564286717549"/>
</node>
<node TEXT="Los sistemas Gnu linux mantienen copias redundantes de superblock" FOLDED="true" ID="ID_1901709554" CREATED="1564286721063" MODIFIED="1564286851794">
<node TEXT="Para encontrar la copia del superblock utilice el siguiente comando como super usuario" FOLDED="true" ID="ID_1982494037" CREATED="1564286852244" MODIFIED="1564286911700">
<node TEXT="#dumpe2fs /dev/sda1 | grep -i superblock" ID="ID_206020412" CREATED="1564286914683" MODIFIED="1564286991003"/>
</node>
<node TEXT="Para restaurar el superblock utilice el siguiente comando" FOLDED="true" ID="ID_1772756975" CREATED="1564287057833" MODIFIED="1564287124522">
<node TEXT="#e2fsck -b 8139 /dev/sda1" ID="ID_816290588" CREATED="1564287126945" MODIFIED="1564287209605"/>
<node TEXT="Nota: el parametro -b junto con el n&#xfa;mero 8139 significa que el programa toma el superblock en la posici&#xf3;n 8139 para la restauraci&#xf3;n." ID="ID_802371243" CREATED="1564287209913" MODIFIED="1564287383193"/>
</node>
</node>
</node>
<node TEXT="Copia de seguridad y restauraci&#xf3;n del MBR" FOLDED="true" ID="ID_1468497262" CREATED="1564287396289" MODIFIED="1564287429804">
<node TEXT="Al instalar otro gestor de arranque, el MBR seria destruido accidentalmente." FOLDED="true" ID="ID_1657713773" CREATED="1564287429810" MODIFIED="1564287459884">
<node TEXT="El siguiente comando crea una copia de seguridad del primer disco" FOLDED="true" ID="ID_1366986240" CREATED="1564287462522" MODIFIED="1564287500654">
<node TEXT="#dd if=/dev/sda of=backup.mbr bs=512 count=1&#xa;#ls -ltr" ID="ID_1870078297" CREATED="1564287502353" MODIFIED="1564287731421"/>
</node>
<node TEXT="El siguiente comando restaura la copia de seguridad del mbr" FOLDED="true" ID="ID_935044157" CREATED="1564287553767" MODIFIED="1564287753068">
<node TEXT="#dd if=backup.mbr  of=/dev/sda bs=512" ID="ID_1772693309" CREATED="1564287756228" MODIFIED="1564287812406"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="102.3 Librer&#xed;as compartidas" FOLDED="true" POSITION="right" ID="ID_509884292" CREATED="1565483911052" MODIFIED="1565483932754">
<edge COLOR="#808080"/>
<node TEXT="&#xbf;Que es una libreria?" FOLDED="true" ID="ID_91031439" CREATED="1565483936612" MODIFIED="1565485518832">
<node TEXT="Archivos que contienen c&#xf3;digo o scripts, necesarios para que otros&#xa;programas puedan funcionar correctamente." FOLDED="true" ID="ID_1302848657" CREATED="1565485520185" MODIFIED="1565485586050">
<node TEXT="Puede recibir el nombre de API (Application Programmable Interface) y est&#xe1; disponible&#xa;para  que cualquier programa lo utilice de ser necesario." ID="ID_1855750366" CREATED="1565485927943" MODIFIED="1565486005881"/>
<node TEXT="Varios programas pueden acceder a una librer&#xed;a compartda al mismo tiempo." ID="ID_250105821" CREATED="1565486347666" MODIFIED="1565486375727"/>
<node TEXT="Las librer&#xed;as compartidas aparecen en forma de archivos .so API (Application Programmable Interface)" ID="ID_802829711" CREATED="1565487321738" MODIFIED="1565487389963"/>
</node>
</node>
<node TEXT="Se pueden encontrar en los siguientes directorios" FOLDED="true" ID="ID_596108689" CREATED="1565487419419" MODIFIED="1565487457205">
<node TEXT="/lib: librer&#xed;as esenciales para el funcionamiento del sistema." ID="ID_250362119" CREATED="1565487457212" MODIFIED="1565487872278"/>
<node TEXT="/lib: librer&#xed;as esenciales para el funcionamiento del sistema." ID="ID_1273767703" CREATED="1565487889925" MODIFIED="1565487959979"/>
<node TEXT="Sin embargo, en distribuciones modernas que utilizan systemd /lib y /lib64 son en realidad enlaces simb&#xf3;licos a /usr/lib y /usr/lib64, respectivamente. En estos directorios tambi&#xe9;n encontramos las librer&#xed;as de programas de usuario. Si bien son importantes, estas &#xfa;ltimas no son cr&#xed;ticas para la operaci&#xf3;n del sistema." ID="ID_1201505467" CREATED="1565487965934" MODIFIED="1565487969895"/>
</node>
<node TEXT="&#xbf;Como averiguar cu&#xe1;les son las librer&#xed;as de las que depende un programa determinado?" FOLDED="true" ID="ID_1012559948" CREATED="1565488048016" MODIFIED="1565488120619">
<node TEXT="Se usar&#xe1; el comando     ldd donde nos muestra las librer&#xed;as de las cuales de&#x1e55;ende el programa top" FOLDED="true" ID="ID_1207827471" CREATED="1565488120626" MODIFIED="1565488507670">
<node TEXT="#ldd /usr/bin/top" ID="ID_343685732" CREATED="1565488510469" MODIFIED="1565488663079"/>
<node ID="ID_734885458" CREATED="1565488664435" MODIFIED="1565488673168"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    A la derecha, entre par&#xe9;ntesis tambi&#xe9;n podemos apreciar la direcci&#xf3;n de memoria en la que est&#xe1; cargada cada librer&#xed;a. La primer l&#xed;nea (linux-<strong http-equiv="content-type" content="text/html; charset=utf-8">vdso</strong>.so.1) no indica ninguna ubicaci&#xf3;n en disco debido a que se trata de una librer&#xed;a residente en el kernel. Se la conoce con el nombre de <em><strong>v</strong>irtual <strong>d</strong>ynamic <strong>s</strong>hared <strong>o</strong>bject</em>.
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="ld-linux.so" FOLDED="true" ID="ID_964415395" CREATED="1565490129186" MODIFIED="1565490180024">
<node TEXT="Es un ejecutable por si mismo" FOLDED="true" ID="ID_1437940305" CREATED="1565490180973" MODIFIED="1565490189014">
<node TEXT="Es el c&#xf3;digo responsable de cargar de forma din&#xe1;mica" ID="ID_1669047595" CREATED="1565490190334" MODIFIED="1565490251936"/>
<node TEXT="Determina que librer&#xed;as son reuqeridas y cuales necesitan ser cargadas." ID="ID_1297737187" CREATED="1565490252324" MODIFIED="1565490309346"/>
<node TEXT="Realiza la vinculaci&#xf3;n din&#xe1;mica para areglar todos los punteros de direccionamiento dentro del ejecutable y las librer&#xed;as cargadas para que el programa se ejecute correctamente." ID="ID_776984879" CREATED="1565490310664" MODIFIED="1565490507903"/>
</node>
</node>
<node TEXT="ldconfig" FOLDED="true" ID="ID_975602725" CREATED="1565490509782" MODIFIED="1565490516654">
<node TEXT="Se asegura     que el archivo del cach&#xe9; de las librer&#xed;as se encuentre actualizado" ID="ID_1761482358" CREATED="1565490564415" MODIFIED="1565490642087"/>
<node TEXT="#ldconfig -p" ID="ID_185998827" CREATED="1565490644206" MODIFIED="1565490712759"/>
<node TEXT="Configuraci&#xf3;n de las librer&#xed;as" FOLDED="true" ID="ID_864983463" CREATED="1565490713140" MODIFIED="1565491034937">
<node TEXT="ldconfig busca los directorios basados en el archivo de configuraci&#xf3;n /etc/ld.so.conf" FOLDED="true" ID="ID_1599055226" CREATED="1565491038283" MODIFIED="1565491141218">
<node TEXT="El proposito de los archivos de este directorio es redireccionar al&#xa;sistema a los archivos de librer&#xed;as en los directorios nuevo estandar." ID="ID_340988828" CREATED="1565491180102" MODIFIED="1565491373227"/>
<node TEXT="#less /etc/ld.so.conf" ID="ID_265622561" CREATED="1565491373818" MODIFIED="1565491512107"/>
</node>
</node>
</node>
<node TEXT="LD_LIBRARY_PATH" FOLDED="true" ID="ID_1440643839" CREATED="1565491515999" MODIFIED="1565491564257">
<node TEXT="Si se ejecuta una aplicaci&#xf3;n antigua que necesita una versi&#xf3;n antigua de una libreria compartida, puede que quiera reemplazar las rutas de busqueda por defecto utilizadas por el cargador de librer&#xed;as" ID="ID_208951752" CREATED="1565491564263" MODIFIED="1565491753615"/>
<node TEXT="Se puede establecer una ruta de una libreria  con:" FOLDED="true" ID="ID_1217942675" CREATED="1565491754275" MODIFIED="1565491775700">
<node TEXT="echo $LD_LIBRARY_PATH" ID="ID_1356543290" CREATED="1565491777221" MODIFIED="1565491829505"/>
</node>
</node>
</node>
<node TEXT="102.4 Sistema de gestion de paquetes Debian" FOLDED="true" POSITION="right" ID="ID_1097779356" CREATED="1566023287226" MODIFIED="1566051204169">
<edge COLOR="#808080"/>
<node TEXT="El Comando dpkg" FOLDED="true" ID="ID_488624040" CREATED="1566051206854" MODIFIED="1566051333026">
<node TEXT="Este comando se puede utilizar para instalar, eliminar o purgar los paquetes de Debian&#xa;posee las extenciones.deb" ID="ID_123729591" CREATED="1566051333036" MODIFIED="1566051879404"/>
<node TEXT="-Lista todos los archivos de un paquete instalado.&#xa;-Lista los detalles de un paquete instalado ." ID="ID_309240028" CREATED="1566052114624" MODIFIED="1566052217117"/>
<node TEXT="No es capaz de gestionar las dependec&#xed;as de un paquete." FOLDED="true" ID="ID_1278646131" CREATED="1566051916119" MODIFIED="1566052113666">
<node TEXT="La aplicaciones dependen de las librer&#xed;as de los programas,&#xa;los paquetes son amenudo dependientes de otros paquetes que tienen que ser&#xa;instalados de forma simultanea durante su instalaci&#xf3;n, estos paquetes se llaman dependencias y no se pueden administrar con el comando dpkg." ID="ID_349549240" CREATED="1566052227535" MODIFIED="1566052377861"/>
</node>
<node TEXT="Opciones m&#xe1;s comunes del comado dpkg" FOLDED="true" ID="ID_1717238063" CREATED="1566052722791" MODIFIED="1566052737754">
<node TEXT="--i  --install Instala el paquete especificado&#xa;-l   --list Lista los paquetes instalados si; si el nombre del paquete es incluido&#xa;        El comando muestra el estado de instalaci&#xf3;n del paquete mencionado.&#xa; -L  --listfile lista los archivos de los paquetes espeficicados.&#xa;-p  --print-avail  Incluye los detalles del paquete espeficicado.&#xa;-P  --purge Desinstala el paquete y remueve todos los archivos de configuraci&#xf3;n asociados   al mismo.&#xa;-r  --remove Desinstala el paquete sin remover los archivos de configuraci&#xf3;n asociados al mism.&#xa;-S   --search Indentifica el archivo asociado con el archivo especifico." ID="ID_881149146" CREATED="1566052737763" MODIFIED="1566053305357">
<font SIZE="9"/>
</node>
</node>
<node TEXT="Instalar" FOLDED="true" ID="ID_294845940" CREATED="1566601279247" MODIFIED="1566601294537">
<node TEXT="#dpkg -i nombre-archivo.deb" ID="ID_1069982163" CREATED="1566601294554" MODIFIED="1566601315567"/>
</node>
<node TEXT="Ejemplos:" FOLDED="true" ID="ID_179794101" CREATED="1566053350449" MODIFIED="1566053356307">
<node TEXT="#man dpkg&#xa;#cd /var/cache/apt/archives/&#xa;#/var/cache/apt/archives/#ls -la | grep vinagre" ID="ID_714882743" CREATED="1566053356316" MODIFIED="1566053733401"/>
</node>
<node TEXT="dpkg-reconfigure" FOLDED="true" ID="ID_826724851" CREATED="1566053892289" MODIFIED="1566601087773">
<node TEXT="Permite reconfigurar un programa o paquete instalado, en caso de que se haya olvidado&#xa;alguna opci&#xf3;n en el momento de la configuraci&#xf3;n." ID="ID_968915256" CREATED="1566053908310" MODIFIED="1566601867807"/>
<node TEXT="Ejemplo: #dpkg-reconfigure dma" ID="ID_639818238" CREATED="1566601889089" MODIFIED="1566601908722"/>
</node>
<node TEXT="dpkg" FOLDED="true" ID="ID_367717330" CREATED="1568139630198" MODIFIED="1568139644169">
<node TEXT="Definici&#xf3;n" FOLDED="true" ID="ID_1326768046" CREATED="1568139698282" MODIFIED="1568139889482">
<node TEXT="Es una abreviaci&#xf3;n de Debian pakage por sus siglas en ingl&#xe9;s (paquetes de debian)" ID="ID_1602501098" CREATED="1568139889487" MODIFIED="1568139996247"/>
<node TEXT="        Se trata de un equipamiento utilizado para la gesti&#xf3;n de la paqueter&#xed;a de debian" FOLDED="true" ID="ID_1465568441" CREATED="1568140000532" MODIFIED="1568140147131">
<node TEXT="Nota: Un paquete se traduce como un software empaquetado para su instalaci&#xf3;n, es decir.&#xa;conjunto de ficheros relacionados con una aplicaci&#xf3;n los cuales contienen:&#xa;Objetos ejecutables, archivos de configuraci&#xf3;n, informaci&#xf3;n acerca del uso e instalaci&#xf3;n de la aplicaci&#xf3;n.&#xa;Todos ellos agrupados en un mismo contenedor." ID="ID_441286067" CREATED="1568140149054" MODIFIED="1568141490004"/>
</node>
<node TEXT="Modo de intalaci&#xf3;n" FOLDED="true" ID="ID_599560720" CREATED="1568141702893" MODIFIED="1568141718266">
<node TEXT="Se instala con el par&#xe1;metro -i" FOLDED="true" ID="ID_128417917" CREATED="1568141718273" MODIFIED="1568141741105">
<node TEXT="Ejemplo:&#xa;#dpkg -i (Nombre del paquete).deb" FOLDED="true" ID="ID_319573566" CREATED="1568141743047" MODIFIED="1568142000136">
<node TEXT="Par&#xe1;metros y opciones." FOLDED="true" ID="ID_1730222511" CREATED="1568141838783" MODIFIED="1568142382508">
<node ID="ID_1168427888" CREATED="1568142382511" MODIFIED="1568142476108"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    1- dpkg<br http-equiv="content-type" content="text/html; charset=utf-8"/><br/>El comando dpkg permite la instalaci&#xf3;n, la actualizaci&#xf3;n y la supresi&#xf3;n de los programas. Se utiliza en los S.O. Deribados de Debian y trabaja sobre los archivos con la extensi&#xf3;n .deb.<br/>En el directorio /var/lib/dpkg se encuentra la base de datos. NO debe modificar el archivo. En /var/lib/dpkg/status esta la lista de paquetes conocidos por dpkg.<br/>2- Instalaci&#xf3;n, actualizaci&#xf3;n y supresi&#xf3;n.<br/>dpkg -i o dpkg -install [nombre_del_archivo.deb] As&#xed; se puede instalar un paquete o los paquetes descriptos.<br/>dpkg -R directorio Permite instalar recursiva todos los archivos .deb del directorio nombrado<br/>No existe opci&#xf3;n del comando dpkg para forzar la actualizaci&#xf3;n o instalaci&#xf3;n del paquete.<br/>La supresi&#xf3;n de un paquete se lleva acabo con la opci&#xf3;n -r. Esta opci&#xf3;n elimina todo menos los archivos de configuraci&#xf3;n. Para suprimir todo incluso estos archivos se debe usar la opci&#xf3;n -P.<br/>La opciones -a o --pending elimina todos los datos pendientes de borrado.<br/>La opciones --force-all y --purge en conjunto permiten la desinstalaci&#xf3;n forzosa de paquetes.<br/>dpkg --force-all --purge nombre_del_paquete<br/>3- Listar paquetes<br/>El par&#xe1;metro -l permite listar los paquetes conocidos por dpkg.<br/>El par&#xe1;metro -S seguido del nombre del fichero permite encontrar el paquete de origen.<br/>dpkg -S /usr/bin/basename<br/>4- Convertir paquetes<br/>Algunos programas solo vienen para ciertas distros espec&#xed;ficamente. Para poder usar estos programas en nuestro S.O. aunque su distribuci&#xf3;n oficial no sea la misma que nuestro S.O. podemos utilizar el comando alien.<br/>Alien permite convertir paquetes de RPM a DPKG y viceversa.<br/>El par&#xe1;metro -d convertiremos de rpm a dpkg:<br/>alien -d lgtoclnt-7.4-1.i686.rpm<br/>Debemos agregar al comando anterior el par&#xe1;metro --scripts para que agrege al nuevo archivo los scripts.<br/>alien --scripts -d lgtoclnt-7.4-1.i686.rpm
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Comando apt-get" FOLDED="true" ID="ID_259443832" CREATED="1566603429779" MODIFIED="1568142518722">
<node TEXT="Gestiona los paquetes instalables disponibles en los repositorios" ID="ID_1505976616" CREATED="1566603451794" MODIFIED="1566603532916"/>
<node TEXT="Sint&#xe1;xis" FOLDED="true" ID="ID_136752823" CREATED="1566603680644" MODIFIED="1568142518721">
<node TEXT="#apt-get (opciones) (Nombre del programa a instalar)" FOLDED="true" ID="ID_499537408" CREATED="1566603693862" MODIFIED="1566603732897">
<node TEXT="Ejemplo:" FOLDED="true" ID="ID_38429009" CREATED="1566603732902" MODIFIED="1566603748971">
<node TEXT="#apt-get update" FOLDED="true" ID="ID_831437754" CREATED="1566603748979" MODIFIED="1566603762353">
<node TEXT="Actualiza el lsitado de paquetes dsiponibles." ID="ID_1180036279" CREATED="1566603762358" MODIFIED="1566603803676"/>
</node>
<node TEXT="#apt-get check" FOLDED="true" ID="ID_1603557285" CREATED="1566603805524" MODIFIED="1566603902587">
<node TEXT="Comprueba que todo est&#xe9; bien tras haber usado apt-get update." ID="ID_1842823459" CREATED="1566603848199" MODIFIED="1566603878584"/>
</node>
<node TEXT="#apt-get install (nombre del paquete a instalar)" FOLDED="true" ID="ID_1279295147" CREATED="1566603880125" MODIFIED="1566604087274">
<node TEXT="Instala un programa o paquete" ID="ID_399769064" CREATED="1566604087692" MODIFIED="1566604104165"/>
</node>
<node TEXT="#apt-get reinstal (paquete)" FOLDED="true" ID="ID_1846744057" CREATED="1566604108622" MODIFIED="1567632035004">
<node TEXT="Reinstala un programa." ID="ID_1887393566" CREATED="1566604142260" MODIFIED="1566604152535"/>
</node>
<node TEXT="#apt-get upgrade" FOLDED="true" ID="ID_603964216" CREATED="1566604154191" MODIFIED="1566604325335">
<node TEXT="Actualiza solo los paquetes ya instalados que no necesitan , como dependencia, la instalaci&#xf3;n o desinstalaci&#xf3;n de otros paquetes" ID="ID_1563686765" CREATED="1566604187068" MODIFIED="1566604263491"/>
</node>
<node FOLDED="true" ID="ID_811530939" CREATED="1566604264766" MODIFIED="1566604343538"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#apt-get remove --purge paquete</pre>
  </body>
</html>
</richcontent>
<node TEXT="Desinstalar un paquete y elimina los archivos de configuraci&#xf3;n" ID="ID_765033301" CREATED="1566604344634" MODIFIED="1566604368148"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Comando aptitude" FOLDED="true" ID="ID_1858510663" CREATED="1566604920299" MODIFIED="1566604931062">
<node ID="ID_1335414163" CREATED="1566604931072" MODIFIED="1566604950087"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <strong http-equiv="content-type" content="text/html; charset=utf-8">Aptitude</strong>&#xa0;es un comando dentro de <strong>Debian</strong>&#xa0;el cual nos permite realizar <em>actualizaciones, instalaciones o desinstalaciones</em>&#xa0;de cualquier programa o componente dentro de nuestra distribuci&#xf3;n.
  </body>
</html>
</richcontent>
</node>
<node TEXT="Diferencia con apt-get" FOLDED="true" ID="ID_1961485423" CREATED="1566604952464" MODIFIED="1566604964834">
<node TEXT="aptitude recuerda las dependencias de la instalaci&#xf3;n, a la hora de desinstalar un programa borrara tambi&#xe9;n sus dependencias, excepto las usadas por otros paquetes. En cambio apt-get a la hora de desinstalar solo eliminara el paquete, dejando intactas todas sus dependencias." ID="ID_1430815236" CREATED="1566604964847" MODIFIED="1566604980892"/>
</node>
<node TEXT="Sint&#xe1;xis" FOLDED="true" ID="ID_371441149" CREATED="1566605005119" MODIFIED="1566605014360">
<node TEXT="#aptitude (opciones) (Nombre del paquete)" FOLDED="true" ID="ID_1883351408" CREATED="1566605014365" MODIFIED="1566605085640">
<node TEXT="Acciones" FOLDED="true" ID="ID_1136362081" CREATED="1567735600568" MODIFIED="1567735618094">
<node TEXT="Ejemplo:" FOLDED="true" ID="ID_180209475" CREATED="1566605085647" MODIFIED="1566605090794">
<node FOLDED="true" ID="ID_93998774" CREATED="1566605090801" MODIFIED="1566605120148"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <em http-equiv="content-type" content="text/html; charset=utf-8">#aptitude update</em>
  </body>
</html>
</richcontent>
<node TEXT="Actualiza la base de datos." ID="ID_920574500" CREATED="1566605121965" MODIFIED="1566605129405"/>
</node>
<node FOLDED="true" ID="ID_355433765" CREATED="1566605131821" MODIFIED="1566605165986"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <em http-equiv="content-type" content="text/html; charset=utf-8">#aptitude install nombre_del_paquete</em>
  </body>
</html>
</richcontent>
<node TEXT="Instala un paquete y sus dependencias." ID="ID_1110340020" CREATED="1566605165996" MODIFIED="1566605189185"/>
</node>
<node FOLDED="true" ID="ID_1246450506" CREATED="1566605191988" MODIFIED="1566605218185"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <em http-equiv="content-type" content="text/html; charset=utf-8">#aptitude upgrade</em>
  </body>
</html>
</richcontent>
<node TEXT="actualiza cualquier software que tenga candidatos para actualizar" ID="ID_1365908290" CREATED="1566605218198" MODIFIED="1566605252895"/>
</node>
<node FOLDED="true" ID="ID_1295583946" CREATED="1566605256037" MODIFIED="1566605303698"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <em http-equiv="content-type" content="text/html; charset=utf-8">#aptitude dist-upgrade</em>

    <p>
      
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Instala nuevas versiones de paquete y desinstala lo que no sea necesario." ID="ID_1299239283" CREATED="1566605315159" MODIFIED="1566605345606"/>
</node>
</node>
</node>
<node TEXT="Opciones" FOLDED="true" ID="ID_26463014" CREATED="1567735620016" MODIFIED="1567735926112">
<node FOLDED="true" ID="ID_88686221" CREATED="1567735640509" MODIFIED="1567735796292"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <strong http-equiv="content-type" content="text/html; charset=utf-8">Significado de los caracteres</strong>
  </body>
</html>
</richcontent>
<node FOLDED="true" ID="ID_1180699676" CREATED="1567735814178" MODIFIED="1567736099192"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    El <strong http-equiv="content-type" content="text/html; charset=utf-8">primer car&#xe1;cter</strong>&#xa0;de cada l&#xed;nea indica el estado actual del paquete:
  </body>
</html>
</richcontent>
<node ID="ID_872850573" CREATED="1567736099203" MODIFIED="1567736131861"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" class="especial">
      <span class="objeto-special">i</span>&#xa0;el paquete est&#xe1; instalado y todas sus dependencias est&#xe1;n satisfechas.
    </p>
    <p class="especial">
      <span class="objeto-special">c</span>&#xa0;el paquete fue eliminado, pero sus archivos de configuraci&#xf3;n todav&#xed;a est&#xe1;n presentes.
    </p>
    <p class="especial">
      <span class="objeto-special">p</span>&#xa0;el paquete y todos sus archivos de configuraci&#xf3;n se eliminaron, o nunca fue instalado.
    </p>
    <p class="especial">
      <span class="objeto-special">v</span>&#xa0;el paquete es virtual.
    </p>
    <p class="especial">
      <span class="objeto-special">B</span>&#xa0;el paquete esta roto dependencias.
    </p>
    <p class="especial">
      <span class="objeto-special">u</span>&#xa0;el paquete ha sido desempaquetado, pero no se ha configurado.
    </p>
    <p class="especial">
      <span class="objeto-special">C</span>&#xa0;a medio configurar- La configuraci&#xf3;n fall&#xf3; y requiere ser reparada.
    </p>
    <p class="especial">
      <span class="objeto-special">H</span>&#xa0;a medio configurar- Fall&#xf3; la eliminaci&#xf3;n y requiere ser reparada.
    </p>
    <p class="parrafoo">
      El <strong>segundo car&#xe1;cter</strong>&#xa0;indica la acci&#xf3;n (si los hubiere), que se realiz&#xf3; sobre el paquete:
    </p>
    <p class="especial">
      <span class="objeto-special">i</span>&#xa0;el paquete ser&#xe1; instalado.
    </p>
    <p class="especial">
      <span class="objeto-special">u</span>&#xa0;el paquete ser&#xe1; actualizado.
    </p>
    <p class="especial">
      <span class="objeto-special">d</span>&#xa0;el paquete ser&#xe1; eliminado, pero sus archivos de configuraci&#xf3;n se mantendr&#xe1;n.
    </p>
    <p class="especial">
      <span class="objeto-special">p</span>&#xa0;el paquete ser&#xe1; purgado: el y sus archivos de configuraci&#xf3;n ser&#xe1;n removidos.
    </p>
    <p class="especial">
      <span class="objeto-special">h</span>&#xa0;el paquete ser&#xe1; mantenido: se mantendr&#xe1; su estado actual, aunque exista una versi&#xf3;n m&#xe1;s nueva.
    </p>
    <p class="especial">
      <span class="objeto-special">F</span>&#xa0;una actualizaci&#xf3;n del paquete ha sido prohibido.
    </p>
    <p class="especial">
      <span class="objeto-special">r</span>&#xa0;el paquete ser&#xe1; reinstalado.
    </p>
    <p class="especial">
      <span class="objeto-special">B</span>&#xa0;el paquete ser&#xe1; "roto": algunas de sus dependencias no ser&#xe1;n satisfechas. En aptitude no le permitir&#xe1; instalar, eliminar o actualizar mientras que usted tiene los paquetes rotos.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Las opciones mas com&#xfa;nes son" FOLDED="true" ID="ID_1499478442" CREATED="1567735914296" MODIFIED="1567735946270" HGAP_QUANTITY="30.499999508261695 pt" VSHIFT_QUANTITY="17.99999946355821 pt">
<node ID="ID_1921399635" CREATED="1567735962755" MODIFIED="1567735989286"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <span http-equiv="content-type" content="text/html; charset=utf-8" class="objeto-special">-d</span>&#xa0;S&#xf3;lo descarga paquetes, no instala o desinstala nada.
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1845720594" CREATED="1567735990295" MODIFIED="1567736041997"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <span http-equiv="content-type" content="text/html; charset=utf-8" class="objeto-special">-s</span>&#xa0;Simula las acciones, pero en realidad no las realiza.
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ejemplo:" FOLDED="true" ID="ID_1403997264" CREATED="1567738939230" MODIFIED="1567738947424">
<node FOLDED="true" ID="ID_1655483171" CREATED="1567739026752" MODIFIED="1567739063618"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    salida del comando <span http-equiv="content-type" content="text/html; charset=utf-8" class="comand">aptitude install alien</span>, (para instalar el paquete <a href="https://francisconi.org/linux/comandos/alien" title="ver comando alien" target="_new" class="comand">alien</a>), como se ve nos consulta si, <span class="comand">&#xbf;Quiere continuar?</span>&#xa0;a lo que respondemos <span class="comand">y</span>&#xa0;(resaltado en color).
  </body>
</html>
</richcontent>
<node ID="ID_1718217255" CREATED="1567738947429" MODIFIED="1567738955719"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8"># aptitude install alien</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="lkjg" FOLDED="true" ID="ID_487194802" CREATED="1567738956428" MODIFIED="1568139560360">
<node ID="ID_743635875" CREATED="1567738999327" MODIFIED="1567739017647"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8"># aptitude show ccze</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="102.5 Utilizaci&#xf3;n de los gestores de paquetes RPM y YUM" FOLDED="true" POSITION="right" ID="ID_689683767" CREATED="1568314794854" MODIFIED="1568314850565">
<edge COLOR="#808080"/>
<node TEXT="Un paquete RPM, al igual que los DEBs, consiste de un archivo de ficheros que el manejador de paquetes colocar&#xe1; en nuestro sistema." ID="ID_588736309" CREATED="1568314850573" MODIFIED="1568314884877"/>
<node TEXT="Al igual que el los paquetes Deb un paquete rpm puede contener el binario o ejecutable, el&#xa;c&#xf3;digo fuente o archivos independientes de la arquitectura como documentaci&#xf3;n." ID="ID_1773932716" CREATED="1568314950741" MODIFIED="1568315075353"/>
<node TEXT=" Un archivo RPM es un fichero cpio, el cual es un formato de archivo usado originalmente en cintas de copias de seguridad." ID="ID_1197731487" CREATED="1568315178309" MODIFIED="1568315224221"/>
<node TEXT="RPM por sus siglas en Ingl&#xe9;s (Redhat Pakage Magnament)" FOLDED="true" ID="ID_1587823010" CREATED="1568315977728" MODIFIED="1568316486670">
<node TEXT="Gestor de paquetes" FOLDED="true" ID="ID_1347518841" CREATED="1568316503433" MODIFIED="1568316515624">
<node TEXT="El gestor de paquetes RPM simplifica la instalaci&#xf3;n, actualizaci&#xf3;n, y eliminaci&#xf3;n de software. Este consiste de una base de datos local, el comando rpm y los paquetes rpm creados para el uso de este. Sus opciones mas usuales son las siguientes:" FOLDED="true" ID="ID_501210609" CREATED="1568316515632" MODIFIED="1568316939886">
<node FOLDED="true" ID="ID_1727197855" CREATED="1568316939894" MODIFIED="1568317102507"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    &#xa0;<span http-equiv="content-type" content="text/html; charset=utf-8" style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">i in</font></span><font size="18px" face="sans-serif"><span style="font-size: 18px; font-family: sans-serif">stall: instalaci&#xf3;n de paquetes </span></font><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">F freshen: actualizaci&#xf3;n de paquetes </font></span><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">e remove: eliminar paquetes </font></span><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">v : salida en modo detallado </font></span><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">qa : mostrar todos los paquetes instalados </font></span><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">qf : mostrar informaci&#xf3;n de un paquete instalado </font></span><span style="font-size: 17.999999999999996px; font-family: sans-serif"><font size="17.999999999999996px" face="sans-serif">-</font></span><span style="font-size: 18px; font-family: sans-serif"><font size="18px" face="sans-serif">qp : mostrar informaci&#xf3;</font></span><font size="18px" face="sans-serif"><span style="font-size: 18px; font-family: sans-serif">n de un paquete desinstalado</span></font>
  </body>
</html>
</richcontent>
<node TEXT="Ejemplo&#xa;#rpm -i yum-utilitys3.2.4.centos.rpm" ID="ID_373697462" CREATED="1568317920696" MODIFIED="1568317972758"/>
</node>
</node>
<node TEXT="Opciones para enlistar y verificar paquetes." FOLDED="true" ID="ID_1666821294" CREATED="1568317385929" MODIFIED="1568317451846">
<node TEXT="-q :Muestra la versi&#xf3;n del programa&#xa;-qa :Muestra la versi&#xf3;n de todos los comandos instalados.&#xa;-qc :Lista todos los archivos de configuraci&#xf3;n del paquete especificado.&#xa;-qd :List toda la documentaciones del paquete especificado.&#xa;--qf :Determina el due&#xf1;o o el propietario del paquete especificado.&#xa;-qi :Lista la desripcion completa del paquete especificado.&#xa;-ql : Lista todos los archivos del paquete especificado.&#xa;-K : Verifica la firma digital del paquete especificado.&#xa;-V :Verifica el etado de instalaci&#xf3;n del paquete especificado." ID="ID_587999786" CREATED="1568317451857" MODIFIED="1568317918045"/>
</node>
<node TEXT="Comando alien" FOLDED="true" ID="ID_1639339341" CREATED="1568318254202" MODIFIED="1568318262599">
<node TEXT="Convierte del paquete debian a paquete rpm y viceversa" FOLDED="true" ID="ID_1176127328" CREATED="1568318277574" MODIFIED="1568318345910">
<node TEXT="# alien nombre_del_paquete.rpm" ID="ID_1740707989" CREATED="1568318988020" MODIFIED="1568318996780"/>
</node>
</node>
<node TEXT="Comando rmp2cipio" FOLDED="true" ID="ID_1861689541" CREATED="1568318624941" MODIFIED="1568318636370">
<node TEXT="Extre un archivo cipio de un paquete rpm" FOLDED="true" ID="ID_1347358289" CREATED="1568318636379" MODIFIED="1568318672593">
<node TEXT="Ejemplo: #rpm2cipio samba-* &gt; samba.cipio" ID="ID_631082600" CREATED="1568318676964" MODIFIED="1568318794253"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="102.5 Utilizaci&#xf3;n de los gestores de paquetes RPM y YUM parte 2" FOLDED="true" POSITION="right" ID="ID_1129116776" CREATED="1568674544964" MODIFIED="1568674574156">
<edge COLOR="#808080"/>
<node TEXT="Yum.- Es un gestor de paquetes capaz de instalar, eliminar y actualizar paquetes en Gnu linux, a dem&#xe1;s tiene la capacidad de resolver problema de dependencias al igual que apt-get" FOLDED="true" ID="ID_554252693" CREATED="1568674580508" MODIFIED="1568674795983">
<node ID="ID_1914822715" CREATED="1568674799047" MODIFIED="1568675109235"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Yum significa:
    </p>
    <span http-equiv="content-type" content="text/html; charset=utf-8" class="Apple-style-span" style="font-family: Arial, Helvetica, sans-serif"><font face="Arial, Helvetica, sans-serif">Yellowdog Updater Modified</font></span>

    <p>
      &#xa0;por sus siglas en ingl&#xe9;s.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Es utilizado por las siguientes distribuciones de Gnu/ Linux:&#xa;red hat CENTOS, tambi&#xe9;n viene instalado por defecto en FEDORA&#xa;En general todas las distribuciones derivadas de Red Hat." ID="ID_995576588" CREATED="1568675500394" MODIFIED="1568675558988"/>
<node TEXT="Opciones de yum" FOLDED="true" ID="ID_447654090" CREATED="1568676290060" MODIFIED="1568676300762">
<node TEXT="list:- Lista todos los paquetes instalados y disponibles de los repositorios configurados&#xa;check-update Compara los paquetes rpm con los que se encuentran en el repositorio.&#xa;configurados. Esta opci&#xf3;n crea una lista con todos los paquetes disponibles para actualizaci&#xf3;n pero no los actualiza.&#xa;install .- instala el paquete especificado con todas sus dependencias. Si el paquete ya se encuentra instalado esta opci&#xf3;n revisa si existe una actualizaci&#xf3;n disponible  para el mismo. En caso de encontrarse el paquete es actualizado.&#xa;remove .- Desinstala el paquete especificado con todas sus dependencias.&#xa;update .-Actualiza todos los paquetes instalados." FOLDED="true" ID="ID_750402444" CREATED="1568676303003" MODIFIED="1568676981694">
<node TEXT="" ID="ID_891883598" CREATED="1568678158531" MODIFIED="1568678158531"/>
</node>
</node>
<node TEXT="Archivos de configuraci&#xf3;n  de yum" FOLDED="true" ID="ID_621799255" CREATED="1568676982987" MODIFIED="1568677003060">
<node TEXT="yum.conf ubicado en la ruta /etc/yum" ID="ID_966288104" CREATED="1568678074043" MODIFIED="1568678108561"/>
<node TEXT="Con el siguiente comando podemos ver que es lo que contiene el archivo yum.conf" FOLDED="true" ID="ID_1249669834" CREATED="1568678109784" MODIFIED="1568678153466">
<node TEXT="less /etc/yum/yum.conf" FOLDED="true" ID="ID_58502541" CREATED="1568678179285" MODIFIED="1568678206077">
<node TEXT="En el p&#xe1;rrafo principal se pueden observar las distintas directivas" ID="ID_838894372" CREATED="1568678234596" MODIFIED="1568678316613"/>
<node TEXT="cachedir.- Espeficica el directorio que cachea informaci&#xf3;n desde los repositorios remotos&#xa;keepcache.- Determina si yum presenta el cache de los encabezados y paquetes, luego de la instalaci&#xf3;n de los mismos o no.&#xa;debugleverl.- Establece el nivel de mensaje de los.&#xa;logfile.- Especifica el archivo donde los mensajes originados por yum son descritos.&#xa;pkgpolice.- Indica que a yum que versi&#xf3;n debe utilizar acerca de los paquetes." ID="ID_1580407553" CREATED="1568678318138" MODIFIED="1568678966849"/>
<node TEXT="tolerant.- Continua en caso de haber errores o no&#xa;exacttarch.- Limita el acceso a una arquitectura especificada.&#xa;obsolete.- Limita el acceso a una arqitectura espeficicada.&#xa;gpgcheck.- Esta opci&#xf3;n le dice a yum si debe o no realozar una verificaci&#xf3;n de firma digital GPG en los paquetes .&#xa;plugins.- Habiita los plug-ins de yum encontrados en el directorio /etc/yum/pluginconf.d" ID="ID_295881099" CREATED="1568678983744" MODIFIED="1568679316645"/>
</node>
</node>
<node TEXT="Uno de los archivos importantes es:&#xa;yum.repos.d ubicado en /etc" FOLDED="true" ID="ID_1357185909" CREATED="1568680547280" MODIFIED="1568680646789">
<node TEXT="Uno de los archivos dentro de yum.repos.d es:&#xa;Cent0s-Base.repo" FOLDED="true" ID="ID_1707702547" CREATED="1568680650078" MODIFIED="1568680769482">
<node TEXT="Directictivas de configuraci&#xf3;n de repositorio" FOLDED="true" ID="ID_862978123" CREATED="1568679779670" MODIFIED="1568679800380">
<node TEXT="base.- Especifica los archivos de instalaci&#xf3;n b&#xe1;sicos&#xa;updates.- Apunta a los updates de una instalaci&#xf3;n b&#xe1;sica&#xa;addons.- Incluye paquetes adicionales aun no liberados&#xa;extras.- Especifica un repositorio de paqueteadicional.&#xa;centosplug.- Aputa a los paquetes que estienden la funcionalidad de los paquetes ya instalados." ID="ID_1200396729" CREATED="1568679800382" MODIFIED="1568680046029"/>
<node TEXT="name.-Nombre del repositorio.&#xa;gpgcheck.- Esto le dice a yum sidebe o no realizar una verificaci&#xf3;n de forma digital GPG en los paquetes.&#xa;gpgkey.- Espefica la ubicaci&#xf3;n de la firma GPG&#xa;enable.- Esto le dice a yum si debe o no utilizar este repositorio" ID="ID_688739492" CREATED="1568680101727" MODIFIED="1568680310027"/>
<node TEXT="Variables" FOLDED="true" ID="ID_1412894329" CREATED="1568680310730" MODIFIED="1568680318093">
<node TEXT="$releasever.- Muestra la versi&#xf3;n de la distribuci&#xf3;n que se encuentra en el archivo /etc/redhat-release.&#xa;$basearch.- Especifica la arquitectura del CPU." ID="ID_896700772" CREATED="1568680318098" MODIFIED="1568680478135"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Programa yumdownloader" FOLDED="true" ID="ID_1766258382" CREATED="1568680831425" MODIFIED="1568680862076">
<node TEXT="Permite descargar paquetes rpm de los repositorios yum" ID="ID_1441627410" CREATED="1568680862082" MODIFIED="1568680899453"/>
<node TEXT="sint&#xe1;xis: #yumdownloader (Opciones) paquete" ID="ID_351300015" CREATED="1568680899855" MODIFIED="1568680939901"/>
<node TEXT="Opciones:&#xa;-h, or --help.- Muestra un mensaje de ayuda y luego se cierra autom&#xe1;ticamente.&#xa;--destdir DIR.- Especifica el directorio destino para las descargas.&#xa;--urls.- En vez de descargar los RPM&apos;S, lista las direcciones de URL que ser&#xe1;n descargadas.&#xa;--resolve.- Al descargar los RPM&apos;S, resuelve y recarga tambi&#xe9;n las dependencias.&#xa;--source.- En vez de descargar los RPM&apos;S binarios, descarga los RPM&apos;S fuente" ID="ID_342560716" CREATED="1568681007280" MODIFIED="1568681319080"/>
</node>
</node>
<node TEXT="103.1 Trabajando con linea de comandos parte1" FOLDED="true" POSITION="right" ID="ID_983159099" CREATED="1568842979896" MODIFIED="1568843000403">
<edge COLOR="#808080"/>
<node TEXT="" FOLDED="true" ID="ID_98017052" CREATED="1568843000415" MODIFIED="1568843000415">
<node TEXT="La shell" FOLDED="true" ID="ID_617438143" CREATED="1568843038489" MODIFIED="1568843045793">
<node TEXT="La interfaz de linea de comandos, es una interfaz de texto para los comandos escritos:&#xa;Esta interfaz de linea de comandos es llamada shell." ID="ID_1765747269" CREATED="1568843045807" MODIFIED="1568843221639"/>
<node TEXT="Es la interfaz en tre el usuario y el sistema operativo." ID="ID_1382469315" CREATED="1568843222108" MODIFIED="1568843512319"/>
</node>
<node TEXT="Bash" FOLDED="true" ID="ID_1838832879" CREATED="1568843513171" MODIFIED="1568843518418">
<node TEXT="Born again shell" ID="ID_930308357" CREATED="1568843518426" MODIFIED="1568843532827"/>
<node TEXT="La shell por defecto para cada usuario se basa en la descripci&#xf3;n de la autenticaci&#xf3;n de la base de datos del sistema" ID="ID_1024315477" CREATED="1568845186941" MODIFIED="1568845244900"/>
<node TEXT="Cuando se implementa a nivel local, la base de datos de autenticaci&#xf3;n se inicia con el archivo: /etc/passwd" FOLDED="true" ID="ID_207985877" CREATED="1568845245718" MODIFIED="1568845392487">
<node TEXT="Ejemplo: #less /etc/passwd | grep israel&#xa;israel:x:1000:1000:Israel:/home/israel:/bin/bash" FOLDED="true" ID="ID_1364141791" CREATED="1568845394612" MODIFIED="1568845901653">
<node TEXT="Muestra la shell /bin/bahs para el usuario israel" ID="ID_668212093" CREATED="1568845904523" MODIFIED="1568845936499"/>
</node>
</node>
</node>
<node TEXT="Uno de los archivos de configuraci&#xf3;n del sistema es /etc/profile" FOLDED="true" ID="ID_1073241595" CREATED="1568846026629" MODIFIED="1568846099185">
<node TEXT="#less /etc/profile" ID="ID_978327152" CREATED="1568846102069" MODIFIED="1568846150553"/>
<node TEXT="Invoca otros archivos de configuraci&#xf3;n de sistema. Solo se ejecutan cuando un usuario&#xa;inicia una sesi&#xf3;n en el sistema y son invocados antes de que el usuario consiga acceso a la linea de comandos. Son archivos ocultos y se encuentran en el directorio home del usuario." FOLDED="true" ID="ID_1228851087" CREATED="1568846154596" MODIFIED="1568846286515">
<node TEXT="Los nombres de los archivos comienzan con un punto&#xa;ejemplo .basrc" ID="ID_628152124" CREATED="1568846288673" MODIFIED="1568846330983"/>
<node TEXT="Se pueden vizualizar con el comando ls -la" ID="ID_1153683041" CREATED="1568846331898" MODIFIED="1568846350752"/>
<node TEXT="Cuando un usuario se desconecta del sistema los comandos&#xa;.bash_logout xon ejecutados" ID="ID_1852967278" CREATED="1568846506836" MODIFIED="1568846588841"/>
</node>
</node>
<node TEXT="El comando Bash" FOLDED="true" ID="ID_391617180" CREATED="1568846596930" MODIFIED="1568846608588">
<node TEXT="bash es tambi&#xe9;n un comando que invoca otra sentencia de la shel bash&#xa;ejecutado cada vez que un uuario inicia sesi&#xf3;n el los sistema linux." ID="ID_1653884609" CREATED="1568846608592" MODIFIED="1568846803967"/>
</node>
<node TEXT="Comando env" FOLDED="true" ID="ID_1923197489" CREATED="1568846850973" MODIFIED="1568846860774">
<node TEXT="Cada proceso incluye las variables de entorno, estas variables son par&#xe1;metros como algun nombre de usuario, la ruta de acceso al directorio home del usuario y los usuarios estandar de la shell por ejemplo" ID="ID_1158750329" CREATED="1568846873645" MODIFIED="1568847065824"/>
<node TEXT="Es utilizado para listar todas las variables de entorno, o ejecutar otra utilidad en un entorno alterado sin tener que modificar el ambiente que existe actualmente." ID="ID_1440443494" CREATED="1568847066333" MODIFIED="1568847176017"/>
<node TEXT="#env | less" FOLDED="true" ID="ID_1062197539" CREATED="1568847176839" MODIFIED="1568847190106">
<node TEXT="Esto es una salida de variables de entorno, recordando que una variable de entorno     son en letras mayusculas" ID="ID_1177900180" CREATED="1568847190109" MODIFIED="1568847231730"/>
</node>
<node TEXT="variable de entorno" FOLDED="true" ID="ID_342381574" CREATED="1568847813464" MODIFIED="1568847820890">
<node TEXT="Una variable de entorno es un nombre asociado a una cadena de caracteres." ID="ID_8808690" CREATED="1568847820898" MODIFIED="1568847826223"/>
<node TEXT="Dependiendo de la variable, su utilidad puede ser distinta. Algunas son &#xfa;tiles para no tener que escribir muchas opciones al ejecutar un programa, otras las utiliza el propio shell (PATH, PS1,&#x2026;). La tabla siguiente muestra la lista de variables m&#xe1;s usuales:" ID="ID_61850150" CREATED="1568848194228" MODIFIED="1568848198637"/>
<node ID="ID_437968751" CREATED="1568848199649" MODIFIED="1568848237872"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table http-equiv="content-type" content="text/html; charset=utf-8" class="wiki-content-table">
      <tr>
        <th>
          Variable
        </th>
        <th>
          Descripci&#xf3;n
        </th>
      </tr>
      <tr>
        <td>
          DISPLAY
        </td>
        <td>
          Donde aparecen la salidas de X-Windows.
        </td>
      </tr>
      <tr>
        <td>
          HOME
        </td>
        <td>
          Directorio personal.
        </td>
      </tr>
      <tr>
        <td>
          HOSTNAME
        </td>
        <td>
          Nombre de la m&#xe1;quina.
        </td>
      </tr>
      <tr>
        <td>
          MAIL
        </td>
        <td>
          Archivo de correo.
        </td>
      </tr>
      <tr>
        <td>
          PATH
        </td>
        <td>
          Lista de directorios donde buscar los programas.
        </td>
      </tr>
      <tr>
        <td>
          PS1
        </td>
        <td>
          Prompt.
        </td>
      </tr>
      <tr>
        <td>
          SHELL
        </td>
        <td>
          Int&#xe9;rprete de comandos por defecto.
        </td>
      </tr>
      <tr>
        <td>
          TERM
        </td>
        <td>
          Tipo de terminal.
        </td>
      </tr>
      <tr>
        <td>
          USER
        </td>
        <td>
          Nombre del usuario.
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Comando echo" FOLDED="true" ID="ID_262037512" CREATED="1568846864748" MODIFIED="1568846870753">
<node TEXT="La configuraci&#xf3;n del entorno es variable, esta puede ser revisada mediante el comando echo" ID="ID_1893564363" CREATED="1568854731506" MODIFIED="1568854777427"/>
<node TEXT="Para comprobar el valor del directorio actual debemos ejecutar:" FOLDED="true" ID="ID_1972789579" CREATED="1568854778234" MODIFIED="1568857974122">
<node TEXT="#echo $HOME" ID="ID_1404231925" CREATED="1568857976187" MODIFIED="1568858016436"/>
</node>
<node TEXT="Para encontrar la variable path debemos ejecutar:" FOLDED="true" ID="ID_1046998225" CREATED="1568858239616" MODIFIED="1568858259088">
<node TEXT="#echo $PATH" ID="ID_597609801" CREATED="1568858259091" MODIFIED="1568858271381"/>
</node>
</node>
<node TEXT="Comando export" FOLDED="true" ID="ID_137802120" CREATED="1568858291947" MODIFIED="1568858302109">
<node TEXT="Exporta una variable de entorno" ID="ID_646883757" CREATED="1568858302118" MODIFIED="1568858310686"/>
<node TEXT="#PAUL=paulito&#xa;#echo PAUL // devuelve:&#xa;paulito&#xa;Pero si se cambia de basho no aparece ninguna varialble si se le invoca con #echo $PAUL&#xa;Para eso usamos el comando export&#xa;#export PAUL&#xa;Ahora usted puede ejecutar #echo $PAUL&#xa;Y nos devolver&#xe1; #paulito" ID="ID_1761568029" CREATED="1568858311110" MODIFIED="1568858936502"/>
</node>
<node TEXT="El comando sent" FOLDED="true" ID="ID_302227877" CREATED="1568858937716" MODIFIED="1568858968453">
<node TEXT="Es usado para definir una variable al igual que export pero solo en el bash actual" ID="ID_547002383" CREATED="1568858968457" MODIFIED="1568858993864"/>
</node>
<node TEXT="El comando unset" FOLDED="true" ID="ID_542089858" CREATED="1568859036630" MODIFIED="1568859043971">
<node TEXT="Sirve para desactivar la variable creada ejemplo:" FOLDED="true" ID="ID_388047818" CREATED="1568859043974" MODIFIED="1568859059909">
<node TEXT="#unset PAUL" ID="ID_927131408" CREATED="1568859059914" MODIFIED="1568859081364"/>
</node>
</node>
</node>
</node>
<node TEXT="103.1 Trabajando con linea de comandos parte2" FOLDED="true" POSITION="right" ID="ID_1186557418" CREATED="1568925755534" MODIFIED="1568925774844">
<edge COLOR="#808080"/>
<node TEXT="Uso del (.)" FOLDED="true" ID="ID_169166494" CREATED="1568925774853" MODIFIED="1568925881950">
<node TEXT="El punto es un caracter importante y representa el directorio actual, se utiliza con los comandos ejecutables, ejemplo:" FOLDED="true" ID="ID_1257921774" CREATED="1568925881959" MODIFIED="1568927468240">
<node TEXT="Se ha creado un archivo ejecutable en la siguiente ruta:&#xa;# cd /home/paul&#xa;#ls -la | grep date.sh //el cual solo ejecuta al comando date.sh&#xa;A&#xfa;n teniedo los permisos de ejecuci&#xf3;n establecidos, no esposible ejecutar el fichero directamente. Para ello se ejecuta de la siguiente forma:&#xa;#./date.sh" ID="ID_1503938134" CREATED="1568927468251" MODIFIED="1568927699677"/>
</node>
<node TEXT="Un punto doble especifica el siguiente directorio de nivel superior" FOLDED="true" ID="ID_201930071" CREATED="1568927748912" MODIFIED="1568927786040">
<node TEXT="home/paul#cd ..&#xa;paul#pwd&#xa;/home" ID="ID_755040988" CREATED="1568927787795" MODIFIED="1568927876784"/>
</node>
</node>
<node TEXT="La variable PATH" FOLDED="true" ID="ID_1616093889" CREATED="1568927936644" MODIFIED="1568927942659">
<node TEXT="Variable de entorno en sistemas operativos tipo unix que singnifica un conjunto de&#xa;directorios donde se encuentran los programas ejecutables." ID="ID_1187511314" CREATED="1568927943541" MODIFIED="1568928003013"/>
<node TEXT="Se especifica como uno o mas nombres de directorios separados por dos puntos" FOLDED="true" ID="ID_1501579043" CREATED="1568928102155" MODIFIED="1568928221185">
<node TEXT="root@PC-L440:~# echo $PATH&#xa;/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin" FOLDED="true" ID="ID_1340978361" CREATED="1568928221191" MODIFIED="1568928250356">
<node TEXT="Estos directorios suelen ubicarse en la variable PTAH por que son los lugares&#xa;donde la mayoria de los ejecutables se encuentran." ID="ID_545521560" CREATED="1568928252183" MODIFIED="1568928448563"/>
</node>
</node>
<node TEXT="PATH Absoluto o ruta absoluta y PATH relativo o ruta relativa" FOLDED="true" ID="ID_132862917" CREATED="1568928456770" MODIFIED="1568928487773">
<node TEXT="Si nos encontramos en determinado directorio pr ejemplo:&#xa;root@PC-L440:~/Documentos#&#xa;Y me quiero ir a un subdirectorio uso la ruta relativa ejemplo&#xa;root@PC-L440:~/Documentos#cd Programas&#xa;Ahora me quiero ir al directorio network, tengo que usar la ruta absoluta ya que no se encuentra en ningun subdirectorio de Programas que es donde estoy situado, quedando de la siguente forma:&#xa;root@PC-L440:~/Documentos/Programas#cd /etc/network&#xa;La ruta relativa solo funciona cuando se encuentra un subdirectorio en donde estamos actualmente." ID="ID_432090813" CREATED="1568928495774" MODIFIED="1568928998487"/>
</node>
</node>
<node TEXT="Uso del tabulador" FOLDED="true" ID="ID_1490555049" CREATED="1568929068239" MODIFIED="1568929075535">
<node TEXT="La tecla tabulador es importante ya que nos permite completar la sint&#xe1;xis del programa o cualquier texto." ID="ID_1869264341" CREATED="1568929077814" MODIFIED="1568929303554"/>
</node>
<node TEXT="Operadores de control de la shell bash" FOLDED="true" ID="ID_1911402916" CREATED="1568929391443" MODIFIED="1568929418548">
<node TEXT="Ayudan hacer el trabajo de administrador de sistema mucho mas f&#xe1;cil" FOLDED="true" ID="ID_389680518" CREATED="1568929418553" MODIFIED="1568929601698">
<node TEXT="Algunos de los operadores son:&#xa;&lt;&gt; | || &amp; &amp;&amp;; ;; ()" ID="ID_521816332" CREATED="1568929601708" MODIFIED="1568929700153"/>
<node TEXT="En el siguiente caso se pueden usar  las ; para utilizar tres comandos a  la vez, ejemplo:&#xa;#ls -la;date;cd /etc&#xa;EL siguiente comando lista y redirecciona el resultado al programa date.sh&#xa;#ls -la | grep date.sh" ID="ID_1043709118" CREATED="1568929786221" MODIFIED="1568930171937"/>
</node>
</node>
</node>
<node TEXT="103.1 Trabajando con linea de comandos parte 3" FOLDED="true" POSITION="right" ID="ID_287100900" CREATED="1568930505143" MODIFIED="1568930531677">
<edge COLOR="#808080"/>
<node TEXT="Comando pwd" FOLDED="true" ID="ID_1519007031" CREATED="1568930531684" MODIFIED="1568930567498">
<node TEXT="Se usa para definir la ruta absoluta del directorio actual, ejemplo:" FOLDED="true" ID="ID_48336216" CREATED="1568930621740" MODIFIED="1568930656137">
<node TEXT="root@PC-L440:~/Documentos# pwd&#xa;/home/israel/Documentos" ID="ID_1471461507" CREATED="1568930656142" MODIFIED="1568930766966"/>
</node>
<node TEXT="Significa Print working Directory" ID="ID_624949275" CREATED="1568930567505" MODIFIED="1568930619306"/>
</node>
<node TEXT="Comando man" FOLDED="true" ID="ID_604098227" CREATED="1568930913367" MODIFIED="1568930929342">
<node TEXT="Se usa para acceder a los manuales de los comandos conocidos como manuales de usuarios" ID="ID_1966507390" CREATED="1568930929347" MODIFIED="1568930996461"/>
<node TEXT="Ejemplo el manual del mismo comando osease:&#xa;#man man&#xa;MAN(1)                  Utilidades del paginador manual                  MAN(1)&#xa;&#xa;NOMBRE&#xa;       man - un interfaz para las referencias de manuales por conexi&#xf3;n&#xa;&#xa;SINOPSIS&#xa;       man  [-C fichero] [-d] [-D] [--warnings[=advertencia]] [-R codificaci&#xf3;n]&#xa;       [-L  localizaci&#xf3;n]  [-m  sistema[,...]]  [-M  ruta]  [-S  listado]   [-e&#xa;       extensi&#xf3;n]   [-i|-I]   [--regex|--wildcard]   [--names-only]  [-a]  [-u]&#xa;       [--no-subpages] [-P  paginador]  [-r  r&#xe9;plica]  [-7]  [-E  codificaci&#xf3;n]&#xa;       [--no-hyphenation]   [--no-justification]   [-p   cadena  textual]  [-t]&#xa;       [-T[dispositivo]]    [-H[explorador]]    [-X[ppp]]    [-Z]    [[secci&#xf3;n]&#xa;       p&#xe1;gina[.secci&#xf3;n] ...] ...&#xa;       man -k [prop&#xf3;sito opciones] expreg ...&#xa;       man -K [-w|-W] [-S listado] [-i|-I] [--regex] [secci&#xf3;n] t&#xe9;rmino ...&#xa;       man -f [qu&#xe9; es opciones] p&#xe1;gina ...&#xa;       man -l [-C fichero] [-d] [-D] [--warnings[=aviso]] [-R codificaci&#xf3;n] [-L&#xa;       localizaci&#xf3;n] [-P paginador]  [-r  prompt]  [-7]  [-E  codificando]  [-p&#xa;       cadena  textual]  [-t]  [-T[dispositivo]]  [-H[explorar]] [-X[ppp]] [-Z]&#xa;       fichero ...&#xa;       man -w|-W [-C fichero] [-d] [-D] p&#xe1;gina ...&#xa;       man -c [-C fichero] [-d] [-D] p&#xe1;gina ...&#xa;       man [-?V]&#xa;&#xa;DESCRIPCI&#xd3;N&#xa;       man es la p&#xe1;gina manual del sistema.  Cada argumento de p&#xe1;gina argumento&#xa;       dado  a man normalmente es el nombre de un programa, utilidad o funci&#xf3;n.&#xa;       La p&#xe1;gina  del  manual  asociada  con  cada  de  estos  argumentos  est&#xe1;&#xa;       encontrado  y  ense&#xf1;ado.  Una section, si proporcion&#xf3;, man dirigido para&#xa;       mirar solo en esa secci&#xf3;n del manual.  La  operaci&#xf3;n  predeterminada  es&#xa;       buscar  en  todoas  las  secciones  disponibles  seguidas  de  un  orden&#xa;       predefinido (&quot;1 n l 8 3 2 3posix 3pm 3perl 3am 5 4 9 6 7&quot; por defecto, a&#xa;       no ser que sobrescriba por la SECCI&#xd3;N directiva en /etc/manpath.config),&#xa;       y para mostrar solo la primera p&#xe1;gina encontrada, incluso si  la  p&#xe1;gina&#xa;       existe dentro de varias secciones." ID="ID_1948987323" CREATED="1568931271048" MODIFIED="1568931329830"/>
<node TEXT="Con la opci&#xf3;n -k puede ayudar a buscar, ejemplo:&#xa;Para buscar una lista de comandos que se usan con la descomprensi&#xf3;n de archivos insertamos:&#xa;#man -k compress | less&#xa;7z (1)               - A file archiver with high compression ratio format&#xa;7za (1)              - A file archiver with high compression ratio format&#xa;7zr (1)              - A file archiver with high compression ratio format&#xa;bunzip2 (1)          - a block-sorting file compressor, v1.0.6&#xa;bzcat (1)            - decompresses files to stdout&#xa;bzcmp (1)            - compare bzip2 compressed files&#xa;bzdiff (1)           - compare bzip2 compressed files&#xa;bzegrep (1)          - search possibly bzip2 compressed files for a regular exp...&#xa;bzexe (1)            - compress executable files in place&#xa;bzfgrep (1)          - search possibly bzip2 compressed files for a regular exp...&#xa;bzgrep (1)           - search possibly bzip2 compressed files for a regular exp...&#xa;bzip2 (1)            - a block-sorting file compressor, v1.0.6&#xa;bzless (1)           - file perusal filter for crt viewing of bzip2 compressed ...&#xa;bzmore (1)           - file perusal filter for crt viewing of bzip2 compressed ...&#xa;Dpkg::Compression (3perl) - simple database of available compression methods&#xa;Dpkg::Compression::FileHandle (3perl) - object dealing transparently with file ...&#xa;Dpkg::Compression::Process (3perl) - run compression/decompression processes&#xa;fsck.cramfs (8)      - fsck compressed ROM file system&#xa;gunzip (1)           - compress or expand files&#xa;gzexe (1)            - compress executable files in place&#xa;gzip (1)             - compress or expand files&#xa;logrotate (8)        - rotates, compresses, and mails system logs&#xa;logrotate.conf (5)   - rotates, compresses, and mails system logs&#xa;lzcat (1)            - Compress or decompress .xz and .lzma files&#xa;lzcmp (1)            - compare compressed files&#xa;lzdiff (1)           - compare compressed files&#xa;lzegrep (1)          - search compressed files for a regular expression&#xa;lzfgrep (1)          - search compressed files for a regular expression&#xa;lzgrep (1)           - search compressed files for a regular expression&#xa;lzless (1)           - view xz or lzma compressed (text) files&#xa;lzma (1)             - Compress or decompress .xz and .lzma files&#xa;lzmore (1)           - view xz or lzma compressed (text) files&#xa;mkfs.cramfs (8)      - make compressed ROM file system&#xa;mkzftree (1)         - Create a zisofs/RockRidge compressed file tree&#xa;mscompress (1)       - compress data using LZ77 algorithm&#xa;msexpand (1)         - decompress data compressed using mscompress(1) or COMPRE...&#xa;p7zip (1)            - Wrapper on 7-Zip file archiver with high compression ratio&#xa;:" ID="ID_941155826" CREATED="1568931340767" MODIFIED="1568931593824"/>
<node TEXT="Para ver las paginas del comando de alg&#xfa;n programa solo tiene que agregar la opci&#xf3;n -f&#xa;#man -f sync&#xa;sync (1)             - Synchronize cached writes to persistent storage&#xa;sync (2)             - commit filesystem caches to disk" ID="ID_170069351" CREATED="1568931740585" MODIFIED="1568931817976"/>
</node>
<node TEXT="Comando uname" FOLDED="true" ID="ID_131377376" CREATED="1568931899564" MODIFIED="1568931906258">
<node TEXT="Muestra informaci&#xf3;n relevante del sistema y del kernel" ID="ID_1287096326" CREATED="1568931906263" MODIFIED="1568931960305"/>
<node TEXT="#uname -a&#xa;Linux PC-L440 5.0.0-29-generic #31-Ubuntu SMP Thu Sep 12 13:05:32 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux" ID="ID_194831599" CREATED="1568931980635" MODIFIED="1568932069819"/>
<node TEXT="Opciones del comando" FOLDED="true" ID="ID_359748790" CREATED="1568932074076" MODIFIED="1568932079940">
<node TEXT="uname -a: Nos muestra toda la informaci&#xf3;n de nuestro sistema y nuestro ordenador.&#xa;&#xa;uname -r: La versi&#xf3;n del Kernel de nuestro sistema&#xa;&#xa;uname -v: La fecha de publicacion del Kernel de nuestro sistema&#xa;&#xa;uname -n: Muestra el nombre de nuestro ordenador (el que asignamos al instalar el SO).&#xa;&#xa;uname -m: Muestra la arquitectura de nuestro procesador (i386, i486, i586, i686).&#xa;&#xa;uname -s: Nos muestra el nombre del kernel.&#xa;&#xa;uname -o: Nos muestra el nombre del sistema operativo" ID="ID_1381127802" CREATED="1568932079947" MODIFIED="1568932206780"/>
</node>
</node>
<node TEXT="Comando exec" FOLDED="true" ID="ID_1861482194" CREATED="1568932252166" MODIFIED="1568932273807">
<node TEXT="Se utiliza para ejecutar otro programa que reemplace la sheel actual" ID="ID_660481392" CREATED="1568932273812" MODIFIED="1568932349019"/>
<node TEXT="Ejemplo: se inicia una shell bash hija o subordinada y luego se utiliza exec para sustituirla por una shell korn:&#xa;root@PC-L440:~/Documentos# echo $$&#xa;4140&#xa;root@PC-L440:~/Documentos# bash&#xa;root@PC-L440:~/Documentos# echo $$&#xa;5467&#xa;root@PC-L440:~/Documentos# exec ksh&#xa;bash: exec: ksh: no encontrado&#xa;root@PC-L440:~/Documentos# exit}&#xa;exit}: orden no encontrada&#xa;root@PC-L440:~/Documentos# exit&#xa;exit&#xa;root@PC-L440:~/Documentos# echo $$&#xa;4140" ID="ID_1306288575" CREATED="1568932351061" MODIFIED="1568932761704"/>
</node>
<node TEXT="Comando history" FOLDED="true" ID="ID_759222364" CREATED="1568932769364" MODIFIED="1568932780189">
<node TEXT="La shell bash mantiene un registro con los comando ejecutados recientemente conocido como el comando history" ID="ID_1968361347" CREATED="1568932820656" MODIFIED="1568932920003"/>
<node TEXT="#history&#xa; 212  sudo -i&#xa;  213  echo $PATH&#xa;  214  pwd&#xa;  215  ls&#xa;  216  cd Docuemntos&#xa;  217  cd Documentos&#xa;  218  ls&#xa;  219  history&#xa;  220  pwd&#xa;  221  man man&#xa;  222  man -k compress | less&#xa;  223  man -f sync&#xa;  224  uname -a&#xa;  225  echo $$&#xa;  226  bash&#xa;  227  echo $$&#xa;  228  history&#xa;root@PC-L440:~/" ID="ID_1125926831" CREATED="1568932920615" MODIFIED="1568932972689"/>
<node TEXT="El historial de comandos de cada usuario se almacena en un archivo oculto llamado .bash_history en el directorio home del usuario" ID="ID_1028317676" CREATED="1568933011903" MODIFIED="1568933189628"/>
<node TEXT="Una variable de entorno llamada HITZIZE le muestra a bash cuantas lineas debe mantener del historial de comandos&#xa;root@PC-L440:~/Documentos# echo $HISTSIZE&#xa;1000&#xa;root@PC-L440:~/Documentos#" ID="ID_1090935482" CREATED="1568933190332" MODIFIED="1568933315851"/>
<node TEXT="Opciones del comando history" FOLDED="true" ID="ID_65625806" CREATED="1568933529842" MODIFIED="1568933542860">
<node TEXT="history.- muestra el historial completo.&#xa;history N.- Muestra las &#xfa;ltimas Nlineas del historial&#xa;!!.- ejecuta el comando mas reciente.&#xa;!N ejecuta el comando n&#xfa;mero N del historial.&#xa;!-N.- Ejecuta el comando N del historial de foma inversa.&#xa;!string.- Ejecuta el comando m&#xe1;s reciente que comienza con string" ID="ID_61686389" CREATED="1568933542865" MODIFIED="1568933716731"/>
</node>
</node>
</node>
<node TEXT="103.2 Procesamiento del flujo de texto parte1" FOLDED="true" POSITION="right" ID="ID_1114263615" CREATED="1569015420574" MODIFIED="1569015518586">
<edge COLOR="#808080"/>
<node TEXT="Comando cat" FOLDED="true" ID="ID_1378127692" CREATED="1569015518594" MODIFIED="1569015627698">
<node TEXT="Es utilizado para mostrar el contenido de un archivo de texto en pantalla." ID="ID_199109397" CREATED="1569015627705" MODIFIED="1569015701722"/>
<node TEXT="Ejemplo #cat /etc/passwd" FOLDED="true" ID="ID_1385419761" CREATED="1569015703160" MODIFIED="1569016053078">
<node TEXT="root@PC-L440:~# cat /etc/passwd&#xa;root:x:0:0:root:/root:/bin/bash&#xa;daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin&#xa;bin:x:2:2:bin:/bin:/usr/sbin/nologin&#xa;sys:x:3:3:sys:/dev:/usr/sbin/nologin&#xa;sync:x:4:65534:sync:/bin:/bin/sync" ID="ID_1076706765" CREATED="1569016053087" MODIFIED="1569016057018"/>
</node>
</node>
<node TEXT="Comando cut" FOLDED="true" ID="ID_1229722722" CREATED="1569016109980" MODIFIED="1569016388807">
<node TEXT="Fue dise&#xf1;ado para separar columnas especificas en un archivo de texto" ID="ID_286065542" CREATED="1569016118002" MODIFIED="1569016207630"/>
<node TEXT="Ejemplo: #cut  -d: -f6 /etc/passwd&#xa;El caracter separador son los dos puntos." FOLDED="true" ID="ID_983615234" CREATED="1569016213179" MODIFIED="1569016281894">
<node TEXT="root@PC-L440:~# cut  -d: -f6 /etc/passwd&#xa;/root&#xa;/usr/sbin&#xa;/bin&#xa;/dev&#xa;/bin&#xa;/usr/games&#xa;/var/cache/man&#xa;/var/spool/lpd&#xa;/var/mail&#xa;/var/spool/news&#xa;/var/spool/uu" ID="ID_301439425" CREATED="1569016281907" MODIFIED="1569016363020"/>
</node>
</node>
<node TEXT="Comando expand" FOLDED="true" ID="ID_543528049" CREATED="1569016421851" MODIFIED="1569016428883">
<node TEXT="Reemplaza las tabulaciones por espacio en un archivo de texto." ID="ID_1549100714" CREATED="1569016428894" MODIFIED="1569016544787"/>
<node TEXT="Ejemplo #man expand&#xa;EXPAND(1)                                                   User Commands                                                  EXPAND(1)&#xa;&#xa;NAME&#xa;       expand - convert tabs to spaces&#xa;&#xa;SYNOPSIS&#xa;       expand [OPTION]... [FILE]...&#xa;&#xa;DESCRIPTION&#xa;       Convert tabs in each FILE to spaces, writing to standard output.&#xa;&#xa;       With no FILE, or when FILE is -, read standard input.&#xa;&#xa;       Mandatory arguments to long options are mandatory for short options too.&#xa;&#xa;       -i, --initial&#xa;              do not convert tabs after non blanks&#xa;&#xa;       -t, --tabs=N&#xa;              have tabs N characters apart, not 8&#xa;&#xa;       -t, --tabs=LIST&#xa;              use  comma  separated list of tab positions The last specified position can be prefixed with &apos;/&apos; to specify a tab size&#xa;              to use after the last explicitly specified tab stop.  Also a prefix of &apos;+&apos; can be used to align  remaining  tab  stops&#xa;              relative to the last specified tab stop instead of the first column&#xa;&#xa;       --help display this help and exit&#xa;&#xa;       --version&#xa;              output version information and exit" ID="ID_801657288" CREATED="1569016545093" MODIFIED="1569016562655"/>
</node>
<node TEXT="Comando unexpand" FOLDED="true" ID="ID_1732744778" CREATED="1569016567722" MODIFIED="1569016645736">
<node TEXT="Es lo opuesto al comando expand, este reemplaza los espacios por tabulaciones." ID="ID_1879948488" CREATED="1569016645744" MODIFIED="1569016667820"/>
<node TEXT="Ejemplo: #man unexpand&#xa;NAME&#xa;       unexpand - convert spaces to tabs&#xa;&#xa;SYNOPSIS&#xa;       unexpand [OPTION]... [FILE]...&#xa;&#xa;DESCRIPTION&#xa;       Convert blanks in each FILE to tabs, writing to standard output.&#xa;&#xa;       With no FILE, or when FILE is -, read standard input.&#xa;&#xa;       Mandatory arguments to long options are mandatory for short options too.&#xa;&#xa;       -a, --all&#xa;              convert all blanks, instead of just initial blanks&#xa;&#xa;       --first-only&#xa;              convert only leading sequences of blanks (overrides -a)&#xa;&#xa;       -t, --tabs=N&#xa;              have tabs N characters apart instead of 8 (enables -a)&#xa;&#xa;       -t, --tabs=LIST&#xa;              use  comma  separated list of tab positions The last specified position can be prefixed with &apos;/&apos; to specify a tab size&#xa;              to use after the last explicitly specified tab stop.  Also a prefix of &apos;+&apos; can be used to align  remaining  tab  stops&#xa;              relative to the last specified tab stop instead of the first column&#xa;&#xa;       --help display this help and exit&#xa;&#xa;       --version&#xa;              output version information and exit" ID="ID_1620586798" CREATED="1569016696225" MODIFIED="1569016749097"/>
</node>
<node TEXT="Comando fmt" FOLDED="true" ID="ID_700585022" CREATED="1569016812467" MODIFIED="1569016844471">
<node TEXT="Cambia el formato de un texto dentro de un archivo" ID="ID_1012443687" CREATED="1569016844476" MODIFIED="1569016855905"/>
<node TEXT="Ejemplo: #man fmt" FOLDED="true" ID="ID_184251405" CREATED="1569016861859" MODIFIED="1569016871384">
<node TEXT="En este caso nosotros queremos que el archivo de texto princat tenga un ancho de 30 caracteres para lo cual escribimos:" FOLDED="true" ID="ID_1214268742" CREATED="1569016929648" MODIFIED="1569016979726">
<node TEXT="root@PC-L440:~# fmt -w30 /etc/printcap&#xa;# This file was automatically&#xa;generated by cupsd(8) from&#xa;the # /etc/cups/printers.conf&#xa;file.  All changes to this&#xa;file # will be lost.&#xa;root@PC-L440:~#" ID="ID_1978426947" CREATED="1569016979733" MODIFIED="1569017085226"/>
</node>
</node>
</node>
</node>
<node TEXT="103.2 Procesamiento del flujo de texto parte2" FOLDED="true" POSITION="right" ID="ID_85399639" CREATED="1569017280709" MODIFIED="1569017308749">
<edge COLOR="#808080"/>
<node TEXT="Comando head" FOLDED="true" ID="ID_468587540" CREATED="1569017312969" MODIFIED="1569017460515">
<node TEXT="Puede ser usado como litro o puede tomar el nombre de un archivo como argumento" ID="ID_468512557" CREATED="1569017460520" MODIFIED="1569017511050"/>
<node TEXT="Por defecto muestra las primeras 10 lineas de un archivo de texto Ejemplo:" FOLDED="true" ID="ID_1850534854" CREATED="1569017535576" MODIFIED="1569017689269">
<node TEXT="root@PC-L440:~# cat /etc/passwd | head&#xa;root:x:0:0:root:/root:/bin/bash&#xa;daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin&#xa;bin:x:2:2:bin:/bin:/usr/sbin/nologin&#xa;sys:x:3:3:sys:/dev:/usr/sbin/nologin&#xa;sync:x:4:65534:sync:/bin:/bin/sync&#xa;games:x:5:60:games:/usr/games:/usr/sbin/nologin&#xa;man:x:6:12:man:/var/cache/man:/usr/sbin/nologin&#xa;lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin&#xa;mail:x:8:8:mail:/var/mail:/usr/sbin/nologin&#xa;news:x:9:9:news:/var/spool/news:/usr/sbin/nologin&#xa;root@PC-L440:~#" ID="ID_1095385202" CREATED="1569017868077" MODIFIED="1569017908989"/>
</node>
</node>
<node TEXT="Comando tail" FOLDED="true" ID="ID_1081161885" CREATED="1569017778658" MODIFIED="1569017786361">
<node TEXT="Puede ser usado como litro o puede tomar el nombre de un archivo como argumento" FOLDED="true" ID="ID_1828661910" CREATED="1569017803065" MODIFIED="1569017829099">
<node TEXT="root@PC-L440:~# cat /etc/passwd | tail&#xa;saned:x:115:122::/var/lib/saned:/usr/sbin/nologin&#xa;whoopsie:x:116:123::/nonexistent:/bin/false&#xa;colord:x:117:124:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/nologin&#xa;hplip:x:118:7:HPLIP system user,,,:/var/run/hplip:/bin/false&#xa;mpd:x:119:29::/var/lib/mpd:/usr/sbin/nologin&#xa;sddm:x:120:125:Simple Desktop Display Manager:/var/lib/sddm:/bin/false&#xa;geoclue:x:121:126::/var/lib/geoclue:/usr/sbin/nologin&#xa;pulse:x:122:127:PulseAudio daemon,,,:/var/run/pulse:/usr/sbin/nologin&#xa;israel:x:1000:1000:Israel:/home/israel:/bin/bash&#xa;systemd-coredump:x:999:999:systemd Core Dumper:/:/sbin/nologin&#xa;root@PC-L440:~#" ID="ID_242269812" CREATED="1569017689275" MODIFIED="1569017723786"/>
<node TEXT="root@PC-L440:~# cat /etc/passwd | tail -c 50&#xa;ump:x:999:999:systemd Core Dumper:/:/sbin/nologin&#xa;root@PC-L440:~#" ID="ID_1241690314" CREATED="1569018305720" MODIFIED="1569018322420"/>
</node>
<node TEXT="Muestra 10 lineas por default al igual que head pero se puede configurar como mejor le convenga en ambos comandos ejemplo:" FOLDED="true" ID="ID_1804267752" CREATED="1569018027627" MODIFIED="1569018058670">
<node TEXT="root@PC-L440:~# cat /etc/passwd | head -n 3&#xa;root:x:0:0:root:/root:/bin/bash&#xa;daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin&#xa;bin:x:2:2:bin:/bin:/usr/sbin/nologin" ID="ID_71038084" CREATED="1569018058677" MODIFIED="1569018099404"/>
</node>
</node>
<node TEXT="Comando od" FOLDED="true" ID="ID_919526979" CREATED="1569018381499" MODIFIED="1569018393723">
<node TEXT="Muestra los formatos octal a otros formatos" FOLDED="true" ID="ID_989913872" CREATED="1569018393728" MODIFIED="1569028840403">
<node TEXT="Ejemplo:&#xa;root@PC-L440:~# od /etc/passwd&#xa;0000000 067562 072157 074072 030072 030072 071072 067557 035164&#xa;0000020 071057 067557 035164 061057 067151 061057 071541 005150&#xa;0000040 060544 066545 067157 074072 030472 030472 062072 062541&#xa;0000060 067555 035156 072457 071163 071457 064542 035156 072457&#xa;0000100 071163 071457 064542 027556 067556 067554 064547 005156&#xa;0000120 064542 035156 035170 035062 035062 064542 035156 061057" ID="ID_1682598977" CREATED="1569028843779" MODIFIED="1569028905241"/>
</node>
<node TEXT="Para convertir el archivo passwd a asci use:" FOLDED="true" ID="ID_945668428" CREATED="1569028906495" MODIFIED="1569028935460">
<node TEXT="root@PC-L440:~# od -Ad -t c /etc/passwd&#xa;0000000   r   o   o   t   :   x   :   0   :   0   :   r   o   o   t   :&#xa;0000016   /   r   o   o   t   :   /   b   i   n   /   b   a   s   h  \n&#xa;0000032   d   a   e   m   o   n   :   x   :   1   :   1   :   d   a   e&#xa;0000048   m   o   n   :   /   u   s   r   /   s   b   i   n   :   /   u&#xa;0000064   s   r   /   s   b   i   n   /   n   o   l   o   g   i   n  \n&#xa;0000080   b   i   n   :   x   :   2   :   2   :   b   i   n   :   /   b&#xa;0000096   i   n   :   /   u   s   r   /   s   b   i   n   /   n   o   l&#xa;0000112   o   g   i   n  \n   s   y   s   :   x   :   3   :   3   :   s" ID="ID_1898783104" CREATED="1569028935465" MODIFIED="1569029137556"/>
</node>
</node>
<node TEXT="Comando join" FOLDED="true" ID="ID_1814391420" CREATED="1569029204527" MODIFIED="1569029220516">
<node TEXT="Combinan columnas de diferentes archivos" FOLDED="true" ID="ID_1462725817" CREATED="1569029220519" MODIFIED="1569029245829">
<node TEXT="root@PC-L440:~/Documentos/prueba# cat file1&#xa;# Este documento trata de nombres y direcciones&#xa;&#xa;Oscar Mu&#xf1;os Alamos 123&#xa;David llanos Gardenia 23&#xa;Israel M&#xe9;jia Venustiano 7&#xa;&#xa;root@PC-L440:~/Documentos/prueba# cat file2&#xa;# Este documento trata de oficios y profesiones&#xa;&#xa;Oscar Ingeniero&#xa;David Adminnistrador&#xa;Israel Inform&#xe1;tico" ID="ID_137759042" CREATED="1569029245836" MODIFIED="1569030716747"/>
<node TEXT="root@PC-L440:~/Documentos/prueba# join file1 file2&#xa;# Este documento trata de nombres y direcciones Este documento trata de oficios y profesiones&#xa;&#xa;Oscar Mu&#xf1;os Alamos 123 Ingeniero&#xa;David llanos Gardenia 23 Adminnistrador&#xa;Israel M&#xe9;jia Venustiano 7 Inform&#xe1;tico&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_725461926" CREATED="1569030723909" MODIFIED="1569030791638"/>
<node TEXT="" ID="ID_1486635379" CREATED="1569032027233" MODIFIED="1569032027233"/>
</node>
<node TEXT="Une las lineas de dos archivos sobre una columna en com&#xfa;n" ID="ID_1143756646" CREATED="1569029321798" MODIFIED="1569029336762"/>
</node>
<node TEXT="Comando paste" FOLDED="true" ID="ID_608889549" CREATED="1569031081276" MODIFIED="1569031094137">
<node TEXT="Une dos archivos y no necesita de ningun campo en com&#xfa;n para realizar dicha tarea" FOLDED="true" ID="ID_1072089662" CREATED="1569031094151" MODIFIED="1569031117963">
<node TEXT="root@PC-L440:~/Documentos/prueba# paste file1 file2&#xa;# Este documento trata de nombres y direcciones # Este documento trata de oficios y profesiones&#xa;&#xa;Oscar Mu&#xf1;os Alamos 123  Oscar Ingeniero&#xa;David llanos Gardenia 23        David Adminnistrador&#xa;Israel M&#xe9;jia Venustiano 7       Israel Inform&#xe1;tico" ID="ID_529799971" CREATED="1569031355597" MODIFIED="1569031361025"/>
</node>
</node>
<node TEXT="Comando nl" FOLDED="true" ID="ID_781980892" CREATED="1569032105900" MODIFIED="1569032116362">
<node TEXT="Enumera las lineas de un archivo" FOLDED="true" ID="ID_233595108" CREATED="1569032116374" MODIFIED="1569032125769">
<node TEXT="root@PC-L440:~/Documentos/prueba# nl file1&#xa;     1  # Este documento trata de nombres y direcciones&#xa;&#xa;     2  Oscar Mu&#xf1;os Alamos 123&#xa;     3  David llanos Gardenia 23&#xa;     4  Israel M&#xe9;jia Venustiano 7" ID="ID_1882790342" CREATED="1569032176879" MODIFIED="1569032292498"/>
</node>
<node TEXT="Si quiere enumerar las lineas en blanco  use:" FOLDED="true" ID="ID_446749197" CREATED="1569032295630" MODIFIED="1569032310209">
<node TEXT="root@PC-L440:~/Documentos/prueba# nl -b a file1&#xa;     1  # Este documento trata de nombres y direcciones&#xa;     2&#xa;     3  Oscar Mu&#xf1;os Alamos 123&#xa;     4  David llanos Gardenia 23&#xa;     5  Israel M&#xe9;jia Venustiano 7&#xa;     6" ID="ID_174475106" CREATED="1569032310217" MODIFIED="1569032398419"/>
</node>
</node>
<node TEXT="Comando pr" FOLDED="true" ID="ID_231239730" CREATED="1569032422925" MODIFIED="1569032428133">
<node TEXT="Convierte los archivos de texto para impresi&#xf3;n" ID="ID_1281437749" CREATED="1569032428139" MODIFIED="1569032445166"/>
</node>
<node TEXT="Editor de texto sed" FOLDED="true" ID="ID_831995994" CREATED="1569032549140" MODIFIED="1569032560982">
<node TEXT="stream editor por sus siglas en ingl&#xe9;s" ID="ID_1328926761" CREATED="1569032560989" MODIFIED="1569032585681"/>
<node TEXT="Puede cambiar las letras de mayusculas a minusculas indicandole cuales&#xa;Para este ejemplo se cambian a por A en el archivo file1" FOLDED="true" ID="ID_1356191981" CREATED="1569032767042" MODIFIED="1569033152357">
<node TEXT="root@PC-L440:~/Documentos/prueba# sed &apos;s/a/A/g&apos; file1 &gt; file3&#xa;root@PC-L440:~/Documentos/prueba# cat file3&#xa;# Este documento trAtA de nombres y direcciones&#xa;&#xa;OscAr Mu&#xf1;os AlAmos 123&#xa;DAvid llAnos GArdeniA 23&#xa;IsrAel M&#xe9;jiA VenustiAno 7&#xa;&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_1853728473" CREATED="1569032872434" MODIFIED="1569033160248"/>
</node>
<node TEXT="Opciones&#xa;-d.- Deleted&#xa;-s.- Sustitute&#xa;-g.- Glovaly&#xa;-y.- Traslate" ID="ID_462415392" CREATED="1569033286929" MODIFIED="1569033350209"/>
</node>
</node>
<node TEXT="103.2 Procesamiento del flujo de texto parte3" FOLDED="true" POSITION="right" ID="ID_16675083" CREATED="1569351712594" MODIFIED="1569351739988">
<edge COLOR="#808080"/>
<node TEXT="Comando wc" FOLDED="true" ID="ID_321411705" CREATED="1569351739997" MODIFIED="1569352008570">
<node TEXT="Por sus siglas en ingl&#xe9;s conteo de palabras (word count)" FOLDED="true" ID="ID_791107264" CREATED="1569352008579" MODIFIED="1569353167174">
<node TEXT="Muestra el n&#xfa;mero total de nuevas lineas; palabras y bytes acerca de los archivos." FOLDED="true" ID="ID_1155022385" CREATED="1569353167980" MODIFIED="1569353656361">
<node TEXT="Opciones mas utiliz&#xe1;das&#xa;-c.- Identifica el n&#xfa;mero de caracteres dentro de un archivo.&#xa;-l.- Muestra la cantidad de nuevas lineas.&#xa;-w.- Identifica la cantidad de palabras dentro de un archivo." ID="ID_1077961230" CREATED="1569353688840" MODIFIED="1569353788755"/>
</node>
<node TEXT="Ejemplo:&#xa;root@PC-L440:~/Documentos/prueba# wc file3&#xa;  6  20 126 file3&#xa;root@PC-L440:~/Documentos/prueba# wc /etc/network/interfaces&#xa; 3 14 82 /etc/network/interfaces&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_1904437165" CREATED="1569353657206" MODIFIED="1569353663614"/>
</node>
</node>
<node TEXT="Comando sort" FOLDED="true" ID="ID_356034751" CREATED="1569353918580" MODIFIED="1569353929649">
<node TEXT="Toma el comienzo de cada linea de un archivo de texto y las reordena ya sea alfabeticamente o numeralmente." FOLDED="true" ID="ID_1484494971" CREATED="1569353931072" MODIFIED="1569354008455">
<node TEXT="Con la pci&#xf3;n -r invierte el orden, desde z hacia a y desde 9 hacia 0." ID="ID_1894683460" CREATED="1569354388431" MODIFIED="1569354433567"/>
</node>
<node TEXT="Ejemplo:&#xa;root@PC-L440:~/Documentos/prueba# sort file3&#xa;&#xa;&#xa;# Este documento trAtA de nombres y direcciones&#xa;DAvid llAnos GArdeniA 23&#xa;IsrAel M&#xe9;jiA VenustiAno 7&#xa;OscAr Mu&#xf1;os AlAmos 123&#xa;root@PC-L440:~/Documentos/prueba# sort /etc/network/interfaces&#xa;# interfaces(5) file used by ifup(8) and ifdown(8)&#xa;auto lo&#xa;iface lo inet loopback&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_1766374985" CREATED="1569354342168" MODIFIED="1569354348631"/>
</node>
<node TEXT="Comando uniq" FOLDED="true" ID="ID_482382951" CREATED="1569354563965" MODIFIED="1569354573410">
<node TEXT="Salta las lineas duplicadas" ID="ID_599315770" CREATED="1569354575333" MODIFIED="1569354590610"/>
<node TEXT="Excluye todos los grupos de renglones adyacentes duplicados menos uno (sirve para detectar l&#xed;neas repetidas de texto)." FOLDED="true" ID="ID_849944066" CREATED="1569354689640" MODIFIED="1569354701801">
<node TEXT="Algunas opciones del comando uniq son:&#xa;&#xa;-dimprime solo renglones duplicados.&#xa;&#xa;-u elimina las lineas consecutivas iguales.&#xa;&#xa;-c cuenta el n&#xfa;mero de ocurrencias de cada rengl&#xf3;n.&#xa;&#xa;-i considera iguales las may&#xfa;sculas y min&#xfa;sculas." ID="ID_1462723831" CREATED="1569354746146" MODIFIED="1569354756514"/>
</node>
</node>
<node TEXT="Comando tr" FOLDED="true" ID="ID_1643045477" CREATED="1569355086202" MODIFIED="1569355098108">
<node TEXT="Traduce y procesa los caracteres en una linea de texto." FOLDED="true" ID="ID_65467356" CREATED="1569355098113" MODIFIED="1569355113139">
<node TEXT="En el siguiente ejemplo muestra como cambiar los espacios en blanco por tabulaciones:&#xa;root@PC-L440:~/Documentos/prueba# cat file2 | tr &apos;&apos; &apos;\t&apos;&#xa;# Este documento trata de oficios y profesiones&#xa;&#xa;Oscar Ingeniero&#xa;David Adminnistrador&#xa;Israel Inform&#xe1;tico&#xa;root@PC-L440:~/Documentos/prueba#" FOLDED="true" ID="ID_866249979" CREATED="1569355113145" MODIFIED="1569355794872">
<node TEXT="Opciones:&#xa;&#xa;    -d Suprime los caracteres asignados en el argumento-l.&#xa;    -s Filtra los caracteres repetidos en la salida por aquellos especificados en el argumento-2, dejando en la salida solo uno de los caracteres repetidos.&#xa;    -c Sustituye en la salida todos los caracteres excepto los indicados en el argumento por los especificados en el argumento2. (Argumento2 debe ser un solo car&#xe1;cter)." ID="ID_262258966" CREATED="1569355794886" MODIFIED="1569355840091"/>
<node TEXT="tr [opciones] caracter_buscar caracter_reemplazar fichero_modificar &gt;nuevo_fichero Reemplaza globalmente una cadena/car&#xe1;cter dentro de un fichero.&#xa;echo &quot;laaaa casa&quot; | tr -s &apos;a&apos; Eliminara todas las repeticiones de la letra a dejando una &#xfa;nicamente (&quot;la casa&quot;)&#xa;tr -d &quot;\n&quot; &lt; documento Elimina el salto de linea del documento&#xa;tr -t &quot;\n&quot; &quot;-&quot; &lt; documento Reemplaza el salto de linea del documento por un guion (-)&#xa;echo &quot;casa&quot; | tr -dc &quot;a&quot; Devuelve &#xfa;nicamente la letras &apos;a&apos;.&#xa;echo &quot;casa&quot; | tr -dc &quot;a&quot; | wc -c Devuelve el numero de letras &apos;a&apos; que contiene el texto&#xa;echo &quot;casa&quot; | tr [:lower:] [:upper:] Convierte el texto que esta en min&#xfa;sculas a may&#xfa;sculas." ID="ID_1475506088" CREATED="1569356781503" MODIFIED="1569356785425"/>
</node>
</node>
</node>
<node TEXT="Comando split" FOLDED="true" ID="ID_472772896" CREATED="1569356794056" MODIFIED="1569356805598">
<node TEXT="DIvide archivos de gran tama&#xf1;o" FOLDED="true" ID="ID_989649187" CREATED="1569357007614" MODIFIED="1569357155015">
<node TEXT="&#xbf;Posees archivos que sobrepasan varios gigabytes y no sabes c&#xf3;mo partirlos en piezas m&#xe1;s peque&#xf1;as para tu CD de 700 MB? &#xbf;Tu servicio de almacenamiento de archivos -Megaupload, Gmail, Rapidshare- limita la cantidad de bytes que puedes subir? &#xbf;Est&#xe1;s harto de que Hacha no te permita partir archivos mayores a 2 GB? Bien, entonces creo que el comando Linux split es tu opci&#xf3;n.&#xa;&#xa;Split es software libre escrito por Torbjorn Granlund y Richard M. Stallman, disponible por defecto en pr&#xe1;cticamente toda distribuci&#xf3;n GNU/Linux gracias a las coreutils.&#xa;&#xa;Dividir&#xa;&#xa;En este ejemplo partimos un archivo de 176384515 bytes en piezas de 20MB (usando notaci&#xf3;n SI).&#xa;&#xa;$ split video.mp4 -b 20MB&#xa;&#xa;Por defecto, split crea archivos con prefijo &#xab;x&#xbb; y dos car&#xe1;cteres extra a partir de la letra &#xab;a&#xbb;, de modo que conserven un orden lexicogr&#xe1;fico.&#xa;&#xa;$ ls -hs&#xa;&#xa;169M video.mp4 20M xaa 20M xab 20M xac 20M xad 20M xae 20M xaf 20M xag 20M xah 16M xai" ID="ID_18909528" CREATED="1569357155022" MODIFIED="1569357163815"/>
<node TEXT="Unir&#xa;&#xa;Pegar las piezas no puede ser m&#xe1;s simple. Suponiendo archivos con prefijo &#xab;video&#xbb;:&#xa;&#xa;$ cat video* &gt; video.mp4&#xa;&#xa;El int&#xe9;rprete de comandos ordena autom&#xe1;ticamente los archivos con el prefijo &#xab;video&#xbb; antes de entregarlos al comando cat." ID="ID_589601114" CREATED="1569357166102" MODIFIED="1569357249108"/>
</node>
<node TEXT="Conclusiones&#xa;&#xa;Split es un alternativa sencilla para partir archivos de cualquier tama&#xf1;o, incluso mayores a 2 GB, y sin ser ineficiente en el uso de RAM y CPU. Cat conserva m&#xe1;s o menos el mismo rendimiento para pegar archivos." ID="ID_413686612" CREATED="1569357258223" MODIFIED="1569357279934"/>
</node>
<node TEXT="Comando more" FOLDED="true" ID="ID_573605762" CREATED="1569357323509" MODIFIED="1569357328737">
<node TEXT="Comando de tipo paginador ya que solo muestra una pagina a la vez" FOLDED="true" ID="ID_608171220" CREATED="1569357334889" MODIFIED="1569357351253">
<node TEXT="Ejemplo" FOLDED="true" ID="ID_624322363" CREATED="1569357468958" MODIFIED="1569357483328">
<node TEXT="root@PC-L440:~/Documentos/prueba# more /etc/passwd&#xa;root:x:0:0:root:/root:/bin/bash&#xa;daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin&#xa;bin:x:2:2:bin:/bin:/usr/sbin/nologin&#xa;sys:x:3:3:sys:/dev:/usr/sbin/nologin&#xa;sync:x:4:65534:sync:/bin:/bin/sync&#xa;games:x:5:60:games:/u" ID="ID_169033742" CREATED="1569357353265" MODIFIED="1569357467106"/>
</node>
</node>
</node>
<node TEXT="Comando less" FOLDED="true" ID="ID_1180402814" CREATED="1569357537237" MODIFIED="1569357543658">
<node TEXT="Similar al comando more pero posee la ventaja de utilizar las teclas de salto de p&#xe1;gina" ID="ID_997323951" CREATED="1569357543662" MODIFIED="1569357596664"/>
<node TEXT="Tambi&#xe9;n  permite la busqueda dentro de un archhivo." FOLDED="true" ID="ID_1066374021" CREATED="1569357615117" MODIFIED="1569357628840">
<node TEXT="Ejemplo" FOLDED="true" ID="ID_1260007689" CREATED="1569357632247" MODIFIED="1569357636949">
<node TEXT="#less /etc/passwd&#xa;a continuaci&#xf3;n le muestra el archivo, abajo aparece como una miniconsola.&#xa;escriba /mail por ejemplo si desea buscar la palabra mail o cualquiero otra" ID="ID_1879817641" CREATED="1569357636997" MODIFIED="1569358086605"/>
<node TEXT="La &#xfa;nica condici&#xf3;n es que inicie con una /" ID="ID_459731439" CREATED="1569358088098" MODIFIED="1569358106666"/>
</node>
</node>
</node>
</node>
<node TEXT="103.3 Manejo B&#xe1;sico de archivos parte1" FOLDED="true" POSITION="right" ID="ID_573839035" CREATED="1569444409313" MODIFIED="1569454028723">
<edge COLOR="#808080"/>
<node TEXT="Comando mkdir" FOLDED="true" ID="ID_675980088" CREATED="1569444446297" MODIFIED="1569451743786">
<node TEXT="Su funci&#xf3;n principal es crear un directorio" ID="ID_1614383503" CREATED="1569451743795" MODIFIED="1569452495958"/>
<node TEXT="Con la opci&#xf3;n pv se crea el directorio con toda la ruta" FOLDED="true" ID="ID_1767089389" CREATED="1569452498817" MODIFIED="1569452956944">
<node TEXT="Esta opci&#xf3;n es utilizada para crear jerarqu&#xed;as completas de directorios" ID="ID_678758386" CREATED="1569452999565" MODIFIED="1569453059244"/>
<node TEXT="Ejemplo #mkdir -pv paul/paulito/test" ID="ID_1817996625" CREATED="1569453061686" MODIFIED="1569453823371"/>
</node>
<node TEXT="Con la opci&#xf3;n -m especifica los permisos en formato octal" FOLDED="true" ID="ID_1879962355" CREATED="1569453834684" MODIFIED="1569453884999">
<node TEXT="Ejemplo #mkdir -m 600 file1" ID="ID_957755264" CREATED="1569453887194" MODIFIED="1569453904876"/>
</node>
<node TEXT="Con el uso de llaves se pueden crear varios directorios a la vez" FOLDED="true" ID="ID_226609130" CREATED="1569458285032" MODIFIED="1569458310170">
<node TEXT="Ejemplo #mkdir {dir1, dir2, dir3}" ID="ID_529517093" CREATED="1569458310177" MODIFIED="1569458599849"/>
<node TEXT="se pueden crear directorios y subdirectorios a la vez con la siguente sintaxis" FOLDED="true" ID="ID_1489646587" CREATED="1569459027856" MODIFIED="1569459049156">
<node TEXT="root@PC-L440:~/Documentos/prueba# mkdir -p blak/{yellow/orange/{green,blue},red,white}&#xa;root@PC-L440:~/Documentos/prueba# ls&#xa;blak  file1  file2  file3&#xa;root@PC-L440:~/Documentos/prueba# ls blak&#xa;red  white  yellow&#xa;root@PC-L440:~/Documentos/prueba# ls blak/yellow&#xa;orange&#xa;root@PC-L440:~/Documentos/prueba# ls blak/yellow&#xa;orange&#xa;root@PC-L440:~/Documentos/prueba# ls blak/yellow/orange&#xa;blue  green&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_541893501" CREATED="1569459053896" MODIFIED="1569459070847"/>
</node>
</node>
</node>
<node TEXT="Comando rmdir" FOLDED="true" ID="ID_652318102" CREATED="1569454412052" MODIFIED="1569454748398">
<node TEXT="Remueve directorios vacios y sus antecedentes en la jerarqu&#xed;a de directorios tambi&#xe9;n llamados directorios padre." FOLDED="true" ID="ID_1565567201" CREATED="1569454749494" MODIFIED="1569454885725">
<node TEXT="Ejemplo #rmdir -pv paul/paulito/test" ID="ID_141184151" CREATED="1569454964886" MODIFIED="1569455543911"/>
</node>
<node TEXT="Este comando no removera un directorio que no se encuentre vacio." ID="ID_1734243312" CREATED="1569455546312" MODIFIED="1569455562151"/>
</node>
<node TEXT="rm" FOLDED="true" ID="ID_313694604" CREATED="1569455663842" MODIFIED="1569455670734">
<node TEXT="Es utilizado para eliminar uno o mas archivos o directorios" FOLDED="true" ID="ID_1055069216" CREATED="1569455670742" MODIFIED="1569456456392">
<node TEXT="Opciones" FOLDED="true" ID="ID_687719810" CREATED="1569457120184" MODIFIED="1569457299342">
<node TEXT="-f --force.- Ignora archivos existentes y no efectua preguntas&#xa;-i --interactive.- Pregunta antes de remover cada archivo.&#xa;-r -R --recursive.- Remueve los directorios y su contenido recursivamente.&#xa;-v --vervose.- Muestra en detalle lo que se esta efectuando." ID="ID_1787315797" CREATED="1569457299348" MODIFIED="1569457757063"/>
<node TEXT="Ejemplo #rm-r file1" ID="ID_853162291" CREATED="1569457838034" MODIFIED="1569457852219"/>
</node>
</node>
<node TEXT="Cabe mencionar que el uso de este comando debe hacerse con sumo cuidado, ya que una vez que se borra en archivo o directorio no se puede recuperar." ID="ID_21381445" CREATED="1569456457394" MODIFIED="1569456637335"/>
</node>
<node TEXT="mv" FOLDED="true" ID="ID_53053463" CREATED="1569459202978" MODIFIED="1569459210669">
<node TEXT="Mueve archivos y directorios a otras ubicaciones" FOLDED="true" ID="ID_1248855248" CREATED="1569459210671" MODIFIED="1569459241698">
<node TEXT="Ejemplo:&#xa;root@PC-L440:~/Documentos/prueba# ls&#xa;{Archivo1,  Archivo1  Archivo2,  Archivo3}  blak  file2  file3&#xa;root@PC-L440:~/Documentos/prueba# cd Archivo1&#xa;root@PC-L440:~/Documentos/prueba# mv Archivo1 /home/israel/Documentos/prueba2&#xa;root@PC-L440:~/Documentos/prueba# ls&#xa;{Archivo1,  Archivo2,  Archivo3}  blak  file2  file3&#xa;root@PC-L440:~/Documentos/prueba2# pwd&#xa;/home/israel/Documentos/prueba2&#xa;root@PC-L440:~/Documentos/prueba2# ls&#xa;Archivo1" ID="ID_1944555209" CREATED="1569460721159" MODIFIED="1569460792847"/>
</node>
<node TEXT="Sirve tambi&#xe9;n para renombrar archivos" FOLDED="true" ID="ID_744624355" CREATED="1569459242089" MODIFIED="1569459308699">
<node TEXT="Ejemplo:&#xa;root@PC-L440:~/Documentos/prueba# ls&#xa;{Archivo1,  Archivo2,  Archivo3}  blak  file2  file3&#xa;root@PC-L440:~/Documentos/prueba# mv file2 file5&#xa;root@PC-L440:~/Documentos/prueba# ls&#xa;{Archivo1,  Archivo2,  Archivo3}  blak  file3  file5&#xa;root@PC-L440:~/Documentos/prueba#" ID="ID_1109349023" CREATED="1569461035746" MODIFIED="1569461044447"/>
</node>
<node TEXT="Opciones mas comunes del comando" FOLDED="true" ID="ID_1415382369" CREATED="1569459375049" MODIFIED="1569459399755">
<node TEXT="-f --force.- No pregunta antes de sobreescribir&#xa;-i --interactive.- Pregunta antes de sobreescribir&#xa;-v --verbose.- muestra en detalle lo que se esta efectuando." ID="ID_822871872" CREATED="1569459399759" MODIFIED="1569459513352"/>
</node>
</node>
<node TEXT="cp" FOLDED="true" ID="ID_1629389708" CREATED="1569461186580" MODIFIED="1569461190626">
<node TEXT="Este comando sirve para copiar archivos y directorios" ID="ID_471656226" CREATED="1569461190632" MODIFIED="1569461565222"/>
<node TEXT="Se puede usar para copiar archivos o directorios como se mencion&#xf3; anteriormente o copiar el contenido de un archivo a otro." FOLDED="true" ID="ID_496997169" CREATED="1569461565353" MODIFIED="1569462750845">
<icon BUILTIN="full-1"/>
<node TEXT="Ejemplo:&#xa;#cp -r directorio1  /tmp" ID="ID_275070653" CREATED="1569462115729" MODIFIED="1569462404967"/>
<node TEXT="Ejemplo2:&#xa;#cp date1.sh date2.sh" ID="ID_1969357363" CREATED="1569462442909" MODIFIED="1569462664204"/>
</node>
<node TEXT="Opciones:&#xa;-d --force.- Preserva los v&#xed;nculos.&#xa;-i --interactive.- Pregunta antes de sobreescribir.&#xa;-p.- Preserva los modos como los propietarios y las estampas de tiempo.&#xa;-r -R --recursive.- Copia directorios recursivamente.&#xa;-v --verbose.- Muestra en detalle lo que se esta efectuando." ID="ID_504406492" CREATED="1569462779593" MODIFIED="1569463013615"/>
</node>
<node TEXT="ls" FOLDED="true" ID="ID_1422789231" CREATED="1569463018163" MODIFIED="1569463022279">
<node TEXT="Muestra el contenido de un directorio" FOLDED="true" ID="ID_900060970" CREATED="1569463022284" MODIFIED="1569463032618">
<node TEXT="Opciones&#xa;-a.- Muestra archivos ocultos&#xa;-i.- Muestra los n&#xfa;meros inode de los archivos.&#xa;-l.- Muestra una laga lista detallada.&#xa;-r:- Reinvierte el orden.&#xa;-R --Recursive.-Muestra el contenido de los directorios.&#xa;-t.- Ordena por la fecha de modificaci&#xf3;n." ID="ID_1879304984" CREATED="1569463032623" MODIFIED="1569463366199"/>
</node>
</node>
<node TEXT="fine" FOLDED="true" ID="ID_114693563" CREATED="1569463412304" MODIFIED="1569463425864">
<node TEXT="Busca archivos en el &#xe1;rbol de directorios" FOLDED="true" ID="ID_1701501128" CREATED="1569463425871" MODIFIED="1569463479213">
<node TEXT="Existen diferentes par&#xe1;metros a tomar encuenta para las busquedas" FOLDED="true" ID="ID_1022405122" CREATED="1569463479218" MODIFIED="1569463662136">
<node TEXT="En el siguiente ejemplo se buscaran todos los ficheros con la terminaci&#xf3;n .conf del directorio /etc&#xa;root@PC-L440:~/Documentos/prueba# find /etc -name &quot;*.conf&quot;&#xa;/etc/UPower/UPower.conf&#xa;/etc/hdparm.conf&#xa;/etc/wodim.conf&#xa;/etc/signond.conf&#xa;/etc/pulse/daemon.conf&#xa;/etc/pulse/client.conf&#xa;/etc/pulse/client.conf.d/00-disable-autospawn.conf&#xa;/etc/debconf.conf&#xa;/etc/dhcp/dhclient.conf" ID="ID_501828716" CREATED="1569463663732" MODIFIED="1569463978703"/>
<node TEXT="En el sigueinte ejemplo usaremos el comando find para identificar a un archivo por su tipo.&#xa;# find /-type d -name samba" ID="ID_1702423874" CREATED="1569463980492" MODIFIED="1569464429633"/>
<node TEXT="Usamos el par&#xe1;metro -type d, diciendo as&#xed; a find que busque ficheros del tipo &#xab;d&#xab;, que son directorios.&#xa;&#xa;find / -type d -name &quot;apt&quot;" ID="ID_647030324" CREATED="1569464603880" MODIFIED="1569464607034"/>
<node TEXT="En el siguiente ejemplo muestra los archivos que han sido modificados hace dos d&#xed;as:&#xa;#find /etc -name &quot;*.conf&quot; -mtime1" ID="ID_1451856210" CREATED="1569465289661" MODIFIED="1569465364239"/>
<node TEXT="En el siguiente ejemplo se buscaran archivos mayores a 2MB:&#xa;#find /etc -size +2M" ID="ID_297397029" CREATED="1569465439432" MODIFIED="1569465518153"/>
</node>
<node TEXT="Opciones" FOLDED="true" ID="ID_1274642651" CREATED="1569464780108" MODIFIED="1569464785229">
<node TEXT="-name Busca dicho archivo, en el directorio actual o en cualquier subdirectorio.&#xa;-atime n Verdadero si se accedi&#xf3; al archivo hace n d&#xed;as. El tiempo de acceso de los directorios en la ruta se cambia por el find mismo.&#xa;-ctime n Verdadero si el estado del archivo se cambi&#xf3; hace n d&#xed;as.&#xa;-group gname Verdadero si el archivo pertenece al grupo gname. Si gname es num&#xe9;rico y no aparece en el archivo /etc/group, se toma como identificador de grupo.&#xa;-mtime n Verdadero si la informaci&#xf3;n del archivo fue modificada hace n d&#xed;as.&#xa;-size n[c] Verdadero si el archivo tiene n bloques de largo (512 octetos por bloque). Si n va seguida de una c, el tama&#xf1;o es en octetos." ID="ID_363443820" CREATED="1569464788817" MODIFIED="1569464792082"/>
</node>
<node TEXT="Tipos de archivo vara el comando find:&#xa;b.- Bloque mayormente asociado con dispositivos de almacenamiento.&#xa;c.- C&#xe1;racter.&#xa;d.-Directorio.&#xa;p.- Tuberia nombrada FIFO.&#xa;f.-Archivo regular.&#xa;l.-V&#xed;nculo simb&#xf3;lico." ID="ID_1879992897" CREATED="1569465079258" MODIFIED="1569465195550"/>
</node>
</node>
</node>
<node TEXT="103.3 Manejo B&#xe1;sico de archivos parte2" FOLDED="true" POSITION="right" ID="ID_410152050" CREATED="1569949243705" MODIFIED="1569949277583">
<edge COLOR="#808080"/>
<node TEXT="Comando Touch" FOLDED="true" ID="ID_400002219" CREATED="1569949284689" MODIFIED="1569949292203">
<node TEXT="El comando touch puede modificar la fecha en que este fue modificado por &#xfa;ltima vez. Para ello existen tres opciones que son normalmente utilizadas." FOLDED="true" ID="ID_246444170" CREATED="1569949292209" MODIFIED="1569949390092">
<node TEXT="-a.- Cambia la fecha del ultimo acceso.&#xa;-m.- Cambia la fecha de la &#xfa;ltima modificaci&#xf3;n.&#xa;-t,. Permite modificar la hora y fecha en particular usando el formato YYMMDDhhmm estos caracteres respresentan el a&#xf1;o, el mes, el dia la hora y los minutos." ID="ID_1452972516" CREATED="1569949390102" MODIFIED="1569949565254"/>
<node TEXT="Ejempo: #touch -m traslate.tx" ID="ID_472755165" CREATED="1569949925908" MODIFIED="1569949951873"/>
</node>
<node TEXT="Tambi&#xe9;n es usado para crear archivos vac&#xed;os" FOLDED="true" ID="ID_1105006037" CREATED="1569949957154" MODIFIED="1569950156096">
<node TEXT="Ejemplo: #touch var/log/Paul_logs&#xa;Se puede verifiar con el comando&#xa;#cd /var/logs/; ls -ltr" ID="ID_1872262828" CREATED="1569950159224" MODIFIED="1569950316277"/>
</node>
</node>
<node TEXT="Comando tar" FOLDED="true" ID="ID_570168073" CREATED="1569951193694" MODIFIED="1569952606711">
<node TEXT="Ultilidad que permite agrupar un conjunto de archivos en un mismo archivo, con las opciones adecuadas tambi&#xe9;n puede comprimir o descomprimir un archivo" FOLDED="true" ID="ID_1899557848" CREATED="1569951199163" MODIFIED="1569951662211">
<node TEXT="Ejemplo: vaya a una carpeta donde haya varios archivos a comprimir&#xa;#tar cvzf  archivo-a-comprimir.tar.gz&#xa;Para descomprimir el comando queda de la siguiente forma:&#xa;#tar xvzf archivo-a-descomprimir.tar.gz" ID="ID_1652716586" CREATED="1569951662217" MODIFIED="1569952097422"/>
</node>
<node TEXT="Opciones m&#xe1;s usadas del comando tar:&#xa;-c.- Permite crear un archivo.&#xa;-f Permite asignar un nombre al archivo tar creado.&#xa;-j.- Permite comprimir o descomprimir un archivo usando el formato bzip2.&#xa;-r.- Permite agregar una archivo al archivo tar existente.&#xa;-t.- Muestra todos los archivos a un archivo tar.&#xa;-u.- Actualiza un archivo tar existente.&#xa;-v.- Muestra la operacion efectuada de forma detallada.&#xa;-z.-Permite comprimir o descomprimir un archivo utilizando el formato gzip." ID="ID_972676938" CREATED="1569952100327" MODIFIED="1569952414519"/>
</node>
<node TEXT="Comando cpio" FOLDED="true" ID="ID_1456798747" CREATED="1569952614527" MODIFIED="1569954198613">
<node TEXT="Es utilizado para crear o extraer archivos o para copiar archivos de un directorio a otro." ID="ID_1371719369" CREATED="1569952630863" MODIFIED="1569952775326"/>
<node TEXT="La diferencia entre este comando y el comando tar, es que tar no posee opcioes para comprimir, por lo que se usan herramientas distintas para comprimir en bzip  o gzip por separado." FOLDED="true" ID="ID_1428096393" CREATED="1569952775758" MODIFIED="1569953409154">
<node TEXT="Ejemplo:&#xa;&#xa;find . -print | cpio -ocv &gt; /dev/sda2&#xa;Encuentra la lista de archivos y directorios y los copia al la memoria o disco externo.&#xa;&#xa;find . -print | cpio -dumpv /home/nirmala&#xa;Encuentra la lista de archivos y directorios y los copia o hace una copia de seguridad al usuario.&#xa;&#xa;cpio -icuvd &lt; /dev/dda2&#xa;&#xa;Recupera los archivos del desco externo." ID="ID_1424335197" CREATED="1569953411952" MODIFIED="1569953650565"/>
</node>
<node TEXT="El comando cpio sirve para copiar o extraer archivos a un contenedor .cpio . Este es un archivo que contiene archivos y permisos que fueron almacenados. El contenedor se puede guardar en una unidad de cinta, en el disco o simplemente en un archivo de texto como ahora les voy a demostrar." FOLDED="true" ID="ID_1264743881" CREATED="1569954179767" MODIFIED="1569954190323">
<node TEXT="# find /home/beagle |cpio -o &gt; backup.cpio&#xa;Con este comando lo que estamos haciendo es un backup del home del usuario, vamos parte por parte asi entendemos como funciona.&#xa;&#xa;find /home/beagle = lista todos los archivos y se los pasa por stdin a cpio&#xa;cpio -o &gt; backup.cpio = almacena en un archivo backup.cpio todos los archivos listados" ID="ID_1731171412" CREATED="1569954274342" MODIFIED="1569954361543"/>
<node TEXT="Algunos detalles a tenes en cuenta es que use find y no ls , porque ls no muestra la ruta completa , muesta solo el nombre del archivo.&#xa;&#xa;Si queremos listar el contenido de un contenedor&#xa;$ cpio -t &lt; backup.cpio&#xa;&#xa;Si deseamos extraer todos los archivos de un contenedor&#xa;$ cpio -i &lt; backup.cpio&#xa;&#xa;Si queremos solo extraer por ejemplo un archivo&#xa;$ echo &quot;/home/beagle/Documentos.txt&quot; |cpio -i &lt; backup.cpio&#xa;&#xa;Con esto le pasamos que archivo es el que vamos a descomprimir, por ejemplo Documentos.txt" ID="ID_1339078211" CREATED="1569954473938" MODIFIED="1569954478138"/>
</node>
<node TEXT="Opciones principales:&#xa;OPCIONES:&#xa;&#xa;-i Extrae archivos del entrada est&#xe1;ndar.&#xa;-o Lee el entrada est&#xe1;ndar para obtener una lista de nombres de ruta y copia esos archivos en el output est&#xe1;ndar.&#xa;-p Lee el entrada est&#xe1;ndar para obtener una lista de nombres de ruta de archivos.&#xa;-c Lee o escribe la informaci&#xf3;n de la cabecera en ASCII para portabilidad.&#xa;-d Crea directorios cuando sea necesario.&#xa;-u Copia incondicionalmente (normalmente, un archivo viejo no replazar&#xe1; un archivo nuevo con el mismo nombre).&#xa;-m Mantiene la fecha de modificaci&#xf3;n de archivo anterior. Esta opci&#xf3;n es in&#xfa;til en directorios que est&#xe1;n siendo copiados.&#xa;-v Verbose. Muestra una lista de nombres de archivo." ID="ID_1749393422" CREATED="1569953382388" MODIFIED="1569953393784"/>
</node>
<node TEXT="Comando dd" FOLDED="true" ID="ID_747133554" CREATED="1569954725116" MODIFIED="1569954753083">
<node TEXT="El comando dd (Dataset Definition), es una herramienta sencilla, &#xfa;til, y sorprendentemente f&#xe1;cil de usar; con esta herramienta se puede hacer lo mismo, sobre dispositivos: discos y particiones, que con programas comerciales como Norton Ghost, en Windows o libres como CloneZilla, en Linux, con solo una peque&#xf1;a l&#xed;nea de comandos." FOLDED="true" ID="ID_1063502482" CREATED="1569954753087" MODIFIED="1569954764738">
<node TEXT="Sintaxis B&#xe1;sica:&#xa;&#xa;La sintaxis mas b&#xe1;sica para el uso del comando dd, seria esta:&#xa;&#xa;sudo dd if=origen of=destino&#xa;&#xa;Donde if significa &#x201c;input file=archivo de entrada&#x201c;, es decir, lo que se quiere copiar y of significa &#x201c;output file=archivo de salida&#x201c;, o sea, el archivo destino (donde se van a copiar los datos);origen y destino pueden ser dispositivos (lectora de CD o DVD, disco duro, diskettera, pendrive, partici&#xf3;n, etc.), archivo de copia de seguridad o imagen de disco, etc, pero no carpetas o subcarpetas." FOLDED="true" ID="ID_697481725" CREATED="1569954943807" MODIFIED="1569954952200">
<node TEXT="Ejemplo:&#xa;dd if=/dev/sda1 of=/home/paul/boot.img&#xa;Se crea una imagen de la partici&#xf3;n boot" ID="ID_236805776" CREATED="1569955073804" MODIFIED="1569955298498"/>
<node ID="ID_1337513368" CREATED="1569955376026" MODIFIED="1569957169177"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejemplo para clonar un disco duro:
    </p>
    <p>
      sudo dd if=/dev/sda |pv|dd of=/dev/sdb bs=1M
    </p>
    <p>
      
    </p>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      Crear una imagen &#x2013; puede ser bin o iso &#x2013; del disco duro (Sda) , en el directorio /home:
    </p>
    <p>
      <code>sudo dd if=/dev/sda |pv|dd of=/home/sda.bin </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=/dev/zero |pv|dd of=/dev/sdx (Borrado de disco completo) </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>Para crear la imagen .iso de un CD en el directorio /home: </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=/dev/cdrom |pv|dd of=/home/imagendeCD.iso </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>&#xa0;Copiar/Restaurar el Master Boot Record (MBR): </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>Para copiar el MBR: </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=/dev/hda |pv|dd of=mbr count=1 bs=512 </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>Para restaurar el MBR: </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=mbr |pv|dd of=/dev/hda </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>= Limpiar nuestro MBR y la tabla de particiones: </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=/dev/zero |pv|dd of=/dev/hda bs=512 count=1 </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>= Limpia el MBR pero no toca la tabla de particiones, ( muy &#xfa;til para borrar el GRUB sin perder datos en las particiones): </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>sudo dd if=/dev/zero |pv|dd of=/dev/hda bs=446 count=1 </code>
    </p>
    <p>
      
    </p>
    <p>
      <code>&#xa0;</code>
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Para mayor referencia  vea https://blog.desdelinux.net/uso-del-comando-dd/" ID="ID_726051548" CREATED="1569957210696" MODIFIED="1569957229836"/>
</node>
<node TEXT="Sintaxis con el comando pv: Usar el comando dd con la sintaxis anterior tiene un peque&#xf1;o inconveniente, ya que es un comando muy reservado &#x2013; no da informaci&#xf3;n &#x2013; , pues al ejecutarlo, el prompt de la terminal queda inm&#xf3;vil, por lo que no sabemos que es lo que esta pasando y cuanto tiempo falta para que termine de ejecutarse. Este peque&#xf1;o inconveniente se puede solucionar a&#xf1;adiendo el comando pv, ( *) &#x2013; el cual act&#xfa;a como una tuber&#xed;a de terminal que mide los datos que pasan a trav&#xe9;s de ella- a la sintaxis del comando dd , de forma que ahora la sintaxis seria:&#xa;&#xa;dd if=origen |pv|dd of=destino&#xa;&#xa;Como resultado obtendr&#xed;amos en el terminal una especie de barra de progreso, la informaci&#xf3;n sobre bytes transferidos, el tiempo que lleva ejecut&#xe1;ndose y la tasa de transferencia, todo esto en tiempo real." ID="ID_1954485500" CREATED="1569954954506" MODIFIED="1569955372768"/>
</node>
</node>
<node TEXT="Comando file" FOLDED="true" ID="ID_753223931" CREATED="1569957269414" MODIFIED="1569957275347">
<node TEXT="Linux no requiere de extensiones para determinar el tipo de archivo, se puede utilizar el comando file para determinar que clase de archivo es." FOLDED="true" ID="ID_651532974" CREATED="1569957275351" MODIFIED="1569957321386">
<node TEXT="Ejemplo:&#xa;#file *" ID="ID_1768170909" CREATED="1569957344013" MODIFIED="1569957369903"/>
</node>
</node>
</node>
<node TEXT="103.3 Manejo b&#xe1;sico de archivos parte3" FOLDED="true" POSITION="right" ID="ID_990405966" CREATED="1571081728262" MODIFIED="1571081748736">
<edge COLOR="#808080"/>
<node TEXT="gzip" FOLDED="true" ID="ID_576418130" CREATED="1571081750154" MODIFIED="1571081769408">
<node TEXT="Por lo general los archivos comprimidos mediante gzip reciben la terminaci&#xf3;n .gz" FOLDED="true" ID="ID_1567245249" CREATED="1571081769417" MODIFIED="1571081840211">
<node TEXT="Las opciones m&#xe1;s utilizadas son las siguientes:&#xa;-d.- Para descomprimimr esta aplica unicamente al comando gzip&#xa;-f.- Fuerza la compresi&#xf3;n o descompresi&#xf3;n a&#xfa;n si el archivo posee varios v&#xed;nculos po si el archivo ya existe.&#xa;-r.- Comprime o descomprime recursivamente.&#xa;-v.- Detalla la tarea a realizar." FOLDED="true" ID="ID_52234238" CREATED="1571081916629" MODIFIED="1571082109060">
<node TEXT="Ejemplo:&#xa;#gzip boot.img&#xa;boot.img.gz" ID="ID_1554648316" CREATED="1571082266621" MODIFIED="1571082336273"/>
</node>
</node>
</node>
<node TEXT="gunzip" FOLDED="true" ID="ID_663712281" CREATED="1571081848444" MODIFIED="1571081858741">
<node TEXT="Remueve la extensi&#xf3;n .gz al momento de comprimir los archivos" FOLDED="true" ID="ID_1116795266" CREATED="1571081858748" MODIFIED="1571081888850">
<node TEXT="Las opciones m&#xe1;s utilizadas son las siguientes:&#xa;-d.- Para descomprimimr esta aplica unicamente al comando gzip&#xa;-f.- Fuerza la compresi&#xf3;n o descompresi&#xf3;n a&#xfa;n si el archivo posee varios v&#xed;nculos po si el archivo ya existe.&#xa;-r.- Comprime o descomprime recursivamente.&#xa;-v.- Detalla la tarea a realizar." FOLDED="true" ID="ID_1970222082" CREATED="1571082120657" MODIFIED="1571082124992">
<node TEXT="Ejemplo:&#xa;#gunzip boot.img.gz&#xa;boot.img" ID="ID_1425610124" CREATED="1571082341760" MODIFIED="1571082413228"/>
</node>
</node>
</node>
<node TEXT="bzip2" FOLDED="true" ID="ID_75644165" CREATED="1571082446131" MODIFIED="1571082464328">
<node TEXT="Comprime archivos basado en el algoritmo de ordenamiento por bloques" FOLDED="true" ID="ID_607664408" CREATED="1571082464334" MODIFIED="1571110255332">
<node TEXT="Genera una extension bz2 al momento de comprimir los archivos" FOLDED="true" ID="ID_887114289" CREATED="1571111485240" MODIFIED="1571111534970">
<node TEXT="Ejemplo:&#xa;#bzip2 boot.img&#xa;#ls&#xa;#boot.img.bz2" ID="ID_1736114604" CREATED="1571111534976" MODIFIED="1571111670822"/>
</node>
<node TEXT="Opciones mas frecuentes:&#xa;-f.- Fuerza la compresi&#xf3;n o descompresi&#xf3;n a&#xfa;n si el archivo posee varios v&#xed;nculos po si el archivo ya existe.&#xa;-r.- Comprime o descomprime recursivamente.&#xa;-v.- Detalla la tarea a realizar.&#xa;-z.- Para comprimir, esta opci&#xf3;n solo aplica para bzip2" ID="ID_1177242267" CREATED="1571112664460" MODIFIED="1571112731469"/>
</node>
</node>
<node TEXT="bunzip2" FOLDED="true" ID="ID_355229053" CREATED="1571110157001" MODIFIED="1571110167605">
<node TEXT="Descomprime archivos basados en algoritmos por bloques" FOLDED="true" ID="ID_1901200985" CREATED="1571110261531" MODIFIED="1571110288621">
<node TEXT="Quita la extensi&#xf3;n bz2 al momento de descomprimir" FOLDED="true" ID="ID_1359390154" CREATED="1571111679791" MODIFIED="1571111709048">
<node TEXT="Ejemplo:&#xa;#bunzip boot.img.bz2&#xa;#ls&#xa;#boot.img" ID="ID_776224952" CREATED="1571111709052" MODIFIED="1571111768230"/>
</node>
<node TEXT="Opciones mas frecuentes:&#xa; -f.- Fuerza la compresi&#xf3;n o descompresi&#xf3;n a&#xfa;n si el archivo posee varios v&#xed;nculos po si el archivo ya existe.&#xa; -r.- Comprime o descomprime recursivamente.&#xa; -v.- Detalla la tarea a realizar.&#xa;-z.- Para comprimir, esta opci&#xf3;n solo aplica para bzip2" ID="ID_1053035488" CREATED="1571113401797" MODIFIED="1571113504468"/>
</node>
</node>
<node TEXT="Caracteres Glovales o Wildcards" FOLDED="true" ID="ID_1264625805" CREATED="1571113516524" MODIFIED="1571113568216">
<node TEXT="Se podr&#xed;a decir que los wildcards son caracteres de abreviaci&#xf3;n." ID="ID_1762440286" CREATED="1571113575167" MODIFIED="1571166890027"/>
<node TEXT="Sirven para realizar operaciones en un grupo de ficheros por ejemplo:&#xa;si quieres borrar todos los ficheros que empiezan con &#x201c;pal&#x201d; y terminan con &#x201c;txt&#x201d;, se puede usar el car&#xe1;cter de asterisco para poder realizar esta tarea. El comando ser&#xed;a algo as&#xed;: rm pal*txt para poder borrar este tipo de ficheros (el comando rm es usado para borrar, aunque solo se est&#xe1; poniendo de ejemplo). Lo que hace el asterisco * es decirle al shell de Linux que encuentre cualquier fichero que empiece con &#x201c;pal&#x201d; y que termine con &#x201c;txt&#x201d; independientemente del n&#xfa;mero de caracteres entre ellos. Incluso encontrar&#xe1; ficheros son caracteres entre estos dos t&#xe9;rminos, como por ejemplo &#x201c;paltxt&#x201d; . Por tanto, un fichero que se llama pal234.txt ser&#xe1; tambi&#xe9;n eliminado." ID="ID_208559758" CREATED="1571166890554" MODIFIED="1571167453518"/>
<node TEXT="Las operaciones mas comunes son:&#xa;*.- Corresponde a 0 m&#xe1;s caracteres &quot;*ab&quot; corresponder&#xe1; a aab, lab, aaab, 1ab y as&#xed; sucesivamente.&#xa;?.- Corresponde exactamente a un caracter &quot;?ab&quot;  corresponder&#xe1; a cab, 1ap y as&#xed; sucesivamente.&#xa;[at].- Corresponde a todosa los caracteres dentro de los corchetes. &quot;[at]ab&quot; corresponder&#xe1; a aab y tab.&#xa;[!c-z].- Corresponde a todos los caracteres menos al rango determinado; por lo tanto [!c-z]ab correspondera a aab y bab." ID="ID_1019398650" CREATED="1571167457256" MODIFIED="1571168342970"/>
</node>
</node>
<node TEXT="103.4 Utilizaci&#xf3;n de streams, pipes y caracteres de redireccionamiento" FOLDED="true" POSITION="right" ID="ID_634654936" CREATED="1572133621812" MODIFIED="1572133688641">
<edge COLOR="#808080"/>
<node TEXT="La escencia de gnu/ linux son archivos de textos, y estos se pueden redireccionar su informaci&#xf3;n en otros archivos, para poder entender estas tareas primero debemos saber como linux manipula los archivos de texto." FOLDED="true" ID="ID_1035858608" CREATED="1572133690276" MODIFIED="1572140188316">
<node TEXT="Entrada est&#xe1;ndar" FOLDED="true" ID="ID_1806657578" CREATED="1572140188330" MODIFIED="1572140213719">
<node TEXT="Comunmente llamada Stdin (0) recibe la informaci&#xf3;n probeniente de los dispositivos de entrada como el teclado, y redirecciona normalmente a los programas." FOLDED="true" ID="ID_1305401786" CREATED="1572140215102" MODIFIED="1572140317622">
<node TEXT="Ejemplo:&#xa;#less &lt; /etc/passwd" ID="ID_1312805986" CREATED="1572141908254" MODIFIED="1572142013085"/>
</node>
<node TEXT="Su caracter descriptivo es el n&#xfa;mero 0 ." ID="ID_1004079111" CREATED="1572140318233" MODIFIED="1572140360638"/>
</node>
<node TEXT="Salida estandar" FOLDED="true" ID="ID_1542667001" CREATED="1572140363171" MODIFIED="1572140380024">
<node TEXT="Comunmente llamada stdout (1) es donde los programas escriben su salida o resultado, esta informaci&#xf3;n se muestra en pantalla, ya sea pantalla de texto o una interfaz gr&#xe1;fica." FOLDED="true" ID="ID_1386126652" CREATED="1572140380030" MODIFIED="1572141830510">
<node TEXT="Ejemplo:&#xa;#ls -la /etc&#xa;En el siguiente ejemplo redirecccionaremos la salida estanadar a un archivo de texto:&#xa;&quot;find /etc -name  &apos;*.html&apos; &gt; test.txt" ID="ID_1646829691" CREATED="1572141873362" MODIFIED="1572142154457"/>
</node>
<node TEXT="El caracter descriptivo de la salida standar es el n&#xfa;mero 1." ID="ID_852922752" CREATED="1572143195542" MODIFIED="1572143215910"/>
</node>
<node TEXT="Salida de errores" FOLDED="true" ID="ID_257760764" CREATED="1572232767958" MODIFIED="1572232807278">
<node TEXT="Stderr (2)" FOLDED="true" ID="ID_1014583518" CREATED="1572232807309" MODIFIED="1572232849111">
<node TEXT="Es una salida utilizada por los programas para mostrar los mensajes de error o de diagnostico." ID="ID_650461287" CREATED="1572232850178" MODIFIED="1572232919839"/>
<node TEXT="Es independiente de la salida estandar, por lo tanto esta puede redireccionada individualmente." FOLDED="true" ID="ID_47866168" CREATED="1572232920432" MODIFIED="1572233049526">
<node TEXT="#find /etc -name &apos;*.html&apos; 2&gt; error.txt | less&#xa;En este comando los mensajes de error son redireccionados al archivo error.txt y la salida est&#xe1;ndar es redireccionada a la entrada estandar del comando less mediante la utilizaci&#xf3;n del caracter pipe." ID="ID_153150315" CREATED="1572233051575" MODIFIED="1572233281869"/>
</node>
<node TEXT="El caracter descriptivo es el n&#xfa;mero 2." ID="ID_759974859" CREATED="1572233291201" MODIFIED="1572233300448"/>
<node TEXT="Para escribir la salida estandar y la salida de errores a un mismo archivo debemos utilizar el siguente comando:" FOLDED="true" ID="ID_488408717" CREATED="1572233333284" MODIFIED="1572233367703">
<node TEXT="#find /etc -name &apos;*.html&apos; &gt; error_out.txt 2&gt;&amp;1&#xa;#less error_out.txt" ID="ID_301689434" CREATED="1572233368940" MODIFIED="1572233510401"/>
</node>
</node>
</node>
<node TEXT="Caracteres de redirecci&#xf3;n de datos." FOLDED="true" ID="ID_55805096" CREATED="1572233520601" MODIFIED="1572233532846">
<node TEXT="&gt;.- Crea un nuevo archivo conteniendo la salida est&#xe1;ndar. Si el archivo especificado existe este ser&#xe1; sobre escrito.&#xa;&gt;&gt;.- Agrega la salida est&#xe1;ndar al archivo existente. SI el archivo espeficicado no existe, este ser&#xe1; creado.&#xa;2&gt;.- Crea un nuevo archivo conteniedo la salida de errores. Si el archivo especificado existe, este ser&#xe1; sobreescrito.&#xa;2&gt;&gt;.- Agrega la salida de errores al archivo existente. Si el archivo especificado no existe, ese ser&#xe1; creado.&#xa;&amp;&gt;.- Crea un nuevo archivo conteniendo la salida est&#xe1;ndar y la salida de errores. Si el archivo especificado existe, este ser&#xe1; sobreescrito.&#xa;2&gt;&amp;1.- Tambi&#xe9;n envia la salida est&#xe1;ndar y la salida de errores a un mismo archivo.&#xa;&lt;.- Env&#xed;a el contenido especifico de un archivo  a la entrada est&#xe1;ndar.&#xa;&lt;&lt;.- Acepta el texto de las lineas consiguientes como una entrada st&#xe1;ndar.&#xa;|.- El caracter pipe enc&#xed;a la salida estandar de un programa a la entrada est&#xe1;ndar de otro programa." ID="ID_283776646" CREATED="1572233532851" MODIFIED="1572234422525"/>
</node>
<node TEXT="El comando tee" FOLDED="true" ID="ID_246064638" CREATED="1572234456385" MODIFIED="1572234462188">
<node TEXT="Lee la informaci&#xf3;n de la entrada est&#xe1;ndar y la escribe en la salida est&#xe1;ndar o en los archivos de texto." FOLDED="true" ID="ID_702779295" CREATED="1572234462196" MODIFIED="1572234565610">
<node TEXT="Ejemplo:&#xa;#find /etc -name &apos;*.html&apos; | tee test1.txt&#xa;Env&#xed;a el resultado de la b&#xfa;squeda y los mensajes de error a la salida est&#xe1;ndar. pero solamente los resultados de la busqueda son finalmente enviados al archivo test1.txt&#xa;#less test1.txt&#xa;##find /etc -name &apos;*.html&apos; 2&gt;&amp;1 | tee test2.txt&#xa;El resultado de la busqueda y los mensajes de error son enviados a la salida est&#xe1;ndar, en donde os resultados de la busqueda como la salida de errores son enviados al archivo text2.txt&#xa;#less text2.txt" ID="ID_97077661" CREATED="1572234567169" MODIFIED="1572235089101"/>
</node>
</node>
<node TEXT="Comando xargs" FOLDED="true" ID="ID_1709817676" CREATED="1572235099167" MODIFIED="1572235107894">
<node TEXT="Construye y ejecuta lineas del comando de la entrada est&#xe1;ndar" ID="ID_733127114" CREATED="1572235107901" MODIFIED="1572235204606"/>
<node TEXT="Permite tomar la entrada est&#xe1;ndar por lo tanto la eliminaci&#xf3;n de los archivos es ahora posible con el siuiente comando:" FOLDED="true" ID="ID_1348804349" CREATED="1572235204948" MODIFIED="1572235235061">
<node TEXT="#ls *html | xargs rm" ID="ID_1916962893" CREATED="1572235236343" MODIFIED="1572235292712"/>
</node>
</node>
</node>
</node>
<node TEXT="103.5 creaci&#xf3;n, monitoreo y terminaci&#xf3;n de procesos parte 1" FOLDED="true" POSITION="right" ID="ID_11259015" CREATED="1572721615115" MODIFIED="1572721785914">
<edge COLOR="#808080"/>
<node TEXT="Comandos bg y fg" FOLDED="true" ID="ID_1205322238" CREATED="1572721792150" MODIFIED="1572722630459">
<node TEXT="Cuando un usuario ejecuta un proceso ya sea por la ejecuci&#xf3;n de un programa, el proceso se puede ejecitar en primer plano y segundo plano." ID="ID_172877100" CREATED="1572722631729" MODIFIED="1572722972143"/>
<node TEXT="Si el proceso que se ejecuta toma el control de su terminal o shell , se puede decir que este proceso se est&#xe1; ejecutando en primer plano." FOLDED="true" ID="ID_1703727367" CREATED="1572722973109" MODIFIED="1572723040597">
<node TEXT="Ejemplo: El comando top, por que toma el control de la tarminal hasta que es terminado por el usuario." ID="ID_1284301606" CREATED="1572723040610" MODIFIED="1572723071195"/>
</node>
<node TEXT="El comando bg vuelve a iniciar los procesos suspendidos que se encuentran en segundo plano." ID="ID_374273760" CREATED="1572723214236" MODIFIED="1572723264488"/>
<node TEXT="El comando bg pasa un proceso del segundo plano al primer plano." ID="ID_1137387285" CREATED="1572723271555" MODIFIED="1572723311493"/>
<node TEXT="Con la combinaci&#xf3;n de teclas ctrl +z  se puede suspender un programa en ejecuci&#xf3;n.&#xa;Con la combinaci&#xf3;n de teclas ctrl +c  se detiene el programa por completo." ID="ID_1398548539" CREATED="1572723321384" MODIFIED="1572723540901"/>
</node>
<node TEXT="COmandos jobs y &amp;" FOLDED="true" ID="ID_167629843" CREATED="1572723574809" MODIFIED="1572723582304">
<node TEXT="Muetra una lista con todos los comandos que se encuentran en segundo plano o que han sido suspendidos," ID="ID_1670589242" CREATED="1572723605717" MODIFIED="1572723930666"/>
<node TEXT="Hay que diferenciar entre el Id de la tarea y el Ide del proceso" ID="ID_167803422" CREATED="1572723931348" MODIFIED="1572723965884"/>
<node TEXT="Las opciones mas comunes del comando jobs son:&#xa;jobs -l" FOLDED="true" ID="ID_189254249" CREATED="1572723967011" MODIFIED="1572724041301">
<node TEXT="Provee informaci&#xf3;n como el n&#xfa;mero de tarea, la tarea actual, el id del n&#xfa;mero del proceso, el estado de la tarea y el comando que invoc&#xf3; la tarea." ID="ID_307487166" CREATED="1572724041309" MODIFIED="1572724155263"/>
</node>
<node TEXT="jobs -r" FOLDED="true" ID="ID_1821870525" CREATED="1572724178140" MODIFIED="1572724183701">
<node TEXT="Muestra unicamente las tareras que se ejecutan actualmente." ID="ID_1183465119" CREATED="1572724183704" MODIFIED="1572724251113"/>
</node>
<node TEXT="Comando &amp;" FOLDED="true" ID="ID_1027071086" CREATED="1572724252756" MODIFIED="1572724268791">
<node TEXT="Permite enviar un comando directo a segundo plano ingresando el caracter &amp; al finalizar la sentencia." ID="ID_633848142" CREATED="1572724268797" MODIFIED="1572724406408"/>
</node>
</node>
<node TEXT="Comandos Kill y killall" FOLDED="true" ID="ID_1431480475" CREATED="1572724410634" MODIFIED="1572724419465">
<node TEXT="En algunas ocaciones un proceso toma poseci&#xf3;n del sistema simplemente por que puede enlentecer a otros programas, o por que bloquea eal sistema debido al agotamiento de los recursos disponibles, por tal motivo se puede terminar este proceso." ID="ID_1707475990" CREATED="1572724419470" MODIFIED="1572725326899"/>
<node TEXT="Para ver una lista de todas las se&#xf1;ales del comando kill ejecute:&#xa;#kill -l" ID="ID_1257943243" CREATED="1572725328099" MODIFIED="1572725360521"/>
<node TEXT="Para eliminar un proceso escriba:&#xa;#top&#xa;Para ver los procesos en ejecuci&#xf3;n, tambien se puede con el comando&#xa;#ps aux | grep&#xa;#kill -9 10281&#xa;Donde -9 es la se&#xf1;al que usara el comando kill y 10281 el id del proceso." ID="ID_890768267" CREATED="1572725366122" MODIFIED="1572725990788"/>
<node TEXT="Comando killall finaliza un proceso usando su nombre, no id de proceso." FOLDED="true" ID="ID_429174791" CREATED="1572726039765" MODIFIED="1572726112245">
<node TEXT="Ejemplo:&#xa;#ps aux | grep sleep&#xa;#killall sleep" ID="ID_13577838" CREATED="1572726112252" MODIFIED="1572726205489"/>
</node>
</node>
<node TEXT="Comando nohup" FOLDED="true" ID="ID_1510059939" CREATED="1572726707565" MODIFIED="1572726736613">
<node TEXT="Le permitira a un proceso ejecutandose incluso si el propietario de la sesi&#xf3;n cerra su sesi&#xf3;n en el sistema." ID="ID_1713406954" CREATED="1572726736616" MODIFIED="1572726842164"/>
<node TEXT="Cuando usted termina una sesi&#xf3;n, todos lo programa que se ejecutan durante la misma reciben la se&#xf1;al hangout signal o se&#xf1;al de finalizaci&#xf3;n, una ves recibida esta se&#xf1;al, los programas son terminados." ID="ID_1478621293" CREATED="1572726843934" MODIFIED="1572726939502"/>
<node TEXT="Escribe la salida del proceso que esta protegiendo  a ubn archivo llamado nohup.out  en el directorio actual." ID="ID_1025323259" CREATED="1572727219160" MODIFIED="1572727296107"/>
</node>
<node TEXT="&#xbf;Que es un proceso?" FOLDED="true" ID="ID_702643681" CREATED="1573443438318" MODIFIED="1573443487522">
<node TEXT="Es una instancia de un programa" ID="ID_1647671868" CREATED="1573443487531" MODIFIED="1573443548324"/>
<node TEXT="Un programa es una serie de procesos que se estan ejecutando. O mejor dicho un proceso es un programa que se esta ejecutando." ID="ID_126446453" CREATED="1573443549122" MODIFIED="1573443565994"/>
<node TEXT="Modo de ejecuci&#xf3;n" FOLDED="true" ID="ID_1593357818" CREATED="1573443817512" MODIFIED="1573443841542">
<node TEXT="Primerplano (foreground)" ID="ID_1017370548" CREATED="1573443846143" MODIFIED="1573443875072"/>
<node TEXT="Segundo plano (background)" ID="ID_217955485" CREATED="1573443875560" MODIFIED="1573443906617"/>
</node>
<node TEXT="Ejecutmos un proceso en primer plano" FOLDED="true" ID="ID_1657888850" CREATED="1573443947549" MODIFIED="1573443961285">
<node TEXT="gdit f_prueba" ID="ID_1432589577" CREATED="1573443976501" MODIFIED="1573443982897"/>
</node>
<node TEXT="Ejecutamos en segundo &#x1e55;lano" FOLDED="true" ID="ID_1200036180" CREATED="1573444020525" MODIFIED="1573444031012">
<node TEXT="gedit f_prueba &amp;" ID="ID_695498588" CREATED="1573444031857" MODIFIED="1573444051071"/>
</node>
<node TEXT="&#xbf;Como puedo saber que procesos se estan ejecutando en mi S.O.?" FOLDED="true" ID="ID_696546675" CREATED="1573444072397" MODIFIED="1573444079380">
<node TEXT="Ejecutamos en una terminal elcomando ps-aux" ID="ID_1568069067" CREATED="1573444079384" MODIFIED="1573444210126"/>
</node>
<node TEXT="&#xbf;Como puedo matar un proceso?" FOLDED="true" ID="ID_1845925197" CREATED="1573444215650" MODIFIED="1573444236910">
<node TEXT="Primero se necesita saber el n&#xfa;mero de PID del proceso, una vez obtenida la informaci&#xf3;n use elcomando kill de la siguiente manera" FOLDED="true" ID="ID_1448113574" CREATED="1573444236915" MODIFIED="1573444264046">
<node TEXT="#kill (n&#xfa;mero de proceso)&#xa;#kill 2345&#xa;por ejemplo." ID="ID_1264691186" CREATED="1573444264051" MODIFIED="1573444292043"/>
</node>
</node>
</node>
</node>
<node TEXT="103.5 Creaci&#xf3;n, monitoreo y finalizaci&#xf3;n de procesos parte2" FOLDED="true" POSITION="right" ID="ID_1638152952" CREATED="1573444327998" MODIFIED="1573444391226">
<edge COLOR="#808080"/>
<node TEXT="Comando ps y pstree" FOLDED="true" ID="ID_991228558" CREATED="1573444337706" MODIFIED="1573444420234">
<node TEXT="ps significa process status por sus siglas en ingl&#xe9;s:&#xa;reporta un resumen de todos los procesos ejecutados actualmente." ID="ID_623154321" CREATED="1573444444075" MODIFIED="1573445033457"/>
<node TEXT="Una de las opciones mas comunes del comando ps es ps -aux o ps aux&#xa;Muestra todos los procesos que le perteneces a otros usuarios a de m&#xe1;s de los procesos que no estan asociados a una terminal y a los precesos en modo usuario." FOLDED="true" ID="ID_1525809588" CREATED="1573445035970" MODIFIED="1573445058201">
<node TEXT="Ejemplo:&#xa;#ps -aux" ID="ID_1863101003" CREATED="1573445511558" MODIFIED="1573445527332"/>
</node>
<node TEXT="Opciones m&#xe1;s comunes:&#xa;-a muestra los procesos que leperteneces a otro usuario y que est&#xe1;n asociados a  una terminal.&#xa;-l.- formato detallado, estoincluye la prioridad el Id del proceso padre, y otra informaci&#xf3;n relevante.&#xa;-u.- Formato de usuario, esta incluye la hora de inicio de los procesos y el nombre del usuario.&#xa;-x: Incluye a los procesos que no estan asociados a ninguna terminal, esta opci&#xf3;n es utilizada para ver a un proceso demonio y otros procesos que no son iniciados mediante una terminal.&#xa;-C.- Muestra las instancias del comando.&#xa;-U.- Muestra todos los procesos pertenecientes a un usuario-" FOLDED="true" ID="ID_895137381" CREATED="1573445059091" MODIFIED="1573445084578">
<node TEXT="Ejemplo:&#xa;#ps aux | grep httpd&#xa;#ps ae&#xa;#ps  al&#xa;#ps axj&#xa;#ps -U paul | less" ID="ID_167364894" CREATED="1573445545954" MODIFIED="1573445551026"/>
</node>
<node TEXT="pstree muestra una lista jerarquica de todos los procesosen forma de &#xe1;rbol." ID="ID_1593692268" CREATED="1573445485884" MODIFIED="1573445589739"/>
<node TEXT="Sirve para entender la relaci&#xf3;n padre e hijo entre los procesos" ID="ID_1046743462" CREATED="1573445611342" MODIFIED="1573445618373"/>
<node TEXT="Las opciones mas utilizadas del comando pstree son las siguientes:&#xa;-a.- Muestra los argumentos de la linea de comandos.&#xa;-c:.- Deshabilita lacompactaci&#xf3;n del &#xe1;rbol de procesos.&#xa;-p.- Muestra los ID&#x15b; de los procesos .&#xa;-u.- Muestra las transiciones entre ID&#x15b; de usuarios.&#xa;-V.- Muestra la versi&#xf3;n del programa." FOLDED="true" ID="ID_1408017037" CREATED="1573445635483" MODIFIED="1573445644728">
<node TEXT="Ejemplos:&#xa;#pstree -a&#xa;#pstree -p&#xa;#pstree -V" ID="ID_135703702" CREATED="1573445668171" MODIFIED="1573445674034"/>
</node>
</node>
<node TEXT="Comandos uptime y free" FOLDED="true" ID="ID_845972243" CREATED="1573444391232" MODIFIED="1573444442050">
<node TEXT="El comando uptime le permite saber hace cuanto tiempo el comando se ha iniciado, retorna una unica linea mostrando la siguiente informaci&#xf3;n" FOLDED="true" ID="ID_1242392180" CREATED="1573445740262" MODIFIED="1573445761307">
<node TEXT="Ejemplo:&#xa;#man uptime&#xa;#uptime" ID="ID_605675320" CREATED="1573445761311" MODIFIED="1573445776476"/>
</node>
<node TEXT="La &#xfa;nica opci&#xf3;n disponible es uptime -V la cual muestra la versi&#xf3;n disponible de uptime." ID="ID_48046343" CREATED="1573445682787" MODIFIED="1573445696802"/>
<node TEXT="El comando free muestra el total de memoria libre, el total de memoria utilizada por el sistema" FOLDED="true" ID="ID_636557487" CREATED="1573445796015" MODIFIED="1573445816732">
<node TEXT="Ejemplo:&#xa;#man free&#xa;#free&#xa;#free -V muestra la versi&#xf3;n de free" ID="ID_1591844123" CREATED="1573445819281" MODIFIED="1573445835449"/>
</node>
</node>
<node TEXT="Comando top" FOLDED="true" ID="ID_242603459" CREATED="1573445851238" MODIFIED="1573445858827">
<node TEXT="Muestra la misma informaci&#xf3;n del comado ps, con la diferencia de que es actualizada constantemente." ID="ID_1966411525" CREATED="1573445861939" MODIFIED="1573445884448"/>
<node TEXT="Muestra la misma informaci&#xf3;n en su encabezado de los comandos uptime y free." ID="ID_848042746" CREATED="1573445884793" MODIFIED="1573447469782"/>
<node TEXT="Muestra informaci&#xf3;n en orden descendente de acuerdo a la utilizaci&#xf3;n  del procesador de los mismos" ID="ID_1661267788" CREATED="1573447471050" MODIFIED="1573447496792"/>
<node TEXT="Si ninguna opci&#xf3;n especificada, la informaci&#xf3;n es refrescada cada 3 segundos.&#xa;Si desea que se refresque cada 5 segundos entonces debe ingresar la opci&#xf3;n:" FOLDED="true" ID="ID_1989378223" CREATED="1573447497991" MODIFIED="1573447520082">
<node TEXT="Ejemplo:&#xa;#top -d 5" ID="ID_1053536786" CREATED="1573447640985" MODIFIED="1573447655715"/>
</node>
<node TEXT="Opciones mas utilizadas del comando top:&#xa;-b.- Ejecuta top en modo batch&#xa;-d.- Especifica el tiempo de espera en segundos para que top refresque la pantalla.&#xa;-i.- Ignora los procesos en espera.&#xa;-n.- Muestra n cantidad de refrescos de pantalla antes de salir.&#xa;-s.- Ejecuta top en modo seguro." ID="ID_1205266470" CREATED="1573447536871" MODIFIED="1573447545182"/>
<node TEXT="Posee teclas de navegaci&#xf3;n aque hace que su utilizaci&#xf3;n sea interactiva:&#xa;spacebar.- refresca la pantalla.&#xa;h.- Muestra lapantalla de ayuda.&#xa;k.- Finaliza un proceso. Luego de presionar la tecla k top le preguntar&#xe1; el ID del proceso que usted desea finalizary la se&#xf1;al que desea enviar a dicho proceso.&#xa;q.- Finaliza el programa top y regresa a la linea de comandos." FOLDED="true" ID="ID_7305831" CREATED="1573447562982" MODIFIED="1573447572545">
<node TEXT="Ejemplos:&#xa;#top -d 2&#xa;#top -n 2&#xa;#top bi n 3 d 1 &gt; file4.txt&#xa;#less file4.txt" ID="ID_176905838" CREATED="1573447578195" MODIFIED="1573447601380"/>
</node>
</node>
</node>
<node TEXT="103.6 Modificaci&#xf3;n de la prioridad de ejecuci&#xf3;n de los procesos" FOLDED="true" POSITION="right" ID="ID_1582855649" CREATED="1573621093736" MODIFIED="1573621174055">
<edge COLOR="#808080"/>
<node TEXT="Comando nice" FOLDED="true" ID="ID_1001579264" CREATED="1573621175658" MODIFIED="1573621380675">
<node TEXT="Existen procesos que usan mayor porcentaje del CPU, por ejemplo editores de v&#xed;deo" ID="ID_46377912" CREATED="1573621380691" MODIFIED="1573621541597"/>
<node TEXT="La prioridad va de -20 la cual es la m&#xe1;s alta, a 19 que es la m&#xe1;s baja." ID="ID_485349637" CREATED="1573621544488" MODIFIED="1573621889105">
<icon BUILTIN="female1"/>
</node>
<node TEXT="Sint&#xe1;xis" FOLDED="true" ID="ID_605232375" CREATED="1573931951220" MODIFIED="1573931963521">
<node TEXT="#nice -15 xclock &amp;&#xa;Significa que el comando xclock se ejecuta con prioridad 15&#xa;#nice --15 xclock &amp;&#xa;Significa que el proceso se iniciar&#xe1; con una prioridad de -15, si se confunde con el doble gui&#xf3;n, se puede ingresar de la siguiente forma.&#xa;#nice -n -15 xclock &amp;" ID="ID_215282277" CREATED="1573931963559" MODIFIED="1573932729180"/>
</node>
<node TEXT="Solo el usuario root puede aumentar la prioridad de proceso, otros usuarios solo pueden disminuir la prioridad del proceso." ID="ID_110694833" CREATED="1573932731777" MODIFIED="1573932796114"/>
</node>
<node TEXT="COmando renice" FOLDED="true" ID="ID_1405919303" CREATED="1573939512040" MODIFIED="1573939519492">
<node TEXT="Cambia el valor de prioridad de un proceso en ejecuci&#xf3;n." ID="ID_1865999999" CREATED="1573939519498" MODIFIED="1573939587990"/>
<node TEXT="Al igual que el comando nice el nivel de prioridad va de -20 como la mas alta y 19 como la mas baja." ID="ID_1678288602" CREATED="1573939588274" MODIFIED="1573939630028"/>
<node TEXT="Opciones y objetivos." FOLDED="true" ID="ID_1910458077" CREATED="1573939661241" MODIFIED="1573939841942">
<node TEXT="-u.- Interpreta los objetivos como los nombres de usuario.&#xa;-p.- Interpreta los objetivos como ID&apos;s del proceso." ID="ID_1343534691" CREATED="1573939843046" MODIFIED="1573940679263"/>
</node>
<node TEXT="Ejemplo:&#xa;#root@PC-L440:~# xclock &amp;&#xa;[1] 9167&#xa;root@PC-L440:~# ps -l&#xa;F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD&#xa;4 S     0  9153  9141  0  80   0 -  3506 poll_s pts/2    00:00:00 sudo&#xa;4 S     0  9154  9153  0  80   0 -  3295 do_wai pts/2    00:00:00 bash&#xa;4 S     0  9167  9154  0  80   0 - 11053 poll_s pts/2    00:00:00 xclock&#xa;4 R     0  9172  9154  0  80   0 -  3510 -      pts/2    00:00:00 ps&#xa;Identifique el proceso de xclock, ahora escriba:&#xa;root@PC-L440:~# renice -19 -p 9167&#xa;9167 (process ID) prioridad anterior 0, nueva prioridad -19&#xa;root@PC-L440:~# ps -l&#xa;F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD&#xa;4 S     0  9153  9141  0  80   0 -  3506 poll_s pts/2    00:00:00 sudo&#xa;4 S     0  9154  9153  0  80   0 -  3295 do_wai pts/2    00:00:00 bash&#xa;4 S     0  9167  9154  0  61 -19 - 11053 poll_s pts/2    00:00:00 xclock&#xa;4 R     0  9246  9154  0  80   0 -  3510 -      pts/2    00:00:00 ps&#xa;Notese que en el proceso xclock en la columna NI se ve reflejada la prioridad como -19&#xa;root@PC-L440:~# renice 15 -u root -p 9167&#xa;0 (user ID) prioridad anterior -20, nueva prioridad 15&#xa;9167 (process ID) prioridad anterior 15, nueva prioridad 15&#xa;root@PC-L440:~# ps -l&#xa;F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD&#xa;4 S     0  9153  9141  0  95  15 -  3506 poll_s pts/2    00:00:00 sudo&#xa;4 S     0  9154  9153  0  95  15 -  3295 do_wai pts/2    00:00:00 bash&#xa;4 S     0  9167  9154  0  95  15 - 11053 poll_s pts/2    00:00:00 xclock&#xa;4 R     0  9271  9154  0  95  15 -  3510 -      pts/2    00:00:00 ps&#xa;Notese  que ahora todos los procesos en la columna NI tienen prioridad 15" ID="ID_1690971070" CREATED="1573940683249" MODIFIED="1573941856065"/>
</node>
</node>
<node TEXT="103.7 Busqueda de archivos de texto utilizando expresiones regulares." FOLDED="true" POSITION="right" ID="ID_745036371" CREATED="1574530312495" MODIFIED="1574530368027">
<edge COLOR="#808080"/>
<node TEXT="Expresiones regulares" FOLDED="true" ID="ID_1262836767" CREATED="1574530368692" MODIFIED="1574534835548">
<node TEXT="Una expresi&#xf3;n regular es una serie de caracteres especiales que permiten describir un texto que queremos buscar. Por ejemplo, si quisi&#xe9;ramos buscar la palabra &#x201c;linux&#x201d; bastar&#xed;a poner esa palabra en el programa que estemos usando. La propia palabra es una expresi&#xf3;n regular. Hasta aqu&#xed; parece muy simple, pero, &#xbf;y si queremos buscar todos los n&#xfa;meros que hay en un determinado fichero? &#xbf;O todas las lineas que empiezan por una letra may&#xfa;scula? En esos casos ya no se puede poner una simple palabra. La soluci&#xf3;n es usar una expresi&#xf3;n regular." ID="ID_1621528239" CREATED="1574536400009" MODIFIED="1574536438093"/>
<node ID="ID_919641529" CREATED="1574536440747" MODIFIED="1574542236360"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      . .- Corresponde a cualquier caracter siemple.
    </p>
    <p>
      * .- Corresponde a un n&#xfa;mero de caracteres simples, sin importar que lo proceda.
    </p>
    <p>
      '.-&#xa0;&#xa0;Comillas simples, las variables y comandos entre comillas simples no son procesados.
    </p>
    <p>
      ".-&#xa0;&#xa0;Comillas dobles, las variables entre comillas dobles, son procesadas, los comandos no lo son.
    </p>
    <p>
      '.- Acentos graves, los comandos y variables entre acentos graves son procesados.
    </p>
    <p>
      ~.- tilde representa el directorio ho
    </p>
    <b http-equiv="content-type" content="text/html; charset=utf-8">^.- </b>Acento circunflejo, especifica com&#xfa;nnmente el principio de una linea, especialmente en la b&#xfa;squeda. [^] .- Corresponde a todos los caracteres, excepto aquellos especificados entre corchetes. $.- Especifica a una variable o a la finalizaci&#xf3;n de una linea. \.- La barra invertida, permite omitir el significado de una expresi&#xf3;n. {}.- Ambas llaves encierran funciones. [].- Ambos corchetes especifican un rango. ().- Ambos parentesis especifican opciones. !.- El signo de exclamaci&#xf3;n es tambi&#xe9;n conocido como "bang" el cual niega un intento de rango.

    <p>
      
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
</node>
<node TEXT="El comando grep y las expresiones regulares." FOLDED="true" ID="ID_1099614597" CREATED="1574542246198" MODIFIED="1574542395669">
<node TEXT="Significa.- Procesador generalizado de expresiones regulares..- permite la utilizaci&#xf3;n de expresiones regulares en busquedas de archivos o en las salidas de los programas." ID="ID_281437331" CREATED="1574542395673" MODIFIED="1574542461770"/>
<node TEXT="Muestra las lineas que coinciden con el patron dado." ID="ID_1931018091" CREATED="1574542463072" MODIFIED="1574542565065"/>
<node TEXT="Opciones m&#xe1;s utilizadas del comando grep." FOLDED="true" ID="ID_840677733" CREATED="1574542575992" MODIFIED="1574542589179">
<node TEXT="-e.- Soporta todas las b&#xfa;squedas. &#xfc;til para proteger patrones que comiencen con un gui&#xf3;n (-).&#xa;-i.- Deshabilita la sensibilidad de caracteres.&#xa;-l.- Se detiene ante la primer coincidencia y la muestra.&#xa;-v.-Invierte el orden de b&#xfa;squeda.." ID="ID_1687000942" CREATED="1574542589186" MODIFIED="1574542873790"/>
<node TEXT="Ejemplo:&#xa;root@PC-L440:~# grep israel /etc/passwd&#xa;israel:x:1000:1000:Israel:/home/israel:/bin/bash&#xa;# grep bash /etc/passwd&#xa;root:x:0:0:root:/root:/bin/bash&#xa;israel:x:1000:1000:Israel:/home/israel:/bin/bash" ID="ID_1790133601" CREATED="1574543850827" MODIFIED="1574543988844"/>
</node>
</node>
<node TEXT="Comandos egrep y fgrep" FOLDED="true" ID="ID_359199867" CREATED="1574545313368" MODIFIED="1574545327093">
<node TEXT="El comando egrep es lo mismo que ejecutar grep -E" FOLDED="true" ID="ID_1954516693" CREATED="1574545327097" MODIFIED="1574545663984">
<node TEXT="Es una extensi&#xf3;n de grep donde las expresiones regulares:&#xa;+ ? |() han sido agregados." ID="ID_239948917" CREATED="1574545664444" MODIFIED="1574545737301"/>
<node TEXT="Ejemplo:&#xa;root@PC-L440:~# grep &apos;+ash&apos; /etc/passwd&#xa;root@PC-L440:~# egrep &apos;+ash&apos; /etc/passwd&#xa;root:x:0:0:root:/root:/bin/bash&#xa;israel:x:1000:1000:Israel:/home/israel:/bin/bash&#xa;notese que con el comando grep no ocurrio nada." ID="ID_196572957" CREATED="1574546050699" MODIFIED="1574546526612"/>
</node>
<node TEXT="El comando fgrep equivale a grep -F" FOLDED="true" ID="ID_1960889285" CREATED="1574545760060" MODIFIED="1574545776014">
<node TEXT="Es conocido como grep fijo o grep r&#xe1;pido, no reconoce ningna expresi&#xf3;n regular extendida de forma especial." ID="ID_732165057" CREATED="1574545777714" MODIFIED="1574545879345"/>
<node TEXT="Ejemplo:&#xa;# grep pa.* /etc/passwd&#xa;root@PC-L440:~# fgrep pa.* /etc/passwd&#xa;root@PC-L440:~# fgrep pa /etc/passwd&#xa;root@PC-L440:~#" ID="ID_815871403" CREATED="1574546537857" MODIFIED="1574546547831"/>
</node>
</node>
</node>
<node TEXT="104.1 creaci&#xf3;n de particiones y sistemas de archivos" FOLDED="true" POSITION="right" ID="ID_1187742275" CREATED="1576168483943" MODIFIED="1576168644866">
<node TEXT="Creaci&#xf3;n de particiones utilizando fdisk" FOLDED="true" ID="ID_237587203" CREATED="1576168644882" MODIFIED="1576168807095">
<node TEXT="fdisk que significa arreglo del disco, es una herramienta-" ID="ID_810653270" CREATED="1576168807104" MODIFIED="1576169105932"/>
<node TEXT="fdisk -l .- Es utilizado para ver las particiones existentes, el cominezo y finalizaci&#xf3;n de los cilindros, el n&#xfa;mero de bloques asociados a cada partici&#xf3;n, el id de cada partici&#xf3;n si los dispositivos son de arranque o no, y por &#xfa;ltimo el nombre de los dispositivos." ID="ID_1774825151" CREATED="1576169115811" MODIFIED="1576169393244"/>
<node TEXT="En un disco sin ninguna partici&#xf3;n se pueden crear facilmente con el comando fdisk." FOLDED="true" ID="ID_1119573819" CREATED="1576169427012" MODIFIED="1576169544946">
<node TEXT="Ejemplo:&#xa;#fdisk /dev/sdb&#xa;-nos pide insertar una opci&#xf3;n seleccionamos m.&#xa;-luego n&#xa;-enseguida nos pide que tipo de partici&#xf3;n queremos primaria o extendida En la primaria solo nos deja crear 4 aparece as&#xed; (1-4): seleccionamos 1.&#xa;-Luego entonces nos dice que ingresemos el tama&#xf1;o en megabites-&#xa;-Ingrese la letra m para que nos muestre el men&#xfa; y seleccionamos la letra n para agregar una nueva partici&#xf3;n.&#xa;-repetimos con la letra p para seleccionar una partici&#xf3;n primaria, seleccionamos el 1, e ingresamos el tama&#xf1;o en megas.&#xa;- en seguida ingrese laletra p&#xe1;ta que imprima latabla de particiones." ID="ID_1059449909" CREATED="1576169544955" MODIFIED="1576172460863">
<font SIZE="14"/>
</node>
</node>
<node TEXT="Opciones.&#xa;a.- Marca una opci&#xf3;n como de arranque.&#xa;d.- Elimina una partici&#xf3;n.&#xa;l.- Muestra los tipos de particiones conocidas.&#xa;m.- Muestra este men&#xfa; de opciones.&#xa;n.- Crea una nueva partici&#xf3;n.&#xa;p.- Muestra la tabla de particiones.&#xa;q.- Sale sin guardar los cambios.&#xa;t.- Cambia el id de sistema de una partici&#xf3;n.&#xa;w.- Guarda los cambios en la tabla de particiones y sale." ID="ID_214520833" CREATED="1576169609618" MODIFIED="1576169869759"/>
<node TEXT="Id y particiones mas importantes.-&#xa;7 HPFS/NTFS&#xa;82 Partici&#xf3;n swap de linux.&#xa;83 Partici&#xf3;n estandar de linux&#xa;85 Partici&#xf3;n extendida de linux&#xa;8e Partici&#xf3;n LVM de linux dise&#xf1;ada para volumenes l&#xf3;gicos.&#xa;fd Partici&#xf3;n raid auto de linux" ID="ID_758141939" CREATED="1576172491741" MODIFIED="1576173269065"/>
</node>
<node TEXT="Formateo de sistema de archivos utilizando mkfs" FOLDED="true" ID="ID_192248139" CREATED="1576173271468" MODIFIED="1576173285794">
<node TEXT="Luego de crear lasparticiones, debemos darle formato.Por tal motivo se debe asignar un sistema de archivos para esas particiones." ID="ID_1445007467" CREATED="1576173292004" MODIFIED="1576173411700"/>
<node TEXT="La frase sistema de archivos tiene dos significados diferentes." FOLDED="true" ID="ID_636187904" CREATED="1576173412461" MODIFIED="1576173433445">
<node TEXT="Puede referirse a que los archivos y directorios son fisicamente estructurados en un disco duro ejemplo: ext2 y ext3" FOLDED="true" ID="ID_181916367" CREATED="1576173433453" MODIFIED="1576173515650">
<node TEXT="Para poder formatear las particiones se utiliza el  comando mkfs" FOLDED="true" ID="ID_1665725249" CREATED="1576173612179" MODIFIED="1576173743970">
<node TEXT="Ejemplo:#mkfs&#xa;-presione tabulador y podr&#xe1; ver algo como esto:&#xa;mkfs          mkfs.ext2     mkfs.ext4dev  mkfs.ntfs&#xa;mkfs.bfs      mkfs.ext3     mkfs.minix    mkfs.vfat&#xa;mkfs.cramfs   mkfs.ext4     mkfs.msdos" ID="ID_1366658929" CREATED="1576173748057" MODIFIED="1576174536444"/>
</node>
<node TEXT="La siguiente tabla describe los formatos de sistemas de archivos disponibles en Gnu linux:&#xa;ext2.- No journaling, old default linux.&#xa;ext3.- Journaling, default on many linux.&#xa;reiserfs.- Journaling, versi&#xf3;n 3 is current.&#xa;swap.- Linux extended&#xa;vfat.- Compatible whit microsoft FAT32&#xa;xfs.- 64-bits journaling filesystem" ID="ID_1979596730" CREATED="1576174660121" MODIFIED="1576175186500"/>
<node TEXT="Ejemplo: si swe quiere formatear la partici&#xf3;n /dev/sdx se ingresa el siguente comando:&#xa;#mkfs.ext3 /dev/sdb1" FOLDED="true" ID="ID_1453082392" CREATED="1576175248604" MODIFIED="1576175567896">
<node TEXT="Opciones equivalentes:" ID="ID_301793069" CREATED="1576175569830" MODIFIED="1576175584453"/>
<node TEXT="mkfs.ext2&#xa;mkfs.ext3&#xa;mkfs.reiserfs&#xa;mkfs.xfs&#xa;mkfs.vfat&#xa;fd" ID="ID_1464209868" CREATED="1576175585113" MODIFIED="1576175672689"/>
<node TEXT="mkfs -t ext2&#xa;mkfs -t ext3&#xa;mkfs -t reiserfs&#xa;mkfs -t xfs&#xa;mkfs -t ext2&#xa;mkfs -t ext3&#xa;mkfs -t reiserfs&#xa;mkfs -t vfat&#xa;linux raid auto" ID="ID_1807923403" CREATED="1576175673379" MODIFIED="1576175774231"/>
<node TEXT="mk2fs&#xa;mk2fs -j&#xa;mkreiserfs&#xa;mkdosfs -F 32" ID="ID_1778366508" CREATED="1576175775769" MODIFIED="1576175844345"/>
</node>
<node TEXT="Para formatear la partici&#xf3;n swap ingrese el siguiente comando:&#xa;#fdisk -l&#xa;-para localizar  la partici&#xf3;n swap&#xa;#mkswap /dev/sdb3" ID="ID_1242006683" CREATED="1576176031113" MODIFIED="1576176184672"/>
</node>
<node TEXT="Segundo.- Puede referirse a la estructura de la gerarqu&#xed;a de directorios del sistema de almacenamiento." ID="ID_378851" CREATED="1576173516264" MODIFIED="1576173608253"/>
</node>
</node>
</node>
<node TEXT="104.2 Manteniendo la integridad de los sistemas de archivos." FOLDED="true" POSITION="right" ID="ID_1108048876" CREATED="1576258800781" MODIFIED="1576258836751">
<node TEXT="Reportes de espacio en disco utilizando df" FOLDED="true" ID="ID_76084524" CREATED="1576259260416" MODIFIED="1576259382017">
<node TEXT="Un archivo o directorio se encuentra en un conjunto de bloques, la informaci&#xf3;n sobre el archivo o directorio se encuentra en un indice llamado inode" ID="ID_474764401" CREATED="1576259384670" MODIFIED="1576260024803"/>
<node TEXT="Debido a que los bloques de datos y los bloques de inodos ocupan espacio en nuestro disco y por que solo se puede designar a un cierto n&#xfa;mero de indices como el n&#xfa;mero m&#xe1;ximo de inodos disponibles en una particion particular debemos  corroborar el espacio libre y los inodos libres de nuestro sistema de vez en cuando." ID="ID_943674209" CREATED="1576259559008" MODIFIED="1576259992625"/>
<node TEXT="EL comando df" FOLDED="true" ID="ID_1576106066" CREATED="1576259993478" MODIFIED="1576260035833">
<node TEXT="Significa disk free en ingl&#xe9;s, o espacio libre en disco.- Muestra informaci&#xf3;n sobre el espacio ocupado y libre sobre el sistema de archivos montado en nuestro sistema." FOLDED="true" ID="ID_1382852323" CREATED="1576260035842" MODIFIED="1576260628712">
<node TEXT="Ejemplo: root@israel-Presario-V3500-Notebook-PC:~# df -h&#xa;S.ficheros     Tama&#xf1;o Usados  Disp Uso% Montado en&#xa;/dev/sda2         23G   3,8G   19G  18% /&#xa;udev             1,5G   4,0K  1,5G   1% /dev&#xa;tmpfs            593M   780K  592M   1% /run&#xa;none             5,0M      0  5,0M   0% /run/lock&#xa;none             1,5G   116K  1,5G   1% /run/shm&#xa;/dev/sda1        472M    39M  410M   9% /boot&#xa;/dev/sda6         28G   2,4G   24G  10% /home&#xa;/dev/sdb1        3,8G   1,8G  2,0G  48% /media/disk" ID="ID_1854228439" CREATED="1576260628766" MODIFIED="1576260657216"/>
</node>
<node TEXT="Tambi&#xe9;n puede mostrar los inodos utilizados y los inodos sin utilizar." FOLDED="true" ID="ID_924471248" CREATED="1576261451879" MODIFIED="1576262042045">
<node TEXT="Ejemplo: root@israel-Presario-V3500-Notebook-PC:~# df -k&#xa;S.ficheros     1K-bloques  Usados Disponibles Uso% Montado en&#xa;/dev/sda2        24031100 3918292    18892100  18% /&#xa;udev              1506424       4     1506420   1% /dev&#xa;tmpfs              606476     780      605696   1% /run&#xa;none                 5120       0        5120   0% /run/lock&#xa;none              1516180     116     1516064   1% /run/shm&#xa;/dev/sda1          482922   39127      418861   9% /boot&#xa;/dev/sda6        28835836 2492284    24878772  10% /home&#xa;/dev/sdb1         3944900 1885920     2058980  48% /media/disk&#xa;root@israel-Presario-V3500-Notebook-PC:~# df -i&#xa;S.ficheros     Nodos-i NUsados NLibres NUso% Montado en&#xa;/dev/sda2      1525920  165805 1360115   11% /&#xa;udev            211690     469  211221    1% /dev&#xa;tmpfs           216569     382  216187    1% /run&#xa;none            216569       3  216566    1% /run/lock&#xa;none            216569       7  216562    1% /run/shm&#xa;/dev/sda1       124928     229  124699    1% /boot&#xa;/dev/sda6      1831424   11781 1819643    1% /home&#xa;/dev/sdb1            0       0       0     - /media/disk" ID="ID_362024188" CREATED="1576261477511" MODIFIED="1576262004740"/>
</node>
<node TEXT="Opciones:&#xa;-h.- Muestra los formatos en tama&#xf1;o amigable.&#xa;-i.- Muestra la informaci&#xf3;n de los inodos en vez de lautilizaci&#xf3;n por bloque.&#xa;-k.- Muestra el resultado en kilobytes.&#xa;-v.- Ignorada.&#xa;--version.- Muestra la versi&#xf3;n del programa y sale." ID="ID_1395346818" CREATED="1576262109227" MODIFIED="1576262359463"/>
</node>
<node TEXT="Comando du" FOLDED="true" ID="ID_460673293" CREATED="1576262445064" MODIFIED="1576262452638">
<node TEXT="Significa uso del disco.- muestra informaci&#xf3;n sobre el espacio utilizado por los archivos en un dirctorio." ID="ID_191830079" CREATED="1576262456312" MODIFIED="1576262545013"/>
<node TEXT="Si un directorio es especificado, el comando du calcula recursivamente el tama&#xf1;o de cada archivo y subdirectorio en el directorio dado." FOLDED="true" ID="ID_1021661567" CREATED="1576262546058" MODIFIED="1576262653041">
<node TEXT="Ejemplo:&#xa;root@israel-Presario-V3500-Notebook-PC:~# du -h&#xa;4,0K&#x9;./.pulse&#xa;36K&#x9;.&#xa;root@israel-Presario-V3500-Notebook-PC:~# du -h /usr" ID="ID_815096825" CREATED="1576262653075" MODIFIED="1576263231572"/>
<node TEXT="Si desea realizar un resumen de un directorio use la opci&#xf3;n s.&#xa;Ejemplo: root@israel-Presario-V3500-Notebook-PC:~# du -hs /usr&#xa;2,8G&#x9;/usr" ID="ID_1264881301" CREATED="1576263232252" MODIFIED="1576263677943"/>
<node TEXT="Opciones:&#xa;-h.- Muestra los formatos en tama&#xf1;o amigable.&#xa;-i.- Muestra la informaci&#xf3;n de los inodos en vez de lautilizaci&#xf3;n por bloque.&#xa;-k.- Muestra el resultado en kilobytes.&#xa;-v.- Ignorada.&#xa;--version.- Muestra la versi&#xf3;n del programa y sale." ID="ID_1063831789" CREATED="1576263734132" MODIFIED="1576263740457"/>
</node>
</node>
<node TEXT="Comprobando la integridad de los sistemas de archivos mediante fsck" FOLDED="true" ID="ID_1537847983" CREATED="1576263763067" MODIFIED="1576263788073">
<node TEXT="Cuando el sistema se bloquea o se corta la corriente, gnu linux no es cap&#xe1;z de desmontar limpiamente los sistemas de archivos, de ser as&#xed; estos pueden quedar en un estado incoherente con unos cambios realizados y unos no, por tal motivo se requiere una comprobaci&#xf3;n de los sistemas de archivos." ID="ID_1888102578" CREATED="1576263789417" MODIFIED="1576263942248"/>
<node TEXT="El comando fsck es una rutina de control para los diferentes tipos de sistemas de archivos como ext2 ext o raiserfs." ID="ID_407230277" CREATED="1576263943073" MODIFIED="1576264021493"/>
<node TEXT="En resumen fsck controla y repara sistemas de archivos en gnu linux." ID="ID_342828377" CREATED="1576264022793" MODIFIED="1576264092258"/>
<node TEXT="Corroborar un sistema de archivos   con el comando fsck no es una buena idea ya que puede ocacionar da&#xf1;os en su sistema de archivos. Puede ejecutar cuando la partici&#xf3;n no est&#xe9; montada o bootear su sistema mediante una usb." FOLDED="true" ID="ID_823248173" CREATED="1576264093484" MODIFIED="1576264347832">
<node TEXT="Ejemplo: &#xa;#fsck /dev/sdb1" ID="ID_1258002926" CREATED="1576264349350" MODIFIED="1576264456237"/>
<node TEXT="En general es sistema de archivos debe estar desmontado." ID="ID_190326393" CREATED="1576264456576" MODIFIED="1576264468826"/>
</node>
<node TEXT="Opciones:&#xa;-c.- Comprueba por sectores defectuosos.&#xa;-f.- fuerza el an&#xe1;lisis del sistema de archivos.&#xa;-p.- Repara un sistema de archivos sin perguntar.&#xa;-t.- Especifica un sistema de archivos a analizar.&#xa;-v.- Detalla los mensajes generados.&#xa;-y.- Acepta todos los cambios." FOLDED="true" ID="ID_215070185" CREATED="1576264512682" MODIFIED="1576264759710">
<node TEXT="Si no se especifican opciones este reparar&#xe1; todo el sistema de archivos que figure dentro de /etc/fstab." ID="ID_200409603" CREATED="1576264759785" MODIFIED="1576264875861"/>
<node TEXT="Resumen de todos los comandos equivalentes:&#xa;1&#xa;fsck -t ext2&#xa;fsck -t ext3&#xa;fsck -t reiserfs&#xa;fsck -t xfs&#xa;fsck -t vfat&#xa;2&#xa;fsck.ext2&#xa;fsck.ext3&#xa;fsck.reiserfs&#xa;fsck.xfs&#xa;fsck.vfat&#xa;3&#xa;e2fsck&#xa;e2fsck&#xa;reiserfsck&#xa;xfs_check&#xa;dosfsck" ID="ID_789032447" CREATED="1576264876750" MODIFIED="1576279390315"/>
</node>
</node>
<node TEXT="Herramientas avanzadas de administraci&#xf3;n de los sistemas de archivos" FOLDED="true" ID="ID_717921539" CREATED="1576279449165" MODIFIED="1576279470841">
<node TEXT="Sistemas de archivos ext2 y ext3&#xa;dumpe2fs.- Muestra la informaci&#xf3;n de bloques y superbloques para los sistemas de archivos ext2 y ext3.&#xa;tune2fs.- Esta herramienta ajusta par&#xe1;metros sobre sistema de archivos  ext2 y ext3. Es utilizada para activar la funci&#xf3;n de registro por diario, a una partici&#xf3;n de ext2, o para establecer un n&#xfa;mero m&#xe1;ximo de veces que una partici&#xf3;n debe ser montadaantes de ser analizada.&#xa;debugfs.- Es una herramienta de depuraci&#xf3;n de programas interactiva. Usted puede utilizar esta herramienta para examinar o cambiar el estado de un sistema de archivos de ext2 o ext3." ID="ID_1202993366" CREATED="1576514431694" MODIFIED="1576514876745"/>
<node TEXT="Sistema de archivos reiserfs&#xa;debugreiserfs.- Muestra parte de la informaci&#xf3;n mostrada por el comando dumpe2fs.&#xa;reiserfstune.- Es una herramienta de ajuste del sistema de archivos ReiserFS." ID="ID_884473710" CREATED="1576514903003" MODIFIED="1576515001570"/>
<node TEXT="Sistema de archivos XFS&#xa;xfs_repair.- Esta herramienta repara un sistema de archivos XFS.&#xa;xfs_info.- Muesra la informaci&#xf3;n del sistema de archivos XFS.&#xa;xfs_admin.- Permite cambiar los par&#xe1;metros de un sistema de archivos.&#xa;xfs_db.- Examina o depura un sistema de archivos XFS.&#xa;xfs_growfs.- puede expandir un sistema de archivos XFS." ID="ID_1573197843" CREATED="1576515051650" MODIFIED="1576515276593"/>
</node>
<node TEXT="" ID="ID_903917988" CREATED="1576514385800" MODIFIED="1576514385800"/>
</node>
</node>
<node TEXT="104.3 Montar y desmontar los sistemas de archivos" FOLDED="true" POSITION="right" ID="ID_496148865" CREATED="1578622625357" MODIFIED="1579145571783">
<edge COLOR="#808080"/>
<node TEXT="Cuando una partici&#xf3;n es montada, los archivos contenidos en la misma pasan a ser parte del sistema." ID="ID_365692473" CREATED="1578622666644" MODIFIED="1578622801357"/>
<node TEXT="Las particiones son montadas en directorios, los archivos en las particiones pasan a ser archivos en el directorio montado." ID="ID_1398144940" CREATED="1578622801992" MODIFIED="1578622880966"/>
<node TEXT="la ubicaci&#xf3;n donde el sistema de archivos es montado se le conoce como punto de montaje." ID="ID_1358995853" CREATED="1578622882038" MODIFIED="1578622910414"/>
<node TEXT="Archivo de configuraci&#xf3;n /etc/fstab" ID="ID_126464826" CREATED="1578623632312" MODIFIED="1578623680533">
<node TEXT="Muestra todos los discos y particiones disponibles." ID="ID_567786988" CREATED="1578623680542" MODIFIED="1578623800789"/>
<node TEXT="Indica como las particiones deben ser montadas o integradas a la estructura general a un sistema de archivos." ID="ID_1645086010" CREATED="1578623802212" MODIFIED="1578623877219"/>
<node TEXT="Es invocado durante el arranque del sistema." ID="ID_395603624" CREATED="1578623880333" MODIFIED="1578624162604"/>
<node TEXT="#less /etc/fstab" ID="ID_903546840" CREATED="1578624163384" MODIFIED="1578624176609"/>
</node>
<node TEXT="Directorio /media" ID="ID_338285345" CREATED="1578624419878" MODIFIED="1578624431280">
<node TEXT="Los cd son montados en el directorio /media por defecto." ID="ID_119588530" CREATED="1578624431289" MODIFIED="1578624690427"/>
</node>
<node TEXT="Comando mount" ID="ID_2591249" CREATED="1578624691031" MODIFIED="1578624703283">
<node TEXT="Si ninguna opci&#xf3;n es dada, el comando mount muestra los sistemas de archivos montados actualmente." ID="ID_1243271904" CREATED="1578624703288" MODIFIED="1578624762092"/>
<node TEXT="Posee la siguiente sintaxis: #mount -t type divice dir" ID="ID_1663278095" CREATED="1578624762798" MODIFIED="1578624982955">
<node TEXT="Ejemplo: #mount -t iso9660 /dev/sdc  /media/test/" ID="ID_210910545" CREATED="1578624982965" MODIFIED="1578625083354"/>
<node TEXT="opciones del comando mount:&#xa;sync / async.- Indica la manera en que se debe dedicar la entrada y la salida del sistema de archivos, sync especifica que se realice de manera sincrona.La opci&#xf3;n opuesta es async.&#xa;atime / noatime.- Actualizar o no actualizar el tiempo del nodo-i para cada acceso, esto es lo predeterminado.&#xa;Auto.- Con la opci&#xf3;n auto el dispositivo ser&#xe1; montado autom&#xe1;ticamente, o en caso de que e&#xf1;l comando mount -a sea ejecutado auto es el valor por defecto.&#xa;noauto.- Si no se desea que el dispositivo se monte autom&#xe1;ticamente, se deber&#xe1; sustituir por noauto.&#xa;defaults.- Utiliza las opciones por defecto: rw, suid, dev, exec, nouser, async.&#xa;dev / nodev.- Interpretar, no interpretar dispositivos especiales de bloques en sistema de archivos.&#xa;exec / noexec.- exec permite ejecutar binarios que estan en la partici&#xf3;n, mientras que noexec lo impide.&#xa;suid.- Permite la operaci&#xf3;n sobre todos los bits suid y sgid.&#xa;nosuid.- Bloquea la operaci&#xf3;n sobre los bits suid y sgid" ID="ID_1318805366" CREATED="1578625084153" MODIFIED="1578625948902"/>
<node TEXT="Mas opciones:&#xa;User.- Permite a cualquier usuario montar el sistema de archivos, implica directamente las opciones noexec, no suid, nodev, a menos que se especifiquen otras.&#xa;nouser.- se utiliza esta opci&#xf3;n solo cuando el usuario root podr&#xe1; montar el sistema de archivos.&#xa;owner.- El primer usuario distinto de root tiene derecho a montar el dispositivo.&#xa;remount.- Intentr remontar un sistema de archivos ya montado. Esto se emplea com&#xfa;nmente para cambiar las opciones de&#xf1; montaje en un sistema de ficheros que estaba de lectura exclusiva.&#xa;ro .- Montar para solo lectura.&#xa;rw .- Montar el sistema de archivos para lectura y escritura." ID="ID_1945346226" CREATED="1578626163065" MODIFIED="1578626549483"/>
</node>
</node>
<node TEXT="El comando umount" ID="ID_290866922" CREATED="1578626572925" MODIFIED="1578626583653">
<node TEXT="Instruye al sistema operativo que un determinado sistema de archivos debe ser desasociado de su punto de montaje haciendo que este sea inaccesible." ID="ID_1472312277" CREATED="1578626583660" MODIFIED="1578626642830"/>
<node TEXT="Sintaxis: #umount dev_or_dir" ID="ID_806730470" CREATED="1578626645579" MODIFIED="1578626678085">
<node TEXT="En done dev_or_dir significa el dispositivo o el punto de montaje." ID="ID_371997817" CREATED="1578626695628" MODIFIED="1578626723604"/>
</node>
</node>
</node>
<node TEXT="104.4 Limitaci&#xf3;n del espacio en disco." FOLDED="true" POSITION="right" ID="ID_843557083" CREATED="1579145613478" MODIFIED="1579145646853">
<edge COLOR="#808080"/>
<node TEXT="En la vida cootidiana nos encontramos con algunos casos especiales en los cuales en los sistemas gnu linux con gran cantidad de usuarios como servidor de correo y servidor samba, pero tarde que temprano se termina el espacio debido a que los usuarios almacenan demaciada informaci&#xf3;n." ID="ID_1821578188" CREATED="1579145646891" MODIFIED="1579146178471"/>
<node TEXT="Con el uso de cuotas de disco (disk quotas), es posible limitar el espacio en disco tanto para  cada usuario, como de forma global." ID="ID_164347438" CREATED="1579146178969" MODIFIED="1579146348233"/>
<node TEXT="Tipos de cuota" ID="ID_1817764622" CREATED="1579146349169" MODIFIED="1579146362794">
<node TEXT="Por bloques" ID="ID_288338188" CREATED="1579146362804" MODIFIED="1579146375046">
<node TEXT="Un bloque, corresponde a 1kb, y una cuota por bloques corresponde al total de bloques que un usuario puede utilizar en el sistema." ID="ID_1421695589" CREATED="1579146375057" MODIFIED="1579146460816">
<node TEXT="Los archivos se guardan en bloques de disco, ejemplo:&#xa;Si un archivo que pesa 100 bytes se guardar&#xe1; en un bloque de 1kb en el disco duro." ID="ID_27420905" CREATED="1579146460828" MODIFIED="1579146741668"/>
</node>
</node>
<node TEXT="Por inodos (Inodes)." ID="ID_273889191" CREATED="1579146747025" MODIFIED="1579146761717">
<node TEXT="Un inodo o inode en ingl&#xe9;s (Index Node) es un n&#xfa;mero que actua como apuntador para el sistema de archivos de Linux y le indica en que bloques espec&#xed;ficos del disco duro se encuentran los datos de un archivo. Tambi&#xe9;n el inodo en su referencia guarda informaci&#xf3;n sobre permisos, propietario, atributos, etc. Se podr&#xed;a ver en una analog&#xed;a simple que un inodo es como un n&#xfa;mero de serie &#xfa;nico para cada archivo del sistema y a trav&#xe9;s de este n&#xfa;mero el sistema recupera sus datos (bloques) y sus atributos o metadatos (permisos, propietario, fechas, etc.)." ID="ID_801750540" CREATED="1579146761723" MODIFIED="1579146941845"/>
<node ID="ID_1257256298" CREATED="1579147094483" MODIFIED="1579147152185"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      En el caso de las cutoas, una cuota por inodos indicar&#237;a el total de indos a los que el usuario tiene derecho, casi representar&#237;a el total de archivos que el usuario puede crear y digo &quot;casi&quot; porque los usuarios podemos crear enlaces simb&#243;licos <code>ln -s</code>&#160;sobre archivos ya existentes que no aumentan las cantidad de inodos. Pero por simplicidad puedes verlo como un 1 inodo = 1 archivo.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Tanto las cuotas por bloques o por inodos tienen limites de uso, estas son de dos tipos:" ID="ID_1479118880" CREATED="1579377773769" MODIFIED="1579377817442">
<node TEXT="Hard" ID="ID_1739256444" CREATED="1579377817478" MODIFIED="1579378733698">
<icon BUILTIN="pencil"/>
<icon BUILTIN="pencil"/>
<node TEXT="Duro.- Cuando se establece para bloques o inodos, es el limite absoluto, el usuario no podr&#xe1; exeder ese limite." ID="ID_351489122" CREATED="1579378733719" MODIFIED="1579378815530"/>
</node>
<node TEXT="Soft" ID="ID_1650850055" CREATED="1579379576877" MODIFIED="1579379604381">
<icon BUILTIN="pencil"/>
<icon BUILTIN="pencil"/>
<node TEXT="Suave.-Puede ser exedido por el usuario pero recibiera advertencias constante mente que se termina el espacio." ID="ID_146532027" CREATED="1579379604391" MODIFIED="1579381151655">
<node TEXT="Cuando se usa el limite soft pueden ocurrir dos situaciones." ID="ID_599065968" CREATED="1579381153039" MODIFIED="1579381173145">
<node TEXT="a) Que NO se tenga establecido un tiempo de gracia, y entonces el usuario podr&#xe1; seguir usando bloques o inodos hasta llegar al l&#xed;mite HARD que ser&#xe1; su l&#xed;mite absoluto de uso." ID="ID_1161154695" CREATED="1579381173158" MODIFIED="1579381228221"/>
<node TEXT="b) Que SI se tenga establecido el tiempo de gracia, que puede ser en d&#xed;as, horas, minutos o segundos. En este caso, el usuario podr&#xe1; seguir usando bloques o inodos hasta que termine el tiempo de grac&#xed;a o llegue al l&#xed;mite HARD, cualquiera que ocurra primero." ID="ID_514170298" CREATED="1579381229251" MODIFIED="1579381285518"/>
</node>
</node>
</node>
</node>
<node TEXT="&#xbf;Donde se implementan?" ID="ID_884605903" CREATED="1579382567865" MODIFIED="1579382605803">
<icon BUILTIN="full-1"/>
<node TEXT="Las cuotas se establecen por filesystem o sistema de archivos, es decir, debes de decidir en donde es m&#xe1;s conveniente instalar un sistema de cuotas, pero no hay ning&#xfa;n problema si se instala en todos. Las cuotas pueden establecerse por usuario, por grupos o ambos." ID="ID_453388356" CREATED="1579382605808" MODIFIED="1579382636964"/>
</node>
<node TEXT="Configuraci&#xf3;n" FOLDED="true" ID="ID_1126907380" CREATED="1579382946110" MODIFIED="1579382968967">
<icon BUILTIN="full-2"/>
<node TEXT="Todo debe hacerse como root, y lo primero que haremos es editar el archivo &quot;/etc/fstab&quot; y a&#xf1;adiremos &quot;usrquota&quot; o &quot;grpquota&quot;, dependiendo si se desea cuotas por usuario o grupos, o incluso ambas." ID="ID_1328710439" CREATED="1579382968971" MODIFIED="1579382994068">
<node ID="ID_522699713" CREATED="1579383584278" MODIFIED="1579383639339"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; vi /etc/fstab
/dev/sda2  / 		ext3 	noatime 	1 	1
/dev/sda1  /boot 	ext3 	noatime 	1 	2
/dev/sda3  /home 	ext3 	noatime 	1 	2
....
<label class="t2">(A&#241;adimos en la cuarta columna el tipo de cuotas que deseamos)</label>
/dev/sda2  / 		ext3 	noatime 	1 	1
/dev/sda1  /boot 	ext3 	noatime 	1 	2
/dev/sda3  /home 	ext3 	noatime,usrquota,grpquota 	1 	2</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Lo anterior por si solo, es obvio que no hace nada, habr&#xed;a que reiniciar el sistema para que se apliquen los cambios pero realmente no es necesario, lo siguiente re-monta el sistema de archivos &quot;/home&quot;:" ID="ID_822759266" CREATED="1579383747854" MODIFIED="1579383761269">
<node ID="ID_693896075" CREATED="1579383761279" MODIFIED="1579383798593"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; mount -o remount /home
#&gt; mount
/dev/sda1 on /boot type ext3 (rw,noatime)
/dev/sda2 on / type ext3 (rw,noatime)
/dev/sda3 on /home type ext3 (rw,noatime,usrquota,grpquota)
none on /proc type proc (rw)</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1346201139" CREATED="1579384267196" MODIFIED="1579384274165"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    El siguiente paso es verificar con el comando <code http-equiv="content-type" content="text/html; charset=utf-8">quotacheck</code>&#160;por sistemas de archivos que soporten cuotas. Este comando crea, verifica o repara el control de cuotas en los sistemas que lo soporten, en este caso creara el soporte:
  </body>
</html>
</richcontent>
<node ID="ID_282068636" CREATED="1579384274173" MODIFIED="1579384303016"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quotacheck -augmv
quotacheck: Scanning /dev/sda3 [/home] done
quotacheck: Cannot stat old user quota file: No existe el fichero o el directorio
quotacheck: Cannot stat old group quota file: No existe el fichero o el directorio
quotacheck: Cannot stat old user quota file: No existe el fichero o el directorio
quotacheck: Cannot stat old group quota file: No existe el fichero o el directorio
quotacheck: Checked 2539 directories and 35556 files
quotacheck: Old file not found.
quotacheck: Old file not found.</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1608078131" CREATED="1579384799570" MODIFIED="1579384809497"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Los errores que env&#237;a es precisamente porque no exist&#237;a un sistema de cuotas previo, es normal que los env&#237;e. Cuando las cuotas esten en pleno uso, es conveniente ejecutar <code http-equiv="content-type" content="text/html; charset=utf-8">quotacheck</code>&#160;peri&#243;dicamente para que verifique inconsistencias y se corrijan a tiempo. En cuanto a las opciones estas indican lo siguiente:
  </body>
</html>
</richcontent>
<node ID="ID_1779906262" CREATED="1579384809505" MODIFIED="1579384847521"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul http-equiv="content-type" content="text/html; charset=utf-8" class="lista">
      <li>
        a - all, es decir verifica todos los sistemas de archivos por cuotas.
      </li>
      <li>
        u - user, verifica por soporte de cuotas para usuarios.
      </li>
      <li>
        g - group, verifica por soporte de cuotas para grupos.
      </li>
      <li>
        m - no-remount, evita que el sistema se remonte como de solo lectura.
      </li>
      <li>
        v - verboso, reporta lo que hace conforme progresa, son los mensajes que salen a la terminal.
      </li>
    </ul>
    <p>
      
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1933171467" CREATED="1579384851245" MODIFIED="1579384876651"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Como ves, la opci&#243;n <em http-equiv="content-type" content="text/html; charset=utf-8">-a</em>&#160;en este caso no era necesario puesto que solo tenemos &quot;/home&quot; con cuotas, asi que el comando anterior tambi&#233;n pudiera ser invocado de esta manera:
  </body>
</html>
</richcontent>
<node ID="ID_1743907185" CREATED="1579384881867" MODIFIED="1579384902013"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quotacheck -ugmv /home</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="El sistema esta listo para manipular cuotas de usuario, esto lo podemos comprobar porque en la ra&#xed;z del sistema de archivos soportado con cuotas deben existir los archivos &quot;aquota.user&quot; y &quot;aquota.group&quot; que son binarios, no trates de modificarlos o manipularlos:" ID="ID_156295152" CREATED="1579385000427" MODIFIED="1579385034263">
<node ID="ID_770204583" CREATED="1579385007982" MODIFIED="1579385048708"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; cd /home
#&gt; ls -l
total 72
-rw-------  1 root   root    8192 2008-05-17 21:38 aquota.group
-rw-------  1 root   root    8192 2008-05-17 21:38 aquota.user
drwx--x--x  4 user1  user1   4096 2008-05-12 16:13 user1/
drwx--x--x  4 user2  user2   4096 2008-05-12 16:13 user2/
drwx--x--x  3 user3  user3   4096 2008-05-05 12:01 user3/
drwx--x--x  3 user4  user4   4096 2008-05-05 12:01 user4/</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1817640503" CREATED="1579385054319" MODIFIED="1579385194691"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Ahora bien, lo anterior deja listo el sistema para el soporte de cuotas pero est&#225;s siguen sin ser activadas se requiere activar el soporte de cuotas, para lo cual invocamos el comando <code http-equiv="content-type" content="text/html; charset=utf-8">quotaon</code>:
  </body>
</html>
</richcontent>
<node ID="ID_1275826382" CREATED="1579385194698" MODIFIED="1579385219595"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quotaon -ugv /home
/dev/sda3 [/home]: group quotas turned on
/dev/sda3 [/home]: user quotas turned on</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1653346091" CREATED="1579385220284" MODIFIED="1579385259685"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Activamos para &quot;/home&quot; cuotas de usuario y grupos. Cuando por alguna raz&#243;n sea necesario desactivar las cuotas, entonces utiliza la contraparte, que es el comando <code http-equiv="content-type" content="text/html; charset=utf-8">quotaoff</code>:
  </body>
</html>
</richcontent>
<node ID="ID_1560685254" CREATED="1579385259696" MODIFIED="1579385283788"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quotaoff -v /home
/dev/sda3 [/home]: group quotas turned off
/dev/sda3 [/home]: user quotas turned off</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Aplicando cuotas a usuarios." FOLDED="true" ID="ID_674744629" CREATED="1579385514802" MODIFIED="1579385780206">
<icon BUILTIN="button_ok"/>
<node ID="ID_793459160" CREATED="1579385534319" MODIFIED="1579385812523"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Ahora hay que aplicar la cuota por usuario, aunque el sistema de archivos ya soporta cuotas y est&#225;n habilitadas, por defecto ning&#250;n usuario tiene establecidas cuotas. Asi que para iniciar habr&#225; que administrar cada usuario a trav&#233;s del comando <code http-equiv="content-type" content="text/html; charset=utf-8">edquota</code>, que abrir&#225; el editor de texto que se tenga por defecto y mostrar&#225; lo siguiente:
  </body>
</html>
</richcontent>
<node ID="ID_653686869" CREATED="1579385812530" MODIFIED="1579385830402"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -u user1
Disk quotas for user user1 (uid 502):
  Filesystem                   blocks       soft       hard     inodes     soft     hard
  /dev/sda3                        56          0          0         14   </pre>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Las columnas &quot;blocks&quot; e &quot;inodes&quot; son informativas, es decir nos indican la cantidad de bloques o inodos utilizados actualmente por el usuario, y las que podemos editar son las columnas &quot;soft&quot; y &quot;hard&quot; de cada caso." ID="ID_1777305100" CREATED="1579386074841" MODIFIED="1579386079592"/>
<node TEXT="Como ya se explic&#xf3; en la primera parte de este art&#xed;culo, se puede indicar libremente cualquiera de los cuatro valores, es perfectamente posible establecer valores por bloques, por inodos o ambos, solo recuerda que el l&#xed;mite soft debe ser menor al hard. Si se establece solo el hard, no habr&#xe1; advertencias previas y el usuario ya no podr&#xe1; guardar archivos cuando se llegue al valor. Si se establece soft y hard, avisar&#xe1; cuando se rebase el l&#xed;mite soft y entrar&#xe1; en juego el periodo de gracia. Si se acaba el tiempo de gracias o se llega al har (lo que sea primero) ya no se podr&#xe1;n crear m&#xe1;s archivos hasta que no se eliminen algunos de los que se tengan actualmente." ID="ID_316799221" CREATED="1579386137291" MODIFIED="1579386140651"/>
</node>
<node ID="ID_1718107586" CREATED="1579385952605" MODIFIED="1579386180697"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Para modificar cuotas a nivel grupo, se usa el mismo comando pero con la opci&#243;n <em http-equiv="content-type" content="text/html; charset=utf-8">-g</em>&#160;(<code>edquota -g ventas</code>).
  </body>
</html>
</richcontent>
<node ID="ID_73715688" CREATED="1579386183210" MODIFIED="1579386196359"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    En el ejemplo previo se modifica la cuota del usuario &quot;user1&quot; en el sistema de archivos &quot;/home&quot; que es el que se ha usado de ejemplo en este art&#237;culo de LinuxTotal.com.mx, el comportamiento por default es modificar cuotas para ese usuario en todos los sistemas de archivos que tengan activo el control de cuotas (<code http-equiv="content-type" content="text/html; charset=utf-8">quotaon</code>). Si se desea control de cuotas para un filesystem en espec&#237;fico entonces se agrega la opci&#243;n <em>-f</em>:
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1900073291" CREATED="1579386198100" MODIFIED="1579386214132"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -u user1 -f /home
<label class="t2">(solo aplica la cuota en el sistema de archivos indicado)</label></pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Verificando el uso de las cuotas" FOLDED="true" ID="ID_508774921" CREATED="1579386410469" MODIFIED="1579386421181">
<icon BUILTIN="button_ok"/>
<node ID="ID_1035911428" CREATED="1579386421185" MODIFIED="1579386445933"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      Como usuario administrador 'root' puedes ver el uso de cuotas de cualquier usuario, ya sea individualmente o por medio de un reporte global.
    </p>
    <p>
      Por usuario o individualmente se usa el comando <code>quota</code>, estando como &quot;root&quot;:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_409710032" CREATED="1579386449523" MODIFIED="1579386464708"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quota -u user1
Disk quotas for user user1 (uid 502):
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
      /dev/sda3      56      70     100              14       0       0</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Con usuarios que manejan cantidades muy grandes de cuota, es un poco dificil calcular en t&#xe9;rminos de megas o gigas el espacio usuado y los l&#xed;mites de cuotas:" ID="ID_1291478540" CREATED="1579386903215" MODIFIED="1579386909196">
<node ID="ID_238741166" CREATED="1579386909201" MODIFIED="1579386934419"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quota -u sergio
Disk quotas for user sergio (uid 500):
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
      /dev/sda3 42578888       0 50000000           34895       0       0</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1453262071" CREATED="1579386965428" MODIFIED="1579386971119"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      Usando la opci&#243;n <em>-s</em>&#160;se mejora el informe:
    </p>
    <pre></pre>
  </body>
</html>
</richcontent>
<node ID="ID_1715786782" CREATED="1579386971125" MODIFIED="1579386988978"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; quota -s -u sergon
Disk quotas for user sergon (uid 500):
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
      /dev/sda3  41582M       0  48829M           34905       0       0</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1262052435" CREATED="1579387026747" MODIFIED="1579387030922"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Como usuario individual del sistema, puedes observar tus cuotas con el mismo comando <code http-equiv="content-type" content="text/html; charset=utf-8">quota</code>, sin argumentos
  </body>
</html>
</richcontent>
</node>
<node ID="ID_306674018" CREATED="1579387034360" MODIFIED="1579387047910"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Ahora bien, si se desea un reporte global de las cuotas de todos los usuarios o por grupos, siendo &quot;root&quot; utiliza el comando <code http-equiv="content-type" content="text/html; charset=utf-8">repquota</code>:
  </body>
</html>
</richcontent>
<node ID="ID_1335187015" CREATED="1579387067920" MODIFIED="1579387075701"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; repquota /home
*** Report for user quotas on device /dev/sda3
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
root      --  184280       0       0             11     0     0
sergio    -- 42579852      0 50000000          34902    0     0
user1     --      56      70     100             14     0     0
user2     --      52       0       0             13     0     0
user3     --      28       0       0              7     0     0
user4     --      28       0       0              7     0     0</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_843914838" CREATED="1579387254294" MODIFIED="1579387261074"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Con <code http-equiv="content-type" content="text/html; charset=utf-8">repquota</code>&#160;es tambi&#233;n posible utilizar la opci&#243;n <em>-s</em>&#160; para observar los tama&#241;os en formato legible. Si se usa la opci&#243;n <em>-a</em>&#160; (all) en vez del sistema de archivos &quot;/home&quot;, el reporte ser&#225; para todos los sistemas de archivos en el equipo que soporten cuotas. Asi mismo este reporte por defecto es por usuarios, si se requiere que <code>repquota</code>&#160; reporte por grupos, a&#241;ade entonces la opci&#243;n <em>-g</em>.
  </body>
</html>
</richcontent>
</node>
<node TEXT="Obs&#xe9;rvese en la segunda l&#xed;nea del reporte el tiempo de gracia (grace time), que es de 7 d&#xed;as tanto para cuotas por bloque como para cuotas por archivos o inodos. Esto aplica para todos los usuarios en global, como se aprecia en el listado que ninguno tiene establecido un tiempo de gracia diferente al global." ID="ID_53013576" CREATED="1579387270286" MODIFIED="1579387309015"/>
</node>
</node>
<node TEXT="Estableciendo el tiempo de gracia" FOLDED="true" ID="ID_1803149358" CREATED="1579387316860" MODIFIED="1579387369516">
<icon BUILTIN="button_ok"/>
<node ID="ID_846214179" CREATED="1579387369522" MODIFIED="1579387384043"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    A nivel global, un periodo de gracia para todos, utiliza la opci&#243;n <em http-equiv="content-type" content="text/html; charset=utf-8">-t</em>&#160; del comando <code>edquota</code>, como en el siguiente ejemplo, recuerda que debes ser &quot;root&quot;:
  </body>
</html>
</richcontent>
<node ID="ID_1175037362" CREATED="1579387384050" MODIFIED="1579387401160"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -t
Grace period before enforcing soft limits for users:
Time units may be: days, hours, minutes, or seconds
  Filesystem             Block grace period     Inode grace period
  /dev/sda3                     7days                  7days</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_108905838" CREATED="1579387406118" MODIFIED="1579387453745"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      7 d&#237;as es el periodo por defecto, si lo cambias a digamos 12 horas, ser&#237;a &quot;12hours&quot;. El tiempo de gracia puede ser distinto para el l&#237;mite soft por bloques o por inodos.
    </p>
    <p>
      Por usuario espec&#237;fico se realiza con la opci&#243;n <em>-T</em>&#160;del mismo comando e indicando el usuario:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_7962640" CREATED="1579387453751" MODIFIED="1579387516838"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -u user1 -T
Times to enforce softlimit for user user1 (uid 502):
Time units may be: days, hours, minutes, or seconds
  Filesystem                         block grace               inode grace
  /dev/sda3                              unset                  unset</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_889805766" CREATED="1579387517075" MODIFIED="1579387522469"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Lo &#250;nico que hay que considerar es que los tiempos de gracias por usuario deben ser menores al global. Y que este empieza a correr una vez que se ha llegado al l&#237;mite soft. Cuando esto suceda, si entras a editar de nuevo el tiempo de gracia del usuario (<code http-equiv="content-type" content="text/html; charset=utf-8">edquota -u user -T</code>) se reflejara en segundos el tiempo que le queda, pudi&#233;ndolo aumentar de nuevo si eres &quot;root&quot;. O dejarlo en cero y entonces el global ser&#225; el que se utilice.
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Fijar cuotas de manera local a todos los usuarios" FOLDED="true" ID="ID_367101000" CREATED="1579387528920" MODIFIED="1579387553203">
<icon BUILTIN="button_ok"/>
<node TEXT="En sistemas Linux con pocos usuarios, establecer las cuotas usuario por usuario no representa ning&#xfa;n problema. Pero si hablamos por ejemplo de una universidad donde pudieran existir miles de cuentas entonces si es un problema establecer cuentas individualmente. Realmente no existe una manera &quot;oficial&quot; de establecer cuotas masivamente, sin embargo, no hay problema, usaremos un peque&#xf1;o script que te permitira realizarlo." ID="ID_542824191" CREATED="1579387553209" MODIFIED="1579387578853"/>
<node TEXT="Establece la cuota que deseas globalmente en un solo usuario:" ID="ID_1836092385" CREATED="1579387579414" MODIFIED="1579387595988">
<node ID="ID_618604568" CREATED="1579387595994" MODIFIED="1579387608592"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -u user1
Disk quotas for user user1 (uid 502):
  Filesystem                   blocks       soft       hard     inodes     soft     hard
  /dev/sda3                        68        300        400         17        0        0
:wq</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1861415805" CREATED="1579387645691" MODIFIED="1579387649056"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Veamos el reporte de cuotas con <code http-equiv="content-type" content="text/html; charset=utf-8">repquota</code>:
  </body>
</html>
</richcontent>
<node ID="ID_1021431096" CREATED="1579387649064" MODIFIED="1579387658563"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">[root@segolap ~]# repquota /home
*** Report for user quotas on device /dev/sda3
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
user1     --      68     300     400             17     0     0
user2     --     352       0       0             13     0     0
user3     --      28       0       0              7     0     0
user4     --      28       0       0              7     0     0</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_914133699" CREATED="1579387666743" MODIFIED="1579387677709"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Solo el usuario &quot;user1&quot; tiene cuotas, las columnas de &quot;grace&quot; tendr&#225;n valores una vez que se llegue al l&#237;mite soft o suave. Usaremos entonces la opci&#243;n <em http-equiv="content-type" content="text/html; charset=utf-8">-p</em>&#160;(protptype) para hacer duplicados a partir del ya establecido.
  </body>
</html>
</richcontent>
<node ID="ID_876383186" CREATED="1579387703713" MODIFIED="1579387708405"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -p user1 user2</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Con lo anterior &quot;copias&quot; la informaci&#xf3;n de l&#xed;mites de cuotas del &quot;user1&quot; al &quot;user2&quot;, no hay l&#xed;mite de cuantos usuarios puedes colocar como argumentos asi que lo siguiente es v&#xe1;lido:" ID="ID_1846712398" CREATED="1579387661956" MODIFIED="1579387736125">
<node ID="ID_640053833" CREATED="1579387736134" MODIFIED="1579387745794"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -p user1 user2 user3 user4</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_372818960" CREATED="1579387748098" MODIFIED="1579387759165"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Pr&#225;ctico para unos cuantos usuarios pero in&#250;til si necesitamos duplicarlo en cientos de usuarios, asi que hagamos un comando compuesto que nos extraiga los nombres de los usuarios, se puede usar por ejemplo <code http-equiv="content-type" content="text/html; charset=utf-8">gawk</code>&#160; o <code>awk</code>&#160;para realizar lo anterior:
  </body>
</html>
</richcontent>
<node ID="ID_532700368" CREATED="1579387759175" MODIFIED="1579387769298"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; gawk -F: '$3 &gt; 499 {print $1}' /etc/passwd
user1
user2
user3
user4</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_430247658" CREATED="1579387801228" MODIFIED="1579387806188"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Usamos el separador &quot;:&quot; de campos (-F), e indicamos como acci&#243;n que en el campo 3 ($3) busquemos todos los UID mayores a 499 y que los imprima ({print $1}). Ahora solo tenemos que usar este comando junto con <code http-equiv="content-type" content="text/html; charset=utf-8">edquota -p</code>:
  </body>
</html>
</richcontent>
<node ID="ID_964763686" CREATED="1579387806200" MODIFIED="1579387822642"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; edquota -p user1 `gawk -F: '$3 &gt; 499 {print $1}' /etc/passwd`

<label class="t2"/>(importante: nota el uso de acento grave que abarca al comando awk or gawk,
esto para que el shell lo ejecute primero y el resultado ser&#225;n los argumentos,
uno o cientos de usuarios cuyo UID es mayor a 499)<label/></pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_221837005" CREATED="1579387825917" MODIFIED="1579387848071"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Haciendo uso de <code http-equiv="content-type" content="text/html; charset=utf-8">repquota</code>&#160;de nuevo veamos que pas&#243;:
  </body>
</html>
</richcontent>
<node ID="ID_283355341" CREATED="1579387848084" MODIFIED="1579387859447"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; repquota /home
*** Report for user quotas on device /dev/sda3
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
user1     --      68     300     400             17     0     0
user2     --     352     300     400  7days      13     0     0
user3     --      28     300     400              7     0     0
user4     --      28     300     400              7     0     0</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Primero, todos los usuarios tienen las mismas cuotas que el &quot;user1&quot; que fue el prototipo para los dem&#xe1;s y segundo se observa que el &quot;usuario&quot; que tiene 352 bloques utilizados al pasar el l&#xed;mite suave entro al periodo de gracia autom&#xe1;ticamente que el global es de 7 d&#xed;as. A partir del instante que el l&#xed;mite cambi&#xf3; de 0 a 300, comenz&#xf3; el periodo de gracia. Ahora solo podr&#xe1; crear m&#xe1;s archivos durante 7 d&#xed;as o cuando llegue a 400, lo primero que ocurra, claro, asumiendo que no borre archivos primeros para recuperar espacio." ID="ID_1230128419" CREATED="1579387900936" MODIFIED="1579387904767"/>
</node>
<node TEXT="Avisos de cuotas exedidas (Warnquota)" FOLDED="true" ID="ID_974630742" CREATED="1579387913274" MODIFIED="1579387946527">
<icon BUILTIN="button_ok"/>
<node TEXT="Cuando un usuario llega al l&#xed;mite suave o soft al crear o modificar un documento, algo como lo siguiente aaprecer&#xe1;:" ID="ID_1843516598" CREATED="1579387946533" MODIFIED="1579387960891">
<node ID="ID_240643116" CREATED="1579387960901" MODIFIED="1579387971295"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">user2&gt; ls -l &gt; directorio.txt 
sda3: warning, user block quota exceeded.</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1924726759" CREATED="1579388002909" MODIFIED="1579388007322"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8">
      En este instante como el &quot;user2&quot; no ha llegado al l&#237;mite &quot;hard&quot; ni ha expirado el tiempo de gracia, el sistema permite crear el archivo pero se le notifica con un warning.
    </p>
    <p>
      Pero si lo que deseamos es notificar inmediatamente y via correo electr&#243;nico que un usuario llego a su l&#237;mite, por ejemplo, un server de correo electr&#243;nico, un usuario que ha recibido spam y esta saturando su cuenta, puede ser notificado que su couta esta llegando al l&#237;mite.
    </p>
    <p>
      Para lo anterior usaremos el comando <code>warnquota</code>. Este comando simplemente inv&#243;calo desde la l&#237;nea de comandos, sin argumentos, revisar&#225; los sistemas de archivos con cuotas activadas (<code>quotaon</code>) y revisar&#225; todos los usuarios buscando quien ha excedido el l&#237;mite soft tanto por bloques como por inodos, y a aquellos que lo hayan excedido les enviar&#225; un correo notif&#237;candoles de lo anterior.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1729779954" CREATED="1579387977474" MODIFIED="1579388038674"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Puedes agrgar en cron una l&#237;nea como la siguiente para que <code http-equiv="content-type" content="text/html; charset=utf-8">warnquota</code>&#160; haga su trabajo cada 12 horas:
  </body>
</html>
</richcontent>
<node ID="ID_452561006" CREATED="1579388038679" MODIFIED="1579388048156"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#&gt; vi /etc/crontab
...
0 0,12 * * * root /usr/sbin/warnquota
...</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1399620259" CREATED="1579388082716" MODIFIED="1579388087750"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <code http-equiv="content-type" content="text/html; charset=utf-8">warnquota</code>&#160;viene con los mensajes en ingl&#233;s por defecto, el archivo de configuraci&#243;n es &quot;/etc/warnquota.conf&quot;, es muy intuitivo y f&#225;cil de cambiar, personal&#237;zalo con los mensajes a espa&#241;ol para que sea m&#225;s f&#225;cil entender a tus usuarios que han excedido sus cuotas.
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Referencia: https://www.linuxtotal.com.mx/?cont=info_admon_018" ID="ID_452349165" CREATED="1579388096704" MODIFIED="1579388113422">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node TEXT="104.5 Manejo de asignaci&#xf3;n de perimisos parte1" FOLDED="true" POSITION="right" ID="ID_1679636712" CREATED="1579461512390" MODIFIED="1579462637239">
<edge COLOR="#808080"/>
<node TEXT="Los archivos y directorios pertenecen siempre a un usuario, el propietario determina la clase de usuario para un archivo determinado." ID="ID_1530169833" CREATED="1579461598191" MODIFIED="1579462735707">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Distintos permisos aplican para la clase de usuario." ID="ID_1384942953" CREATED="1579462125779" MODIFIED="1579462510010">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Los archivos y directorios son tambi&#xe9;n asignados a un grupo, el cual define la clase de grupo para ese archivo." ID="ID_1276081890" CREATED="1579462162795" MODIFIED="1579462727521">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Los usuarios que no son los propietarios de ese archivo, ni tampoco son miembros del grupo asignado a ese archivo, pertenecen a la clase del resto, distintos permisos aplican para la clase del resto." ID="ID_380011954" CREATED="1579462925459" MODIFIED="1579463017156">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Hay tres sistemas especificos en los sistemas Gnu linux que aplican para cada clase." ID="ID_1881690231" CREATED="1579463033853" MODIFIED="1579463102311">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="El permiso de rectura (Read) simbolizado por una r" ID="ID_1930990992" CREATED="1579463105745" MODIFIED="1579463196438">
<icon BUILTIN="idea"/>
</node>
<node TEXT="El permiso de escritura (Write) simbolizado por una w" ID="ID_1008567551" CREATED="1579463153899" MODIFIED="1579463183164">
<icon BUILTIN="idea"/>
</node>
<node TEXT="El permiso de ejecuci&#xf3;n (execution) simbolizado por una x" ID="ID_777511420" CREATED="1579463263149" MODIFIED="1579463287910">
<icon BUILTIN="idea"/>
</node>
</node>
<node ID="ID_1577874300" CREATED="1579464209593" MODIFIED="1579464219882">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Usando chmod para cambiar los permisos<br/>chmod (change mode) es el comando utilizado para cambiar permisos, se pueden agregar o remover permisos a uno o mas archivos con + (mas) o &#8211; (menos)
    </p>
    <p style="text-align: justify">
      Si quieres prevenirte de modificar un archivo importante, simplemente quita el permiso de escritura en tu &#8220;archivo&#8221; con el comando chmod
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_350918169" CREATED="1579464222102" MODIFIED="1579464279620"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejemplo:
    </p>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod -w tuArchivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Si quieres hacer un script ejecutable, escribe:" ID="ID_1599377787" CREATED="1579464307715" MODIFIED="1579464344563">
<icon BUILTIN="pencil"/>
<node ID="ID_694412622" CREATED="1579464344566" MODIFIED="1579464379508"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejemplo:
    </p>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod +x tuScript</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Si quieres remover o agregar todos los atributos a la vez." ID="ID_1526391006" CREATED="1579464776400" MODIFIED="1579464891107">
<icon BUILTIN="pencil"/>
<node ID="ID_1985725169" CREATED="1579464891303" MODIFIED="1579464952264"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejemplo:
    </p>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod -rwx archivo
#chmod +rwx archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_991751602" CREATED="1579465028332" MODIFIED="1579465080658">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Tambi&#233;n puedes usar el signo = (igual) para establecer los permisos en una combinaci&#243;n exacta, este comando remueve los permisos de escritura y ejecuci&#243;n dejando solo el de lectura.
    </p>
    <pre></pre>
  </body>
</html>
</richcontent>
<node ID="ID_271316401" CREATED="1579465080664" MODIFIED="1579465128531"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejemplo:
    </p>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod =r archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Estructura b&#xe1;sica de permisos de un directorio" ID="ID_1757636980" CREATED="1579465258570" MODIFIED="1579465420758">
<icon BUILTIN="yes"/>
<node TEXT="Permiso de lectura de un directorio." ID="ID_1814369140" CREATED="1579465420761" MODIFIED="1579465479044">
<icon BUILTIN="full-1"/>
<node TEXT="Si un directorio tiene permiso de lectura, puedes ver los archivos que este contiene. Puedes usar un &#x201c;ls (list directory)&#x201d; para ver su contenido, que tengas permiso de lectura en un directorio no quiere decir que puedas leer el contenido de sus archivos si no tienes permiso de lectura en esos." ID="ID_574778155" CREATED="1579465479050" MODIFIED="1579465508226">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Permiso de escritura de un directorio." ID="ID_376883754" CREATED="1579465514399" MODIFIED="1579465543884">
<icon BUILTIN="full-2"/>
<node TEXT="Con el permiso de escritura puedes agregar, remover o mover archivos al directorio." ID="ID_114486010" CREATED="1579465543890" MODIFIED="1579465579675">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="Permiso de ejecuci&#xf3;n de un directorio." ID="ID_1469287024" CREATED="1579465634487" MODIFIED="1579465658942">
<icon BUILTIN="full-3"/>
<node TEXT="Ejecuci&#xf3;n te permite usar el nombre del directorio cuando estas accediendo a archivos en ese directorio, es decir este permiso lo hace que se tome en cuenta en b&#xfa;squedas realizadas por un programa, por ejemplo, un directorio sin permiso de ejecuci&#xf3;n no seria revisado por el comando find." ID="ID_388498385" CREATED="1579465658949" MODIFIED="1579465700063">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="Usuarios, grupos y otros." FOLDED="true" ID="ID_762469076" CREATED="1579465710444" MODIFIED="1579465764550">
<icon BUILTIN="yes"/>
<node ID="ID_1853142226" CREATED="1579465764560" MODIFIED="1579465833214">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Ahora conocemos los 3 permisos y como agregar o remover estos, pero estos 3 permisos son almacenados en 3 lugares diferentes llamados.<br/>Usuario (u) proviene de user<br/>Grupo (g) proviene de group<br/>Otros (o) proviene de other
    </p>
    <p style="text-align: justify">
      Cuando ejecutas:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_369001564" CREATED="1579465833223" MODIFIED="1579465857985">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod =r archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Cambia los permisos en 3 lugares, cuando haces una lista de directorios con &#x201c;ls -l&#x201d; veras algo parecido a." ID="ID_1214675097" CREATED="1579465915309" MODIFIED="1579465937675">
<icon BUILTIN="pencil"/>
<node ID="ID_1139554743" CREATED="1579465937681" MODIFIED="1579465966092">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">-r--r--r--    1  wada  users  4096 abr 13 19:30 archivo</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_268708080" CREATED="1579465989572" MODIFIED="1579466006173">
<icon BUILTIN="messagebox_warning"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Nota esas 3 &#8220;r&#8221;s para los 3 diferentes tipos de permisos
    </p>
    <p style="text-align: justify">
      donde:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_578304719" CREATED="1579466022375" MODIFIED="1579466039664"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">x-------------x-------------x
|  permisos   |  pertenece  |
x-------------x-------------x
|  rwx------  | usuario     |
|  ---r-x---  | grupo       |
|  ------r-x  | otros       |
x-------------x-------------x</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node ID="ID_1020716684" CREATED="1579466170255" MODIFIED="1579466192290">
<icon BUILTIN="pencil"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Podemos remover permisos para cada due&#241;o; supongamos que tenemos un archivo:
    </p>
    <pre>-rwxr-xr-x    1  wada  users  4096 abr 13 19:30 archivo</pre>
    <p style="text-align: justify">
      Para remover los permisos de ejecuci&#243;n a grupos y otros basta con usar:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_1881980949" CREATED="1579466196733" MODIFIED="1579466221379">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod g-x,o-x archivo</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1369796722" CREATED="1579466315692" MODIFIED="1579466331711">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      Nuestro archivo quedara con estos permisos
    </p>
    <pre>-rwxr--r--    1  wada  users  4096 abr 13 19:30 archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Si deseas remover a usuario el permiso de escritura:" ID="ID_1255263002" CREATED="1579466383271" MODIFIED="1579466407488">
<icon BUILTIN="pencil"/>
<node ID="ID_1329751773" CREATED="1579466407494" MODIFIED="1579466424916">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod u-x archivo</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1027739036" CREATED="1579466478878" MODIFIED="1579466488720">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">-r-xr--r--    1  wada  users  4096 abr 13 19:30 archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Agregando y removiendo dos permisos a la vez:" ID="ID_1956452489" CREATED="1579466499611" MODIFIED="1579466539392">
<icon BUILTIN="pencil"/>
<node ID="ID_1818015422" CREATED="1579466539410" MODIFIED="1579466565658">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">#chmod u-x+w archivo</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Comando chmod en octal." ID="ID_1008201042" CREATED="1579466591276" MODIFIED="1579466625695">
<icon BUILTIN="yes"/>
<node TEXT="La representaci&#xf3;n de dicho comando es muy sencila" ID="ID_1147595417" CREATED="1579466625703" MODIFIED="1579466677344">
<icon BUILTIN="idea"/>
<node ID="ID_671254078" CREATED="1579466677353" MODIFIED="1579466687495">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="text-align: justify">
      <em>Lectura</em>&#160;tiene el valor de <em>4</em><br/><em>Escritura</em>&#160;tiene el valor de <em>2</em><br/><em>Ejecuci&#243;n</em>&#160;tiene el valor de <em>1</em>
    </p>
    <p style="text-align: justify">
      Entonces:
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_752992111" CREATED="1579466694313" MODIFIED="1579466730264">
<icon BUILTIN="bookmark"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">x-----x-----x-----------------------------------x
| rwx |  7  | Lectura, escritura y ejecuci&#243;n    |
| rw- |  6  | Lectura, escritura        |
| r-x |  5  | Lectura y ejecuci&#243;n       |
| r-- |  4  | Lectura               |
| -wx |  3  | Escritura y ejecuci&#243;n             |
| -w- |  2  | Escritura                         |
| --x |  1  | Ejecuci&#243;n             |
| --- |  0  | Sin permisos          |
x-----x-----x-----------------------------------x</pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1570704441" CREATED="1579466739287" MODIFIED="1579466770419">
<icon BUILTIN="bookmark"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Por lo tanto:
    </p>
    <pre http-equiv="content-type" content="text/html; charset=utf-8">x------------------------x-----------x
|chmod u=rwx,g=rwx,o=rx  | chmod 775 | 
|chmod u=rwx,g=rx,o=     | chmod 760 |
|chmod u=rw,g=r,o=r      | chmod 644 |
|chmod u=rw,g=r,o=       | chmod 640 |
|chmod u=rw,go=          | chmod 600 |
|chmod u=rwx,go=         | chmod 700 |
x------------------------x-----------x</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node TEXT="104.5 Manejo de asignaci&#xf3;n de permisos parte2" FOLDED="true" POSITION="right" ID="ID_1162913219" CREATED="1579803251951" MODIFIED="1579803287362">
<edge COLOR="#808080"/>
<node TEXT="Comando umask" FOLDED="true" ID="ID_1657727429" CREATED="1579803287372" MODIFIED="1579805997964">
<node TEXT="Cuando nuevos archivos o directorios son creados por los usuarios, los permisos son heredados por el valor actual de umask" ID="ID_1247316383" CREATED="1579806051988" MODIFIED="1579806821510"/>
<node TEXT="Significa mascara de archivos y directorios." ID="ID_866143635" CREATED="1579806822814" MODIFIED="1579806983117">
<node TEXT="Por default cada vez que se usa una cuenta de un usuario limitado para crear un archivo o un directorio, se establecen por defecto los permisos rw-rw-r- (octal 664)y rwxrwxr-x (775), respectivamente." ID="ID_1198498916" CREATED="1579806984747" MODIFIED="1579807297533"/>
<node TEXT="Si se hace como root los permisos ser&#xe1;n: rw-r-r (octal 644)y rwxr-xr-x(octal 755)." ID="ID_70246140" CREATED="1579807298226" MODIFIED="1579807418734"/>
<node TEXT="Esto es resultado de aplicar los que se conoce como m&#xe1;scaras de archivos y directorios, cuyos valores se pueden visualizar y cambiar." ID="ID_1654927814" CREATED="1579807419766" MODIFIED="1579807523121"/>
<node TEXT="Ejemplo: Ejecutamos umask para un usuario cualquiera.&#xa;-Al crear un archivo llamado file1 los permisos resultar&#xe1;n de la resta entre 666 y la m&#xe1;scara de creaci&#xf3;n de archivos (002 para un usuario com&#xfa;n y 022 para root).&#xa;-Cuando a&#xf1;adamos un directorio dir1, los permisos resultaran de la resta de entre 777 y la m&#xe1;scara de creaci&#xf3;n de directorios (mismos valores que en el caso anterior).&#xa;-Elimine file1 y dir1.&#xa;-Establezca un nuevo valor para las m&#xe1;scaras (000 en el siguiente ejemplo) y volvamos a ejecutar los comandos de los pasos 1 a 3" ID="ID_678600939" CREATED="1579808342251" MODIFIED="1579809095462">
<node TEXT="Ejemplo:&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ umask&#xa;0002&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ touch file1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ mkdir dir1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ ls -ld file1 dir1&#xa;drwxrwxr-x 2 ubuntu ubuntu 4096 ene 24 16:55 dir1&#xa;-rw-rw-r-- 1 ubuntu ubuntu    0 ene 24 16:55 file1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ rm file1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ rmdir dir1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ umask 000&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ touch file1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ mkdir dir1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$ ls -ld file1 dir1&#xa;drwxrwxrwx 2 ubuntu ubuntu 4096 ene 26 15:19 dir1&#xa;-rw-rw-rw- 1 ubuntu ubuntu    0 ene 26 15:19 file1&#xa;ubuntu@ubuntu-ThinkPad-T440p:~$" ID="ID_1441930099" CREATED="1579809098393" MODIFIED="1580073668587"/>
</node>
</node>
</node>
<node TEXT="Comando id" FOLDED="true" ID="ID_1319423134" CREATED="1580081139653" MODIFIED="1580081157261">
<node TEXT="Los administradores del sistema frecuentemente hacen cambios a los permisos de propietario de los usuarios regulares." ID="ID_1658996967" CREATED="1580081157274" MODIFIED="1580081337612"/>
<node TEXT="Para lograr esto primero se tiene que saber a que grupo pertenece el usuario, o que grupos en general existen en un sistema." ID="ID_551647736" CREATED="1580081342997" MODIFIED="1580081446241"/>
<node TEXT="Posiblemente sea necesario averiguar el id de usuario de algunos usuarios o tal ves necesite verificar su propio id de usuario." ID="ID_1137797574" CREATED="1580081447200" MODIFIED="1580081631149"/>
<node TEXT="El comando id imprime el id de usuario del usuario actual o de un usuario especifico." ID="ID_1260545942" CREATED="1580081633876" MODIFIED="1580081938534">
<node TEXT="root@PC-L440:~# id&#xa;uid=0(root) gid=0(root) grupos=0(root)" ID="ID_1321047320" CREATED="1580081940604" MODIFIED="1580081953266"/>
<node TEXT="root@PC-L440:~# id israel&#xa;uid=1000(israel) gid=1000(israel) grupos=1000(israel),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),119(lpadmin),129(sambashare)" ID="ID_1420190210" CREATED="1580084420691" MODIFIED="1580084426767"/>
</node>
</node>
<node TEXT="Comando whoami" FOLDED="true" ID="ID_1113155745" CREATED="1580084768563" MODIFIED="1580084781698">
<node TEXT="Imprime el nombre del usuario efectivo" FOLDED="true" ID="ID_1972334609" CREATED="1580084781710" MODIFIED="1580084920416">
<node TEXT="root@PC-L440:~# whoami" ID="ID_1657491228" CREATED="1580084937502" MODIFIED="1580084946569"/>
</node>
<node TEXT="Tambi&#xe9;n es equivalente al comando id -un" FOLDED="true" ID="ID_1349028378" CREATED="1580084947415" MODIFIED="1580084981243">
<node TEXT="root@PC-L440:~# id -un" ID="ID_1798300233" CREATED="1580084982281" MODIFIED="1580084993496"/>
</node>
</node>
<node TEXT="Comando groups" FOLDED="true" ID="ID_643392337" CREATED="1580085065644" MODIFIED="1580085079947">
<node TEXT="Imprime todos los grupos  a los que pertenece un usuario." FOLDED="true" ID="ID_1699042512" CREATED="1580085079953" MODIFIED="1580085101458">
<node TEXT="israel@PC-L440:~$ groups&#xa;israel adm cdrom sudo dip plugdev lpadmin sambashare" ID="ID_1978153236" CREATED="1580085102453" MODIFIED="1580085182445"/>
</node>
</node>
<node TEXT="Comando chown" FOLDED="true" ID="ID_1134582544" CREATED="1580085851977" MODIFIED="1580085989786">
<node TEXT="Cambia al propietario y el grupo de un usuario o propietario" FOLDED="true" ID="ID_1560961492" CREATED="1580085989793" MODIFIED="1580086154417">
<node ID="ID_147697418" CREATED="1580086154428" MODIFIED="1580086760938"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre http-equiv="content-type" content="text/html; charset=utf-8"># chown alex test/bar.txt
# ls -l test/
total 0
-rw-r--r-- 1 alex	root       0 2014-09-15 21:14 bar.txt</pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Comando chgrp" FOLDED="true" ID="ID_1753124477" CREATED="1580087273689" MODIFIED="1580087421565">
<node TEXT="Cambia el grupo de un archivo o directorio." FOLDED="true" ID="ID_920928842" CREATED="1580087421573" MODIFIED="1580087440947">
<node ID="ID_1164661700" CREATED="1580087442075" MODIFIED="1580087748052"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <h3 http-equiv="content-type" content="text/html; charset=utf-8">
      
    </h3>
    <br/>
    El comando chgrp se usa para cambiar el grupo de un archivo o directorio. Es un comando de administrador. S&#243;lo el usuario root puede cambiar el grupo de un archivo o directorio.
  </body>
</html>
</richcontent>
</node>
<node TEXT="Sintaxis" ID="ID_982145359" CREATED="1580087753712" MODIFIED="1580087799279">
<node ID="ID_1088615622" CREATED="1580087799286" MODIFIED="1580087807443"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <br http-equiv="content-type" content="text/html; charset=utf-8"/>
    <font color="black">chgrp [opciones] nuevo_grupo nombre_de_archivo/directorio</font>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Opciones" ID="ID_833459584" CREATED="1580088104136" MODIFIED="1580088109133">
<node ID="ID_1118924506" CREATED="1580088109142" MODIFIED="1580088212250"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table http-equiv="content-type" content="text/html; charset=utf-8">
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -R
        </td>
        <td class="new">
          Cambia el permiso en archivos que est&#233;n en subdirectorios del directorio en el que est&#233;s en ese momento.
        </td>
      </tr>
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -c
        </td>
        <td class="new">
          Cambia el permiso para cada archivo.
        </td>
      </tr>
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -f
        </td>
        <td class="new">
          Forzar. No informar de errores.
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ejemplo:" ID="ID_1788323200" CREATED="1580088279612" MODIFIED="1580088287718">
<node ID="ID_517921563" CREATED="1580088287723" MODIFIED="1580088292878"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li http-equiv="content-type" content="text/html; charset=utf-8">
        <div class="resp_code">
          <font color="black">chgrp hiox test.txt</font>
        </div>
        <br/>
        El grupo del archivo &quot;test.txt&quot; es root, cambia al nuevo grupo hiox.

        <div class="clear">
          
        </div>
      </li>
      <li>
        <div class="resp_code">
          <font color="black">chgrp -R hiox test</font>
        </div>
        <br/>
        El grupo del directorio &quot;test&quot; es root. Con -R, los archivos y sus subdirectorios tambi&#233;n cambian al nuevo grupo hiox.

        <div class="clear">
          
        </div>
      </li>
      <li>
        <div class="resp_code">
          <font color="black">chgrp -c hiox calc.txt</font>
        </div>
        <br/>
        El comando anterior se utiliza para cambiar el grupo para el archivo espec&#237;fico &quot;calc.txt&quot;.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Comando chmod" FOLDED="true" ID="ID_1986689502" CREATED="1580088450842" MODIFIED="1580088473602">
<node TEXT="El comando chmod te permite alterar / cambiar los derechos de acceso a archivos y directorios." ID="ID_1850752097" CREATED="1580088473606" MODIFIED="1580088530668"/>
<node TEXT="Sinaxis" ID="ID_1385371127" CREATED="1580088573345" MODIFIED="1580088578820">
<node ID="ID_1395488397" CREATED="1580088578825" MODIFIED="1580088600806"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <font http-equiv="content-type" content="text/html; charset=utf-8" color="black">chmod [options] [MODE] FileName<br/></font><font color="000080"><b>Permiso de Archivo</b></font><font http-equiv="content-type" content="text/html; charset=utf-8" color="black"><br/></font>

    <div class="table">
      <table width="34%" style="margin-left: 17px">
        <tr class="new1">
          <td width="75" align="center">
            #
          </td>
          <td width="300" align="center">
            Permiso de Archivo
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            0
          </td>
          <td class="new" width="300">
            ninguno
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            1
          </td>
          <td class="new" width="300">
            s&#243;lo ejecutar
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            2
          </td>
          <td class="new" width="300">
            s&#243;lo escritura
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            3
          </td>
          <td class="new" width="300">
            escritura y ejecuci&#243;n
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            4
          </td>
          <td class="new" width="300">
            s&#243;lo lectura
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            5
          </td>
          <td class="new" width="300">
            lectura y ejecuci&#243;n
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            6
          </td>
          <td class="new" width="300">
            lectura y escritura
          </td>
        </tr>
        <tr>
          <td class="new" width="75" valign="center" align="center">
            7
          </td>
          <td class="new" width="300">
            todos los permisos
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Opciones" ID="ID_779609354" CREATED="1580088602746" MODIFIED="1580088630651">
<node ID="ID_439545505" CREATED="1580088630658" MODIFIED="1580088695319"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table http-equiv="content-type" content="text/html; charset=utf-8">
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -c
        </td>
        <td class="new">
          Muestra los nombres de aquellos archivos cuyos permisos est&#233;n siendo cambiados
        </td>
      </tr>
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -f
        </td>
        <td class="new">
          Elimina la mayor&#237;a de los mensajes de error
        </td>
      </tr>
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -R
        </td>
        <td class="new">
          Cambia archivos y directorios repetidamente
        </td>
      </tr>
      <tr>
        <td class="new" width="10%" valign="center" align="center">
          -v
        </td>
        <td class="new">
          Mostrar la informaci&#243;n de la versi&#243;n y salir.
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ejemplo:" ID="ID_963291435" CREATED="1580088730801" MODIFIED="1580088737584">
<node ID="ID_942865305" CREATED="1580088737589" MODIFIED="1580088756020"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ol http-equiv="content-type" content="text/html; charset=utf-8" type="1" style="padding-left: 0">
      <li>
        Para ver qu&#233; permisos tienen tus archivos:<br/>

        <div class="resp_code">
          <font color="black">ls -alt</font>
        </div>
        <br/>
        Este comando se usa para ver qu&#233; permisos tienen tus archivos.

        <div class="clear">
          
        </div>
      </li>
      <li>
        Para hacer que un archivo se pueda leer y escribir por el grupo y otros.<br/>

        <div class="resp_code">
          <font color="black">chmod066file1.txt</font>
        </div>
      </li>
      <li>
        Para permitir a cualquier que lea, escriba y ejecute el archivo<br/>

        <div class="resp_code">
          <font color="black">chmod777file1.txt</font>
        </div>
      </li>
    </ol>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Para cambiar los permisos a usuarios grupos y otros puede escribir:" ID="ID_1082566059" CREATED="1580089080400" MODIFIED="1580089104119">
<node TEXT="#chmod u+rwx,g+rwx,o+rwx  (nombre de archivo)." ID="ID_1372305841" CREATED="1580089104126" MODIFIED="1580089203164"/>
</node>
</node>
</node>
<node TEXT="104.6 Creaci&#xf3;n y manejo de enlaces duros y simb&#xf3;licos." FOLDED="true" POSITION="right" ID="ID_1097156031" CREATED="1580182998208" MODIFIED="1580355633126">
<edge COLOR="#808080"/>
<node TEXT="&#xbf;Que son los enlaces?" ID="ID_920279355" CREATED="1580183213112" MODIFIED="1580185075701">
<node TEXT="Es &#xfa;til tener un archivo en lugares diferentes en su sistema de archivos. Para evitar la realizaci&#xf3;n de copias de un archivo en diferentes partes del sistema, gnu linux utiliza los enlaces que apuntan al archivo original." ID="ID_224517319" CREATED="1580185078090" MODIFIED="1580185466253"/>
<node TEXT="Los enlaces ocupan poco espacio y es m&#xe1;s eficiente que tener varias copias del mismo archivo." ID="ID_973622803" CREATED="1580185480684" MODIFIED="1580186072522"/>
<node TEXT="Se utilizan dos tipos de enlaces." ID="ID_478121706" CREATED="1580186073160" MODIFIED="1580186084455">
<node TEXT="Enlaces duros." ID="ID_211264356" CREATED="1580186084462" MODIFIED="1580186090597">
<node TEXT="Son punteros a los mismos datos que el objeto original." ID="ID_172130168" CREATED="1580186717571" MODIFIED="1580187075447"/>
<node TEXT="Siempre comparten el mismo inodo que el objeto original." ID="ID_1071336382" CREATED="1580187076845" MODIFIED="1580187091450"/>
<node TEXT="No es un enlace a todos, es una entrada duplicada dentro del sistema de archivos en una ubicaci&#xf3;n distinta para un archivo ya existente." ID="ID_154374705" CREATED="1580187122658" MODIFIED="1580187298096"/>
<node TEXT="Ejemplo:" ID="ID_1015066105" CREATED="1580187547044" MODIFIED="1580187555021">
<node TEXT="" ID="ID_1988974032" CREATED="1580187555031" MODIFIED="1580187555031"/>
</node>
</node>
<node TEXT="Enlaces simb&#xf3;licos." ID="ID_1770665826" CREATED="1580186091297" MODIFIED="1580186097903"/>
</node>
<node TEXT="Ventajas:" ID="ID_1839837853" CREATED="1580186537992" MODIFIED="1580186545969">
<node TEXT="Si se est&#xe1; editando un archivo, los cambios se replican a los archivos, de lo contrario se tendr&#xed;an que hacer copias en cada archivo manualmente." ID="ID_1899335966" CREATED="1580186545976" MODIFIED="1580187043332"/>
</node>
<node TEXT="Se utiliza el comando ln." ID="ID_1537369658" CREATED="1580187530828" MODIFIED="1580187539606"/>
</node>
</node>
<node TEXT="104.7 Buscar y ubicar archivos del sistema parte1." FOLDED="true" POSITION="right" ID="ID_1342040856" CREATED="1580355637028" MODIFIED="1580355692860">
<edge COLOR="#808080"/>
<node TEXT="Estandar de jerarqu&#xed;a del sistema de archivos o Filesystem HIerarchy Standard (FHS)" ID="ID_369423847" CREATED="1580355692871" MODIFIED="1580356051547">
<node TEXT="Sistema de archivos root o ra&#xed;z tambi&#xe9;n llamado barra. /" ID="ID_1117224636" CREATED="1580356588946" MODIFIED="1580356889534">
<node TEXT="Contiene todo el sistema de archivos en Gnu linux" ID="ID_993930560" CREATED="1580356889544" MODIFIED="1580356913265"/>
</node>
<node TEXT="/bin" ID="ID_1237181264" CREATED="1580357148708" MODIFIED="1580357162060">
<node TEXT="Contiene los programas b&#xe1;sicos del sistema que son necesarios para iniciar el mismo." ID="ID_225627032" CREATED="1580357162068" MODIFIED="1580357236000"/>
</node>
<node TEXT="/dev" ID="ID_173756144" CREATED="1580357237584" MODIFIED="1580357242009">
<node TEXT="Contiene los archivos mas importantes que el kernel necesita para manejar los diferentes dispositivos de hardware." ID="ID_1328758887" CREATED="1580357242017" MODIFIED="1580357278043"/>
</node>
<node TEXT="/etc" ID="ID_611764662" CREATED="1580358608873" MODIFIED="1580358615572">
<node TEXT="Contiene informaci&#xf3;n de donde lasparticiones deben ser montadas." ID="ID_1657701441" CREATED="1580358615575" MODIFIED="1580358686138"/>
</node>
<node TEXT="/lib" ID="ID_731012534" CREATED="1580358688178" MODIFIED="1580358697552">
<node TEXT="Contiene las librer&#xed;as necesarias que usan los programas para poder funcionar. Tambi&#xe9;n en este directorio se encuentra el directorio modules que continiene los modulos del kernel." ID="ID_221428583" CREATED="1580358697555" MODIFIED="1580358855945"/>
</node>
<node TEXT="/sbin" ID="ID_568487883" CREATED="1580358878862" MODIFIED="1580358889084">
<node TEXT="Contiene programas importantes que se utilizan antes que los sistema de archivos sean montados, ejemplo fsck que comprueba la consistencia del sistema de archivos." ID="ID_1073697952" CREATED="1580358889091" MODIFIED="1580359011188"/>
</node>
<node TEXT="/root" ID="ID_542582404" CREATED="1580359015161" MODIFIED="1580359022178">
<node TEXT="En pocas palabras es el directorio home del administrador del sistema que tambi&#xe9;n se llama root." ID="ID_1722703598" CREATED="1580359022185" MODIFIED="1580359485371"/>
</node>
<node TEXT="Otros directorios." ID="ID_998957413" CREATED="1580359495075" MODIFIED="1580359512302">
<node TEXT="/usr" ID="ID_1121435201" CREATED="1580359512312" MODIFIED="1580359611941">
<node TEXT="Todos los directorios del sistema de archivos que no son necesarios durante el arranque se almacenan aqu&#xed;, significa Unix system resource por sus siglas en ingl&#xe9;s." ID="ID_1593290706" CREATED="1580359611948" MODIFIED="1580359711774"/>
</node>
<node TEXT="/var" ID="ID_1018600930" CREATED="1580359729171" MODIFIED="1580359735351">
<node TEXT="Los archivos variables cuyo contenido se espera que cambien contnuamente durante el funcionamiento normal del sistema, como los archivos logs, spools y los temporales de correo electr&#xf3;nico se almacenan aqu&#xed;. Dentro de los cuales se encuentran directorios como:" ID="ID_1249392915" CREATED="1580359735356" MODIFIED="1580359886812">
<node TEXT="/var/logs.- Se encuentran los mensajes logs del sistema." ID="ID_313763585" CREATED="1580359886824" MODIFIED="1580359924004"/>
<node TEXT="/var/spool.- se encuentran archivos spoool de tareas que estan esperando a ser procesadas, por ejemplo las colas de impresion." ID="ID_131409421" CREATED="1580359924655" MODIFIED="1580359999299"/>
<node TEXT="/var/tmp.- Se alojan archivos temporales que son preservados durante el reinicio del sistema." ID="ID_8192623" CREATED="1580360000593" MODIFIED="1580360193220"/>
</node>
</node>
<node TEXT="/home" ID="ID_1454678362" CREATED="1580360198714" MODIFIED="1580360213235">
<node TEXT="Se guardan los directorios home de los usuarios que contienen archivos personales de los usuarios del sistema as&#xed; como configuraciones personales." ID="ID_967192093" CREATED="1580360213242" MODIFIED="1580360404948"/>
</node>
<node TEXT="/media" ID="ID_190116771" CREATED="1580360406755" MODIFIED="1580360411845">
<node TEXT="Se encuenran los puntos de montaje de medios removibles com los cdrooms y/o flopy." ID="ID_1127455150" CREATED="1580360411855" MODIFIED="1580360621911"/>
</node>
<node TEXT="/mnt" ID="ID_1088415771" CREATED="1580360623903" MODIFIED="1580360628811">
<node TEXT="Es utilizado para montar sistema de archivos temporalmente." ID="ID_1225881846" CREATED="1580360628816" MODIFIED="1580360668571"/>
</node>
<node TEXT="/opt" ID="ID_669648959" CREATED="1580360671061" MODIFIED="1580360677265">
<node TEXT="Se instala todo el software adicional." ID="ID_873410547" CREATED="1580360677272" MODIFIED="1580360955927"/>
</node>
<node TEXT="/srv" ID="ID_1118589283" CREATED="1580360961833" MODIFIED="1580360966600">
<node TEXT="Guarda informaci&#xf3;n acerca de los servicios que provee el servidor." ID="ID_884351247" CREATED="1580360966606" MODIFIED="1580361081454"/>
</node>
<node TEXT="/tmp" ID="ID_361023411" CREATED="1580361083182" MODIFIED="1580361088827">
<node TEXT="Ofrece a los programas guardar sus archivos temporales, puede ser escrito por todos los usuarios." ID="ID_718740597" CREATED="1580361088831" MODIFIED="1580361139921"/>
</node>
<node TEXT="/pro" ID="ID_891097011" CREATED="1580361145169" MODIFIED="1580361159688">
<node TEXT="Sistema de archivos virtual del kernel que nos provee con informacion relevante del sistema en tiempo real." ID="ID_196564240" CREATED="1580361159693" MODIFIED="1580361218896"/>
</node>
<node TEXT="/boot" ID="ID_1327106755" CREATED="1580361221053" MODIFIED="1580361226002">
<node TEXT="" ID="ID_202304062" CREATED="1580361226006" MODIFIED="1580361226006"/>
</node>
</node>
</node>
</node>
<node TEXT="104.7 Buscar y ubicar archivos del sistema parte2." FOLDED="true" POSITION="right" ID="ID_416430236" CREATED="1580408791995" MODIFIED="1580408815327">
<edge COLOR="#808080"/>
<node TEXT="Comando find" FOLDED="true" ID="ID_636630679" CREATED="1580408815352" MODIFIED="1580409115401">
<node TEXT="Busca los archivos a trav&#xe9;s del &#xe1;rbol del directorio mediante la directiva de expresi&#xf3;n otorgada al mismo." FOLDED="true" ID="ID_1033598491" CREATED="1580409115412" MODIFIED="1580409250789">
<node TEXT="Ejemplos de directivas de expresi&#xf3;n" ID="ID_1836668357" CREATED="1580409250800" MODIFIED="1580409269364">
<node TEXT="Nombre de archivo&#xa;Nombre de usuario&#xa;O los permisos sobre los archivos." ID="ID_1979154137" CREATED="1580409269376" MODIFIED="1580409349257"/>
</node>
</node>
<node TEXT="Sintaxis" ID="ID_1094811017" CREATED="1580409352224" MODIFIED="1580409368002">
<node TEXT="#find (par&#xe1;metros) (ruta) (expresiones)" ID="ID_1168314766" CREATED="1580409368014" MODIFIED="1580409617688"/>
</node>
<node TEXT="Ejemplo:" ID="ID_1990148653" CREATED="1580409618515" MODIFIED="1580409627236">
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find . -name Desktop&#xa;./Desktop" ID="ID_107632007" CREATED="1580409627246" MODIFIED="1580409785221">
<node TEXT="El punto le dice al comando actual que  busque en el directorio actual y la expresi&#xf3;n name que busque por el nombre Desktop, este comando es sensible a may&#xfa;sculas y min&#xfa;sculas, lo que significa que este buscara primero por nombre con D may&#xfa;scula." ID="ID_1996661337" CREATED="1580409795937" MODIFIED="1580409990831"/>
</node>
<node TEXT="buntu@ubuntu-ThinkPad-T440p:~$ find . -iname Desktop&#xa;./desktop&#xa;./Desktop" ID="ID_346804526" CREATED="1580410087987" MODIFIED="1580410097184">
<node TEXT="Si se utiliza la opci&#xf3;n inme en lugar de name, la b&#xfa;squeda ya no ser&#xe1; sensibles a may&#xfa;sculas y min&#xfa;sculas." ID="ID_1677054137" CREATED="1580410103201" MODIFIED="1580410196199"/>
</node>
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find / -iname Desktop&#xa;find: &#x2018;/root&#x2019;: Permission denied&#xa;find: &#x2018;/etc/polkit-1/localauthority&#x2019;: Permission denied&#xa;find: &#x2018;/etc/cups/ssl&#x2019;: Permission denied&#xa;find: &#x2018;/etc/ssl/private&#x2019;: Permission denied" ID="ID_1267664056" CREATED="1580410269843" MODIFIED="1580410361835">
<node TEXT="Si se desconoce en que directorio se encuentra el archivo se coloca / en lugar del . y de esta forma buscar&#xe1; en todo el &#xe1;rbol de directorios." ID="ID_218370667" CREATED="1580410361851" MODIFIED="1580410425622"/>
<node TEXT="Si usted hace la b&#xfa;squeda como usuario normal recibir&#xe1; mensajes de permission denied o permiso denegado por que los usuarios normales no tienen permiso de acceso a todo el &#xe1;rbol de directorio." ID="ID_478321448" CREATED="1580410469605" MODIFIED="1580410531584"/>
</node>
<node TEXT="/var/lib/snapd/desktop&#xa;/usr/share/doc/HTML/ru/kcontrol/desktop&#xa;/usr/share/doc/HTML/nl/kcontrol/desktop&#xa;/usr/share/doc/HTML/sr/kcontrol/desktop&#xa;/usr/share/doc/HTML/pt_BR/kcontrol/desktop&#xa;/usr/share/doc/HTML/uk/kcontrol/desktop&#xa;/usr/share/doc/HTML/it/kcontrol/desktop&#xa;/usr/share/doc/HTML/de/kcontrol/desktop&#xa;/usr/share/doc/HTML/ca/kcontrol/desktop&#xa;/usr/share/doc/HTML/pt/kcontrol/desktop&#xa;/usr/share/doc/HTML/es/kcontrol/desktop&#xa;/usr/share/doc/HTML/sr@latin/kcontrol/desktop&#xa;/usr/share/doc/HTML/en/kcontrol/desktop&#xa;/usr/share/doc/HTML/sv/kcontrol/desktop&#xa;/usr/lib/libreoffice/share/config/soffice.cfg/desktop&#xa;/usr/lib/x86_64-linux-gnu/qt5/qml/QtQuick/Controls/Styles/Desktop&#xa;/usr/lib/x86_64-linux-gnu/qt5/qml/org/kde/private/desktopcontainment/desktop&#xa;/home/ubuntu/desktop&#xa;/home/ubuntu/Desktop" ID="ID_823233136" CREATED="1580410641663" MODIFIED="1580410649893">
<node TEXT="Si usted lo ejecuta con el comando sudo y tiene el password, se evitaran los mensajes de acceso denegado." ID="ID_1822855418" CREATED="1580410649911" MODIFIED="1580410695668"/>
</node>
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find /home -user ubuntu" ID="ID_1352031268" CREATED="1580411192811" MODIFIED="1580411376819">
<node TEXT="Busca todos los archivos que pertenecen a un mismo usuario." ID="ID_1251995218" CREATED="1580411376831" MODIFIED="1580411414983"/>
</node>
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find /usr -name *info" ID="ID_1033536403" CREATED="1580411638020" MODIFIED="1580411642636">
<node TEXT="Busca archivos que pertenecen al directorio /usr y que contengan la palabra info en su nombre de archivo" ID="ID_1450511679" CREATED="1580411642644" MODIFIED="1580411820275"/>
</node>
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find /home/ubuntu -atime 6" ID="ID_26600333" CREATED="1580411822165" MODIFIED="1580411830906">
<node TEXT="Listar todos los archivos dentro del directorio de trabajo ubuntu que fueron accedidos por &#xfa;ltima vez hace n cantidad de d&#xed;as." ID="ID_846999624" CREATED="1580411835852" MODIFIED="1580412017684"/>
</node>
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ find /home/ubuntu -perm -644" ID="ID_45496032" CREATED="1580412159468" MODIFIED="1580412165663">
<node TEXT="En el mismo usuario busca los archivos que tengan los permisos 644." ID="ID_127976944" CREATED="1580412165672" MODIFIED="1580412195663"/>
</node>
</node>
<node TEXT="Opciones mas usadas del comando find." FOLDED="true" ID="ID_439746781" CREATED="1580410725181" MODIFIED="1580410743564">
<node TEXT="-atime n.-El archivo fue accedido por &#xfa;ltima vez hace n*24 hrs.&#xa;-ctime n.-El estatus del archivo fue cambiado por &#xfa;ltima vez hace n*24 hrs.&#xa;-maxdepth n.- Busca en n cantidad de niveles o subdirectorios.&#xa;-mount.- no deciende a otros directorios que se encuentren en otros sistemas de archivos.&#xa;-mtime n.- Busca archivos que fueron modificados hace n+1 d&#xed;as.&#xa;-name term busca archivos que poseen el nombre de archivo especificado." ID="ID_1892069860" CREATED="1580410743570" MODIFIED="1580411075936"/>
</node>
</node>
<node TEXT="Comado locate y update db" ID="ID_1340552194" CREATED="1580412222844" MODIFIED="1580412251585">
<node TEXT="Para ejecutar una b&#xfa;squeda mas r&#xe1;pida puede ejecutar el comando locate." ID="ID_82077980" CREATED="1580412252829" MODIFIED="1580412296992">
<node TEXT="Ejemplo:" ID="ID_627283212" CREATED="1580412368938" MODIFIED="1580412378337">
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ locate desktop" ID="ID_1911811283" CREATED="1580412378345" MODIFIED="1580412386202"/>
</node>
</node>
<node TEXT="A diferencia de find, locate busca por archivos cuya informaci&#xf3;n sea almacenada en una base de datos que es diariamente actualizada mediante el comando cron." ID="ID_260639254" CREATED="1580412433897" MODIFIED="1580412545914">
<node TEXT="Puede encontrar la bd en mlocate.db ubucado en /var/lib/mlocate." ID="ID_26916106" CREATED="1580412538812" MODIFIED="1580412594381"/>
</node>
<node TEXT="Para actualizar dicha base de datos el administrador del sistema usa el comando updatedb." ID="ID_1618971044" CREATED="1580412629752" MODIFIED="1580412672042">
<node TEXT="Su archivo de configuraci&#xf3;n es updatedb.conf." ID="ID_1276779333" CREATED="1580412691179" MODIFIED="1580412733445"/>
<node TEXT="Contiene una ruta de directorios que no deben ser revisados mientras se genera una b&#xfa;squeda con el comando locate." ID="ID_223196923" CREATED="1580412734176" MODIFIED="1580412789521"/>
</node>
</node>
<node TEXT="Comandos which, type y whereis." ID="ID_991338095" CREATED="1580412810661" MODIFIED="1580412852040">
<node TEXT="Comando which ubica un archivo de programa dentro del sistema, tambi&#xe9;n llamado binario o ejecutable." ID="ID_878392972" CREATED="1580412852043" MODIFIED="1580413144057">
<node TEXT="Si usted desea obtener la ruta completa de un programa puede usar el comando which para dicha informaci&#xf3;n" ID="ID_167772715" CREATED="1580413144454" MODIFIED="1580413190859">
<node TEXT="Ejemplo: #which date&#xa;/bin/date" ID="ID_1111596699" CREATED="1580413190865" MODIFIED="1580413220676"/>
</node>
</node>
<node TEXT="El comando type identifica alias existentes que pueden afectar como un determido comando es ejecutado." ID="ID_475155245" CREATED="1580413250794" MODIFIED="1580413350677">
<node TEXT="Identifica comandos o funciones que son invocados desde dentro de una shell" ID="ID_1913895479" CREATED="1580413354905" MODIFIED="1580413492544"/>
<node TEXT="Ejemplo: Si se desea buscar el alias y la ruta completa de un comando use:" ID="ID_1219395869" CREATED="1580413493272" MODIFIED="1580414373114">
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ type -a which&#xa;which is /usr/bin/which&#xa;which is /bin/which" ID="ID_387156616" CREATED="1580413497124" MODIFIED="1580413502084"/>
</node>
</node>
<node TEXT="El comando whereis." ID="ID_387658389" CREATED="1580413533621" MODIFIED="1580413543723">
<node TEXT="Ubica el archivo binario, el archivo fuente y el manual de usuario de un determinado comando." ID="ID_376709822" CREATED="1580413543731" MODIFIED="1580414559602">
<node TEXT="Ejemplo:" ID="ID_1654158680" CREATED="1580414563856" MODIFIED="1580414568173">
<node TEXT="ubuntu@ubuntu-ThinkPad-T440p:~$ whereis updatedb.conf&#xa;updatedb: /usr/bin/updatedb.mlocate /usr/bin/updatedb /etc/updatedb.conf /usr/share/man/man8/updatedb.8.gz" ID="ID_1848768481" CREATED="1580414568177" MODIFIED="1580414714651"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_1022288420" CREATED="1572232741451" MODIFIED="1572232743326">
<edge COLOR="#808080"/>
</node>
</node>
</map>
