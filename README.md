# Guía de Estudio para el Examen LPIC1

## Introducción

En este repositorio almacenamos el código de la documentación de la guía de LPIC1,
la documentación la escribimos en markdown, y la construimos usando Jekyll, la
publicaremos usando GitLab Pages.

## Código

En el directorio **txt-notas** se encuentran notas en txt que hice hace años dando
cursos, estas notas las convertiremos a markdown y la combinaremos con las notas
que están en el mapa mental para generar nuevos contenidos.

En el directorio **mapas-mentales** se encuentra un mapa en formato **freeplane** con los temas y contenidos para desarrollar el curso.

## Contribuir

Para contrubuir abrir un issue con el tema en el que se quiere participar, hacer
un merge request y subir tus cambios para validación.
